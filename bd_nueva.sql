-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-08-2020 a las 16:47:34
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_nueva`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidato`
--

CREATE TABLE `candidato` (
  `id` int(11) NOT NULL,
  `tipo_candidato` enum('POSTULANTE','REINCORPORADO') NOT NULL,
  `dni` int(11) NOT NULL,
  `nombres` varchar(255) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `provincia` varchar(150) NOT NULL,
  `localidad` varchar(150) NOT NULL,
  `codigo_postal` varchar(150) NOT NULL,
  `telefono` varchar(150) NOT NULL,
  `celular` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `cuil` varchar(150) NOT NULL,
  `nacionalidad` enum('ARGENTINO_NAT','ARGENTINO_OP') NOT NULL,
  `sexo` enum('MASCULINO','FEMENINO') NOT NULL,
  `estado_civil` enum('SOLTERO') NOT NULL,
  `grupo_sanguineo` enum('0+','0-','A+','A-','AB+','AB-','B+','B-') NOT NULL,
  `clave_candidato` varchar(255) DEFAULT NULL,
  `primerlogin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `id` int(11) NOT NULL,
  `descripcion` enum('SUPERUSUARIO','ADMINISTRADOR','PUBLICADOR','VISUALIZADOR') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`id`, `descripcion`) VALUES
(1, 'SUPERUSUARIO'),
(2, 'ADMINISTRADOR'),
(3, 'PUBLICADOR'),
(4, 'VISUALIZADOR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro_preseleccion`
--

CREATE TABLE `centro_preseleccion` (
  `id` int(11) NOT NULL,
  `nombre` enum('ESCUELA DE SUBOFICIALES DEL EJÉRCITO "SARGENTO CABRAL"','LICEO MILITAR "GENERAL ROCA"','LICEO MILITAR "GENERAL ESPEJO"','LICEO MILITAR "GENERAL LAMADRID"','LICEO MILITAR "GENERAL BELGRANO"','LICEO MILITAR "GENERAL PAZ"','COMANDO DE LA Vta BRIGADA DE MONTAÑA','COMANDO DE LA XIIda BRIGADA DE MONTE','REGIMIENTO DE INFANTERÍA DE MONTE 29 "CNL IGNACIO JOSÉ WARNES"') NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentacion_inscripcion`
--

CREATE TABLE `documentacion_inscripcion` (
  `id` int(11) NOT NULL,
  `estado` enum('NO INGRESÓ','INGRESÓ') NOT NULL,
  `id_inscripcion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documento`
--

CREATE TABLE `documento` (
  `id` int(11) NOT NULL,
  `tipo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `documento`
--

INSERT INTO `documento` (`id`, `tipo`) VALUES
(1, 'DNI'),
(2, 'DUPIE'),
(3, 'Acta de Nacimiento'),
(4, 'Constancias de CUIT o CUIL'),
(5, 'Constancia o Certificado de estudios'),
(6, 'Fotografías'),
(7, 'Certificado RNR'),
(8, 'Comprobante de pago'),
(9, 'Libreta de conducir'),
(10, 'Partida de Nacimiento y DNI de hijos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilio`
--

CREATE TABLE `domicilio` (
  `id` int(11) NOT NULL,
  `calle` varchar(150) NOT NULL,
  `numero` int(11) NOT NULL,
  `piso` varchar(150) NOT NULL,
  `depto` varchar(150) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `editardatos`
--

CREATE TABLE `editardatos` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_candidato` int(11) NOT NULL,
  `permiso` enum('DATOS_PERSONALES','PLAN_CARRERA','SOLD_VOL','SECUNDARIO') COLLATE utf8_unicode_ci NOT NULL,
  `fecha_emision` datetime NOT NULL,
  `fecha_uso` datetime DEFAULT NULL,
  `usado` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_modif`
--

CREATE TABLE `historial_modif` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `tabla` varchar(150) NOT NULL,
  `campo` varchar(150) NOT NULL,
  `valor_anterior` varchar(255) NOT NULL,
  `valor_nuevo` varchar(255) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripcion`
--

CREATE TABLE `inscripcion` (
  `id` int(11) NOT NULL,
  `detalle` blob NOT NULL,
  `estado` enum('INSCRIPTO','INSCRIPTO CONDICIONAL','INSCRIPCIÓN NO APROBADA','EN OBSERVACION') NOT NULL,
  `fecha_aceptacion` datetime DEFAULT NULL,
  `nro_inscripto` varchar(255) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista_aprobados`
--

CREATE TABLE `lista_aprobados` (
  `id` int(11) NOT NULL,
  `dni` int(11) NOT NULL,
  `nombres` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `nro_inscr` varchar(150) NOT NULL,
  `estado` enum('APTO','DESAPROBADO','CONDICIONAL') NOT NULL,
  `tipo` enum('POSTULANTE','REINCORPORADO') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista_documentos`
--

CREATE TABLE `lista_documentos` (
  `id_documentacion_inscripcion` int(11) NOT NULL,
  `id_documento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan_carrera`
--

CREATE TABLE `plan_carrera` (
  `id` int(11) NOT NULL,
  `tipo_plan` enum('ESPECIALIDADES','ARMAS') NOT NULL,
  `orientacion` varchar(150) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preinscripcion`
--

CREATE TABLE `preinscripcion` (
  `id` int(11) NOT NULL,
  `detalle` blob NOT NULL,
  `estado` enum('APROBADA','DESAPROBADA','EN OBSERVACION','') NOT NULL,
  `fecha_alta` datetime NOT NULL DEFAULT current_timestamp(),
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recuperar_pass`
--

CREATE TABLE `recuperar_pass` (
  `id` int(11) NOT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_expiracion` datetime NOT NULL,
  `usado` tinyint(1) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secundario`
--

CREATE TABLE `secundario` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `provincia` varchar(255) NOT NULL,
  `localidad` varchar(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `telefono` varchar(150) NOT NULL,
  `cant_materias_adeudadas` int(11) NOT NULL,
  `plan` enum('7 años','6 años','5 años','4 años','3 años','2 años','PLAN_FINES','POLIMODAL') NOT NULL,
  `anio_cursado` enum('COMPLETO','4to_5to_6to','3ro','2do','1ro') NOT NULL,
  `en_curso` tinyint(1) NOT NULL,
  `resolucion` varchar(150) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `soldado_voluntario`
--

CREATE TABLE `soldado_voluntario` (
  `id` int(11) NOT NULL,
  `destino` varchar(255) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `cargo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `user`, `pass`, `cargo`) VALUES
(2, 'eliseo', '3s3sc2019', 1),
(4, 'sprodriguez', 'Ferchu08', 1),
(5, 'equipodina', 'preinscripcion2020', 1),
(6, 'cbfigueroa', 'OEdLgW2uQV', 1),
(7, 'DivIncorp1', 'CSESE_01', 1),
(8, 'DivIncorp2', '3s3sc2019', 0),
(9, 'DivIncorp3', 'CSESE_03', 1),
(10, 'DivIncorp4', 'CSESE_04', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `candidato`
--
ALTER TABLE `candidato`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `centro_preseleccion`
--
ALTER TABLE `centro_preseleccion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `documentacion_inscripcion`
--
ALTER TABLE `documentacion_inscripcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_inscripcion` (`id_inscripcion`),
  ADD KEY `id_inscripcion_2` (`id_inscripcion`);

--
-- Indices de la tabla `documento`
--
ALTER TABLE `documento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `editardatos`
--
ALTER TABLE `editardatos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`),
  ADD KEY `id_candidato_2` (`id_candidato`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `historial_modif`
--
ALTER TABLE `historial_modif`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `lista_aprobados`
--
ALTER TABLE `lista_aprobados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `lista_documentos`
--
ALTER TABLE `lista_documentos`
  ADD PRIMARY KEY (`id_documentacion_inscripcion`,`id_documento`),
  ADD KEY `id_documento` (`id_documento`),
  ADD KEY `id_documentacion_inscripcion` (`id_documentacion_inscripcion`);

--
-- Indices de la tabla `plan_carrera`
--
ALTER TABLE `plan_carrera`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `preinscripcion`
--
ALTER TABLE `preinscripcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `recuperar_pass`
--
ALTER TABLE `recuperar_pass`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `secundario`
--
ALTER TABLE `secundario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `soldado_voluntario`
--
ALTER TABLE `soldado_voluntario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `candidato`
--
ALTER TABLE `candidato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `centro_preseleccion`
--
ALTER TABLE `centro_preseleccion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `documentacion_inscripcion`
--
ALTER TABLE `documentacion_inscripcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `documento`
--
ALTER TABLE `documento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `editardatos`
--
ALTER TABLE `editardatos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `historial_modif`
--
ALTER TABLE `historial_modif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lista_aprobados`
--
ALTER TABLE `lista_aprobados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `plan_carrera`
--
ALTER TABLE `plan_carrera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `preinscripcion`
--
ALTER TABLE `preinscripcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `recuperar_pass`
--
ALTER TABLE `recuperar_pass`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `secundario`
--
ALTER TABLE `secundario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `soldado_voluntario`
--
ALTER TABLE `soldado_voluntario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `centro_preseleccion`
--
ALTER TABLE `centro_preseleccion`
  ADD CONSTRAINT `centro_preseleccion_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `documentacion_inscripcion`
--
ALTER TABLE `documentacion_inscripcion`
  ADD CONSTRAINT `documentacion_inscripcion_ibfk_1` FOREIGN KEY (`id_inscripcion`) REFERENCES `inscripcion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD CONSTRAINT `domicilio_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `editardatos`
--
ALTER TABLE `editardatos`
  ADD CONSTRAINT `editardatos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `editardatos_ibfk_2` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD CONSTRAINT `inscripcion_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`);

--
-- Filtros para la tabla `lista_documentos`
--
ALTER TABLE `lista_documentos`
  ADD CONSTRAINT `lista_documentos_ibfk_1` FOREIGN KEY (`id_documento`) REFERENCES `documento` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lista_documentos_ibfk_2` FOREIGN KEY (`id_documentacion_inscripcion`) REFERENCES `documentacion_inscripcion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `plan_carrera`
--
ALTER TABLE `plan_carrera`
  ADD CONSTRAINT `plan_carrera_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `preinscripcion`
--
ALTER TABLE `preinscripcion`
  ADD CONSTRAINT `preinscripcion_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `recuperar_pass`
--
ALTER TABLE `recuperar_pass`
  ADD CONSTRAINT `recuperar_pass_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `secundario`
--
ALTER TABLE `secundario`
  ADD CONSTRAINT `secundario_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `soldado_voluntario`
--
ALTER TABLE `soldado_voluntario`
  ADD CONSTRAINT `soldado_voluntario_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
