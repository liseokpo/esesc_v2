--
-- Base de datos: `bd_nueva`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_fisico`
--

CREATE TABLE `examen_fisico` (
  `id` int(11) NOT NULL,
  `ffbb` int(11) NOT NULL,
  `abdo` int(11) NOT NULL,
  `trote` int(11) NOT NULL,
  `ffbb_puntaje` decimal(10,2) NOT NULL,
  `abdo_puntaje` decimal(10,2) NOT NULL,
  `trote_puntaje` decimal(10,2) NOT NULL,
  `promedio` decimal(10,2) NOT NULL,
  `detalle` varchar(255) NOT NULL,
  `ausente` tinyint(4) NOT NULL DEFAULT 0,
  `fecha_carga` datetime NOT NULL DEFAULT current_timestamp(),
  `estado` enum('APROBADO','DESAPROBADO') NOT NULL,
  `id_incorporacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Estructura de tabla para la tabla `examen_intelectual`
--

CREATE TABLE `examen_intelectual` (
  `id` int(11) NOT NULL,
  `lengua_puntaje` int(10) NOT NULL,
  `mate_puntaje` int(10) NOT NULL,
  `promedio` decimal(10,2) NOT NULL,
  `detalle` blob NOT NULL,
  `secundario_tecnico` tinyint(4) NOT NULL DEFAULT 0,
  `ausente` tinyint(4) NOT NULL DEFAULT 0,
  `fecha_carga` datetime DEFAULT current_timestamp(),
  `estado` enum('APROBADO','DESAPROBADO') NOT NULL,
  `id_incorporacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_medico`
--

CREATE TABLE `examen_medico` (
  `id` int(11) NOT NULL,
  `detalle` varchar(255) NOT NULL,
  `fecha_carga` datetime DEFAULT current_timestamp(),
  `estado` enum('APTO','APTO CONDICIONAL','NO APTO') DEFAULT NULL,
  `ausente` tinyint(4) NOT NULL DEFAULT 0,
  `id_incorporacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_psicologico`
--

CREATE TABLE `examen_psicologico` (
  `id` int(11) NOT NULL,
  `fecha_carga` datetime DEFAULT current_timestamp(),
  `estado` enum('APTO','NO APTO') DEFAULT NULL,
  `ausente` tinyint(4) NOT NULL DEFAULT 0,
  `id_incorporacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incorporacion`
--

CREATE TABLE `incorporacion` (
  `id` int(11) NOT NULL,
  `detalle` varchar(255) NOT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `merito` decimal(10,2) DEFAULT NULL,
  `estado` enum('APROBADA','DESAPROBADA','EN OBSERVACION','CONDICIONAL') NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `examen_fisico`
--
ALTER TABLE `examen_fisico`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_incorporacion` (`id_incorporacion`);

--
-- Indices de la tabla `examen_intelectual`
--
ALTER TABLE `examen_intelectual`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_incorporacion` (`id_incorporacion`);

--
-- Indices de la tabla `examen_medico`
--
ALTER TABLE `examen_medico`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_incorporacion` (`id_incorporacion`);

--
-- Indices de la tabla `examen_psicologico`
--
ALTER TABLE `examen_psicologico`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_incorporacion` (`id_incorporacion`);

--
-- Indices de la tabla `incorporacion`
--
ALTER TABLE `incorporacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `examen_fisico`
--
ALTER TABLE `examen_fisico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `examen_intelectual`
--
ALTER TABLE `examen_intelectual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `examen_medico`
--
ALTER TABLE `examen_medico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT de la tabla `examen_psicologico`
--
ALTER TABLE `examen_psicologico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `incorporacion`
--
ALTER TABLE `incorporacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2892;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `examen_fisico`
--
ALTER TABLE `examen_fisico`
  ADD CONSTRAINT `examen_fisico_ibfk_1` FOREIGN KEY (`id_incorporacion`) REFERENCES `incorporacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `examen_intelectual`
--
ALTER TABLE `examen_intelectual`
  ADD CONSTRAINT `examen_intelectual_ibfk_1` FOREIGN KEY (`id_incorporacion`) REFERENCES `incorporacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `examen_medico`
--
ALTER TABLE `examen_medico`
  ADD CONSTRAINT `examen_medico_ibfk_1` FOREIGN KEY (`id_incorporacion`) REFERENCES `incorporacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `examen_psicologico`
--
ALTER TABLE `examen_psicologico`
  ADD CONSTRAINT `examen_psicologico_ibfk_1` FOREIGN KEY (`id_incorporacion`) REFERENCES `incorporacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `incorporacion`
--
ALTER TABLE `incorporacion`
  ADD CONSTRAINT `incorporacion_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;