-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-12-2020 a las 15:05:09
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_nueva`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turnos_examen`
--

CREATE TABLE `turnos_examen` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `fecha_desde` date NOT NULL,
  `fecha_hasta` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `turnos_examen`
--

INSERT INTO `turnos_examen` (`id`, `nombre`, `fecha_desde`, `fecha_hasta`) VALUES
(1, '1er Turno', '2021-01-04', '2021-01-06'),
(2, '2do Turno', '2021-01-07', '2021-01-09'),
(3, '3er Turno', '2021-01-11', '2021-01-13'),
(4, '4to Turno', '2021-01-14', '2021-01-16'),
(5, '5to Turno', '2021-01-18', '2021-01-20'),
(6, '6to Turno', '2021-01-21', '2021-01-23'),
(7, '7mo Turno', '2021-01-25', '2021-01-27'),
(8, '8vo Turno', '2021-02-01', '2021-02-05');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `turnos_examen`
--
ALTER TABLE `turnos_examen`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `turnos_examen`
--
ALTER TABLE `turnos_examen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
