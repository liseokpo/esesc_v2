<!-- <script>
window.location.replace("index.php");
</script> -->

<?php

$fechaStringIncorp = "2021-05-15 00:00:00";
$fechaIncorp = date($fechaStringIncorp);

date_default_timezone_set('America/Argentina/Buenos_Aires');
$fechaActual = date("Y-m-d H:i:s", strtotime('+0 hours'));


if($fechaIncorp < $fechaActual){

if(isset($_SESSION['usuario'])){
    echo"<h2> Usted ya se encuentra inscripto.</h2>";
}

else{
?>
    <section id="cuerpo">
        <div class="border" style="background-color:rgb(0, 101, 46,.1)">
            <h3>Requisitos</h3>
            
            <ul class="mr-4">
                <ul>
                    <li>Antes de llenar el formulario, lea con atención todos los <a href="?p=requisitos" target="_blank" rel="noopener noreferrer">requisitos de ingreso</a> y verifique que cumpla con estos.</li>
                </ul>
            </ul>
        </div>
        <br>
        <h2 class="text-center text-shadow">INSCRIPCIÓN</h2>
        <?php
        if(isset($_GET['m'])){
            switch ($_GET['m']) {
                case 'errordni':
                    echo'<div class="alert alert-danger">El DNI que usted ingresó se encuentra registrado. Intente nuevamente o contáctese con el instituto.</div>';
                    break;
                case 'errormail':
                    echo'<div class="alert alert-danger">El email que usted ingresó se encuentra registrado. Intente nuevamente o contáctese con el instituto.</div>';
                    break;

                default:
                    echo'';
                    break;
            }
        }
        ?>
        <br>
        <div class="progress">
            <div class="progress-bar active" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
        </div><br/>
        <form id="regiration_form" action="paginas/metainsc_postulante.php"  method="post">
            <input type="hidden" name="tipoCandidato" value="POSTULANTE"/>
            <fieldset><h2>PASO 1/3</h2>
            <h1>Información general</h1>
                <div class="form-group mx-auto mb-4">
                    <medium style="color:#FF4848;">(*) Datos obligatorios </medium>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="nombres">Nombres (*)</label>
                            <input type="text" class="form-control text-uppercase" name="nombres" id="nombres" onchange="this.value = this.value.toUpperCase();" required>
                        </div>
                        <div class="col-md-6">
                            <label for="apellidos">Apellidos (*)</label>
                            <input type="text" class="form-control text-uppercase" name="apellidos" id="apellidos" onchange="this.value = this.value.toUpperCase();" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-5">
                            <label for="domicilio">Calle domicilio (*)</label>
                            <input type="text" class="form-control text-uppercase" name="domicilio" id="domicilio" onchange="this.value = this.value.toUpperCase();" required>
                            <small class="form-text">Recuerde que esta es la calle del domicilio donde usted vive actualmente.</small>
                        </div>
                        <div class="col-md-2">
                            <label for="nro">Altura (*)</label>
                            <input type="number" min=0 class="form-control" name="nro" id="nro" required>
                        </div>
                        <div class="col">
                            <label for="piso">Piso</label>
                            <input type="text" class="form-control" name="piso" id="piso">
                        </div>
                        <div class="col">
                            <label for="depto">Dpto</label>
                            <input type="text" class="form-control" name="depto" id="depto">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6 ">
                            <label for="selectprov">Provincia (*)</label>
                            <select id="selectprov" name="provincia" class="form-control" required>
                                <option value="" selected disabled>Elija provincia</option>
                            </select>
                        </div>
                        <div class="col-md-6 ">
                            <label for="selectloc">Localidad (*)</label>
                            <select id="selectloc" name="localidad" class="form-control" required>
                                <option value="" selected disabled>Elija localidad</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="codigopostal">Código postal (*)<a href="https://www.correoargentino.com.ar/formularios/cpa" target="_blank" rel="noopener noreferrer">(buscar)</a> </label>
                    <input type="text" class="form-control text-uppercase" name="codigopostal" id="codigopostal" onchange="this.value = this.value.toUpperCase();" required>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6 ">
                        <label for="telefono">Teléfono</label>
                        <input type="text" class="form-control" name="telefono" id="telefono">                
                    </div>
                    <div class="form-group col-md-6 ">
                        <label for="celular">Celular (*)</label>
                        <input type="number" min=0 class="form-control" name="celular" id="celular" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="email" >Email (*)</label>
                        <input type="email" class="form-control" name="email" id="email" required>             
                    </div>
                    <div class="form-group col-md-6">
                        <label for="reemail" >Re-escriba Email</label>
                        <input type="reemail" class="form-control" name="reemail" id="reemail" onfocus="validateMail(document.getElementById('email'), this, 'Los emails no coinciden');" 
                        oninput="validateMail(document.getElementById('email'), this,'Los emails no coinciden');">             
                    </div>
                </div>    
                
                <input type="button" id="siguiente" name="next" class="next btn btn-primary" value="Siguiente" />
            </fieldset>

            <fieldset><h2>PASO 2/3</h2>
            <h1>Datos personales</h1>    
                
                <div class="form-group">
                    <label for="fnacmiento">Fecha de nacimiento (*)</label>
                    <input type="date" class="form-control" name="fnacimiento" id="fnacimiento" max="<?php echo fechaNacimientoLimite('18','02','01'); ?>" min="<?php echo fechaNacimientoLimite('24','02','01'); ?>" required>
                </div>
                
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="dni">DNI(sin puntos) (*)</label>
                        <input type="number" min=0 class="form-control" name="dni" id="dni" required>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="redni" >Re-escriba DNI</label>
                        <input type="number" min=0 class="form-control" name="redni" id="redni" onfocus="validateMail(document.getElementById('dni'), this, 'Los dni no coinciden');" 
                        oninput="validateMail(document.getElementById('dni'), this,'Los dni no coinciden');">             
                    </div>
                    <div class="form-group col-md-4">
                        <label for="cuil">Número de Cuil(con guiones) (*)<a href="http://mi-cuil.com.ar/" target="_blank" rel="noopener noreferrer">(buscar)</a></label>
                        <input type="text" placeholder="Ej: XX-XXXXXXXX-X" class="form-control text-uppercase" name="cuil" id="cuil" onchange="this.value = this.value.toUpperCase();" required>
                    </div>        
                </div>

                <div class="form-group">
                    <label for="nacionalidad">Nacionalidad (*)</label>
                    <select class="custom-select" name="nacionalidad" id="nacionalidad" required>
                        <option value="" selected disabled>Indique su nacionalidad</option>
                        <option value="ARGENTINO_NAT">ARGENTINO (NATIVO)</option>
                        <option value="ARGENTINO_OP">ARGENTINO (POR OPCIÓN)</option>
                    </select>
                </div>
                <!-- SEXO / GRUPO SANG / EST CIVIL -->
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="sexo">Sexo (*)</label>
                        <select class="custom-select" name="sexo" id="sexo" required>
                            <option value="" selected disabled>Indique su sexo</option>
                            <option value="FEMENINO">FEMENINO</option>
                            <option value="MASCULINO">MASCULINO</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="gruposanguineo">Grupo Sanguíneo</label>
                        <select class="custom-select" name="gruposanguineo" id="gruposanguineo" required>
                            <option value="" selected disabled>Indique grupo sanguíneo</option>
                            <option value="0+">0+</option>
                            <option value="0-">0-</option>
                            <option value="A+">A+</option>
                            <option value="A-">A-</option>
                            <option value="AB+">AB+</option>
                            <option value="AB-">AB-</option>
                            <option value="B+">B+</option>
                            <option value="B-">B-</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="estadocivil">Estado Civil (*)</label>
                        <select class="custom-select" name="estadocivil" id="estadocivil" required>
                            <option value="" selected disabled>Indique estado civil</option>
                            <option value="SOLTERO (SIN HIJOS)">SOLTERO (SIN HIJOS)</option>
                            <option value="SOLTERO (CON HIJOS)">SOLTERO (CON HIJOS)</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="d-flex justify-content-center" for="essoldvol">¿Es Soldado Voluntario? (*)</label>
                    <div class="form-group btn-group btn-group-toggle d-flex justify-content-center" id="essoldvol" data-toggle="buttons">            
                        <label class="btn btn-primary">
                            <input type="radio" name="soldvol" id="option1" value="1" autocomplete="off" onchange="cambiarVisibilidad('destino', 'inline');siSoldVol();" required>SI
                        </label>
                        <label class="btn btn-primary">
                            <input type="radio" name="soldvol" id="option2" value="0" autocomplete="off" onchange="cambiarVisibilidad('destino', 'none');noSoldVol();">NO
                        </label>
                    </div>
                    <script>
                        function siSoldVol(){
                            $('#destinosoldvol').attr('required','required');
                        };
                        function noSoldVol(){
                            $('#destinosoldvol').removeAttr('required');
                            document.getElementById("destinosoldvol").value = "";
                        };
                    </script>
                </div>
                <div class="form-group" id="destino" style="display:none">
                        <label for="destinosoldvol">Indique el código de la unidad (*)</label>
                        <select id="destinosoldvol" name="destinosoldvol" class="form-control">
                            <option value="" selected disabled>Elija destino</option>
                        </select>
                </div>

                <br>
                
                <input type="button" name="previous" class="previous btn btn-secondary" value="Anterior"/>
                <input type="button" name="next" class="next btn btn-primary" value="Siguiente" />
            </fieldset>


            <fieldset><h2>PASO 3/3</h2>
                <h1>Estudio secundario</h1>
                <div class="alert alert-primary p-2" role="alert">
                    Importante: Una vez haya aprobado el examen de admisión y tenga su fecha de incorporación, deberá acreditar el certificado  del <b>Secundario completo</b> o <b>Secundario incompleto</b>, adeudando UNA (1) materia sin excepciones.
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <label for="plansecundario">Plan (*)</label>
                        <select class="custom-select" name="plansecundario" id="plansecundario" required>
                            <option value="" selected disabled>Indique tipo de secundario</option>
                            <option value="TECNICO">TÉCNICO</option>
                            <option value="BACHILLER">BACHILLER</option>
                            <option value="FINES">FINES</option>
                        </select>
                    </div>
                    
                    <div class="form-group col-md-6 pl-md-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="anio_cursado" id="exampleRadios1" value="COMPLETO" onchange="cambiarVisibilidad('debematerias', 'none');ponerMaterias(0);" required>
                            <label class="form-check-label" for="exampleRadios1">
                                Secundario completo
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="anio_cursado" id="exampleRadios2" value="INCOMPLETO" onchange="cambiarVisibilidad('debematerias', 'none');ponerMaterias(1);">
                            <label class="form-check-label" for="exampleRadios2">
                                Secundario incompleto, adeudando UNA (1)  materia.
                            </label>
                        </div>
                        <div class="form-check disabled">
                            <input class="form-check-input" type="radio" name="anio_cursado" id="exampleRadios3" value="INCOMPLETO" onchange="cambiarVisibilidad('debematerias', 'inline');ponerMaterias(2);">
                            <label class="form-check-label" for="exampleRadios3">
                                Secundario incompleto, adeudando más de UNA (1) materia.
                            </label>
                        </div>
                        <div class="form-check disabled">
                            <input class="form-check-input" type="radio" name="anio_cursado" id="exampleRadios4" value="EN_CURSO" onchange="cambiarVisibilidad('debematerias', 'none');ponerMaterias(0);">
                            <label class="form-check-label" for="exampleRadios4">
                                Secundario incompleto, cursando el último año del mismo.
                            </label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group" id="debematerias" style="display:none">
                        <label for="cantmateriassecundario">Indique la cantidad de materias adeudadas (*)</label>
                        <input type="number" class="form-control" id="cantmateriassecundario" name="cantmateriassecundario" min="0" value="0">                
                </div><br>
                <script>
                function ponerMaterias(numero){
                    document.getElementById("cantmateriassecundario").value = numero;
                    console.log("seteo nuevamente");
                }
                
                </script>

                <hr>
                <div class="form-row">
                    <div class="col">
                        <label for="tsecundario"><b>Título</b> con el que egresó/ Egresará (*)</label>
                        <input type="text" class="form-control text-uppercase" name="tsecundario" id="tsecundario" onchange="this.value = this.value.toUpperCase();" required>
                    </div>       
                </div>
                <div class="form-group">
                    <label for="nsecundario">Nombre del colegio/ Instituto (*)</label>
                    <input type="text" class="form-control text-uppercase" name="nsecundario" id="nsecundario"  onchange="this.value = this.value.toUpperCase();" required>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="psecundario">Provincia del colegio/ Instituto (*)</label>
                        <input type="text" class="form-control text-uppercase" name="psecundario" id="psecundario" onchange="this.value = this.value.toUpperCase();" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="lsecundario">Localidad del colegio/ Instituto (*)</label>
                        <input type="text" class="form-control text-uppercase" name="lsecundario" id="lsecundario" onchange="this.value = this.value.toUpperCase();" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="dircolegio">Dirección del colegio/ Instituto (*)</label>
                        <input type="text" class="form-control text-uppercase" name="dircolegio" id="dircolegio" onchange="this.value = this.value.toUpperCase();" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="telcolegio">Teléfono del colegio/ Instituto (*)</label>
                        <input type="number" class="form-control text-uppercase" name="telcolegio" id="telcolegio" onchange="this.value = this.value.toUpperCase();" required>

                    </div>
                </div>
                <div class="form-group">
                <h1>Plan de Carrera (*)</h1>
                    <h5><small class="form-text" style="color:#FF4848;">Importante: La elección final del Arma o Especialidad y Servicio se realiza una vez que el postulante está incorporado.</small></h5>

                    <div class="form-group btn-group btn-group-toggle d-flex justify-content-center" id="plancarrera" data-toggle="buttons">            
                        <label class="btn btn-primary p-4">
                            <input type="radio" name="plancarrera" id="option1" value="ESPECIALIDADES" autocomplete="off" onchange="cambiarVisibilidad('planarmas','none');cambiarVisibilidad('planespecialidades', 'inline');prueba2()" required>ESPECIALIDADES
                        </label>
                        <label class="btn btn-primary p-4">
                            <input type="radio" name="plancarrera" id="option2" value="ARMAS" autocomplete="off" onchange="cambiarVisibilidad('planespecialidades','none');cambiarVisibilidad('planarmas','inline');prueba()">ARMAS
                        </label>
                    </div>
                </div>

                <script>
                function prueba2(){
                    $('#orientacionesp').attr('required','required');
                    $('#centropreseleccionesp').attr('required','required');
                
                    $('#orientacionarm').removeAttr('required');
                    $('#centropreseleccionarm').removeAttr('required');
                    $('#orientacionarm')[0].selectedIndex = 0;
                    $('#centropreseleccionarm')[0].selectedIndex = 0;
                };
                function prueba(){
                    $('#orientacionarm').attr('required','required');
                    $('#centropreseleccionarm').attr('required','required');
                    
                    $('#orientacionesp').removeAttr('required');
                    $('#centropreseleccionesp').removeAttr('required');
                    $('#orientacionesp')[0].selectedIndex = 0;
                    $('#centropreseleccionesp')[0].selectedIndex = 0;
                };
                </script>

                <div class="form-group" id="planespecialidades" style="display:none">

                    <label for="orientacion">Elija orientación de especialidades (*)</label>
                    <select class="custom-select mb-3" name="orientacion" id="orientacionesp">
                        <option value="" selected disabled>Seleccione una opción</option>
                        <option value="MÚSICO">MÚSICO</option>
                        <option value="CONDUCTOR MOTORISTA">CONDUCTOR MOTORISTA</option>
                        <option value="ENFERMERO">ENFERMERO</option>
                        <option value="MECÁNICO EN INFORMÁTICA">MECÁNICO EN INFORMÁTICA</option>
                        <option value="" disabled>ADMINISTRACIÓN</option>
                        <option value="OFICINISTA">OFICINISTA</option>
                        <option value="INTENDENCIA">INTENDENCIA</option>
                        <option value="" disabled>MECÁNICOS</option>
                        <option value="MECÁNICO MOTORISTA A RUEDA">MECÁNICO MOTORISTA A RUEDA</option>
                        <option value="MECÁNICO MOTORISTA ELECTRICISTA">MECÁNICO MOTORISTA ELECTRICISTA</option>
                        <option value="MECÁNICO MOTORISTA A ORUGA">MECÁNICO MOTORISTA A ORUGA</option>
                        <option value="MECÁNICO ARMERO">MECÁNICO ARMERO</option>
                        <option value="MECÁNICO DE MUNICIÓN Y EXPLOSIVOS">MECÁNICO DE MUNICIÓN Y EXPLOSIVOS</option>
                        <option value="MECÁNICO DE ARTILLERÍA">MECÁNICO DE ARTILLERÍA</option>
                        <option value="MECÁNICO DE INSTALACIONES">MECÁNICO DE INSTALACIONES</option>
                        <option value="MECÁNICO DE INGENIEROS">MECÁNICO DE INGENIEROS</option>
                        <option value="MECÁNICO DE AVIACIÓN">MECÁNICO DE AVIACIÓN</option>
                        <option value="MECÁNICO DE ÓPTICA Y APARATOS DE PRECISIÓN">MECÁNICO DE ÓPTICA Y APARATOS DE PRECISIÓN</option>
                        <option value="" disabled>ELECTRÓNICA</option>
                        <option value="MECÁNICO DE EQUIPO DE CAMPAÑA">MECÁNICO DE EQUIPO DE CAMPAÑA</option>
                        <option value="MECÁNICO DE EQUIPOS FIJOS">MECÁNICO DE EQUIPOS FIJOS</option>
                        <option value="MECÁNICO DE RADAR">MECÁNICO DE RADAR</option>

                    </select>

                    <label for="centropreseleccion">Elija un centro de preselección (*)</label>
                    <select class="custom-select mb-3" name="centropreseleccion" id="centropreseleccionesp">
                        <option value="" selected disabled>Seleccione una opción</option>
                        <option value='ESCUELA DE SUBOFICIALES DEL EJÉRCITO "SARGENTO CABRAL"'>ESCUELA DE SUBOFICIALES DEL EJÉRCITO "SARGENTO CABRAL" - BUENOS AIRES</option>
                        <option value='LICEO MILITAR "GENERAL ROCA"'>LICEO MILITAR "GENERAL ROCA" - CHUBUT</option>
                        <option value='LICEO MILITAR "GENERAL PAZ"'>LICEO MILITAR "GENERAL PAZ" - CÓRDOBA</option>
                        <option value='LICEO MILITAR "GENERAL LAMADRID"'>LICEO MILITAR "GENERAL LAMADRID" - TUCUMÁN</option>
                        <option value='COMANDO DE LA XIIda BRIGADA DE MONTE'>COMANDO DE LA XIIda BRIGADA DE MONTE - MISIONES</option>
                        <option value='LICEO MILITAR "GENERAL BELGRANO"'>LICEO MILITAR "GENERAL BELGRANO" - SANTA FÉ</option>
                        <option value='LICEO MILITAR "GENERAL ESPEJO"'>LICEO MILITAR "GENERAL ESPEJO" - MENDOZA</option>
                        <option value='COMANDO DE LA Vta BRIGADA DE MONTAÑA'>COMANDO DE LA Vta BRIGADA DE MONTAÑA - SALTA</option>
                        <option value='REGIMIENTO DE INFANTERÍA DE MONTE 29 "CNL IGNACIO JOSÉ WARNES"'>REGIMIENTO DE INFANTERÍA DE MONTE 29 "CNL IGNACIO JOSÉ WARNES" - FORMOSA</option>
                        
                    </select>
                </div>
                <div class="form-group" id="planarmas" style="display:none">

                    <label for="orientacion">Elija orientación de armas (*)</label>
                    <select class="custom-select  mb-3" name="orientacion" id="orientacionarm">
                        <option value="" selected disabled>Seleccione una opción</option>
                        <option value="INFANTERÍA">INFANTERÍA</option>
                        <option value="CABALLERÍA">CABALLERÍA</option>
                        <option value="ARTILLERÍA">ARTILLERÍA</option>
                        <option value="INGENIEROS">INGENIEROS</option>
                        <option value="COMUNICACIONES">COMUNICACIONES</option>
                    </select>

                    <label for="centropreseleccion">Elija un centro de preselección (*)</label>
                    <select class="custom-select mb-3" name="centropreseleccion" id="centropreseleccionesp">
                        <option value="" selected disabled>Seleccione una opción</option>
                        <option value='ESCUELA DE SUBOFICIALES DEL EJÉRCITO "SARGENTO CABRAL"'>ESCUELA DE SUBOFICIALES DEL EJÉRCITO "SARGENTO CABRAL" - BUENOS AIRES</option>
                        <option value='LICEO MILITAR "GENERAL ROCA"'>LICEO MILITAR "GENERAL ROCA" - CHUBUT</option>
                        <option value='LICEO MILITAR "GENERAL PAZ"'>LICEO MILITAR "GENERAL PAZ" - CÓRDOBA</option>
                        <option value='LICEO MILITAR "GENERAL LAMADRID"'>LICEO MILITAR "GENERAL LAMADRID" - TUCUMÁN</option>
                        <option value='COMANDO DE LA XIIda BRIGADA DE MONTE'>COMANDO DE LA XIIda BRIGADA DE MONTE - MISIONES</option>
                        <option value='LICEO MILITAR "GENERAL BELGRANO"'>LICEO MILITAR "GENERAL BELGRANO" - SANTA FÉ</option>
                        <option value='LICEO MILITAR "GENERAL ESPEJO"'>LICEO MILITAR "GENERAL ESPEJO" - MENDOZA</option>
                        <option value='COMANDO DE LA Vta BRIGADA DE MONTAÑA'>COMANDO DE LA Vta BRIGADA DE MONTAÑA - SALTA</option>
                        <option value='REGIMIENTO DE INFANTERÍA DE MONTE 29 "CNL IGNACIO JOSÉ WARNES"'>REGIMIENTO DE INFANTERÍA DE MONTE 29 "CNL IGNACIO JOSÉ WARNES" - FORMOSA</option>
                        
                    </select>
                </div>  
                
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck10" required>
                    <label class="form-check-label" for="defaultCheck10">
                        He releído y verificado toda la información brindada.
                    </label>
                </div>
                <input type="button" name="previous" class="previous btn btn-secondary" value="Anterior" />
                <input type="submit" name="enviarformpre" class="submit btn btn-primary" value="Enviar" />
            </fieldset>
        </form>
    </section>
<?php
}
}
?>

