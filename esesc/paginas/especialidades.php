<section id="cuerpo" style="color:#444444">
<h1  class="text-center text-shadow">Especialidades y Servicios</h1><br>

<div class="row m-0 p-0 w-100 justify-content-center ">
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card border-0 text-center w-100" style="background-color:rgb(255,255,255,0);">
            <div class="card-body">
            <img src="./assets/img/carreras/especialidades/MUSICOS.png" class="card-img-top" style="width:80%" alt="...">
                <h3>Músico
                
                </h3>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#musico">
                Ver
                </button>
                <p class="card-text"></p>
                
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card border-0 text-center w-100" style="background-color:rgb(255,255,255,0);">
            <div class="card-body">
                <img src="./assets/img/carreras/especialidades/MOTORISTA.png" class="card-img-top" style="width:80%" alt="...">

                <h3>Conductor Motorista</h3>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#motorista">
                Ver
                </button>
                <p class="card-text"></p>
                
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card border-0 text-center w-100" style="background-color:rgb(255,255,255,0);">
            <div class="card-body">
                <img src="./assets/img/carreras/especialidades/SANIDAD.png" class="card-img-top" style="width:80%" alt="...">

                <h3>Enfermero</h3>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#enfermero">
                Ver
                </button>
                <p class="card-text"></p>
                
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card border-0 text-center w-100" style="background-color:rgb(255,255,255,0);">
            <div class="card-body">
                <img src="./assets/img/carreras/especialidades/ARSENALES.png" class="card-img-top" style="width:80%" alt="...">

                <h3>Arsenales<br>(Mecánicos)</h3>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#arsenales">
                Ver
                </button>
                <p class="card-text"></p>
                
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card border-0 text-center w-100" style="background-color:rgb(255,255,255,0);">
            <div class="card-body">
                <img src="./assets/img/carreras/especialidades/OFICINISTAS.png" class="card-img-top" style="width:80%" alt="...">

                <h3>Oficinista</h3>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#oficinista">
                Ver
                </button>
                <p class="card-text"></p>
                
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card border-0 text-center w-100" style="background-color:rgb(255,255,255,0);">
            <div class="card-body">
                <img src="./assets/img/carreras/especialidades/INTENDENCIA.png" class="card-img-top" style="width:80%" alt="...">

                <h3>Intendencia</h3>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#intendencia">
                Ver
                </button>
                <p class="card-text"></p>
                
            </div>
        </div>
    </div>    
</div>

<div class="modal fade" id="musico" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Músico</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/carreras/especialidades/musicos_foto.jpg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Es el servicio con Personal organizado equipado e instruido para contribuir al mantenimiento y elevación de la moral por medio de la música.
        <br><br>
        El Suboficial Músico ejecuta música militar durante los desfiles y marchas, como así también música folclórica y popular en festividades que se desarrollan en distintas Unidades del Ejército, Instituciones Educativas, Religiosas, Culturales y Deportivas.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="motorista" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Conductor Motorista</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/carreras/especialidades/mec_motorista_foto.jpg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Son los Suboficiales conductores de los vehículos de dotaciones en toda su variedad: automóviles, camionetas de doble tracción, camiones, máquinas viales y blindados. En todos los casos tiene a su cargo tareas de mantenimiento de rutina.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="enfermero" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Enfermero</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/carreras/especialidades/sanidad_foto.jpeg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Es el servicio organizado, equipado e instruido para conservar y recuperar la aptitud psicofísica del personal, abastecer y mantener los efectos médicos y odontológicos, y realizar tareas ejecutivas correspondientes al registro necrológico.
        <br><br>
        El Suboficial de la especialidad de Enfermero es el responsable de la atención y el cuidado de pacientes en distintos servicios y dependencias del Ejército, también participa de campañas para la prevención y cuidado de la salud. 
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="arsenales" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Arsenales</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h4>Mecánico de Informática</h4>
        <img src="./assets/img/carreras/especialidades/mec_informatica_foto.jpeg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Será el Suboficial cuya responsabilidad será el control, mantenimiento y reparación de las computadoras y sus respectivos periféricos.
        </p>
        <br>
        <h4>Mecánico Motorista a Rueda</h4>
        <img src="./assets/img/carreras/especialidades/mec_rueda_foto.jpg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Es el especialista en Vehículos a Rueda, tiene la responsabilidad del mantenimiento y la reparación de automóviles y camiones que la Fuerza tiene como dotación.
        </p>
        <br>
        <h4>Mecánico Motorista Electricista</h4>
        <img src="./assets/img/carreras/especialidades/mec_electricista_foto.jpg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Tiene la responsabilidad del mantenimiento y la reparación del sistema eléctrico de la totalidad de los vehículos con que cuenta la Fuerza (a rueda y oruga).
        </p>
        <br>
        <h4>Mecánico Motorista a Oruga</h4>
        <img src="./assets/img/carreras/especialidades/mec_oruga_foto.jpg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Es el Suboficial especialista en Vehículos a Oruga, tiene la responsabilidad del mantenimiento y la reparación de los vehículos blindados que la Fuerza tiene como dotación.
        </p>
        <br>
        <h4>Mecánico Armero</h4>
        <img src="./assets/img/carreras/especialidades/mec_armero_foto.jpeg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Será responsable de la reparación y mantenimiento del armamento portátil con que cuenta la Fuerza, entre ellos: pistolas, fusiles, ametralladoras y lanzacohetes.
        </p>
        <br>
        <h4>Mecánico de Munición y Explosivos</h4>
        <img src="./assets/img/carreras/especialidades/municionyexplosivos.jpeg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Está capacitado para desarrollar tareas de mantenimiento, control y destrucción de la munición y explosivos, como así también tiene la responsabilidad de intervenir técnica y administrativamente en la inspección del material de municiones con que cuenta la Fuerza.
        </p>
        <br>
        <h4>Mecánico de Artillería</h4>
        <img src="./assets/img/carreras/especialidades/mec_artilleria_foto.jpeg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Tiene a su cargo la reparación y el mantenimiento de los sistemas de disparo, eléctricos, hidráulicos y mecánicos de cañones y ametralladoras de gran calibre, como así también el arma de los tanques de guerra.
        </p>
        <br>
        <h4>Mecánico de Instalaciones</h4>
        <img src="./assets/img/carreras/especialidades/mec_instalaciones_foto.jpeg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Tiene la responsabilidad del mantenimiento y reparación de sistemas de red de agua, gas, calefacción y electricidad en infraestructuras domiciliarias. Asimismo, participa en los proyectos y la construcción de edificios hasta de un piso de elevación.
        </p>
        <br>
        <h4>Mecánico de Ingenieros</h4>
        <img src="./assets/img/carreras/especialidades/mec_ingenieros_foto.jpeg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Es aquel suboficial que tiene a su cargo el mantenimiento y la reparación de equipos de navegación, máquinas viales, puentes, equipos potabilizadores de agua y grupos electrógenos.
        </p>
        <br>
        <h4>Mecánico de Óptica y Aparatos de Precisión</h4>
        <img src="./assets/img/carreras/especialidades/mec_optico_foto.jpg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Este Suboficial tendrá a su cargo el mantenimiento y reparación de aparatos ópticos y de precisión (anteojos binoculares, brújulas, visores nocturnos, instrumentos de puntería de cañones e instrumentos de topografía).
        </p>
        <br>
        <h4>Mecánico de Equipos de Campaña</h4>
        <img src="./assets/img/carreras/especialidades/mec_equiposdecampania_foto.jpeg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Tiene a su cargo el control, mantenimiento, reparación y operación de los equipos portátiles, antenas y centrales telefónicas analógicas y digitales con que cuenta la Fuerza.
        </p>
        <br>
        <h4>Mecánico de Equipos Fijos</h4>
        <img src="./assets/img/carreras/especialidades/mec_equiposfijos_foto.jpeg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Será el responsable del control, mantenimiento, reparación y operación de los equipos, antenas y centros de comunicaciones con que cuenta la Fuerza.
        </p>
        <br>
        <h4>Mecánico de Radar</h4>
        <img src="./assets/img/carreras/especialidades/mec_radar_foto.jpeg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Tiene a su cargo el control, mantenimiento, reparación y operación de los radares con que cuenta la Fuerza.
        </p>
        <br>
        <h4>Mecánico de Aviación</h4>
        <img src="./assets/img/carreras/especialidades/mec_aviacion_foto.jpg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Es aquel que tiene a su cargo el mantenimiento y la reparación de aviones y helicópteros de la Fuerza, siendo a la vez integrante de la tripulación en vuelo.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="oficinista" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Oficinista</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/carreras/especialidades/oficinistas_foto.jpg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        El Suboficial de esta especialidad desarrolla sus tareas en el área de administración, elaborando mediante la herramienta informática la documentación y correspondencia de la Fuerza. Se desempeña además como auxiliar de la Justicia Militar.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="intendencia" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Intendencia</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/carreras/especialidades/intendencia_foto.jpeg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Es la especialidad organizada, equipada e instruida para el abastecimiento de los efectos relativos a la alimentación del personal, vestuario, equipo, materiales para alojamiento, muebles, combustibles, lubricantes y efectos afines destinados a mantener la aptitud operacional del Ejército.
        <br><br>
        El suboficial de Intendencia tiene a su cargo tareas como: recepción, almacenamiento y distribución de alimentos, vestimenta y combustibles de uso en la fuerza, como así también llevará la contabilidad con sistemas informáticos, realizará presupuestos y tendrá responsabilidad en el área de tesorería y liquidación de haberes.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="motoristarueda" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Mecánico Motorista a Rueda</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/trabajando.png" class="w-100"alt="">
        <p>
        Es el especialista en Vehículos a Rueda, tiene la responsabilidad del mantenimiento y la reparación de automóviles y camiones que la Fuerza tiene como dotación.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="motoristaelectricista" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Mecánico Motorista Electricista</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/trabajando.png" class="w-100"alt="">
        <p>
        Tiene la responsabilidad del mantenimiento y la reparación del sistema eléctrico de la totalidad de los vehículos con que cuenta la Fuerza (a rueda y oruga).
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="motoristaoruga" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Mecánico Motorista a Oruga</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/trabajando.png" class="w-100"alt="">
        <p>
        Es el Suboficial especialista en Vehículos a Oruga, tiene la responsabilidad del mantenimiento y la reparación de los vehículos blindados que la Fuerza tiene como dotación.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="mecanicoarmero" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Mecánico Armero</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/trabajando.png" class="w-100"alt="">
        <p>
        Será responsable de la reparación y mantenimiento del armamento portátil con que cuenta la Fuerza, entre ellos: pistolas, fusiles, ametralladoras y lanzacohetes.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="mecanicomunicionexplosivos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Mecánico de Munición y Explosivos</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/trabajando.png" class="w-100"alt="">
        <p>
        Está capacitado para desarrollar tareas de mantenimiento, control y destrucción de la munición y explosivos, como así también tiene la responsabilidad de intervenir técnica y administrativamente en la inspección del material de municiones con que cuenta la Fuerza.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="mecanicoartilleria" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Mecánico de Artillería</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/trabajando.png" class="w-100"alt="">
        <p>
        Tiene a su cargo la reparación y el mantenimiento de los sistemas de disparo, eléctricos, hidráulicos y mecánicos de cañones y ametralladoras de gran calibre, como así también el arma de los tanques de guerra.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="mecanicoinstalaciones" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Mecánico de Instalaciones</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/trabajando.png" class="w-100"alt="">
        <p>
        Tiene la responsabilidad del mantenimiento y reparación de sistemas de red de agua, gas, calefacción y electricidad en infraestructuras domiciliarias. Asimismo, participa en los proyectos y la construcción de edificios hasta de un piso de elevación.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="mecanicoingenieros" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Mecánico de Ingenieros</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/trabajando.png" class="w-100"alt="">
        <p>
        Es aquel suboficial que tiene a su cargo el mantenimiento y la reparación de equipos de navegación, máquinas viales, puentes, equipos potabilizadores de agua y grupos electrógenos.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="mecanicoopticapresicion" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Mecánico de Óptica y Aparatos de Precisión</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/trabajando.png" class="w-100"alt="">
        <p>
        Este Suboficial tendrá a su cargo el mantenimiento y reparación de aparatos ópticos y de precisión (anteojos binoculares, brújulas, visores nocturnos, instrumentos de puntería de cañones e instrumentos de topografía).
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="mecanicoequipocampania" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Mecánico de Equipo de Campaña</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/trabajando.png" class="w-100"alt="">
        <p>
        Tiene a su cargo el control, mantenimiento, reparación y operación de los equipos portátiles, antenas y centrales telefónicas analógicas y digitales con que cuenta la Fuerza.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="mecanicoequiposfijos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Mecánico de Equipos Fijos</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/trabajando.png" class="w-100"alt="">
        <p>
        Será el responsable del control, mantenimiento, reparación y operación de los equipos, antenas y centros de comunicaciones con que cuenta la Fuerza.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="mecanicoradar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Mecánico de Radar</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/trabajando.png" class="w-100"alt="">
        <p>
        Tiene a su cargo el control, mantenimiento, reparación y operación de los radares con que cuenta la Fuerza.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="mecanicoaviacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Mecánico de Aviación</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/trabajando.png" class="w-100"alt="">
        <p>
        Es aquel que tiene a su cargo el mantenimiento y la reparación de aviones y helicópteros de la Fuerza, siendo a la vez integrante de la tripulación en vuelo.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<br>
<h2 class="text-center">Título de egreso</h2>
<table class="table table-responsive-sm table-dark text-dark" style="background-color:#f5f5f5;">
    <thead class="thead bg-secondary text-whitesmoke">
        <tr>
            <th scope="col">TÍTULO DE EGRESO</th><th scope="col">ORIENTACIÓN</th><th scope="col">GRADO DE EGRESO</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan="5">TECNICATURA SUPERIOR</td>
        </tr>
        <tr>
            <td>ENFERMERO GENERAL</td>
            <td>CABO</td>
        </tr>
        <tr>
            <td>LOGÍSTICA DE INTENDENCIA Y ADMINISTRACIÓN CONTABLE</td>
            <td>CABO</td>
        </tr>
        <tr>
            <td>CONSTRUCCIONES E INSTALACIONES FIJAS</td>
            <td>CABO</td>
        </tr>
        <tr>
            <td>INFORMÁTICA</td>
            <td>CABO</td>
        </tr>
        <tr>
            <td rowspan="13">TECNICATURAS MILITARES</td>
        </tr>
        <tr>
            <td>AUTOMOTORES CON ORIENTACIÓN A: <br/>VEHÍCULOS A RUEDA<br/>VEHÍCULOS A ORUGA</td>
            <td>CABO</td>
        </tr>
        <tr>
            <td>ELECTRICIDAD DEL AUTOMOTOR</td>
            <td>CABO</td>
        </tr>
        <tr>
            <td>ELECTRÓNICA CON ORIENTACIÓN EN EQUIPOS DE CAMPAÑA</td>
            <td>CABO</td>
        </tr>
        <tr>
            <td>ELECTRÓNICA CON ORIENTACIÓN EN EQUIPOS FIJOS</td>
            <td>CABO</td>
        </tr>
        <tr>
            <td>ELECTRÓNICA CON ORIENTACIÓN A EQUIPOS DE RADAR</td>
            <td>CABO</td>
        </tr>
        <tr>
            <td>MANTENIMIENTO DE AERONAVES ARMAMENTO LIVIANO</td>
            <td>CABO</td>
        </tr>
        <tr>
            <td>MATERIAL DE ARTILLERÍA</td>
            <td>CABO</td>
        </tr>
        <tr>
            <td>MUNICIÓN Y EXPLOSIVOS</td>
            <td>CABO</td>
        </tr>
        <tr>
            <td>ÓPTICA Y APARATOS DE PRECISIÓN</td>
            <td>CABO</td>
        </tr>
        <tr>
            <td>EQUIPOS Y MÁQUINAS DE INGENIEROS</td>
            <td>CABO</td>
        </tr>
        <tr>
            <td>CONDUCTOR MOTORISTA</td>
            <td>CABO</td>
        </tr>
        <tr>
            <td>OFICINISTA</td>
            <td>CABO</td>
        </tr> 
        <tr>
            <td rowspan="2"></td>
        </tr>
        <tr>
            <td>MÚSICO</td>
            <td>CABO</td>
        </tr>
    </tbody>
</table>
<h5>DURACIÓN DE LA CARRERA: DOS (2) años. RÉGIMEN: INTERNADO</h5>



</section>