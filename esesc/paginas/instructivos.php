
<section id="cuerpo" style="color:#444444">
    <h1 class="text-center text-shadow" id="como_inscribirme">¿Cómo inscribirme?</h1><br>
    <div class=" p-3 w-100" style="background-color:rgb(255,248,177)">
        <ul>
            <li>
                La inscripción se realiza vía online a través de nuestro sitio web oficial.
                Hacé click <a href="?p=inscripcion">acá</a> para inscribirte.
            </li>
            <li>
                Proceso de la INSCRIPCIÓN 2021<br>
                <ul>
                    <li>Ingresá a nuestro sitio web y completá el formulario de inscripción. Una vez hecho obtendrás un usuario y una contraseña para conocer tu estado de inscripción.</li>
                    <li>Aboná el derecho a examen a través de e-Recauda</li>
                    <li>Cargá los documentos requeridos como foto del DNI, comprobante de pago y ticket de pago</li>
                    <li>
                        Ingresá <a href="ingreso.php">acá</a> con tu usuario y contraseña para hacer el seguimiento de tu inscripción.
                    </li>
                </ul>
            </li>
            
        </ul>
        <div class="row">
            <div class="col text-center">
                <h3 class="pb-2" ><strong>Video instructivo de inscripción</strong></h3>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/M28Xf7MKrJo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
            
    </div>


    <br>
    <h1 class="text-center text-shadow" id="como_abonar">¿Cómo abonar?</h1><br>
    <div class=" p-3 w-100" style="background-color:rgb(255,248,177)">
        <div class="row">
            <div class="col ">
                <h3 class="pb-2 text-center" ><strong>Aprendé a generar tu boleta de pago/comprobante de pago</strong></h3>
                <ul>
                    <li><h4 class="pb-2">Valor derecho de examen: $ 3000.-</h4></li>
                    <li><h4 class="pb-2">Conozca las <a href="https://dgsiaf.mecon.gov.ar/wp-content/uploads/TE_EREC_ERECAUDA_Entidades_Habilitadas_para_el_Pago.pdf" target="_blank">entidades habilitadas</a> para abonar la boleta de pago generada por eRecauda.</h4></li>
                </ul>
                
                
                <div class="text-center">
                    <iframe  width="560" height="315" src="https://www.youtube.com/embed/lUbbMTwFCIU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                

                
            </div>
        </div>
            
    </div>
</section>