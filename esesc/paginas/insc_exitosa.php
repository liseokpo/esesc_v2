<?php
if(isset($_SESSION["inscripto"])){

?>
<section id="cuerpo">

    <div class="alert text-white" style="background-color: rgb(0,101,46,0.5)"role="alert">
        ¡Su cuenta de usuario ha sido generada correctamente!<br>
        A continuación sus datos serán revisados por personal de la institución.<br><br>
        Para conocer su estado, deberá ingresar con su DNI (sin puntos) y la  siguiente contraseña.<br>
        Una vez dentro de su cuenta, podrá cambiar su contraseña por una que usted desee.<br><br>
        <ul>
         <li>La contraseña brindada no debe ser olvidada. La necesitará para ingresar por primera vez.</li>   
         <li>Respete tanto letras mayúsculas como minúsculas</li>
        </ul>
        DNI: <b><?php echo $_SESSION["inscripto"] ?></b>       CONTRASEÑA: <b><?php echo $_SESSION['clave'] ?></b>
        <br><br>

        Muchas gracias.
    </div>
    <br>
    <h3>Para ingresar, <a href="ingresar.php">haga clic aquí</a></h3>

</section>
<?php
session_destroy();
}
else{
    echo"<h2 class='text-center'>Esta página se encuentra restringida.</h2>";
}
?>


