
    <section id="cuerpo" class="w-100" style="color:#444444">
        <div class="container p-0 m-0 mw-100">
            <br><br>
            <div class="row m-0 p-0">
                <div class="col-12 col-lg-9 m-0 p-0 pr-lg-2 bg-black">
                    
                    <h3 class="text-left"><b>NOVEDADES</b></h3>
                    <div id="carouselNovedades" class="carousel slide carousel-fade" data-ride="carousel" >
                        <ol class="carousel-indicators">
                            <li data-target="#carouselNovedades" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselNovedades" data-slide-to="1"></li>
                            <li data-target="#carouselNovedades" data-slide-to="2"></li>
                            <li data-target="#carouselNovedades" data-slide-to="3"></li>
                            <li data-target="#carouselNovedades" data-slide-to="4"></li>
                            <li data-target="#carouselNovedades" data-slide-to="5"></li>
                            <li data-target="#carouselNovedades" data-slide-to="6"></li>
                            <li data-target="#carouselNovedades" data-slide-to="7"></li>
                            <li data-target="#carouselNovedades" data-slide-to="8"></li>
                            <li data-target="#carouselNovedades" data-slide-to="9"></li>
                            <li data-target="#carouselNovedades" data-slide-to="10"></li>
                            <li data-target="#carouselNovedades" data-slide-to="11"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active carousel('pause')">
                                
                                <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5 d-lg-none">
                                    <a href="?p=noticia49" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>
    
                                <img class="d-block w-100" src="assets/img/noticias/ceremonia_ascenso_portada.jpg" alt="...">
    
                                <div class="carousel-caption d-none d-lg-block">
                                    <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5">
                                        <a href="?p=noticia49" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                    </div>
                                </div>
    
                            </div>
                            <div class="carousel-item">
                                
                                <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5 d-lg-none">
                                    <a href="?p=noticia48" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>
    
                                <img class="d-block w-100" src="assets/img/noticias/galarza_portada.jpg" alt="...">
    
                                <div class="carousel-caption d-none d-lg-block">
                                    <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5">
                                        <a href="?p=noticia48" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                    </div>
                                </div>
    
                            </div>
                            <div class="carousel-item">
                                
                                <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5 d-lg-none">
                                    <a href="https://www.youtube.com/watch?v=fjPlmjvfvic" class="btn-sm text-white" style="background-color:#007bff;">VER VIDEO COMPLETO</a>
                                </div>
    
                                <img class="d-block w-100" src="assets/img/noticias/catedravalores_videocompleto.jpg" alt="...">
    
                                <div class="carousel-caption d-none d-lg-block">
                                    <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5">
                                        <a href="https://www.youtube.com/watch?v=fjPlmjvfvic" class="btn-sm text-white" style="background-color:#007bff;">VER VIDEO COMPLETO</a>
                                    </div>
                                </div>
    
                            </div>
                            <div class="carousel-item">
                                
                                <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5 d-lg-none">
                                    <a href="?p=noticia47" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>
    
                                <img class="d-block w-100" src="assets/img/noticias/catedra_valores_soldado.jpg" alt="...">
    
                                <div class="carousel-caption d-none d-lg-block">
                                    <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5">
                                        <a href="?p=noticia47" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                    </div>
                                </div>
    
                            </div>
                            <div class="carousel-item">
                                
                                <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5 d-lg-none">
                                    <a href="?p=noticia46" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>
    
                                <img class="d-block w-100" src="assets/img/noticias/diasoldado_2021_portada.jpg" alt="...">
    
                                <div class="carousel-caption d-none d-lg-block">
                                    <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5">
                                        <a href="?p=noticia46" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                    </div>
                                </div>
    
                            </div>
                            <div class="carousel-item">
                                
                                <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5 d-lg-none">
                                    <a href="?p=noticia45" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>
    
                                <img class="d-block w-100" src="assets/img/noticias/palabras_portada.jpg" alt="...">
    
                                <div class="carousel-caption d-none d-lg-block">
                                    <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5">
                                        <a href="?p=noticia45" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                    </div>
                                </div>
    
                            </div>
                            <div class="carousel-item">
                                    
                                <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5 d-lg-none">
                                    <a href="?p=noticia43" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>

                                <img class="d-block w-100" src="assets/img/noticias/estado_inscripcion_portada.jpg" alt="...">

                                <div class="carousel-caption d-none d-lg-block">
                                    <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5">
                                        <a href="?p=noticia43" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                    </div>
                                </div>

                            </div>
                            <div class="carousel-item">
                                
                                <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5 d-lg-none">
                                    <a href="?p=noticia42" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>

                                <img class="d-block w-100" src="assets/img/noticias/inscripcion_2021.jpg" alt="...">

                                <div class="carousel-caption d-none d-lg-block">
                                    <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5">
                                        <a href="?p=noticia42" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                    </div>
                                </div>

                            </div>
                            <div class="carousel-item">
                                
                                <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5 d-lg-none">
                                    <a href="?p=noticia41" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>

                                <img class="d-block w-100" src="assets/img/noticias/fund_soldados.jpg" alt="...">

                                <div class="carousel-caption d-none d-lg-block">
                                    <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5">
                                        <a href="?p=noticia41" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                    </div>
                                </div>

                            </div>
                            <div class="carousel-item">
                                
                                <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5 d-lg-none">
                                    <a href="?p=noticia40" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>

                                <img class="d-block w-100" src="assets/img/noticias/becas_estrategicas.jpg" alt="...">

                                <div class="carousel-caption d-none d-lg-block">
                                    <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5">
                                        <a href="?p=noticia40" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                    </div>
                                </div>

                            </div>
                           
                            <div class="carousel-item">
                                <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5 d-lg-none">
                                    <a href="?p=eldirector" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>

                                <img class="d-block w-100" src="assets/img/noticias/cr_sivori_portada.jpg" alt="...">

                                <div class="carousel-caption d-none d-lg-block">
                                    <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5">
                                        <a href="?p=eldirector" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5 d-lg-none">
                                    <a href="?p=noticia34" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>
                                <img class="d-block w-100" src="assets/img/noticias/salida_terreno_portada.jpg" alt="...">
                                
                                <div class="carousel-caption d-none d-lg-block">
                                    <div class="w-100 h-100 position-absolute d-flex justify-content-center align-items-end pb-5">
                                        <a href="?p=noticia34" class="btn-sm text-white" style="background-color:#007bff;">VER NOTICIA</a>
                                    </div>
                                </div>
                            </div>                     
                        </div>
                        <a class="carousel-control-prev" href="#carouselNovedades" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Anterior</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselNovedades" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Siguiente</span>
                        </a>
                    </div>
                    
                </div>
                
                <div class="col-12 col-lg-3 p-lg-0 m-0 p-0 pt-2 text-center h-auto" >
                    <div class="row p-0 m-0">
                        <h3 class="text-left"><b>VIDEO</b></h3>
                    </div>
                    <div class="row p-0 m-0" style=" background-color: black;">
                        <a class="w-100" href="#" onclick="lightbox_open();">
                            <img  class="w-100 portadavidlat" style="object-fit: cover;" src="assets/img/vid_lat_3.jpg" >
                        </a>
                    </div>
                                        
                </div>
            </div>
        </div>    
        <div id="light" class="w-100 text-center">
            <video id="VisaChipCardVideo" class="w-auto mw-100 p-auto" controls>
                <source src="assets/img/esesc_spot.mp4" type="video/mp4">
            </video>
            <!-- <a class="boxclose position-absolute d-flex justify-content-end align-items-start" id="boxclose" onclick="lightbox_close();"></a> -->

        </div>
        <div id="fadevid" onClick="lightbox_close();"></div>
        </div>
        <div class="pt-2"></div>
        <div class="text-center">
            <br><br>
            <h3 class="text-center"><b>EL INSTITUTO EN IMÁGENES</b></h3>
        </div>
        <div id="carouselGaleria" class="carousel slide w-auto" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselGaleria" data-slide-to="0" class="active"></li>
                <li data-target="#carouselGaleria" data-slide-to="1"></li>
                <li data-target="#carouselGaleria" data-slide-to="2"></li>
                <li data-target="#carouselGaleria" data-slide-to="3"></li>
                <li data-target="#carouselGaleria" data-slide-to="4"></li>
                <li data-target="#carouselGaleria" data-slide-to="5"></li>
                <li data-target="#carouselGaleria" data-slide-to="6"></li>
                <li data-target="#carouselGaleria" data-slide-to="7"></li>
                <li data-target="#carouselGaleria" data-slide-to="8"></li>
                <li data-target="#carouselGaleria" data-slide-to="9"></li>
                <li data-target="#carouselGaleria" data-slide-to="10"></li>
                <li data-target="#carouselGaleria" data-slide-to="11"></li>
                
            </ol>
            <div class="carousel-inner p-2 d-flex align-items-center">
                <div class="carousel-item active">
                    <img src="assets/img/galeria/01-galeria.jpeg" class="d-block w-100"  alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/02-galeria.jpeg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/03-galeria.jpeg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/04-galeria.jpeg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/05-galeria.jpeg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/06-galeria.jpeg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/07-galeria.jpeg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/08-galeria.jpeg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/09-galeria.jpeg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/10-galeria.jpeg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/11-galeria.jpeg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/12-galeria.jpeg" class="d-block w-100" alt="...">
                </div>
            </div>
            
            <a class="carousel-control-prev" href="#carouselGaleria" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselGaleria" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>




        <!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content rounded-0" style="background:url('./assets/img/bg.jpg')">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLongTitle">ATENCIÓN</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                        SE INFORMA AL PERSONAL DE EXASPIRANTES QUE SOLICITEN LA REINCORPORACIÓN AL INSTITUTO DEBERÁN COMUNICARSE CON LA DIVISIÓN INCORPORACIÓN, HASTA EL 30 DE NOVIEMBRE DE 2020 A FIN DE INICIAR EL TRÁMITE.
                        <br><br>
                        CONTACTOS: (<a href="mailto:esesc.ejercito@gmail.com">e-mail esesc.ejercito@gmail.com</a>) 
                        <br><br>
                        ASIMISMO, LA REINCORPORACIÓN SE EFECTUARÁ DE ACUERDO A LAS VACANTES DISPONIBLES; Y EN CASO DE SER REINCORPORADO DEBERÁ APROBAR EL EXAMEN MÉDICO, EL TEST PSICOLÓGICO Y LA COMPROBACIÓN FÍSICA.
                        </p>
                        <div class="text-center">
                            <a href="mailto:esesc.ejercito@gmail.com">
                                <input type="button" class="btn btn-primary" value="Enviar mensaje" >                    
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="modal fade" id="noticiaPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content bg-light rounded-1">
                    <div class="row align-items-end">
                        <div class="col shadow-left">
                            <div class="modal-header pb-1 mr-3">
                                <h4 class="modal-title" id="exampleModalLongTitle"><strong>¡Inscribite! y sé Suboficial del Ejército Argentino</strong></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body w-100 position-relative border" style="padding-bottom: 56.25%;">

                                <iframe class="w-100 h-100 position-absolute" style="top: 0;left: 0;" src="https://www.youtube.com/embed/DRviZ8zlF30" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                         
                            </div>
                        </div>
                    </div>


                                    
                </div>
            </div>
        </div>


        <!-- <img src="assets/img/galeria/10-galeria.jpg" 
        onmouseover="this.src='https://concepto.de/wp-content/uploads/2015/03/paisaje-e1549600034372.jpg'" alt=""
        onmouseout="this.src='assets/img/galeria/10-galeria.jpg'"
        /> -->

    </section>
    <script>
    doSomething();

    function getCookie(name) {
        var dc = document.cookie;
        var prefix = name + "=";
        var begin = dc.indexOf("; " + prefix);
        if (begin == -1) {
            begin = dc.indexOf(prefix);
            if (begin != 0) return null;
        }
        else
        {
            begin += 2;
            var end = document.cookie.indexOf(";", begin);
            if (end == -1) {
            end = dc.length;
            }
        }
        return decodeURI(dc.substring(begin + prefix.length, end));
    } 

    function doSomething() {
        var myCookie = getCookie("cartel_popup");

        if (myCookie == null) {
            // do cookie doesn't exist stuff;
            $('#exampleModal').modal('show');
            $('#noticiaPopup').modal('show');

            document.cookie = "cartel_popup=opened; max-age=3600; path=/";
            // alert("Se creó la cookie");
        }
    }
    </script>
