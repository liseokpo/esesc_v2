<section id="cuerpo" style="color:#444444">
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<?php
if(isset($_SESSION['contenido']) && $_SESSION['contenido'] != 0){
    $nombres = $_SESSION['contenido']['nombres'];
    $apellidos = $_SESSION['contenido']['apellidos'];
    $estado = $_SESSION['contenido']['estado'];
    $nro = $_SESSION['contenido']['nro_inscr'];
    $dni = $_SESSION['contenido']['dni'];
    $tipo = $_SESSION['contenido']['tipo'];
    contenido2($apellidos, $nombres, $estado, $nro, $dni,$tipo);
    session_destroy();
}
else if(isset($_SESSION['contenido']) && $_SESSION['contenido'] == 0){
    contenido3();
    session_destroy();
}
else{
     contenido1();
}

function contenido3(){

    echo'<br><br><div class="text-center">
            <h5>Usted <b>NO FIGURA</b> en la lista de aprobados.</h5>
            <a href="?p=listaaprobados" class="submit btn btn-primary pt-2">VOLVER A CONSULTAR</a>
        </div>';
}

function contenido2($apellidos, $nombres, $estado, $nroinsc, $dni, $tipo){
    echo'<h5>Nro Inscripto : <b>'.$nroinsc.'</b></h5>';
    echo'<h5>DNI : <b>'.$dni.'</b></h5>';
    echo'<h5>Nombre y Apellido : <b>'.$apellidos.' '.$nombres.'</b></h5>';
    echo'<h5>Estado : <b>'.$estado.'</b></h5>';
    mostrarMensaje($tipo,$estado);
    echo'<a href="?p=listaaprobados" class="submit btn btn-primary">VOLVER A CONSULTAR</a>';
}


function contenido1(){

    echo'<div class="text-center my-auto">
    <form action="paginas/metalista.php" method="post">
        <h5>A continuación, para averiguar si usted aprobó el exámen de admisión, ingrese su <b>DNI Completo</b>:<br>(sin puntos)</h5><br>
        
            <input type="number" class="col-md-4 p-2" id="dni" name="dni" placeholder="DNI Completo" required>        
            <div class="text-center pt-2">
                <div class="g-recaptcha" data-sitekey="6LeHvckUAAAAAJH76Onq-CfyiOGjWevzyd11fAtM"></div>
            </div>
        <input type="submit" class="btn btn-primary" value="VER MI ESTADO"/>
    </form>
    </div>';
}

function mostrarMensaje($tipo,$estado){
    if($tipo == "REINCORPORADO"){
        echo"<h5>Usted, en calidad de REINCORPORADO APTO deberá presentarse el día martes 18 de febrero del 2020 a las 0700 horas.<br>
Deberá comunicarse mediante mensajería Whatsapp al 11-6123-4798 a los efectos de solicitar los archivos de los formularios que deberá entregar el día de su presentación en el instituto.<br></h5>";
    }
    else if($tipo == "POSTULANTE" && $estado == "APTO"){
        echo"<h5>Usted, en calidad de POSTULANTE APTO deberá presentarse el día miércoles 26 de febrero del 2020 a las 0700 horas.<br>
Deberá comunicarse mediante mensajería Whatsapp al 11-6123-4798 a los efectos de solicitar los archivos de los formularios que deberá entregar el día de su presentación en el instituto.<br></h5>";
    }
    else if($tipo == "POSTULANTE" && $estado == "CONDICIONAL"){
        echo"<h5>Usted, en calidad de POSTULANTE APTO CONDICIONAL deberá presentarse el día martes 18 de febrero del 2020 a las 0700 horas con todos los estudios médicos que había presentado durante el examen de ingreso y los exámenes que adeuda.<br>
Deberá comunicarse mediante mensajería Whatsapp al 11-6123-4798 a los efectos de solicitar los archivos de los formularios que deberá entregar el día de su presentación en el instituto.<br></h5>";
    }
}
?>
    
</section>
