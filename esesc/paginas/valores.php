<section id="cuerpo">
    <h2 class="text-shadow text-center">Valores del Ejército Argentino</h2>
    <br>
    <h3><strong>¿Qué son los valores?</strong></h3>
    <p>Los valores militares son el marco de actuación, personal y profesional,
    que distinguen a todos los integrantes del Ejército Argentino, sin ningún tipo de
    distinción, son característicos de hombres y mujeres, de oficiales, suboficiales y
    soldados, de civiles y militares.</p><br>
    
    <h3><strong>Valores</strong></h3>
    <div class="container">
        <div class="row mx-auto">
            <div class="col  my-auto w-100">
                <ul>
                    <li><h5><a href="#valor">VALOR</a></h5></li>
                    <li><h5><a href="#abnegacion">ABNEGACIÓN</a></h5></li>
                    <li><h5><a href="#lealtad">LEALTAD</a></h5></li>
                    <li><h5><a href="#honor">HONOR</a></h5></li>
                    <li><h5><a href="#profesionalismo_militar">PROFESIONALISMO MILITAR</a></h5></li>
                    <li><h5><a href="#espiritu_de_cuerpo">ESPÍRITU DE CUERPO</a></h5></li>
                    <li><h5><a href="#disciplina">DISCIPLINA</a></h5></li>
                    <li><h5><a href="#integridad">INTEGRIDAD</a></h5></li>
                    <li><h5><a href="#patriotismo">PATRIOTISMO</a></h5></li>
                </ul>
            </div>
            <div class="col col-lg-5 w-100 d-none d-lg-inline d-md-inline pl-5 pb-5 pr-5 pt-1">
                <img src="assets/img/logoejercito.png" class="img-fluid mx-auto d-block" alt="...">
            </div>
            <div class="col d-lg-none d-md-none col-auto mw-50 w-75 mx-auto">
            <img src="assets/img/logoejercito.png" class="w-100 mx-auto d-block" alt="...">
            </div>
        </div>
    </div>
    <br>
    <hr>
    <br>
    <img id="valor" src="assets/img/valores/valor.jpg" class="w-100" style="height:350px;object-fit: cover;">

                <h4 ><strong>Valor</strong></h4>
                <p>
                Es la superación del miedo al peligro físico y moral. El valor militar permite
                el control propio aceptando las responsabilidades para actuar correctamente en
                situaciones (peligrosas defendiendo lo que es correcto)
                Se adquiere a partir de un proceso educativo
                basado en el hábito, el amor propio y, sobre todo, el honor
                y el apego a los grandes ideales de la Nación y la Institución.
                </p>
        </div>

        <br>
        <hr>
        <br>
        <img id="abnegacion" src="assets/img/valores/abnegacion.jpeg" class="w-100" style="height:300px;object-fit: cover;object-position:70% 30%">

        <h4 ><strong>Abnegación</strong></h4>
        <p>
        Es el renunciamiento a todo aquello que, tanto en lo material como en lo
        inmaterial, constituye un obstáculo para el logro del fin propuesto; siendo, en
        consecuencia, un acto deliberado, por elección profesional, de alejamiento de todo
        cuanto se oponga al
        logro de su auténtica realización.<br>
        La abnegación es producto de un proceso educativo que permite reconocer al fin
        perseguido como útil, necesario, conveniente y correcto para la Nación y la Institución,
        por lo que puede abarcar desde el desprendimiento de
        un bien material hasta el
        sacrificio de la propia vida.
        </p>
        <br>
        <hr>
        <br>
        <img id="lealtad" src="assets/img/valores/lealtad.jpg" class="w-100" style="height:300px;object-fit: cover;object-position:50% 50%">
    
        <h4><strong>Lealtad</strong></h4>
        <p>
        La Lealtad es el valor cuyo hábito inclina al bien y lleva a traducirlo en
        pensamientos, palabras y hechos. En función de ello, se manifiesta en un
        comportamiento de acuerdo con las leyes de la fidelidad, el honor y la hombría de bien,
        con honradez y sinceridad.<br><br>
        La lealtad a la Nación, al Ejército, a la Unidad y a los integrantes de la Institución,
        superiores, camaradas y subalternos, contribuye a generar confianza y respeto y
        acrear, en toda organización,
        un ambiente de correspondencia, reciprocidad e
        integridad.
        </p>
            
        <br>
        <hr>
        <br>
        <img id="honor" src="assets/img/valores/honor.jpg" class="w-100" style="height:300px;object-fit: cover;object-position:60% 40%">

        <h4 ><strong>Honor</strong></h4>
        <p>
        Es el sentimiento que impulsa al individuo a proceder de modo que
        merezca la estima de los hombres de bien y la aprobación de la propia conciencia. Se
        inspira en la lealtad que lleva a exteriorizar una conducta coherente con los valores que
        sustenta la Fuerza y muestra la valía individual que conducen al estricto cumplimiento
        del deber y a la excelencia profesional.<br>
        Actuar con honor significa comportarse con nobleza y ecuanimidad en toda
        circunstancia, dejando de lado intereses y dificultades y demostrando
        una actitud
        ejemplar, sobre la que se cimentará el prestigio personal.
        </p>
        <br>
        <hr>
        <br>
        <img id="profesionalismo_militar" src="assets/img/valores/prof_militar.jpeg" class="w-100" style="height:300px;object-fit: cover;object-position:60% 40%">

        <h4 ><strong>Profesionalismo Militar</strong></h4>
        <p>
        La profesión militar se destaca por transformar al individuo en un experto
        hombre de armas que se caracteriza por su dedicación absoluta, renunciamiento de la
        vida en caso de ser necesario y una permanente capacitación para que la Nación
        pueda afrontar el
        máximo conflicto social que es la guerra.<br>
        Es el resultado de una prolongada educación y experiencia que le posibilite al individuo
        poder cumplir acabadamente y dentro de un marco ético, con los distintos desafíos que
        se le planteen en el ejercicio de la profesión.
        </p>
        <br>
        <hr>
        <br>
        <div  id="espiritu_de_cuerpo">
            <img src="assets/img/valores/esp_de_cuerpo.jpeg" class=" d-sm-none" style="max-height:250px;width:100%;object-fit: cover;object-position:60% 40%">
            
            <div class="row mw-100">
                <div class="col">
                    <h4 ><strong>Espíritu de cuerpo</strong></h4>
                    <p>
                    Es el estado mental y emocional de la organización. Se trata de un
                    conjunto de actitudes, valores, intereses, ideales y tradiciones compartidos por los
                    integrantes de la misma y se manifiesta cuando la totalidad de los individuos que la
                    integran están identificados con ese conjunto y lo adoptan como si fuera propio, de
                    manera tal que sienten orgullo y satisfacción por los éxitos del grupo y desánimo por
                    los fracasos.
                    </p>
                </div>
                
                <div class="col d-none d-sm-block text-center">
                    <img src="assets/img/valores/esp_de_cuerpo.jpeg" class="w-auto" style="height:400px;object-position:60% 40%">
                
                </div>
            </div>
        </div>
        <br>
        <hr>
        <br>
        <img id="disciplina" src="assets/img/valores/disciplina.jpg" class="w-100" style="height:300px;object-fit: cover;object-position:60% 40%">

        <h4 ><strong>Disciplina</strong></h4>
        <p>
        Es el estado de obediencia que se manifiesta por la subordinación y el
        respeto en el cumplimiento de las órdenes en la adecuada conducta y en la estricta
        observancia de las leyes y reglamentos militares.<br>
        Su logro y mantenimiento resulta fundamental para lograr todas las acciones de
        manera efectiva.
        </p>
        <br>
        <hr>
        <br>
        <img id="integridad" src="assets/img/valores/integridad.jpg" class="w-100" style="height:300px;object-fit: cover;object-position:15% 85%">         

        <h4><strong>Integridad</strong></h4>
        <p>
        Es la cualidad del militar que lo lleva a obrar con rectitud de carácter y
        juicio de acuerdo a los principios morales identificados con la dignidad, veracidad y
        honestidad.<br><br>
        Fortalece la conducta por el convencimiento y la razón, llevando al más estricto
        cumplimiento de los deberes para con la Nación, la Institución, los superiores,
        camaradas, subordinados y al individuo mismo; siendo por lo tal indispensable en el
        desempeño de cargos de responsabilidad.
        </p>
        
        <br>
        <hr>
        <br>
        <img id="patriotismo" src="assets/img/valores/patriotismo.jpg" class="w-100" style="height:300px;object-fit: cover;object-position:60% 40%">         

        <h4><strong>Patriotismo</strong></h4>
        <p>
        El patriotismo es amor a la Patria. No es exclusivo del militar, pero para él
        su desarrollo es una condición ineludible para afrontar las exigencias que le impone la
        profesión.<br>
        Es el valor que estimula al hombre a obrar resuelta y permanentemente en procura del
        bien de la Patria y que le obliga a sacrificar propio interés por el bien común.
        </p>    
    


</section>