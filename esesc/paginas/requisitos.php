
    <section id="cuerpo" style="color:#444444">

    
        <h1 class="text-center text-shadow">Requisitos</h1><br>
        <div class=" p-3 w-100" style="background-color:rgb(255,248,177)">
            <div class="row">
                <div class="col">
                    <h3 id="reqbasicos" class="pb-2"><strong>Requisitos generales ingreso <?php echo date("Y") + 1; ?></strong></h3>
                    
                    <ul>
                        <li><strong>Límite de edad para el ingreso:</strong></li>
                        <ul>
                            <li>Hasta 23 años de edad para ambos planes</li>
                        </ul>
                        <li><strong>Mínimo de edad para el ingreso:</strong></li>
                        <ul>
                            <li>Desde 18 años de edad para ambos planes</li>
                        </ul>
                        <li><strong>Importante:</strong></li>
                        <ul>
                            <li>Al 01 de febrero del año que ingrese no deberá superar el límite de edad.</li>
                            <li>Al 01 de febrero del año que ingrese deberá cumplir con el mínimo de edad.</li>
                        </ul>
                    </ul>
                    <ul>
                        <li>Ser argentino (nativo o por opción) y de estado civil soltero (Ley para el Personal
                        Militar Nro 19.101, Artículo 17º de su Decreto Reglamentario Nro 1.081/7 y sus
                        actualizaciones).
                        </li>
                        <li>Estatura mínima permitida 1,55 metros(sin excepción).</li>

                        <li>Acreditar estudios de Nivel Secundario completos, con hasta un <b>máximo de 1 (una) materia adeudada</b> que deberá rendir antes del mes de agosto del año de ingreso.</li>

                        <li>Poseer un IMC (índice de masa corporal) inferior a TREINTA (30) y
                        superior a DIECIOCHO (18), de lo contrario se considerará factor de riesgo y por tal motivo no será
                        evaluado. Esta situación será considerada por el Consejo Académico para determinar su
                        futuro como postulante en el Instituto.
                        </li>
                        <li>Poseer las aptitudes psicofísicas necesarias para afrontar la carrera militar y aprobar
                        los exámenes de ingreso (intelectuales, físicos, psicológicos y médicos) exigidos por
                        el Instituto.</li>
                        <li class="mt-3 mb-3"><a href="./assets/docs/tatuajes_permitidos.pdf" class="bg-secondary p-2 text-white" target="_blank">VER TATUAJES PERMITIDOS</a></li>

                    </ul>    
                    <br>
                    <div class="alert alert-light" role="alert">
                    ¿No encontraste lo que buscabas? ¡Podés buscar en la <a href="/assets/docs/guia_ingreso.pdf">Guía de Ingreso</a> o mirar nuestras <a href="?p=preguntas_frecuentes">Preguntas Frecuentes</a>!
                    </div>
                </div>
                <div class="col-3">
                    <img src="./assets/img/util/requisitos_1.jpg" alt="" class="w-100" style="object-fit: cover;height:100%">
                </div>
            </div>
                
        </div>
        
        <div class="p-3 w-100">
            <h3 id="estudiosfines" class="pb-2"><strong>Requisitos particulares </strong><br>para los postulantes al Plan Especialidades y Servicios</h3>
            <ul>
                <li><b>Conductores Motoristas</b>: Original de la Licencia
                Nacional de Conducir <b>(Clase B1)</b>, emitida por la autoridad competente de cada
                jurisdicción provincial, municipal o de la CABA, según corresponda.</li>
                <li><b>Músicos</b>: prueba audio perceptiva y de actuación sobre instrumento musical.</li>
                <li>Las vacantes de las carreras de Conductor Motorista y Músico se completarán por
                orden de mérito.
                </li>
                <li>
                Si un Postulante no pudiere elegir un determinado plan que deseare cursar, se le
                informarán las vacantes de las Tecnicaturas de Especialidades/Servicio entre las
                cuales podrá elegir cursar. 
                </li>
                <li>
                Para el caso de aquellos que opten por <b>Mecánico de Aviación</b>, posteriormente a la
                elección deberán iniciar los trámites correspondientes y aprobar los exámenes
                psicofísicos del IMAE. De no aprobarlos, se le informarán las vacantes de las
                Tecnicaturas de Especialidades/Servicio entre las cuales podrá/n elegir cursar. El
                aspirante que inicie cursando   como Mecánico de Aviación en el curso de 2do Año
                y/o como reincorporado en el curso de 1er/2do Año y no apruebe los exámenes
                psicofísicos del IMAE (el anual correspondiente, <b>siendo una condición
                indispensable para continuar su regularidad en el Instituto), será considerado
                por el Consejo Académico para determinar su futuro en el Instituto.</b>
                </li>

            </ul>
        </div>
        <?php 

        /*
        <div class="p-3 w-100">
        
            <h3><strong>Documentación</strong></h3>
            <h4 class="pb-2">Conformar y presentar la documentación y estudios que se detallan a continuación:</h4>
                        
            <ul>
                    <li>
                                        
                    <button class="btn btn-link text-dark pl-0" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        <b>En oportunidad de formalizar la inscripción <i class="bi bi-caret-down"></i></b>
                    </button>
                    </li>
                    <div class="collapse" id="collapseExample">
                        <div class="card card-body rounded-0 border-0">
                            <ul>
                                <li>Documento Único para la Incorporación al Ejército (DUPIE).</li>
                                <li>Fotocopia del acta de nacimiento legalizada, autenticada por escribano
                                público, juez de paz o Registro Nacional de las Personas.</li>
                                <li>Si tuviere hijos, fotocopia legalizada de la partida de nacimiento y del DNI,
                                del o de los hijos.</li>
                                <li>Fotocopia del acta de defunción legalizada (si correspondiere) del padre y/o
                                de la madre, autenticada por escribano público o juez de paz.</li>
                                <li>DOS (02) fotocopias del DNI; en caso de encontrarse en trámite, fotocopia
                                del comprobante.</li>
                                <li>DOS (02) constancias de CUIT o CUIL.</li>
                                <li>Original del Certificado Registro Nacional de Reincidencia (“Antecedentes
                                Penales”).</li>
                                <li>UNA (01) fotografía color (fondo celeste) de cuerpo entero, de frente, de 10 x
                                15 cm.
                                DOS (02) fotografías color (fondo celeste) tipo carnet, de frente, de 4x4 cm.
                                Para los varones con saco y corbata, para mujeres con pollera o pantalón
                                oscuro, camisa o blusa clara. Todas deberán estar pegadas en el DUPIE.
                                Para el personal proveniente de Organismos y/o Institutos Militares, la foto
                                deberá ser de uniforme sin cubrecabezas.
                                </li>
                                <li>Si es Soldado Voluntario, deberá presentar un concepto sintético firmado por
                                el Jefe de Unidad.</li>
                            </ul>
                        </div>
                    </div>
                    
                    <li>
                    <button class="btn btn-link text-dark pl-0" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample2">
                        <b>En oportunidad de rendir los exámenes de admisión <i class="bi bi-caret-down"></i></b>
                    </button>
                    </li>
                    <div class="collapse" id="collapseExample2">
                        <div class="card card-body rounded-0 border-0">
                            <ul>
                                <li>Los exámenes médicos que deberán realizarse de forma particular por el
                                interesado serán presentados para su evaluación, por una Junta Médica
                                conformada a tal fin. Éstos no deberán exceder los SESENTA (60) días al
                                momento de su presentación. Para los Soldados Voluntarios no tendrá validez
                                la presentación del Anexo 27. Los exámenes médicos son:
                                </li>
                                <ul>
                                    <li>Electroencefalograma con informe.</li>
                                    <li>Bucodental con informe.</li>
                                    <li>Placa panorámica dental (Rx).</li>
                                    <li>Electrocardiograma con informe.</li>
                                    <li>Audiometría, con informe.</li>
                                    <li>Espirometría con informe.</li>
                                    <li>Papanicolau y Colposcopía (para el personal femenino).</li>
                                    <li>Radiología (Rx columna lumbosacra frente y perfil y Rx de tórax) con
                                    informe.</li>
                                    <li>Espinograma completo con inclusión de crestas ilíacas con informe (debe
                                    hacerse en posición de parado).
                                    </li>
                                    <li>Agudeza visual y visión cromática con informe.</li>
                                    <li>Laboratorio: Glucemia, Urea, Creatinina, Hemograma completo con
                                    eritrosedimentación, Colesterol (HDL y LDL).</li>
                                    <li>Hepatograma (GOT, GPT, FAL, Gama GT), Serología para VDRL, HIV
                                    (Decreto Ley 906/95), Toxoplasmosis y Hepatitis A, B y C, Orina
                                    completa, Grupo Sanguíneo y Factor Rh.</li>
                                    <li>Test de embarazo, el cual no debe exceder los quince días a la fecha de
                                    presentación al reconocimiento Médico.</li>
                                    <li>Test de detección precoz de consumo de drogas de abuso (Directiva
                                    JEMGE 906/16 Para la Prevención y Detección de Adicciones en el
                                    Personal, Apartado 7., b., 3).</li>
                                </ul>

                                <li>Libreta de vacunación (original y fotocopia), según Calendario de dosis
                                especificado por el Ministerio de Salud de la República Argentina, y
                                actualizada conforme al año calendario que corresponda.</li>
                                <li> Se considera NO APTO PARA INGRESO (NAPI), al ciudadano que no
                                reúna los requisitos detallados precedentemente, y a quienes padezcan las
                                afecciones que determina el Reglamento de Reconocimientos Médicos (RFP
                                23-02 edición 2001), y las rectificaciones ordenadas por MM COEDOC
                                (DEODOC – DPTO DOCT) Nro 9695/D/10. Entre ellas se mencionan:</li>
                                <ul>
                                    <li>Enfermedades de los órganos de los sentidos: conjuntivitis crónicas,
                                    cicatrices palpebrales, cataratas, estrabismo, trastornos en la visión de los
                                    colores, otitis crónicas, etc.
                                    </li>
                                    <li>Agudeza visual: 10/10, con o sin corrección, exceptuando hasta 7/10 con
                                    corrección de hasta TRES (3) dioptrías en cada ojo.
                                    </li>
                                    <li>Daltonismo puro (incapacidad para reconocer los colores vivos) y la
                                    discromatopsia en cualquier graduación, estrabismo y otras.</li>
                                    <li>Agudeza auditiva: pérdida mono o biaural mayor que un DIEZ (10) %.
                                    El trauma sonoro será causa de eliminación.</li>
                                    <li>Enfermedades infecciosas: las secuelas de reumatismo poliarticular
                                    agudo, poliomielitis, encefalitis, meningitis, brucelosis, sífilis, etc.</li>
                                    <li>Tumores: malignos y/o benignos que dificulten el funcionamiento de un
                                    miembro y/o un órgano.</li>
                                    <li>Enfermedades de la piel: crónicas, tejido celular subcutáneo y cicatrices
                                    que puedan provocar riesgos de infección y/o limitaciones funcionales
                                    para el cumplimiento de las exigencias propias de la instrucción militar. </li>
                                    <li>Enfermedades endócrinas de la nutrición, alérgicas e inmunitarias:
                                    diabetes, obesidad, asma bronquial, eczemas o urticaria a repetición.
                                    Agamaglobulinemia congénita o adquirida y de otras afecciones
                                    que ocasionen disminución de la inmunidad natural y/o adquirida.</li>
                                    <li>Enfermedades de la sangre y de órganos hematopoyéticos: anemias,
                                    leucemias o enfermedades hemorrágicas, así como los
                                    esplenectomizados. </li>
                                    <li>Enfermedades vertebrales: Escoliosis de cualquier etiología, cifosis o
                                    dorso curvo juvenil, secuelas de osteocondritis vertebral, osteomielitis
                                    vertebral y/o sus secuelas, fracturas vertebrales y/o sus secuelas,
                                    espondilolistesis y afecciones de los discos intervertebrales, operados o
                                    no. </li>
                                    <li>Trastornos mentales: personalidad psicopática, y/o trastornos de la
                                    conducta o neurosis. </li>
                                    <li>Enfermedades del aparato circulatorio: hipertensión arterial, cardiopatías
                                    congénitas o adquiridas, flebitis, várices, etc.</li>
                                    <li>Enfermedades del aparato respiratorio: sinusitis crónica, insuficiencia
                                    respiratoria, bronquitis crónicas, afecciones crónicas pleurales o pulmonares. Los que en el examen radiográfico presenten imágenes
                                    anormales.</li>
                                    <li>Enfermedades del aparato digestivo: labio leporino, dispepsias crónicas,
                                    neurosis digestivas, úlceras, afecciones vesiculares o hepáticas, etc. Las
                                    hernias o eventraciones posibles de reparaciones quirúrgicas, serán
                                    clasificadas como “APTO CONDICIONAL”.</li>
                                    <li> Enfermedades del aparato genitourinario: incontinencia urinaria, fístulas
                                    uretrales, orquitis crónicas, etc. </li>
                                    <li>Enfermedades del aparato locomotor: hernias o desgarros musculares,
                                    esguinces a repetición, luxaciones recidivantes, rupturas ligamentarias,
                                    pie plano, alteraciones estáticas de los miembros (genum valgum
                                    recurvatum), pies cavos, polidactilias, sindactilias, lumbalgias a
                                    repetición, etc. </li>
                                    <li>Anomalías congénitas: De cualquier órgano, la carencia parcial o total o
                                    la deformación congénita de un miembro. Quienes presenten
                                    desviaciones congénitas de la columna, costilla cervical, espina bífida
                                    oculta o manifiesta, acompañadas de enuresis, hemivertebras,
                                    sacralización u otro defecto en la segmentación lumbosacra (vértebra
                                    transicional), espondilólisis y cualquier otra deformación congénita de la
                                    columna vertebral o pelvis, o sus secuelas.
                                    </li>
                                    <li>Odontología: Se aceptarán prótesis removibles solamente de cromo
                                    cobalto o acrílico en los casos en que la cantidad o distribución de piezas
                                    faltantes, así lo requieran. Se considera NAPI quien presente la falta de
                                    más de CINCO (5) piezas dentarias, maloclusiones o malformaciones de
                                    los maxilares que afectaren la función masticatoria y paradentosis
                                    severas. Asimismo se considerará NAPI cuando posea carie/s no
                                    tratada/s.
                                    </li>
                                </ul>
                            </ul>
                        </div>
                    </div>

                    <li>
                        <button class="btn btn-link text-dark pl-0" type="button" data-toggle="collapse" data-target="#collapseExample3" aria-expanded="false" aria-controls="collapseExample3">
                            <b>En oportunidad de su incorporación al Instituto <i class="bi bi-caret-down"></i></b>
                        </button>                    
                    </li>
                    <div class="collapse" id="collapseExample3">
                        <div class="card card-body rounded-0 border-0">
                            <ul>
                                <li>Certificado de estudios (en las distintas variantes que se especifican más
                                adelante en el Apartado b.) legalizado por el Ministerio de Educación de la
                                provincia que corresponda o por el Gobierno de la Ciudad Autónoma de
                                Buenos Aires y del Ministerio del Interior.
                                En caso de adeudar materias, constancia autenticada de las autoridades del
                                mismo establecimiento escolar con detalle de las materias adeudadas por año,
                                hasta tanto sea expedido el certificado definitivo. Dicha constancia se
                                considerará como “documento provisorio”, y no reemplazará al certificado
                                correspondiente; en virtud de ello, el Postulante ingresará como Aspirante en
                                forma “CONDICIONAL”.</li>
                                <li>Aquellos estudios médicos no presentados al momento de rendir los
                                exámenes de ingreso y/o que se les haya ordenado realizar nuevamente.</li>
                            </ul>
                        </div>
                    </div>
            </ul>
        
        </div>  
        */
        ?>
        
    </section>
    <script>
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    </script>

