<?php
session_start();
if(isset($_POST["enviarformpre"]))
{
    include ('../assets/func/funciones.php');
    include ('../assets/func/class.DBCandidato.php');

    $pdo = conexion();

    // // //CREO UN CANDIDATO Y LO CARGO EN LA BASE DE DATOS
    $DBCandidato = new DBCandidato($pdo);
    if($DBCandidato -> dniExiste($_POST['dni']))
    {
        header ('Location: ../index.php?p=insc_postulante&m=errordni');    
    }
    else{
        if($DBCandidato -> emailExiste($_POST['email'])){
            header ('Location: ../index.php?p=insc_postulante&m=errormail');    
        }
        else{

            $DBCandidato -> cargarCandidato($_POST);
            $idcandidato = $DBCandidato -> obtenerIdCandidato($_POST["dni"]);
            $clave = generarClaveAleatoria();
            $clavehasheada = hashearPass($clave);
            $DBCandidato -> cargarClaveCandidato($idcandidato,$clavehasheada);
            $_SESSION["inscripto"] = $_POST["dni"];
            $_SESSION["clave"] = $clave;
            header ('Location: ../index.php?p=insc_exitosa');    
        }
    }
}
else{
    echo"<h2 class='text-center'>Esta página se encuentra restringida.</h2>";
}

?>
