
    <section id="cuerpo" style="color:#444444">
        <h2 class="text-center text-shadow">Importante</h2>
        <h3>Índice de masa corporal</h3>
        <p>
        Para preservar la salud del Postulante, le recomendamos que su IMC (Índice de Masa Corporal) no debe ser inferior a 18 y no debe superar los 30.
        <br/>Con 30 IMC no será aceptado su ingreso.
        </p>
        <h3>Estatura permitida masculino y femenino mínimo</h3>
        <h4 style="color:#FF4848;">1,55 metros (sin excepción)</h4>
        <br><h3 id="tatuajes">Tatuajes</h3>
        <p>
        No deben ser visibles en brazos por debajo del hombro, manos, codos, cara, cuello, piernas, pies; Tampoco se aceptan inscripciones del tipo VIOLENTAS, IMÁGENES OBSENAS, ARMAS o frases que INCITEN a la VIOLENCIA o a la DISCRIMINACIÓN de cualquier tipo.
        </p>
        <div class="text-center h-auto" >
            <img src="./assets/img/tatuajes.jpg" alt="" style="max-width:100%;">
        </div><br>

        <h3>Odontología</h3>
        <p>Está permitido utilizar brackets.</p>

        <h3>Oftalmología</h3>
        <p>No se permite rendir el examen OFTALMOLÓGICO con lentes de contacto. Si utiliza anteojos recetados, debe traerlos.<br>
        Debe saber que la agudeza visual mínima es 7/10 y se evalúa sin el uso de anteojos.</p>

        <h3>NO son aptos para el ingreso</h3>
        <p>
        CELIACOS – DIABETES – ASMA BRONQUIAL (CUALQUIERA DE SUS VARIANTES), HIPERTENSIÓN ARTERIAL (CUALQUIERA DE SUS FORMAS) Y CUALQUIER OTRAS ENFERMEDADES DEL TIPO CRÓNICA.
        </p>
        <h4>Soldados Voluntarios</h4><h3>MUY IMPORTANTE</h3>
        <p>
        El anexo 27 para los soldados voluntarios <b style="color:#FF4848;">“NO TIENE VALIDEZ”</b>. Tiene que presentar "TODOS" los estudios médicos.<br/>
        El tratamiento hacia los soldados voluntarios es de "SOLDADO VOLUNTARIO".<br/>
        Las fotografías deben ser de combate (UCA) sin cubre cabeza.<br/>
        Tiene que presentar el anexo 2 del dupie, completo al detalle con el concepto sintético del jefe de unidad, original y firmado.<br/>
        <b>No se olvide de aclarar bien dónde se encuentra destinado a fin de facilitar el llamado cuando sea convocado para su posterior incorporación.</b>
        </p>




        <h3 id="pago">DERECHO DE INSCRIPCIÓN - FORMAS DE PAGO</h3>
        <?php
        /**
        
        <div class="text-center">
            <br>   
            <img src="./assets/img/trabajando.png">
            <p><i>Nos encontramos trabajando en este contenido.</i></p>
        </div>
         */
        ?>
        
        
        <p>
            <ul class="pl-3">
                <li>Digital: Solo por la plataforma "“e-Recauda” Sistema de Recaudación de la Administración Pública".</li>
            </ul>
            Valor del costo de inscripción: $ 2000.- al momento de la inscripción.
            <?php//En caso de desear Alojamiento y Racionamiento, deberá abonarlo en el Centro de Preselección que
            //ha seleccionado para rendir el exámen //: (El valor del Alojamiento $1000.- / Racionamiento: $ 1800.-)?>.
        </p>
        <h4 class="text-center" style="color:#8989ef">ÚNICO MODO DE PAGO DIGITAL</h4>
        <h4 class="text-center" style="color:#FF4848">NO ENVIAR DINERO DENTRO DEL SOBRE  CON LA DOCUMENTACIÓN.       
        </h4>
        <h2>INSCRIPCIÓN: $ 2000.-<br/>
        <?php //ALOJAMIENTO Y RACIONAMIENTO: $ 2800.-?></h2>
        <h3>MUY IMPORTANTE</h3>
        <p>
        Cuando complete el DUPIE, si usted decide rendir en alguno de los centros de preselección, deberá aclarar en caso de requerir alojamiento. Esta información servirá para determinar la posibilidad de brindar este servicio y establecer el monto, así también como la oportunidad de pago del mismo.<br/>
        </p>
        <h3>LEA ATENTAMENTE</h3>
         <p>Al momento de enviar la documentación que se ordene <b>deberán adjuntar el comprobante original</b> del pago efectuado mediante “e-Recauda” (<a href="https://erecauda.mecon.gov.ar/erecauda/" target="_blank" rel="noopener noreferrer">https://erecauda.mecon.gov.ar/erecauda/</a>) mas DOS (2) copias, quedando con ud otra copia más del mismo.<?php //El <b>PAGO EN EFECTIVO</b> sólo se realiza en el área finanzas del instituto. ?><p>

        <p><b style="color:#FF4848;">NO HAY REINTEGRO</b> EN CONCEPTO DE INSCRIPCIÓN<?php// COMO TAMPOCO POR RACIONAMIENTO / ALOJAMIENTO?>.</p>

        <p>La documentación del postulante en el caso que no sea seleccionado para su incorporación, será incinerada. No será devuelto el DUPIE o documento ni estudios médicos.</p>
        
        <h3>PROCEDIMIENTO PARA LA GENERACIÓN DE BOLETA DE PAGO</h3>
        <br>
        <h6>E-recauda link:<a href="https://erecauda.mecon.gov.ar/erecauda/" target="_blank" rel="noopener noreferrer">https://erecauda.mecon.gov.ar/erecauda/</a></h6>
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/HpSWrMJUk58?rel=0" allowfullscreen></iframe>
        </div>
        
        
        </section>