
    <section id="cuerpo" style="color:#444444">
        <h2 class="text-center text-shadow">Documentación</h2>


        <h5>Preinscripción 2020</h5>

        <p>A partir del día 10 de julio del año 2020 finalizará la PREINSCRIPCIÓN ONLINE en nuestro sitio web y se dará inicio a la INSCRIPCIÓN para los postulantes que ya se encuentren preinscriptos, es decir, aquellos aprobados y considerados en observación por la División Incorporación.</p>

        <h5>Inscripción 2020</h5>

        <p>Disponible a partir del 10 de julio hasta el 1 de noviembre del 2020.</p>

        <h5>Deberá armar una (1) carpeta de cualquier color de tapa transparente o folio tamaño a4.</h5>
        <h5>LOS DOCUMENTOS QUE DEBE AGREGAR SON: </h5>
            <ul>
                <li>UNO (1) Documento Único para la Incorporación al Ejército (DUPIE).</li>
                <li>UNA (1) fotocopia del Acta de Nacimiento legalizada, autenticadas por escribano público o juez de paz.</li>
                <li>Si tuviere hijos, UNO (1) fotocopia legalizada de la Partida de Nacimiento y DNI.</li>
                <li>TRES (3) fotocopias del DNI. Si lo tiene tramitando, fotocopia del comprobante.</li>
                <li>TRES (3) constancias de CUIT o CUIL.</li>
                <li>Constancia de Estudios si cursas el Ciclo Secundario o Certificado de Finalización de Estudios ((DOS) 2 Fotocopias autenticadas por el Ministerio de Educación).</li>
                <li>UNA (1) fotografía color (fondo celeste) de cuerpo entero, de frente, de 10 x 15 cm pegada en la última hoja del formulario DUPIE, varones con saco y corbata; mujeres con pollera o pantalón oscuro y camisa o blusa clara. </li>
                <li>UNA (1) fotografía color (fondo celeste) tipo carnet, de frente, de 4x4 cm. Deberá ir pegada en la primera hoja del DUPIE. <br><b>TENGA EN CUENTA QUE AL MOMENTO QUE LE SEA ENTREGADA SU CREDENCIAL, DEBERÁ COLOCAR OTRA FOTO 4X4 EN ESA CREDENCIAL.</b></li>
                <li>CERTIFICADO DE ANTECEDETES PENALES NACIONAL(RNR).</li>
                <li>DOS (2) copias del COMPROBANTE DE PAGO de la INSCRIPCIÓN más el COMPROBANTE ORIGINAL.(<a href="index.php?p=importante#pago">Saber más</a>)<br> <b>Recuerde que usted mismo debe quedarse con otra copia.</b> </li>
                <li style="color:#FF4848;">Fotocopia color de la Libreta Nacional de Conducir(solo los que eligen “Conductor Motorista”).</li>
            </ul>
        </i>

        <h5>CUANDO TENGA LA DOCUMENTACIÓN COMPLETA, DEBERÁ ENVIARLA EN UN SOBRE POR CORREO POSTAL A LA SIGUIENTE DIRECCIÓN:</h5>
        <i>
            <div class="p-4"style="background-color:rgb(0, 101, 46,.1)">
            DIRECTOR ESCUELA DE SUBOFICIALES DEL EJERCITO “SARGENTO CABRAL”<br/>
                DIVISIÓN INCORPORACIÓN<br/>
                RUTA 202 S/NRO<br/>
                CP: 1659<br/>
                CAMPO DE MAYO -  PROVINCIA DE BUENOS AIRES
            </div>
        </i>
        <h5>O BIEN, PUEDE ENTREGARLA PERSONALMENTE EN EL INSTITUTO:</h5>
        <p>De lunes a viernes 0800 a 1800.</p>
        <i>
        <div class="p-4" style="background-color:rgb(0, 101, 46,.1)">
        <img class="mr-1" style="width:20px" src="assets/img/pie/dir.png" alt="">RP 202 S/N CP:1659, Campo de Mayo,
        Buenos Aires, Argentina 
        </div>
        </i>
        <h1 class="text-center text-shadow">DESCARGAS</h1>
        <table style="text-align:center;margin:auto" id="dupie">
            <tr>
            <td>DUPIE</td><td>INSTRUCTIVO<br/>DUPIE</td>
            </tr>
            <tr>
                <td><a class="descargapdf" download href="assets/docs/dupie.pdf"></a></td>
                <td><a class="descargapdf" download href="assets/docs/instructivo_dupie.pdf"></a></td>
            </tr>
            
        </table>
            
            <h3 class="text-center mt-3" id="actacompromiso">ACTA DE COMPROMISO</h3>
            <div class="text-justify mt-1">Aquellos postulantes quienes adeuden documentación deberán descargar el acta de compromiso. Una vez descargada y adjunta a los documentos previamente solicitados usted quedará como INSCRIPTO CONDICIONAL y estará comprometido a la entrega de la documentación faltante.</div>
            
            <div class="text-center"><a class="descargapdf" download href="assets/docs/acta_compromiso.pdf"></a></div>
    </section>
