    <section id="cuerpo" style="color:#444444">
        <h1 class="text-center text-shadow">Misión</h1>
        
        <p class="ml-5 mr-5">
       La Escuela de Suboficiales del Ejército "SARGENTO CABRAL" reclutará, formará y perfeccionará, anualmente, a los futuros suboficiales del cuerpo comando, de las especialidades y servicios que integran el cuadro permanente de la fuerza, para satisfacer los objetivos impuestos, a fin
       de contribuir al cumplimiento de la misión del Ejército.
        </p>
        <br>
        <div class="row mw-100 p-0 m-0">
        <div class="col col-auto pb-2"><img class="mw-100 w-100" src="assets/img/mision1.jpg" alt="" style="height:350px;object-fit: cover;object-position:70% 30%"></div>
        </div>
        <h1 class="text-center text-shadow">Visión</h1>
        <p class=" ml-5 mr-5">
        Posicionar al instituto como la mejor alternativa para la formación de ciudadanos como líderes militares en el escalafón de suboficiales, siendo reconocida por su excelencia académica y compromiso con la formación integral de las personas.
        </p>
        <br>
        <div class="row mw-100 p-0 m-0">
            <div class="col col-auto pb-2"><img class="mw-100 w-100" src="assets/img/mision2.jpg" alt="" style="height:350px;object-fit: cover;object-position:50% 50%"></div>
        </div>
    </section>