<section style="background-color:#fcfcf7">
    <section id="cuerponoticia">
    
    <h3 class="titulo fullancho text-center">REAPERTURA DE PREINSCRIPCIÓN ONLINE 2020</h3>

    <p class="mt-3">
    A partir del <b>2 de octubre</b> y <b>hasta el 30 de octubre</b> la PREINSCRIPCIÓN ONLINE OBLIGATORIA se encontrará disponible nuevamente para todos aquellos quienes aún no se preinscribieron, para el ingreso en el año 2021. <br><br> ¡Esta es su oportunidad de cumplir su sueño de ser futuro suboficial del Ejército Argentino!
    <br>
    <br>
    Comparta esta publicación con sus familiares y amigos para que alcance a todos aquellos quienes se encuentren interesados en formar parte de la #FamiliaESESC.
    </p>

    </section>
</section>