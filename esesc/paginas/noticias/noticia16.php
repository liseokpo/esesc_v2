<section style="background-color:#fcfcf7">

    <section id="cuerponoticia">
    
    <h3 class="titulo fullancho text-center">#QuedateEnCasa</h3>

    <h5 class="mt-3">
    <h2 class="text-center">Quédate en casa, nosotros te cuidamos.</h2>
    <div class="container" style="padding:10px;background-color:rgb(255,255,255,0.1)">
        <div class="row">
            <div class="col-lg-4 text-center">
                <img src="assets/img/noticias/quedateencasa1.jpeg" class="img-fluid" style="max-height:400px;height:auto;width:auto;">
            </div>
            <div class="col-lg-8 my-auto">
            <p>Debido a la emergencia sanitaria que se vive actualmente en el país, se impartió la orden de paralizar
                todas las líneas de producción habitual en la Sastrería Militar para contribuir con la prevención y 
                cuidado de la salud de la comunidad.</p><p> Con tal motivo, aspirantes Art 11 del instituto se encuentran 
                confeccionando barbijos para la comunidad, sábanas para los centros quirúrgicos y camisolines descartables 
                para nuestros enfermeros y médicos de todo el país.    

                </p>
            
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 my-auto">
                <img src="assets/img/noticias/quedateencasa2.jpeg" class="img-fluid" style="max-height:400px;height:auto;width:auto;">
            </div>
            <div class="col-lg-6 text-center mt-2">
            
            <img src="assets/img/noticias/quedateencasa3.jpeg" class="img-fluid" style="max-height:400px;height:auto;width:auto;">

            </div>
        </div>
    </div>
    
    
    </section>
</section>