<section style="background-color:#fcfcf7">

    <img src="assets/img/noticias/transmision_vivo2_foto.jpeg" class="w-100" style="height:350px;object-fit: cover;object-position:75% 25%">

    <section id="cuerponoticia">

    <h2 class="text-center">¡ATENCIÓN POSTULANTES!</h2>
    <h3 class="text-center">Se acerca una nueva transmisión en vivo</h3>
    <p>
    <br>
    <b>El viernes 11 de septiembre a las 11hs</b> se estarán respondiendo en una nueva transmisión en vivo todas sus inquietudes y/o consultas acerca de la inscripción que quedaron pendientes. Recuerde que usted puede dejarnos su pregunta tanto en los comentarios como a través de mensaje directo, ¡las estaremos esperando!
    <br><br>
    <b>No se quede con la duda, esta es su oportunidad. </b>
    <br><br>
    ¿Se perdió la primera transmisión en vivo? Haga <a href="https://youtu.be/6ZUeDOiDYxs">click aquí</a>
    <br>
    </div>

    </section>
</section>