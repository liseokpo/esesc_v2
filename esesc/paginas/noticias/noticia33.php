<section style="background-color:#fcfcf7">
  <img src="assets/img/noticias/diaartilleria1.jpeg" class="w-100" style="height:400px;object-fit: cover;object-position:60% 40%">

  <section id="cuerponoticia">

  <h2 class="text-center">4 de diciembre - Día del Arma de Artillería</h2>
  

  En el día de la fecha, conmemoramos un nuevo aniversario del Arma de Artillería. Con tal motivo, saludamos a todos los integrantes del Arma, en actividad y retirados, que a lo largo de los años contribuyeron al Ejército Argentino. En su día, que su patrona, Santa Bárbara, los proteja y guíe en la ejecución de sus tareas. <br>
  <br>
  “Por la Patria y al pie del cañón”<br>
  <br>
  (Fotografías de archivo) <br>
  <br>
  #ActitudESESC <br>
  #SomosElEjercito <br>



  <br>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
          <img class="d-block w-100" src="assets/img/noticias/diaartilleria3.jpg" style="height:380px;object-fit: cover;object-position:50% 50%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/diaartilleria2.jpg" style="height:380px;object-fit: cover;object-position:50% 50%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/diaartilleria1.jpeg" style="height:380px;object-fit: cover;object-position:60% 40%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/diaartilleria4.jpg" style="height:380px;object-fit: cover;object-position:60% 40%">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>


  </div>

  </section>
</section>