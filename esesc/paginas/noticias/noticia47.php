<section style="background-color:#fcfcf7">
    <section id="cuerponoticia">
    <br>
        <h2 class="text-center">Cátedra de valores profesionales del soldado argentino</h2> <br>
        <div class="p-2">
        <p>
        Con los valores que nos sustentan y fueron legados por nuestros héroes, se cerró la cátedra de valores de Malvinas.<br><br>

        La vigilia se desarrolló en procesión desde el  monumento al héroe nacional, post mortem SI Antonio Cisnero, hasta su nuevo emplazamiento en la plaza de armas del instituto en custodia de toda nuestra comunidad educativa.<br><br>
        En la  procesión se recorrió  diferentes   puntos de estación de aquellos valores destacados en suboficiales héroes de Malvinas  de los últimos combates. Luego, pasada la medianoche del 9 al 10 de junio, hora de su muerte,  finalizó con un toque de silencio en el descubrimiento del busto, en honor a Cisnero.<br><br>

        Actualmente, el busto del SI Cisnero se encuentra ubicado entre las compañías de infantería, cuna que lo vio nacer en el año 1956, y desarrollarse como suboficial hasta convertirse en un héroe nacional. <br><br>

        "Señor ayudame a vivir y si fuera necesario a morir como un soldado"<br><br>
        </p>
        <div class="text-center">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/UGJ9s0D3V8c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        
        </div>
    </section>
</section>