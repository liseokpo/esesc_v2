<section style="background-color:#fcfcf7">

    <section id="cuerponoticia">
        <h2 class="titulo fullancho text-center m-0">Ingreso Aspirantes 1er Año</h2>
        <img src="assets/img/noticias/ingasp1anio.jpg" class="w-100" style="height:300px;object-fit: cover;">
            <div class="row">
                <div class="col my-auto">
                <h4>
                    Luego de tanto esfuerzo y sacrificio, los nuevos aspirantes de Ier año ya se encuentran instalados en el instituto y comenzando con las primeras instrucciones básicas para convertirse en futura columna vertebral y brazo ejecutor del Ejército Argentino.<br>
                </h4><h4>
                    Agradecemos el acompañamiento de los padres y seres queridos tanto en las redes sociales como en la ceremonia de ingreso, quienes son el pilar que nos sustenta y nos hace crecer como familia ESESC.
                </h4>
                </div>
                <video controls width="100%">
                    <source src="assets/img/noticias/ingreso4.mp4" type="video/mp4">
                </video>
            </div>
        </div>
    </section>
</section>
    