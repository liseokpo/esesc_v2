<section style="background-color:#fcfcf7">
    <section id="cuerponoticia">
        <h2 class="titulo fullancho text-center m-0">Espíritu Solidario</h2>
        <img src="assets/img/noticias/panameños.jpg" class="w-100" style="height:250px;object-fit: cover;">
        <div style="padding:10px;background-color:rgb(255,255,255,0.1)">
        
        <p>
        La camaradería es fundamental para la formación militar del aspirante, gracias a ella en la Escuela de Suboficiales del Ejército “Sargento Cabral” se forman lazos de hermandad para toda la vida, como es el caso de los aspirantes panameños, quienes regresarán a su hogar con una especialidad, conocimientos y costumbres nuevas para seguir desarrollando en su país.<br>
        <div class="text-center">
            <img src="assets/img/noticias/panameños1.jpg" alt="" style="max-height:450px;max-width:100%">
        </div>
        Con el fin de instruirse en la especialidad mecánico en aviación y dar un paso más en su carrera militar, miembros pertenecientes al Servicio Nacional Aeronaval de Panamá, integran el cuerpo de aspirantes del instituto, e incluso cuatro de ellos egresaron durante el mes de noviembre.
        <br><br> Recordando sus días de incorporación a la ESESC, aseguran que se trató de una adaptación fácil debido a los respetados valores del instituto y de los efectivos que lo componen.
        </p>
        </div>
    </section>
</section>