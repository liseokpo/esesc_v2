<section style="background-color:#fcfcf7">

    <section id="cuerponoticia">

        <h2 class="text-center">Inscripción 2021</h2>
        <br>
        Estimados/as jóvenes postulantes:
        <br><br>
        El éxito es una condición deseable para todos los que buscan algo en la vida, para todos aquellos que se han trazado una meta. He aquí entonces una invitación para alcanzar un proyecto de vida que puede brindar las mayores y mejores satisfacciones.
        <br><br>
        En tal afán, nuestra Escuela de Suboficiales del Ejército "Sargento Cabral" brinda todas las oportunidades para lograr el éxito, enmarcados en cumplimiento del deber sagrado de servir a la Patria.
        <br><br>
        Deseamos que nuestras expectativas y las de ustedes se cumplan, para ello nos comprometemos a trabajar fuerte, descartando que ustedes lo harán de igual manera. 
        <br><br>
        Como parte esencial de la cultura organizacional de nuestra Institución, columna vertebral y brazo ejecutor del Ejército Argentino inculcamos en cada uno de nuestros integrantes los valores, pilares que guiarán la actuación personal y profesional de cada hombre y mujer que la integra. Estos valores son:
        <ul>
            <li>El Valor</li>
            <li>La Abnegación</li>
            <li>La Lealtad</li>
            <li>El Honor</li>
            <li>El Profesionalismo Militar</li>
            <li>El Espíritu De Cuerpo</li>
            <li>La Disciplina</li>
            <li>La Integridad</li>
            <li>El Patriotismo</li>
        </ul>

        Con sincero deseo, ¡Los estamos esperando!
        <br><br>
        CR Gustavo Adrián Sivori<br>
        Director de la Escuela de Suboficiales del Ejército "Sargento Cabral"
    </section>
</section>



