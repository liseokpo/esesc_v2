<section style="background-color:#fcfcf7">

  <img src="assets/img/noticias/salida_terreno1.jpg" class="w-100" style="height:400px;object-fit: cover;object-position:60% 40%">

  <section id="cuerponoticia">

  <h2 class="text-center">Salida al terreno</h2>
  
  <p>
  Con #ActitudESESC los aspirantes enfrentan las exigencias que impone la salida a los campos de instrucción con el firme objetivo de poner en práctica todos los conocimientos aprendidos que representan al suboficial del Ejército Argentino.
  <br><br>
  #ESESC <br>
  #EsteEsTuEjercito<br>
  #EjercitoArgentino<br>
  </p>


  <br>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
          <img class="d-block w-100" src="assets/img/noticias/salida_terreno2.jpg" style="height:380px;object-fit: cover;object-position:50% 50%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/salida_terreno3.jpg" style="height:380px;object-fit: cover;object-position:50% 50%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/salida_terreno1.jpg" style="height:380px;object-fit: cover;object-position:60% 40%">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>


  </div>

  </section>
</section>