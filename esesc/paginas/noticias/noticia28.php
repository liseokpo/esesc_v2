<section style="background-color:#fcfcf7">

  <img src="assets/img/noticias/visita_jemge_1.jpeg" class="w-100" style="height:400px;object-fit: cover;object-position:30% 70%">

  <section id="cuerponoticia">

  <h2 class="text-center">Visita del Sr jefe del Estado Mayor General del Ejército</h2>
  En la mañana del 31 de agosto, el Sr jefe del Estado Mayor General de Ejército, GB Agustín Humberto Cejas, visitó la ESESC, acompañado por el Sr Director General de Educación, GB Gustavo Tejeda.
  <br><br>
  Durante la visita se llevó a cabo una  formación en la plaza de armas y posteriormente una recorrida por las instalaciones.
  <br><br>

  <br>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
          <img class="d-block w-100" src="assets/img/noticias/visita_jemge_3.jpeg" style="height:350px;object-fit: cover;object-position:60% 40%">

      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/visita_jemge_1.jpeg" style="height:350px;object-fit: cover;object-position:20% 80%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/visita_jemge_2.jpeg" style="height:350px;object-fit: cover;object-position:50% 50%">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>


  </div>

  </section>
</section>
