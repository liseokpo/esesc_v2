<section style="background-color:#fcfcf7">

  <img src="assets/img/noticias/9julio_.jpg" class="w-100" style="height:400px;object-fit: cover;object-position:50% 50%">

  <section id="cuerponoticia">

  <h2 class="text-center">9 de julio - Día de la Independencia Argentina</h2>
  En el día de la fecha, se conmemora aquella jornada en la que un grupo de representantes de las Provincias Unidas debatieron y pactaron en una declaración su intención de lograr una nación nueva y libre, lejos del dominio colonial español. Así fue como hace 204 años, la Declaración de la Independencia surgía dando lugar a uno de los hechos históricos de mayor relevancia para los argentinos que, aún, se mantiene fresco en nuestras memorias.
  <br><br>
  Por eso, con motivo de celebración y festejo, compartimos con ustedes algunas imágenes del desfile cívico militar transcurrido el día 9 de julio de 2019.
  <br><br>



  <blockquote class="blockquote">
    <p class="mb-0">"La vida es nada si la libertad se pierde"</p>
    <footer class="blockquote-footer"><cite title="Source Title">General Manuel Belgrano</cite></footer>
  </blockquote>
  <br>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
          <img class="d-block w-100" src="assets/img/noticias/9julio1.jpg" style="height:350px;object-fit: cover;object-position:60% 40%">

      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/9julio2.jpg" style="height:350px;object-fit: cover;object-position:90% 10%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/9julio3.jpg" style="height:350px;object-fit: cover;object-position:50% 50%">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>


  </div>

  </section>
</section>