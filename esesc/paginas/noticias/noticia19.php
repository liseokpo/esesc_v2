<section style="background-color:#fcfcf7">

  <section id="cuerponoticia">
  <h2 class="text-center">La educación no presencial, en línea</h2>
  <p>
  En virtud de la emergencia sanitaria COVID-19 y las medidas decretadas por el Gobierno nacional, el instituto implementó la modalidad de educación no presencial para continuar con la formación de nuestros  futuros suboficiales, columna vertebral, líderes y brazo ejecutor del Ejército Argentino.<br><br>
  Durante la vigencia del aislamiento social, preventivo y obligatorio los profesores, instructores y aspirantes mantienen contacto mediante las clases virtuales y desde la seguridad de sus hogares.<br><br>

  </p>


  <div class="row">
  <div class="col text-center">
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="./assets/img/noticias/edunopres3.jpg" class="d-block w-100">
      </div>
      <div class="carousel-item">
        <img src="./assets/img/noticias/edunopres5.jpg" class="d-block w-100">
      </div>
      <div class="carousel-item">
        <img src="./assets/img/noticias/edunopres7.jpg" class="d-block w-100">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  </div>
  </div>
  <br>
  <p>
  Ante la emergencia sanitaria, la División Sanidad y Veterinaria, conceden al aspirante la posibilidad de estudiar utilizando los entornos de educación en línea.<br><br>

  De una forma particular los aspirantes de Ier año de sanidad, en la cátedra enfermería de urgencias, a cargo de la profesora González Viviana, investigan y se enriquecen bibliográficamente en grupo, para lograr realizar una demostración práctica de la intervención de enfermería en situaciones de accidente; utilizando diferentes recursos o herramientas para la valorización del sujeto, la escena, la aplicación de triage y las maniobras de RCP básico.<br>
  <br><i>Agradecemos el esfuerzo, responsabilidad y dedicación por parte de la comunidad educativa, los aspirantes y sus familias, quienes con responsabilidad dan continuidad al proceso de  enseñanza y aprendizaje, para convertirse en futuros cabos.</i>
  <br><br>#EducacionNoPresencialESESC <br>
  #LaESESCenCasa<br>
  #QuieroSerAspirante<br>
  </p>

  <div class="row mt-2">

  <div class="col"><img class="img-fluid" src="./assets/img/noticias/edunopres4.jpg"></div>
  <div class="col"><img class="img-fluid" src="./assets/img/noticias/edunopres2.jpg"></div>

  </div>
  </section> 
</section> 
