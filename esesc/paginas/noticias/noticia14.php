<section style="background-color:#fcfcf7">
    <section id="cuerponoticia">
        <h2 class="titulo fullancho text-center m-0">¡Ésta es su oportunidad!</h2>
        <div class="container" style="padding:10px;background-color:rgb(255,255,255,0.1)">
            <div class="row">
                <div class="col-lg-4 text-center">
                    <img src="assets/img/noticias/atencion_postulantes.jpg" class="img-fluid" style="max-height:400px;height:auto;width:auto;">
                </div>
                <div class="col-lg-8 my-auto">
                <h5>Egresados de secundarios técnicos no deberán rendir el examen intelectual de ingreso al instituto.</h5>
                <br>
                <p>
                    Recordamos que durante la inscripción 2020 el postulante egresado de un instituto 
                    secundario técnico quedará exento de rendir el examen intelectual de ingreso al instituto
                </p>
                
                </div>
            </div>
        </div>
    </section>
</section>