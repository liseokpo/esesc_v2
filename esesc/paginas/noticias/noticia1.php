<section style="background-color:#fcfcf7">

    <section id="cuerponoticia">

        <h1 class="titulo fullancho text-left m-0">NOTICIA</h1>
        <img src="assets/img/noticias/cabral.jpg" class="w-100" style="height:100px;object-fit: cover;">
        <div style="padding:10px;background-color:rgb(255,255,255,0.1)">
            <h2>DE INTERÉS PARA LOS QUE RINDEN EN LA <b>ESESC</b> Y YA RINDIERON EL PSICOLÓGICO</h2>
            <p>
            Se informa que hay un segundo llamado, <b>el día miércoles 13, jueves 14 y viernes 15 de noviembre</b>, solo para aquel personal que ya rindió los exámenes psicológicos
            
            en la <b>Escuela de Suboficiales "Sargento Cabral"</b> los días viernes 04, 11, 18 y 25 de octubre. Los cuales 
            rendirán el resto de los exámenes sin posibilidad de alojamiento ni racionamiento. <b>Para esa actividad se presentan a las 0700 y se retiran 1800 durante los tres días.</b>
            <br/><br/>Se recomienda traer colación para evitar gastos.
            </p>
        </div>
        
    </section>
</section>
