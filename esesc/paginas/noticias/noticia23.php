<section style="background-color:#fcfcf7">

  <img src="assets/img/noticias/apoyocomunitario5.jpg" class="w-100" style="height:350px;object-fit: cover;object-position:60% 40%">

  <section id="cuerponoticia">

  <h2 class="text-center">La ESESC ¡Presente!</h2>
  En el marco de la Operación General Belgrano, integrantes de la Agrupación Apoyo del Instituto, junto al Batallón Servicio 2 de Abril, realizaron tareas de apoyo logístico, entre ellas la descarga de colchones y camas, en el Servicio Logístico de la Defensa, Apostadero Naval Buenos Aires.
  <br><br>
  ¡Al virus lo frenamos entre todos!
  <br><br>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
          <img class="d-block w-100" src="assets/img/noticias/apoyocomunitario4.jpg" style="height:350px;object-fit: cover;object-position:60% 40%">

      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/apoyocomunitario2.jpg" style="height:350px;object-fit: cover;object-position:90% 10%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/apoyocomunitario1.jpg" style="height:350px;object-fit: cover;object-position:50% 50%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/apoyocomunitario3.jpg" style="height:350px;object-fit: cover;object-position:80% 20%">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>


  </div>

  </section>
</section>