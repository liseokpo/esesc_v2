<section style="background-color:#fcfcf7">

    <section id="cuerponoticia">

        <h2 class="text-center">18 de octubre - Día de la Madre</h2>
        En su día, enviamos un afectuoso saludos y un feliz día a todas las madres oficiales y suboficiales, a las madres soldados y civiles, a todas aquellas quienes a la distancia apoyan y acompañan a transitar la vida militar del personal de la Fuerza. A todas ellas, ¡Feliz día!<br><br>

        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/dyIfvIQkSsY?rel=0" allowfullscreen></iframe>
        </div>
    </section>
</section>