<section style="background-color:#fcfcf7">

    <section id="cuerponoticia">

        <h1 class="titulo fullancho text-left m-0">EGRESO DE CABOS 2019</h1>
        <div style="padding:10px;background-color:rgb(255,255,255,0.1)">
            
            <h5>
            Se ha cerrado un nuevo ciclo de objetivos y metas cumplidas en el instituto. Hoy, nuestros recientes cabos comienzan a construir un nuevo camino como profesionales del Ejército Argentino junto con la persistencia, la dedicación y los conocimientos aprendidos durante su formación.</h4>
            <h4 class="text-center">¡Felicidades por los logros alcanzados! ¡A seguir por más!</h4>
            <video controls width="100%">
                <source src="assets/img/noticias/egreso_video.mp4" type="video/mp4">
            </video>
        </div>

    </section>
</section>