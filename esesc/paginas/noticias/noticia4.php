<section style="background-color:#fcfcf7">

    <section id="cuerponoticia">
        <h1 class="titulo fullancho text-left m-0">CIERRE DE UN CICLO</h1>
        <img src="assets/img/noticias/aspirantes.png" class="w-100" style="height:200px;object-fit: cover;">
        <div style="padding:10px;background-color:rgb(255,255,255,0.1)">
        <h5>
            Nos encontramos a pocos días de un nuevo comienzo de actividades en el instituto. Un año de nuevos
            conocimientos, vivencias y exigencias los está esperando aspirantes. <br><br>
            <div class="text-center"><b>¡La Escuela de Suboficiales del Ejército "Sargento Cabral" les desea un feliz año nuevo!</b></div>
        </h5>
        <video controls width="100%">
            <source src="assets/img/noticias/aspirantes_video.mp4" type="video/mp4">
        </video>
        </div>

    </section>
</section>