<section style="background-color:#fcfcf7">

    <img src="assets/img/noticias/25mayo7.jpg" class="w-100" style="height:300px;object-fit: cover;">
    <section id="cuerponoticia">
        <h2 class="text-center">Ceremonia 210 años de Patria</h2>
        <p>En el marco de un nuevo aniversario de la Revolución de Mayo, se llevó a cabo la ceremonia en conmemoración de los 210 años de Patria en la plaza de armas del instituto. La misma fue presidida por el director CR Jorge Puebla y participaron oficiales y suboficiales residentes, como también el personal afectados a actividades administrativas y de seguridad.<br><br>
        Considerando las medidas de seguridad decretadas sobre el distanciamiento social, en 
        relación a la emergencia sanitaria, y respetando el uso del tapabocas, se dio inicio al acto patrio con el izamiento de la bandera nacional argentina, seguido de la de lectura del mensaje del Ministro de Defensa y una invocación religiosa llevada a cabo por el sacerdote del instituto, Diego Segundo.<br><br>
        Finalmente, luego de sus palabras alusivas, el director del instituto exclamó la fórmula 
        de ¡Subordinación y valor! ¡Para defender a la Patria!</p>
        <div class="row">
        <div class="col"><img src="assets/img/noticias/25mayo2.jpg" class="w-100"></div>
        <div class="col"><img src="assets/img/noticias/25mayo.jpg" class="w-100"></div>
        <div class="col"><img src="assets/img/noticias/25mayo3.jpg" class="w-100"></div>
        </div>
        <div class="row">
        <div class="col mt-4">
        <img src="assets/img/noticias/25mayo7.jpg" class="w-100">
        </div>
        </div>
    </section>
</section>