<section style="background-color:#fcfcf7">
<img src="assets/img/noticias/dia_ea_2021_portada.jpg" class="w-100" style="height:400px;object-fit: cover;object-position:10% 90%" >

    <section id="cuerponoticia">

        <h2 class="text-center">29 de mayo - Día del Ejército Argentino</h2>
        <br>
        En el día de la fecha, conmemoramos un nuevo aniversario del Ejército Argentino. Un día como hoy pero del año 1810, la Primera Junta decretó oficialmente la creación de nuestro Ejército, con el fin de reconocer el arduo trabajo de las tropas militares durante la semana de mayo más trascendental de la historia argentina. <br><br>
        Por tal motivo, en este día tan especial enviamos un cálido saludo y reconocemos a todos los oficiales, suboficiales, soldados, docentes, y personal civil del instituto  quienes, incluso a la distancia, se esfuerzan día a día por y para el aspirante, siendo de suma importancia en la formación de los futuros suboficiales, columna vertebral y brazo ejecutor del Ejército Argentino. ¡Continuemos formando líderes! <br><br>
    </section>
</section>



