<section style="background-color:#fcfcf7">
    <section id="cuerponoticia">
        <h2 class="titulo fullancho text-left m-0">INCORPORACIÓN PROVINCIAS</h2>
        <img src="assets/img/noticias/incorporacion.jpg" class="w-100" style="height:200px;object-fit: cover;">
        <div style="padding:10px;background-color:rgb(255,255,255,0.1)">
        <h5>
        Más de 3900 jóvenes se presentaron a rendir los exámenes de admisión en los centros de preselección ubicados
         a lo largo y ancho del país con el objetivo de ser suboficiales columna vertebral del Ejército Argentino.<br><br>

        <b>Los resultados de los exámenes de admisión serán publicados el día 6 del mes de enero en nuestro sitio web<b><br><br>
        
        <h3 class="titulo fullancho text-left">ALGUNAS FOTOS DE LOS EXÁMENES</h3>
        <div id="carouselNovedades" class="carousel slide carousel-fade" data-ride="carousel" >
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="assets/img/noticias/index.jpg" alt="...">
                </div>
                <div class="carousel-item position-relative">
                    <img src="assets/img/noticias/index2.jpg" style="z-index:-1;" class="d-block w-100"alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/noticias/index3.jpg" style="z-index:-1;" class="d-block w-100"alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/noticias/index4.jpg" style="z-index:-1;" class="d-block w-100"alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/noticias/index5.jpg" style="z-index:-1;" class="d-block w-100"alt="...">
                </div>
            </div>

            <a class="carousel-control-prev" href="#carouselNovedades" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            <a class="carousel-control-next" href="#carouselNovedades" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Siguiente</span>
            </a>
        </div>
        </h5>
        </div>

    </section>
</section>