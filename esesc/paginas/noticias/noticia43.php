<section style="background-color:#fcfcf7">
<img src="assets/img/noticias/estado_inscripcion.jpg" class="w-100" style="">

    <section id="cuerponoticia">

        <h2 class="text-center">Abierta la inscripción 2021</h2>
        
        

        ¿Ya finalizaste tu inscripción online y querés saber cómo continúa el proceso? Es sencillo, lo único que debes hacer es ingresar a nuestro sitio web y realizar el seguimiento de tu estado de inscripción, el cual puede ser: inscripto, en observación o desaprobado. A continuación te contamos el paso a paso:
        <br><br>
        <ol>
            <li>Hace click en la solapa "ingresar". Una vez dentro, presioná el botón "postulantes" y completa los casilleros con tu usuario (DNI) y contraseña. De esta manera podrás realizar un seguimiento de tu estado de inscripción y de la documentación cargada.</li>
            <li>Luego, la División Extensión se encargará de controlar tu DNI y el SAF chequeará que la boleta de pago y el ticket correspondiente coincidan. Esto puede tardar entre 10 y 15 días, sea paciente.</li>
            <li>Recordá también chequear tu casilla de mail, ya que allí llegará una notificación en caso que deban actualizar algún documento o cuenten con alguna observación en los datos cargados.</li>
            <li>Una vez aprobada la inscripción, obtendrás un número de inscripto y podrás acceder a los contenidos de estudios, exigencias físicas y el DUPIE.</li>
        </ol>
        Si aún tenes dudas o consultas podés comunicarte con algunos de nuestros medios de <a href="?p=contacto">contacto</a>.

    </section>
</section>