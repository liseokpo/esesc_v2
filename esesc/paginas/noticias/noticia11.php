<section style="background-color:#fcfcf7">

    <section id="cuerponoticia">
        <h2 class="titulo fullancho text-center m-0">Intercambio Chile</h2>
        <div class="container" style="padding:10px;background-color:rgb(255,255,255,0.1)">
            <div class="row">
                <div class="col-lg-4 text-center">
                    <img src="assets/img/noticias/chile2.jpg" class="img-fluid" style="max-height:400px;height:auto;width:auto">
                </div>
                <div class="col-lg-8 my-auto">
                    <h4>
                    La comitiva argentina integrada por el SG I Maximiliano Pérez y los CB (s) Gabriel Martínez y 
                    Lucas Melgarejo recientemente egresados de la ESESC, fue partícipe del intercambio con la Escuela 
                    de Suboficiales del Ejército de Chile “Sargento 2do Daniel Rebolledo Sepúlveda”. 
                    </h4><br>
                    <h4>
                    La visita permitió a los efectivos de instituto participar de la ceremonia del aniversario del 
                    instituto. Además, como cultura general visitaron museos y lugares históricos de importancia de nuestro país vecino.
                    </h4>
                </div>
            </div>
            <br>
            <img src="assets/img/noticias/chile.jpg" style="width:100%;height:420px;object-fit: cover;">

        </div>
    </section>
</section>