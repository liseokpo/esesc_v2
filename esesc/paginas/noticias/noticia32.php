<section style="background-color:#fcfcf7">

  <img src="assets/img/noticias/vuelta_asp2.jpg" class="w-100" style="height:400px;object-fit: cover;object-position:75% 25%">

  <section id="cuerponoticia">

  <h2 class="text-center">Actitud ESESC</h2>
  Reanudan gradualmente las actividades en el instituto y, respetando todos los protocolos de sanidad correspondientes, los aspirantes de IIdo año vuelven a instruirse y nutrirse en valores de manera presencial para pronto egresar como cabos del Ejército Argentino.
  <br><br>
  ¡Muchas felicidades en esta última etapa! Continuemos cuidándonos entre todos. 
  <br><br>
  #QuieroSerAspirante<br>
  #PostCOVID19<br>
  #ActitudESESC<br>
  #SomosElEjercito<br>


  <br>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
          <img class="d-block w-100" src="assets/img/noticias/vuelta_asp1.jpeg" style="height:380px;object-fit: cover;object-position:50% 50%">

      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/vuelta_asp3.jpg" style="height:380px;object-fit: cover;object-position:50% 50%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/vuelta_asp4.jpg" style="height:380px;object-fit: cover;object-position:60% 40%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/vuelta_asp2.jpg" style="height:380px;object-fit: cover;object-position:60% 40%">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>


  </div>

  </section>
</section>