
<section style="background-color:#fcfcf7">
    <section id="cuerponoticia">
    <br>
        <h2 class="text-center">INCORPORACIÓN 2021</h2>
        <br>
        <p>
        Finalizaron los exámenes de admisión en todo el país y usted se encuentra más cerca de formar parte de este prestigioso instituto. <br>
        <ul class="fa-ul">
            <li><span class="fa-li"><i class="fas fa-check-square"></i></span>Podrá conocer su estado de inscripción a partir del 8 de marzo. Para realizar el seguimiento deberá ingresar desde nuestro sitio web oficial con su DNI y contraseña.</li>
            <li><span class="fa-li"><i class="fas fa-check-square"></i></span>A partir del 15 de marzo serán citados los postulantes aprobados del AMBA para realizar las evaluaciones psicológicas.</li>
        </ul>
        
        <br>

        #QuieroSerAspirante<br>
        #FormamosLideres<br>
        #Ingreso2021<br>


        </p>


        <br>
    </section>
</section>