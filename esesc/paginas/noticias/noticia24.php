<section style="background-color:#fcfcf7">

    <img src="assets/img/noticias/preinsc_aprobada.jpg" class="w-100" style="height:350px;object-fit: cover;object-position:70% 30%">

    <section id="cuerponoticia">

    <h2 class="text-center">¡ATENCIÓN POSTULANTES!</h2>
    <p>
    Si su solicitud de PREINSCRIPCIÓN ha sido aprobada por la división de incorporación deberá presentar la documentación requerida para continuar con el proceso de INSCRIPCIÓN.
    <br><br>
    Recuerde que <b>el inicio de la inscripción se encuentra sujeta al aislamiento social, preventivo y obligatorio por el COVID 19</b> y será comunicada por nuestro sitio web y redes sociales.
    <br><br>
    Si todavía no se preinscribió aún tiene tiempo de hacerlo hasta el mes de octubre del 2020 haciendo <a href="index.php?p=preinscripcion">clic aquí</a>
    <br>
    </p>
    <h4>PREINSCRIPCIÓN</h4> ⠀⠀⠀
    <ul>
    <li>Disponible <a href="index.php?p=preinscripcion">aquí</a></li>
    <li>Finaliza en el mes de octubre del 2020.</li>
    <li>Debe ser verificada y aprobada por la División de Incorporación.</li>
    </ul>

    <h4>INSCRIPCIÓN</h4>
    <ul>
    <li>Fecha a confirmar debido a la emergencia sanitaria.</li>
    <li>Debe presentar la documentación requerida cuando sea notificado.</li>
    </ul>

    <br><br>
    </div>

    </section>
</section>