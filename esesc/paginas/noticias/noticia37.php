<section style="background-color:#fcfcf7">
<img src="assets/img/noticias/dia_mujer.jpeg" class="w-100" style="height:400px;object-fit: cover;object-position:60% 40%">

    <section id="cuerponoticia">

        <h2 class="text-center">8 de Marzo - Día Internacional de la Mujer</h2>
        Hemos recorrido un largo camino desde que se comienza a contar con personal femenino a partir de la creación de la Escuela de Enfermería en 1960. Siguiendo con el año 1981, se integraron como suboficiales del cuerpo profesional, hasta llegar a la actualidad dónde incluso egresan como cabos de las armas básicas de combate, como Infantería y Caballería. Hoy, las mujeres continúan desarrollando la profesión militar, logrando cumplir sus sueños y alcanzando sus más altas aspiraciones, con valor, profesionalismo y abnegación, siguiendo los pasos y teniendo como arquetipo a las grandes personalidades de nuestra Patria, como lo fue la sargento primero Carmen Ledesma. <br><br>
        
        Desde la Escuela de Suboficiales del Ejército "Sargento Cabral" queremos reconocer y alentar a todas las mujeres del país, a las mujeres oficiales, suboficiales, aspirantes, soldados voluntarios que prestaron servicio y aquellas que aún lo hacen, al personal docente y civil de nuestra comunidad educativa, a las madres que nunca dejan de acompañar, a las hermanas quienes no sueltan nuestras manos, a las abuelas con su cariñoso apoyo incondicional, a las mujeres que nunca bajan los brazos sin importar la dificultad de la situación y a las mujeres que luchan por sus sueños. En su día, les deseamos lo mejor. ¡Felicidades! <br><br>
        
        #DiaDeLaMujer <br>
        #ActitudCabral
        <div class="embed-responsive embed-responsive-16by9">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/-jr4uKxaKZ4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </section>
</section>