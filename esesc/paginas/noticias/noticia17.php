<section style="background-color:#fcfcf7">

    <section id="cuerponoticia">
    
    <h3 class="titulo fullancho text-center">#QuedateEnCasa</h3>

    <h5 class="mt-3">
    <h2 class="text-center">ESESC ¡Presente!</h2>
    <div class="container" style="padding:10px;background-color:rgb(255,255,255,0.1)">
        <div class="row">
            <div class="col-lg-4 text-center">
                <img src="assets/img/noticias/lamatanza1.jpeg" class="img-fluid" style="max-height:400px;height:auto;width:auto;">
            </div>
            <div class="col-lg-8 my-auto">
            <p>
            En el marco del combate contra el COVID-19,<br>
            personal de suboficiales conductores motoristas y cocineros de la ESESC, pertenecientes<br>
            al Comando Conjunto de Zona de Emergencia Metropolitana, brindaron apoyo<br>
            a la comunidad proporcionando viandas calientes a los vecinos que más lo necesitan.<br>
            <h5>Ellos te cuidan, ¡Quédate en casa!</h5>
            
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 my-auto">
                <img src="assets/img/noticias/lamatanza2.jpeg" class="img-fluid" style="max-height:400px;height:auto;width:auto;">
            </div>
            <div class="col-lg-6 text-center mt-2">
            
            <img src="assets/img/noticias/lamatanza3.jpeg" class="img-fluid" style="max-height:400px;height:auto;width:auto;">

            </div>
        </div>
    </div>
    
    
    </section>
</section>