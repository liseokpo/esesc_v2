<section style="background-color:#fcfcf7">
    <section id="cuerponoticia">
        <h2 class="titulo fullancho text-center m-0">Sobresaliente desempeño<br>SG Cepeda</h2>
        <img src="assets/img/noticias/cepeda.jpg" class="w-100" style="height:280px;object-fit: cover;">
        <div style="padding:10px;background-color:rgb(255,255,255,0.1)">
        
        <h3 class="text-center">Los suboficiales “columna vertebral” del Ejército Argentino.</h3>
        <h5>
        El instituto <b>felicita y reconoce</b> al sargento del arma de ingenieros <b>Andrés Alejandro Cepeda</b> 
        por su sobresaliente desempeño y haber aprobado las exigencias del “curso básico de formación 
        y selección de comando” en la Escuela de Tropas Aerotransportada y de Operaciones Especiales, 
        en la provincia de Córdoba.
        </h5>
        </div>
    </section>
</section>
