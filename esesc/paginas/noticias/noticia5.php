<section style="background-color:#fcfcf7">

    <section id="cuerponoticia">
        <h2 class="titulo fullancho text-left m-0">POSTULANTES Y REINCORPORADOS 2019</h2>
        <img src="assets/img/noticias/postulantes.jpg" class="w-100" style="height:200px;object-fit: cover;">
        <div style="padding:10px;background-color:rgb(255,255,255,0.1)">
        <h5>
        <b>¡ATENCIÓN POSTULANTES Y REINCORPORADOS!</b><br>
        Si ustedes desean conocer los resultados de los exámenes de admisión, deberán ingresar a partir del 6 de ENERO <br>
        en el submenú "LISTA DE APROBADOS" y deberán ingresar con su número de documento (completo, sin puntos).<br>
        Los estados son "APROBADO" o "CONDICIONAL".<br>
        Asimismo, deberán comunicarse telefónicamente con la División de Incorporación para obtener información detallada cuando realice su incorporación.<br>
        De Lunes a Viernes de 08:00 Hs a 18:00 Hs<br><br>

        CONMUTADOR <br>
        011 4664-0691 Internos 7525 y 7526.<br>
        <br>
        Vía WhatsApp  1161234798<br>
        Solo MENSAJES de texto.
        </h5>
        </div>

    </section>
</section>