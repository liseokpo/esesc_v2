<section style="background-color:#fcfcf7">

  <img src="assets/img/noticias/140anios_4.jpg" class="w-100" style="height:400px;object-fit: cover;object-position:50% 50%">

  <section id="cuerponoticia">

  <h2 class="text-center">Camino a los 140 años</h2>
  El 2 de julio del corriente año el sr. Jefe del Estado Mayor General del Ejército resolvió determinar como fecha de creación de la Escuela de Suboficiales del Ejército "Sargento Cabral" (ESESC) el 26 de marzo de 1881, oportunidad en que se fundó la "Escuela de Cabos y Sargentos de Artillería", como consta en las Memorias del Ministerio de Guerra y Marina – 1881 – Tomo I. P 37.
  <br><br>
  Desde el año 1881, se produjo el reclutamiento e instrucción de los suboficiales llamados “Clases”, hasta 1916, siendo estos cursos de carácter teórico-práctico y con una duración de cinco meses. 
  <br><br>
  En el año 1916, el instituto pasó a denominarse "Escuela de Suboficiales"; y en marzo de 1933 fue nombrada Escuela de Suboficiales "Sargento Cabral", dejando de funcionar entre 1947 y 1950. 
  <br><br>
  En las instalaciones de la actual ESESC funcionó la Escuela de Suboficiales "General Lemos", que tuvo sus orígenes el 20 de diciembre de 1939, bajo la denominación "Centro de Instrucción de los Cuerpos Auxiliares del Ejército".
  <br><br>
  En el Año 1950, se consolidó el Sistema de Educación de los Suboficiales de las Armas en un nuevo cuartel (Actual Dirección de Educación Operacional) donde la Escuela de Suboficiales "Sargento Cabral" funcionó hasta el año 2003.
  <br><br>
  El 26 de junio de 2003, se produjo la disolución de la Escuela de Suboficiales "Sargento Cabral" y de la Escuela de Suboficiales “General Lemos”, dando paso, en el predio de esta última, a la Escuela de Suboficiales del Ejército "SARGENTO CABRAL”.
  <br><br>
  <b>Desde la ESESC, agradecemos el apoyo brindado a diario y esperamos que continúen acompañándonos durante este camino hacia nuestros 140 años.</b>
  <br><br>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="6"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="7"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
          <img class="d-block w-100" src="assets/img/noticias/140anios_1.jpg" style="height:360px;object-fit: cover;object-position:60% 40%">

      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/140anios_2.jpg" style="height:360px;object-fit: cover;object-position:90% 10%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/140anios_3.jpg" style="height:360px;object-fit: cover;object-position:20% 80%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/140anios_4.jpg" style="height:360px;object-fit: cover;object-position:50% 50%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/140anios_5.jpg" style="height:360px;object-fit: cover;object-position:50% 50%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/140anios_6.jpg" style="height:360px;object-fit: cover;object-position:50% 50%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/140anios_7.jpg" style="height:360px;object-fit: cover;object-position:50% 50%">
      </div>
      <div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/140anios_8.jpg" style="height:360px;object-fit: cover;object-position:50% 50%">
      </div>
      
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>


  </div>

  </section>
</section>