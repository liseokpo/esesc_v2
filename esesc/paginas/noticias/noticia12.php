<section style="background-color:#fcfcf7">

    <section id="cuerponoticia">
        <h2 class="titulo fullancho text-center m-0">SM Mayorga</h2>
        <div class="container" style="padding:10px;background-color:rgb(255,255,255,0.1)">
            <div class="row">
                <div class="col-lg-4 text-center">
                    <img src="assets/img/noticias/mayorga.jpg" class="img-fluid" style="max-height:400px;height:auto;width:auto">
                </div>
                <div class="col-lg-8 my-auto">
                <h4 class="text-center">Homenaje a un veterano de la guerra de Malvinas</h4>
                <p>
                El suboficial mayor Americo Carlos Mayorga, nacido en la provincia de Mendoza en el año 
                1958, egresado de la Escuela de Suboficiales “Sargento Cabral” como cabo del arma de 
                comunicaciones, padre de tres hijos y abuelo de dos nietos, hoy es el único VGM en 
                actividad del instituto. Desplegó rumbo a las islas Malvinas integrando la compañía de 
                comunicaciones 3, con asiento de paz en la ciudad de Curuzú Cuatiá, provincia de Corrientes.
                </p>
                <p>
                Hoy, elegimos reconocer y agradecer a nuestro héroe por su entrega y sacrificio, habiendo 
                jurado fidelidad a la bandera y defendiéndola con heroísmo, orgullo y honor.
                </p>
                <p>
                <i>"Dedico este reconocimiento a nuestros queridos y recordados del ARA General Belgrano, a 
                los 649 héroes caídos, y no nos olvidemos de nuestros camaradas del ARA San Juan. También 
                al apoyo incondicional del encargado de elemento SM Horacio Godoy, a mi familia, a todos aquellos 
                que contribuyeron: oficiales, suboficiales, soldados, personal civil y en especial a mis 
                queridos compañeros de promoción."</i> – SM Mayorga
                </p>
                </div>
            </div>
        </div>
    </section>
</section>