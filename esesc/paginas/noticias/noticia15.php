<section style="background-color:#fcfcf7">

    <section id="cuerponoticia">
    
    <h3 class="titulo fullancho text-center">¡Atención POSTULANTES!</h3>

    <h5 class="mt-3">Debido a la emergencia sanitaria actual el instituto adoptará medidas de prevención con el fin de evitar la expansión de la enfermedad COVID-19 (Coronavirus). 
    <br>Por tal motivo, la actividades de la <b>División de Incorporación</b> se verán afectadas de la siguiente manera:</h5>
    <ul class="mt-3 mb-3">
        <li>Se <b>suspenderá</b> hasta nuevo aviso la <b>atención al público</b> personalizada en las oficinas del instituto.</li>
        <li>Se <b>atenderán llamados telefónicos</b> de lunes a viernes de 08 hs a 13.30 hs, al número (011) 4664-0691, interno 7525/7526/7527.</li>
        <li>Se recibirán y responderán sin limitaciones mensajes de <b>WhatsApp</b> al número 011-61234798 y al correo electrónico esesc.ejercito@gmail.com</li>
    </ul>
    <h5>Recuerde que las pre-inscripciones iniciarán en el mes de abril por medio de nuestro sitio web oficial <b>esesc.ejercito.mil.ar</b></h5>

    <p>Al virus lo frenamos entre todos ¡Quédate en casa!</p>
    
    </section>
</section>