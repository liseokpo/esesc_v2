<section style="background-color:#fcfcf7">

  <img src="assets/img/noticias/admision_2021_1.jpg" class="w-100" style="height:400px;object-fit: cover;object-position:50% 50%">

  <section id="cuerponoticia">

  <h2 class="text-center">Admisión 2021</h2>
  
  <p>
    Con el fin de concluir con la etapa final de su incorporación, postulantes de todo el país se presentaron en el instituto para rendir los exámenes de admisión. A continuación les compartimos algunas fotos tomadas durante los exámenes médicos, físicos e intelectuales. 
    <br><br>
    Si usted desea conocer en qué días y en qué turno debe presentarse a rendir haga click <a href="https://esesc.ejercito.mil.ar/?p=paso3">AQUÍ</a>


  <br><br>
  #QuieroSerAspirante<br>
  #ActitudESESC<br>
  #SomosElEjercito<br>
  </p>


  <br>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="assets/img/noticias/admision_2021_5.jpg" style="height:380px;object-fit: cover;object-position:50% 50%">
        </div><div class="carousel-item">
          <img class="d-block w-100" src="assets/img/noticias/admision_2021_6.jpg" style="height:380px;object-fit: cover;object-position:50% 50%">
        </div><div class="carousel-item">
            <img class="d-block w-100" src="assets/img/noticias/admision_2021_2.jpeg" style="height:380px;object-fit: cover;object-position:50% 50%">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="assets/img/noticias/admision_2021_3.jpg" style="height:380px;object-fit: cover;object-position:50% 50%">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="assets/img/noticias/admision_2021_4.jpg" style="height:380px;object-fit: cover;object-position:60% 40%">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>


  </div>

  </section>
</section>