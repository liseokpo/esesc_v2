<section style="background-color:#fcfcf7">
    <img src="assets/img/noticias/estaessuaoe.jpeg" class="w-100" style="max-height:400px;object-fit: cover;object-position:70% 30%">

    <section id="cuerponoticia">

    <h2 class="text-center">¿ESTA ES SU ARMA O ESPECIALIDAD?</h2>
    El viernes 16 de octubre de 2020 se llevó a cabo una nueva transmisión en vivo en la que aspirantes del instituto presentaron el Arma de Artillería, el Arma de Ingenieros, la Especialidad de Mecánico de Instalaciones y la Especialidad de Intendencia. 
    <br> Si desea conocer más sobre ellas o ver otras transmisiones revívalo en nuestra cuenta oficial de YouTube.
    <br><br>
    <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2mazUP-df9g" allowfullscreen></iframe>
    </div>
    Armas y Especialidades y Servicios previamente presentados en la transmisión en vivo del viernes 2 de octubre:

    <ul>
        <li>Arma de Infantería</li>
        <li>Arma de Caballeria</li>
        <li>Especialidad Mecánico Motorista a Oruga</li>
        <li>Especialidad Mecánico de Radar</li>
    </ul>


    </section>
</section>	