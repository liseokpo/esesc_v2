<section style="background-color:#fcfcf7">

    <img src="assets/img/noticias/diasoldado2.jpg" class="w-100" style="height:350px;object-fit: cover;object-position:70% 30%">
    <section id="cuerponoticia">
    <h2 class="text-center">3 de junio - Día del Soldado Argentino</h2>
    <br>
    <p>
    Un día como hoy en 1770 nació el creador de la Bandera Nacional y vencedor en las batallas de Tucumán y Salta. El abogado, economista, periodista y militar Manuel Joaquín del Corazón de Jesús Belgrano fue uno de los principales patriotas que impulsaron la Revolución de Mayo, es por ello que esta efeméride pretende homenajearle en el Día del Soldado Argentino.
    <br><br>
    En este día especial  reconocemos a todos los soldados de nuestra Patria,  quienes realizan tareas esenciales en la emergencia, durante el  aislamiento social como medida preventiva y colectiva para evitar la propagación del Covid 19. En ese marco, reconocerlos es muy importante  porque junto a otros son quienes garantizan la continuidad de la vida en tiempos de pandemia. Asimismo hacemos extensivo este saludo cálido y afectuoso a los oficiales, suboficiales, aspirantes y soldados, en actividad y retiro de nuestra comunidad educativa, cómo también recordar a todos nuestros caídos en combate o servicio.
    <br>
    </p>
    <p class="text-center">
    <strong>¡Viva la Patria! <br>
    ¡Viva el soldado Argentino! <br>
    ¡Viva! </strong><br><br>

    <img src="assets/img/noticias/diasoldado3.jpeg" class="w-100" style="height:350px;object-fit: cover;object-position:50% 50%">
    </p>
    </section>
</section>
