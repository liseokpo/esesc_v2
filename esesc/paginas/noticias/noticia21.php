<section style="background-color:#fcfcf7">

  <img src="assets/img/noticias/diaea1.jpeg" class="w-100" style="height:310px;object-fit: cover;">
  <section id="cuerponoticia">

  <h2 class="text-center">29 de mayo - Día del Ejército Argentino</h2>

  <br>
  <h5>
  En el año 1810 por decreto de la Primera Junta de Gobierno, se organizan las primeras unidades del Ejército Argentino. Con tal motivo, hoy conmemoramos su 210° aniversario y saludamos con afecto al personal militar, docente y civil, retirados y en actividad, que conforman a la familia militar y que a diario prestan servicio con el fin de defender los intereses de nuestra nación, así como también acompañan dedicadamente durante la formación del aspirante.
  <br><br>En su día, saludamos a los soldados que a diario brindan servicio a nuestra Patria.
  <br><br></h5>

  <div class="row">
  <div class="col text-center">
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="./assets/img/noticias/diaea3.jpg" class="d-block w-100">
      </div>
      <div class="carousel-item">
        <img src="./assets/img/noticias/diaea2.jpg" class="d-block w-100">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  </section>
</section>