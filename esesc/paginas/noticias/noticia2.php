<section style="background-color:#fcfcf7">

    <section id="cuerponoticia">

        <h1 class="titulo fullancho text-left m-0">NOTICIA</h1>
        <img src="assets/img/noticias/noticia2cab.jpg" class="w-100" style="height:100%;object-fit: cover;">
        <div style="padding:10px;background-color:rgb(255,255,255,0.1)">
            
            <h5>
            Nos encontramos cada vez más cerca de la ceremonia de egreso de cabos. La misma se realizará el día <b>29 de noviembre del corriente año a las 18 hs</b>, en la Escuela de Suboficiales del Ejército “Sargento Cabral”.

            Recuerde que la ceremonia se estará <b>transmitiendo en vivo</b> a través de nuestra página oficial de Facebook.</h5> <h4 class="text-center"><b>¡No se lo pierda!</b></h4>
            
        </div>

    </section>
</section>