<section style="background-color:#fcfcf7">
    <section id="cuerponoticia">
        <h2 class="titulo fullancho text-center m-0">Cabo Cerezo Estefanía</h2>
        <div class="container" style="padding:10px;background-color:rgb(255,255,255,0.1)">
            <div class="row">
                <div class="col-lg-4 text-center">
                    <img src="assets/img/noticias/cerezo2.jpg" class="img-fluid" style="max-height:500px;height:auto;width:auto">
                </div>
                <div class="col-lg-8 my-auto">
                    <h3 class="text-center">Los suboficiales “columna vertebral” del Ejército Argentino</h3>
                    <p>
                    Cerezo Estefanía, nació en Tartagal, provincia de Salta, se formó como soldado voluntario en el regimiento de infantería 
                    mecanizado 35 Rospentek, en la provincia de Santa Cruz, y recientemente egresó como cabo 
                    en la ESESC. Hoy, es la única mujer suboficial del arma de infantería en servicio del Ejército Argentino.
                    </p>
                    <p>
                    <i>“¿Por qué elegí ser infante? Esa es una pregunta que me la hacen desde que fui soldado, 
                    mi respuesta es porque me gusta sentir la satisfacción de terminar algo que pensaba inalcanzable”</i> 
                    aseguró Cerezo. <br><i>“Mi carrera aún es demasiado corta y no pasé por todo lo que debe pasar un verdadero 
                    infante, pero ver a mis superiores, la admiración que siento por ellos y todo lo que lograron es mi ejemplo a 
                    seguir, me hace querer ser mejor cada día. A partir de ahí nacen mis proyectos a futuro”</i> concluyó nuestra histórica cabo.
                    </p>
                    <h4>
                    Felicitamos, reconocemos y deseamos éxitos a la mujer que marcó un hito en la historia del instituto.
                    </h4>
                
                
                
                
                </div>
                
            </div>
            <br>
            <img src="assets/img/noticias/cerezo1.jpg" style="width:100%;height:420px;object-fit: cover;">

        </div>
    </section>
</section>