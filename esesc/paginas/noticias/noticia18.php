<section style="background-color:#fcfcf7">

    <section id="cuerponoticia">
    
    <h3 class="titulo fullancho text-center">¡Abierta la preinscripción ONLINE!</h3>

    <p class="mt-3">Se encuentra disponible la PREINSCRIPCIÓN online 2020 en
    nuestro sitio web oficial. Una vez completo el formulario debe ser aprobado por
    la División Incorporación.<br>
    Usted NO debe enviar ningún tipo de documentación
    hasta que la INSCRIPCIÓN esté habilitada y su preinscripción haya sido
    aprobada.
    <br>
    Recuerde que la INSCRIPCIÓN al instituto aún no se encuentra disponible. La fecha de la misma se encuentra sujeta a la emergencia sanitaria COVID-19 y será comunicada pronto
    
    </p>

    <h5>PREINSCRIPCIÓN</h5>
    
    <ul class="mt-3 mb-3">
    <li>Disponible <a href="?p=preinscripcion">aquí</a>.</li>
    <li>Finaliza en el mes de octubre del 2020.</li>
    <li>Debe ser verificada y aprobada por la División de Incorporación.</li>
    </ul>

    <h5>INSCRIPCIÓN</h5>
    <ul class="mt-3 mb-3">
    <li>Fecha a confirmar debido a la emergencia sanitaria.</li>
    <li>Debe presentar la documentación requerida.</li>
    </ul>
    </section>
</section>