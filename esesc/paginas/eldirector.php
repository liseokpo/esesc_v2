

<section id="cuerpo">
<h2 class="text-center">Bienvenida del Sr. Director</h2>
<br>
<div class="text-center d-block d-sm-block d-md-block d-lg-none">
    <img src="assets/img/eldirector.jpg" style="max-height:450px" class="w-auto mx-auto"><br>
    <b>Coronel Gustavo Adrián Sivori</b>

</div>

<div style="float:right" class="w-50 text-center d-none d-lg-block d-xl-block">

    <img src="assets/img/eldirector.jpg" class="mh-100 mw-100 p-2 ">
    <b>Coronel Gustavo Adrián Sivori</b>

</div>
<br>
<div style="text-align:justify">
    
    <p class="">
    Tengo un profundo orgullo en dirigirme a la comunidad educativa de la Escuela de Suboficiales del Ejército “Sargento Cabral” como director de la misma.<br><br>
    Liderar un equipo de mujeres y hombres de tan aquilatado profesionalismo es un desafío fascinante. Sé de nuestra sinergia evidenciada en todos estos años que nuestro instituto ha formado en valores a quienes integraron, integran e integrarán la columna vertebral del Ejército Argentino, nuestros suboficiales.<br><br>
    Toda organización mide sus fortalezas y también debilidades en función de los desafíos que nos plantea el futuro, siendo lo que viene paradigmático para nuestro Ejército y el instituto en particular.<br><br>
    Tenemos frente a nosotros ser los custodios de los 140 años del centro. En el 2021 reafirmáremos nuestro compromiso con la enseñanza en valores y la calidad educativa. Queremos ser, como reza nuestra visión, un centro de formación de excelencia para nuestros suboficiales, y con las fuerzas de todos no tengo dudas que lo lograremos.<br><br>
    El otro gran desafío que nos involucra y, quizás el más importante que se ha planteado nuestro Ejército, es reconvertir a la institución como un centro de formación de características universitarias. A partir del año 2023, dotaremos a nuestros aspirantes de una tecnicatura militar universitaria. Es un antes y un después en la formación de nuestra gente, permitiendo que nos exijamos a todos, y está bien que así sea, porque la vida es eso, ser mejores nosotros para que nuestra gente sea mejor.<br><br>
    No hay mejor satisfacción para cualquier educador, sea civil o militar, sentir que “el discípulo supera al maestro”.<br><br>
    Pongamos toda nuestra inteligencia y nuestra voluntad en llevar adelante esta reconversión, sabiendo que lo se viene siempre va a ser mejor.<br><br>
    Finalmente, tengamos confianza en nuestro equipo, sabemos lo que tenemos que hacer y el coraje para hacerlo.<br><br>
    ¡Muchas gracias!<br><br>


    </p>
</div>

</section>