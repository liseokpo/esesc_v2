<section id="cuerpo"> 

<h2 class="text-center">Curso de formación elemental</h2>
<h5 class="text-center">Para Soldados Voluntarios y Civiles</h5>

<br>
<img src="assets/img/art11/art11_10.jpg" class="w-100" style="height:310px;object-fit: cover;object-position:60% 40%"><br><br>
<h3><strong>¿Qué es?</strong></h3>

<p>
El Curso de Formación Elemental cubre las necesidades de completamiento de efectivos y a la cantidad de personal requerido para cubrir puestos en los mismos.
<br><br>
Se da en torno a la finalización de la capacitación de los saberes previos de los Artículo 11, a lo que se complementa su formación con una capacitación centralizada en un instituto. Es por ello que su intensidad, organización y estructuración curricular es particular y distinta a la formación tradicional de los Suboficiales.
</p>
<br>
<div class="text-center">
    <a class="btn btn-primary btn-lg "  target="_blank" href="https://sites.google.com/view/cursodeformacionelemtal/p%C3%A1gina-principal">Quiero saber más</a>
</div>
</section>