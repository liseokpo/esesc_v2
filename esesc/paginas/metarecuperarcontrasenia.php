<?php
include ('../assets/func/funciones.php');
include ('../assets/func/class.DBCandidato.php');
include ('../assets/func/class.DBUsuario.php');
if(isset($_POST["recupemail"])){
    
    $correo = $_POST['recupemail'];
    $pdo = conexion();
    $DBCandidato = new DBCandidato($pdo);
    $DBUsuario = new DBUsuario($pdo);
    
    //1) Ver que el email exista en nuestra bd.
    if($user = $DBCandidato -> obtenerCandidatoporEmail($correo))
    {
        //2) Si existe, entonces deberíamos generar un codigo de recuperacion...
        //Insertarlo a la tabla recuperar
        //de la bd junto con la fecha de expiración 
        $codigo = createRandomCode();
        
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        
        $fechaRecuperacion = date("Y-m-d H:i:s", strtotime('+24 hours'));

        $url = 'index.php?p=recuperarcontrasenia&crec='.$codigo;
                
        $respuesta = $DBCandidato -> recoverPassword($user['id'], $codigo, $fechaRecuperacion);
            
        if ($respuesta) {
            //3) Enviar email con el codigo de recuperación.
            sendMail($correo, $user['nombres'].' '.$user['apellidos'], $url, "recuperacion_contrasenia", "Recuperación de contraseña - ESESC", 1);
            header ("Location: ../index.php?p=recuperarcontrasenia&c=1&mensaje=Se ha enviado un correo electrónico con las instrucciones para el cambio de su contraseña.<br> Por favor verifica la información enviada.");            
        } else {
            header ("Location: ../index.php?p=recuperarcontrasenia&c=2&mensaje=No se puede recuperar la cuenta. Si los errores persisten comuníquese con el instituto.");
        }
    }
    else if($user = $DBUsuario -> obtenerUsuarioPorEmail($correo)){
        //2) Si existe, entonces deberíamos generar un codigo de recuperacion...
        //Insertarlo a la tabla recuperar
        //de la bd junto con la fecha de expiración 
        $codigo = createRandomCode();
        
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        
        $fechaRecuperacion = date("Y-m-d H:i:s", strtotime('+24 hours'));

        $url = 'index.php?p=recuperarcontrasenia&crec='.$codigo;
                
        $respuesta = $DBUsuario -> recoverPassword($user['id'], $codigo, $fechaRecuperacion);
            
        if ($respuesta) {
            //3) Enviar email con el codigo de recuperación.
            sendMail($correo, $user['user'], $url, "recuperacion_contrasenia", "Recuperación de contraseña - ESESC", 1);
            header ("Location: ../index.php?p=recuperarcontrasenia&c=1&mensaje=Se ha enviado un correo electrónico con las instrucciones para el cambio de su contraseña.<br> Por favor verifica la información enviada.");            
        } else {
            header ("Location: ../index.php?p=recuperarcontrasenia&c=2&mensaje=No se puede recuperar la cuenta. Si los errores persisten comuníquese con el instituto.");
        }
    }
    
    else{
        header ("Location: ../index.php?p=recuperarcontrasenia&c=3&mensaje=El email ingresado no se encuentra registrado.");
    }
}

//Si viene acá significa que está ingresando la contraseña nueva.
//Al cambiar la contraseña debo cambiar el código a usado.
else if(isset($_POST['idusuariorec'])){

    //recupass 
    $nueva = $_POST['recupass'];
    $idusuario = $_POST['idusuariorec'];
    $tipousuario = $_POST['tipousuariorec'];

    $pdo = conexion();

    if($tipousuario == "candidato"){

        $DBCandidato = new DBCandidato($pdo);

        $candidato = $DBCandidato -> obtenerCandidatoporId($idusuario);
        $clavehasheada = hashearPass($nueva);
        $DBCandidato -> cargarClaveCandidato($idusuario,$clavehasheada);
        $DBCandidato -> codigoUsado(1,$idusuario);
        
    }
    else if($tipousuario == "gestor"){
        $DBUsuario = new DBUsuario($pdo);
    
        $usuario = $DBUsuario -> obtenerUsuarioporId($idusuario);
        $clavehasheada = hashearPass($nueva);
        $DBUsuario -> cargarClaveUsuario($idusuario,$clavehasheada);
        $DBUsuario -> codigoUsado(1,$idusuario);    
        
    }

    header ("Location: ../index.php?p=recuperarcontrasenia&c=4&mensaje=La contraseña se ha modificado correctamente. <a href='ingresar.php'>Ingresar</a>");
}
else{
    echo"<h2 class='text-center'>Esta página se encuentra restringida.</h2>";
}
?>



<?php
?>