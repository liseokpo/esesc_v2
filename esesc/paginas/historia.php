    <section id="cuerpo" style="color:#444444">
        <h1 class="text-center text-shadow">Historia</h1>  
        <br>
        <ol>
            <i class="historia"></i> 
            <h1 class="text-center">Escuela de Suboficiales del Ejército “Sargento Cabral”</h1>
            <h3>RESEÑA HISTÓRICA DEL CUARTEL</h3>
            <br/>
            <i class="historiap">
            
            Los últimos años del siglo XIX marcaron el inicio de un proceso de modernización del ejército, tanto en el campo de la tecnología como de las tácticas de combate, lo que trajo como consecuencia la necesidad de contar con personal capacitado e instruido para constituirse en eficaz colaborador del Oficial.<br/><br/>
            Con ese objeto, comenzaron a tomar forma diversos institutos de formación para suboficiales, teniendo como primer antecedente de estos a la Escuela de Cabos y Sargentos de Artillería, creada en 1881 y que funcionó hasta 1897, año en que pasa a denominarse “Escuela Normal de Clases del Ejército”.<br/><br/>
            En el año 1901 son donados terrenos al Ejército Argentino, los que debían ser destinados a maniobras e instrucción, tomando desde entonces el nombre de Campo de Mayo.<br/><br/>
            En el año 1902 comienzan los asentamientos de las primeras unidades militares. Entre ellas podemos mencionar la Escuela de Clases, la cual ocupó el cuartel número 9 de Campo de Mayo desde el año 1908.<br/><br/>
            Posteriormente, en 1913, se crea la Escuela de Armeros Militares, con la finalidad de formar aquellos suboficiales encargados de mantener y reparar las distintas armas que se encontraban en pleno proceso de obtención y provisión, y que paulatinamente irían reemplazando al sable y la lanza.<br/><br/>
            En 1916, con finalidad de formar al personal de cuadros subalternos del cuerpo comando, se la denomina Escuela de Suboficiales, imponiendo el cambio del término “clase” por “suboficial”.<br/><br/>
            En 1933, en ocasión de conmemorarse el 120 aniversario del Combate de San Lorenzo, se nombra a la Escuela de Suboficiales “Sargento Cabral”, en honor al arquetipo de lealtad y coraje de nuestro bravo granadero, quién heroicamente ofrendó su vida para salvar a nuestro Libertador.<br/><br/>
            En el año 1944 se crea la Escuela de Servicios, iniciándose en dicha oportunidad la construcción de estas instalaciones en la zona donde funcionaba el vivero de Campo de Mayo. Las obras finalizan el 1 de enero de 1950, con la escuela funcionando a pleno.<br/><br/>
            En el año 1951 el instituto recibe su nombre histórico y pasa a llamarse Escuela del Cuerpo Profesional Auxiliar “General Lemos”.<br/><br/>
            Producto de las reestructuraciones implementadas en la fuerza, y por superior resolución del Ministerio de Defensa, fecha 20 de diciembre de 2002, la Escuela de Suboficiales “Sargento Cabral” y la Escuela de Suboficiales “General Lemos” se fusionan en un solo instituto, ocupando finalmente este cuartel.<br/><br/>
            Con las nuevas necesidades de alojamiento y de las exigencias que el moderno entrenamiento de las armas requiere, se adecuaron las instalaciones de esta escuela de suboficiales del ejército. Entre las mejoras más importantes se destacan:<br/><br/>
            - Ampliación de las instalaciones del parque automotor.<br/><br/>
            - La creación de nuevas aulas, adiestradores y simuladores para las armas de infantería, caballería y artillería.<br/><br/>
            - Construcción de los nuevos parques de artillería, ingenieros y comunicaciones.<br/><br/>
            - Transformación de algunos edificios en alojamientos para aspirantes y aulas de informática.<br/><br/>
            Todos estos adelantos en infraestructura se suman a los ya presentes talleres de armamento, automotores, electrónica, ingenieros y parque de aviación, empleados en la formación de los aspirantes de las especialidades y servicios.<br/><br/>
            Con su centenaria historia y desde los históricos cuarteles de campo de mayo, la Escuela de Suboficiales del Ejército “Sargento Cabral”, continúa cumpliendo con su misión. <br/><br/>
            </i>
                
        </ol>
        <ol>
            <h1 class="text-center text-shadow">La fusión</h1>
            <i class="historiap">El espíritu de unificación y la idea de que todos los suboficiales procedieran del mismo lugar, hicieron que el 13 de septiembre de 2002, por Resolución Ministerial N°432, se fusionaran las escuelas de armas con la de especialidades, creando una nueva escuela llamada <i><b>Escuela de Suboficiales del Ejército "Sargento Cabral"</b></i>.</i>    
            <br/><br/>
            <i class="lafusion"></i>    
        </ol>
    </section>