<section id="cuerpo" style="color:#444444">
<h1  class="text-center text-shadow">Armas</h1><br>
<div class="row m-0 p-0 w-100 justify-content-center ">
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card border-0 text-center w-100" style="background-color:rgb(255,255,255,0);">
            <div class="card-body">
            <img src="./assets/img/carreras/armas/INFANTERIA.png" class="card-img-top" style="width:80%" alt="...">
                <h3>Infantería
                
                </h3>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#infanteria">
                Ver
                </button>
                <p class="card-text"></p>
                
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card border-0 text-center w-100" style="background-color:rgb(255,255,255,0);">
            <div class="card-body">
                <img src="./assets/img/carreras/armas/CABALLERIA.png" class="card-img-top" style="width:80%" alt="...">

                <h3>Caballería</h3>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#caballeria">
                Ver
                </button>
                <p class="card-text"></p>
                
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card border-0 text-center w-100" style="background-color:rgb(255,255,255,0);">
            <div class="card-body">
                <img src="./assets/img/carreras/armas/ARTILLERIA.png" class="card-img-top" style="width:80%" alt="...">

                <h3>Artillería</h3>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#artilleria">
                Ver
                </button>
                <p class="card-text"></p>
                
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card border-0 text-center w-100" style="background-color:rgb(255,255,255,0);">
            <div class="card-body">
                <img src="./assets/img/carreras/armas/INGENIEROS.png" class="card-img-top" style="width:80%" alt="...">

                <h3>Ingenieros</h3>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#ingenieros">
                Ver
                </button>
                <p class="card-text"></p>
                
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card border-0 text-center w-100" style="background-color:rgb(255,255,255,0);">
            <div class="card-body">
                <img src="./assets/img/carreras/armas/COMUNICACIONES.png" class="card-img-top" style="width:80%" alt="...">

                <h3>Comunicaciones</h3>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#comunicaciones">
                Ver
                </button>
                <p class="card-text"></p>
                
            </div>
        </div>
    </div>
    
</div>




<!-- Modal -->
<div class="modal fade" id="infanteria" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Infantería</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/carreras/armas/infanteria_foto.jpg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Es el arma que conforma elementos básicos de combate del instrumento militar terrestre, adiestrados y equipados para ejecutar combates en contacto directo con el enemigo, empleando una combinación de maniobra, fuego y choque; siendo este último el elemento de su accionar táctico que la particulariza e identifica.
        <br><br>
        La infantería opera a pie, con vehículos de combate a rueda u oruga, o articulada con medios aéreos y, eventualmente, anfibios. Esto le permite desplazarse y combatir en toda clase de terrenos y condiciones meteorológicas o de visibilidad. La naturaleza de los medios y su empleo tipifican al arma como ligera, mediana o pesada.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="caballeria" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Caballería</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/carreras/armas/caballeria_foto.jpeg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        La caballería conforma elementos básicos de combate, en una armónica conjunción de hombres y medios; organizados, equipados y adiestrados para establecer contacto, atacar y aniquilar al enemigo mediante el fuego, el movimiento y la acción de golpe. Actúa ofensivamente en cualquier tipo de operación militar a fin de la lograr la decisión del combate.
        <br><br>
        Actualmente cuenta con vehículos blindados de distinto tipo que le proporcionan gran movilidad, potencia de fuego, velocidad táctica y protección blindada. Dichas características le permiten ejercer sobre el enemigo un efecto psicológico paralizante. 
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="artilleria" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Artillería</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/carreras/armas/artilleria_foto.jpg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Es el Arma de apoyo de fuego de los elementos básicos de combate; está organizada, equipada y adiestrada para conducir y ejecutar el volumen de fuego que posibilite al comandante el desarrollo de la operación táctica, mediante el logro de los efectos deseados sobre blancos terrestres y aéreos, contribuyendo al cumplimiento de la misión general. Está conformada por un sistema de artillería de campaña y otro de artillería antiaérea. 
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ingenieros" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Ingenieros</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/carreras/armas/ingenieros_foto.jpeg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Es un Arma de apoyo de combate, organizada, adiestrada y equipada para ejecutar funciones, actividades y tareas específicas, a fin de contribuir a concretar la misión de la fuerza apoyada.
        <br><br>
        Los ingenieros se encuentran presentes en primera línea, junto a las armas básicas de combate, y en la retaguardia, construyendo caminos, puentes y otras obras de importancia que le permitan hacerles llegar a aquellas, los medios necesarios para el cumplimiento de la misión.
        <br><br>
        Aplica medios, procedimientos y conocimientos técnicos para la solución de problemas tácticos.
        <br><br>
        Contribuye a la preservación de la libertad de maniobra de las propias fuerzas contrarrestando las dificultades que pueda presentar la naturaleza, tales como terrenos intransitables o accidentes geográficos. A su vez, debe limitar la posibilidad de maniobra del enemigo.
        <br><br>
        Las características propias del ambiente geográfico de nuestro país, con amplios espacios, escasez de vías de comunicaciones y recursos, con grandes obstáculos naturales (cursos de agua, cordones montañosos, desiertos, etc.) e insuficiente disponibilidad de fuerzas y medios potencian la importancia del Arma, ya que su misión contribuirá a disminuir los efectos que estas características y limitaciones imponen.
        <br><br>
        A su vez, debe brindar protección contra los efectos de las armas del enemigo, regulares o químicas - biológicas - nucleares (QBN), y mejorar las condiciones de la vida en en el terreno.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="comunicaciones" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="background:url('./assets/img/bg.jpg')">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Comunicaciones</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="./assets/img/carreras/armas/comunicaciones_foto.jpg" class="w-100"alt="">
        <p class="pt-3 mb-0">
        Se encuentra organizada, equipada y adiestrada para proporcionar apoyo de comunicaciones, informática y guerra electrónica, facilitando la conducción de los elementos propios, en todos los niveles, y dificultando la conducción enemiga, en cualquier tiempo y lugar.
        <br><br>
        El apoyo de comunicaciones e informática se brindará sobre la base del diseño, instalación, operación y mantenimiento del sistema único de comunicaciones de la Fuerza, conformado por cuatro subsistemas: comunicaciones fijas, comunicaciones de campaña, guerra electrónica e informático. También, se encarga de la elaboración de órdenes, procedimientos y normas que permiten el funcionamiento coordinado e integrado de todos los subsistemas y la explotación del espectro electromagnético empleado.

        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>



<br>
<h2 class="text-center">Título de Egreso</h2>
<br>
<table class="table table-responsive-sm table-dark text-dark" style="background-color:#f5f5f5;">
    <thead class="thead bg-secondary text-whitesmoke">
        <tr>
        <th scope="col">TÍTULO DE EGRESO</th><th scope="col">ORIENTACIÓN</th><th scope="col">GRADO DE EGRESO</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td rowspan="6">TECNICATURA EN RECURSOS PARA LA DEFENSA</td>
        </tr>
        <tr>
            <td>INFANTERÍA</td>
            <td>CABO</td> 
        </tr>
        <tr>
            <td>CABALLERÍA</td>
            <td>CABO</td> 
        
        </tr>
        <tr>
            <td>ARTILLERÍA</td>
            <td>CABO</td> 

        </tr>
        <tr>
            <td>INGENIEROS</td>
            <td>CABO</td> 

        </tr>
        <tr>
            <td>COMUNICACIONES</td>
            <td>CABO</td> 

        </tr>
    </tbody>
                
                
</table>
<h5>DURACIÓN DE LA CARRERA: DOS (2) años. RÉGIMEN: INTERNADO</h5>



</section>