<!-- <script>
window.location.replace("index.php");
</script> -->
<?php

$fechaStringIncorp = "2021-05-15 00:00:00";
$fechaIncorp = date($fechaStringIncorp);

date_default_timezone_set('America/Argentina/Buenos_Aires');
$fechaActual = date("Y-m-d H:i:s", strtotime('+0 hours'));


if($fechaIncorp < $fechaActual){

    if(isset($_SESSION['usuario'])){
        echo"<h2> Usted ya se encuentra inscripto.</h2>";
    }
    else{
    ?>
        <section id="cuerpo">
                <div class=" p-2">
                    <div style="background-color:rgb(255,255,255,0.5)">
                        <img src="assets/img/postulante.jpg" style="object-fit: cover;height:250px" class="card-img-top" alt="...">
                        <div class="card-body">

                        
                            <p class="card-text">Complete el formulario de inscripción para ingresar a la Escuela de Suboficiales del Ejército "Sargento Cabral".</p>
                            <!-- <p><strong>La inscripción finalizará el 10 de julio.</strong></p> -->
                            <h5>Tener en cuenta: </h5>
                            <p>
                            El formulario de inscripción es considerado una <b>DECLARACIÓN JURADA</b>.
                            Se debe tener en cuenta que si se verifica que la información es falsa, se incurre en una 
                            falta con consecuencias irreversibles para su inscripción.
                            La declaración es de carácter personal. No puede declarar en nombre de terceros.
                            </p>
                            <form action="index.php?p=insc_postulante" method="post">
                                <div class="form-check pb-3">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck10" required>
                                    <label class="form-check-label" for="defaultCheck10">
                                        Acepto en total conformidad
                                    </label>
                                </div>
                                <input type="submit" class="btn btn-primary" value="Ingrese aquí"/>
                            </form>
                        
                        <!-- <h1 class="display-2 text-center d-none d-md-block"><b>PRÓXIMAMENTE</b></h1>
                        <h2 class="text-center d-block d-md-none">PRÓXIMAMENTE</h1>
                        <h1 class="text-center d-none d-md-block">ABRIL 2021</h1>
                        <h3 class="text-center d-block d-md-none">ABRIL 2021</h1> -->
                        
                        </div>
                    </div>
                </div>


            <div class="text-center">
            <br><a href="ingreso.php" class="btn btn-lg btn-primary">CONSULTAR MI ESTADO</a>
            <br><small style="color:#FF4848;">Sólo para aquellos que ya se han inscripto</small>
            </div>
        </section>
    <?php
    }
}
else{
    
    echo'
    <section id="cuerpo" class="text-center">
        <div class="p-5"></div>
        <iframe id="oak-embed" width="100%" style="border: 0;" src="https://embed-countdown.onlinealarmkur.com/es/#2021-05-15T00:00:00"></iframe>
        <div class="p-5"></div>
    </section>   
            
            ';
}
?>