<?php
include('../assets/func/funciones.php');
include('../assets/func/class.DBListaAprobados.php');
session_start();


$sender_dni = stripslashes($_POST["dni"]);
$response = $_POST["g-recaptcha-response"];

$url = 'https://www.google.com/recaptcha/api/siteverify';
$data = array(
    'secret' => '6LeHvckUAAAAAFIQor64bu9tlAUdct1AePA2ptbW',
    'response' => $_POST["g-recaptcha-response"]
);
$options = array(
    'http' => array (
        'method' => 'POST',
        'content' => http_build_query($data)
    )
);
$context  = stream_context_create($options);
$verify = file_get_contents($url, false, $context);
$captcha_success=json_decode($verify);
//SI EL CAPTCHA ES INCORRECTO NO AVANZO
if ($captcha_success -> success == false) {
    echo"Error";
}
//SI EL CAPTCHA ES CORRECTO AVANZO
else{
    $con = conexion();

    $listaAprobados = new DBListaAprobados($con);
    $postulante = $listaAprobados -> obtenerPostulante($_POST);
    
    $_SESSION['contenido'] = $postulante;    
    
    header('Location: ../index.php?p=listaaprobados');
}

?>
