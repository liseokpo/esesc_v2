
<nav class="navbar navbar-expand-lg navbar-light bg-secondary sticky-top font-weight-bold" style="font-family:'Arial'">
    <a class="navbar-brand pull-left d-lg-none" href="?p=inicio">
        <img src="assets/img/cabecera/LOGO ESESC.png" width="50" height="50" alt="">
        
    </a>
    <button class="navbar-toggler pull-right border-0" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">      
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown">
        <ul class="navbar-nav text-center">
            <li class="nav-item">
                <a class="nav-link text-whitesmoke" href="?p=inicio"><img src="assets/img/menu/home1.png"></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link text-whitesmoke dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" data-hover="dropdown" aria-haspopup="true" aria-expanded="false">
                    INSTITUCIONAL
                </a>
                <div class="dropdown-menu bg-secondary rounded-0" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=historia">Historia</a>
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=eldirector">El Director</a>
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=misionvision">Misión y Visión</a>
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=valores">Valores</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link text-whitesmoke dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" data-hover="dropdown" aria-haspopup="true" aria-expanded="false">
                    INSCRIPCIÓN
                </a>
                <div class="dropdown-menu bg-secondary rounded-0" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=requisitos">Requisitos</a>
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="assets/docs/guia_ingreso.pdf" target="_blank">Guía de ingreso</a>
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=instructivos#como_inscribirme">¿Cómo inscribirme?</a>
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=instructivos#como_abonar">¿Cómo abonar?</a>
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=inscripcion">Quiero inscribirme</a>
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=preguntas_frecuentes">Preguntas frecuentes</a>
                    
                </div>

            </li>
            <li class="nav-item dropdown">
                <a class="nav-link text-whitesmoke dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" data-hover="dropdown" aria-haspopup="true" aria-expanded="false">
                    CARRERAS
                </a>
                <div class="dropdown-menu bg-secondary rounded-0" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=armas">Armas</a>
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=especialidades">Especialidades y servicios</a>
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=art11">Artículo 11</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link text-whitesmoke" href="?p=contacto">CONTACTO</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-whitesmoke" href="ingresar.php">INGRESAR</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-whitesmoke" href="?p=140_educando_en_valores"><img src="assets/img/menu/140anios.png"></a>
            </li>
            <!-- <li class="nav-item">
                <a class="nav-link text-whitesmoke border bg-danger ml-1" href="?p=paso2#actacompromiso">ACTA DE COMPROMISO</a>
            </li> -->
        </ul>
    </div>
</nav>
