<div id="cuerpo" style="background:url('assets/img/util/140_bg.jpg');background-repeat: repeat-y;background-repeat: repeat-x;background-size:contain" >
<br>
<!-- <img src="assets/img/util/140_bg.jpg" class="mw-100" > -->
<br><br>

    <div class="rounded mw-100 p-1" style="background-color:rgba(255, 255, 255, 1);">
        
        <div class="text-center">
            <img src="assets/img/pie/logo.png" style="height:200px">
            <h3><strong>140 años Educando en Valores</strong></h3>
        </div>

        <div class="row justify-content-center mx-auto mw-100">
            <div class="col-12 mw-100">
                <div class="row mw-100">
        <?php

        $fechaStringIncorp = "2021-03-25 23:59:59";
        $fechaIncorp = date($fechaStringIncorp);

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fechaActual = date("Y-m-d H:i:s", strtotime('+0 hours'));

        if($fechaIncorp < $fechaActual ){
        
            echo'<p>En este especial de 140 años, queremos presentarles una galería de imágenes de nuestra historia, en la que verán fotos obtenidas de archivos históricos así como fotos que nuestros seguidores nos han enviado.</p>';
            echo'<p></p>';
            $directorio = "./assets/img/140anios/img/";
            $directorio_txt = "./assets/img/140anios/txt/";
            $ficheros1  = array_diff(scandir($directorio,1), array('..', '.'));

            echo'<div class="w-100 mw-100">';
            foreach($ficheros1 as $fichero){
                $remoteImage = $directorio . "/" .$fichero;
                $path_parts_img = pathinfo($remoteImage);

                $remoteTxt = $directorio_txt. "/" .$path_parts_img["filename"] . ".txt";
                if(file_exists($remoteTxt)){
                    echo'<a href="'.$remoteImage.'" data-toggle="lightbox" data-gallery="example-gallery" data-footer="'.file_get_contents($remoteTxt).'" class="col-sm-6 col-6 col-md-3 mb-2 text-center">
                        <img src="'.$remoteImage.'" class="img-fluid figure-img" style="height: 150px;width:200px;object-fit: cover;object-position:50% 10%;max-width:35%;">
                    ';
                    
                    }
                else{
                    echo'<a href="'.$remoteImage.'" data-toggle="lightbox" data-gallery="example-gallery" class="col-sm-6 col-6 col-md-3 mb-2 text-center">
                    <img src="'.$remoteImage.'" class="img-fluid figure-img" style="height: 150px;width:200px;object-fit: cover;object-position:50% 10%;max-width:35%;">
                ';
                }
                echo'</a>';
                
            }
            echo'</div>';
            
        }
        else{
            echo'
                <div class="w-100 position-absolute" style="height:150px"></div>
            ';
            echo'<iframe id="oak-embed" class=" w-100 user-select-none " style="border: 0;" src="https://embed-countdown.onlinealarmkur.com/es/#2021-03-26T00:00:00" ></iframe>';
        }
        ?>

                    </a>
                </div>
            </div>
        </div>

    </div>
</div>
<script>

    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });



    function refreshAt(year, month, day, hours, minutes, seconds) {
        var now = new Date();
        var then = new Date();

        then.setFullYear(year);
        then.setMonth(month - 1); //0-11 January to December
        then.setDate(day);
        then.setHours(hours);
        then.setMinutes(minutes);
        then.setSeconds(seconds);

        var timeout = (then.getTime() - now.getTime());

        if(timeout > 0 && timeout < 2147483640){
            setTimeout(function() { window.location.reload(); }, timeout);
        }
    }

    //refresh the page at 23:00:00
    refreshAt(2021, 3, 26, 00, 00, 0);
</script>
