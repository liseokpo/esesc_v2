<section id="cuerpo" style="color:#444444">
        <h1  class="text-center text-shadow">Preguntas frecuentes <i class="far fa-question-circle"></i></h1>
        <br><br>
        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                    
                        <div class="row">
                            <div class="col text-left">
                                INFORMACIÓN BASICA DE LA INSCRIPCIÓN ONLINE
                            </div>
                                
                            <div class="col-auto my-auto">                        
                                <i class="fas fa-caret-down fa-2x"></i>
                            </div>
                        </div>
                    </button>
                </h2>
                </div>

                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul>
                            <li>
                                <b>¿Cómo realizo mi inscripción?</b><br>
                                La inscripción se realiza vía online a través de nuestro sitio web oficial.
                                Hacé click <a href="?p=inscripcion">acá</a> para inscribirte.
                            </li>
                            <br>
                            <li>
                                <b>¿Cuáles son las etapas del proceso de inscripción?</b><br>
                                INSCRIPCIÓN 2021: El paso a paso<br>
                                <ul>
                                    <li>Ingresá a nuestro sitio web y completá el formulario de inscripción. Una vez hecho obtendrás un usuario y una contraseña para conocer tu estado de inscripción.</li>
                                    <li>Aboná el derecho a examen a través de e-Recauda</li>
                                    <li>Cargá los documentos requeridos como foto del DNI, comprobante de pago y ticket de pago</li>
                                </ul>
                            </li>
                            <li><b>Para mayor información mirá nuestro <a href="https://www.youtube.com/watch?v=M28Xf7MKrJo">INSTRUCTIVO INSCRIPCIÓN ONLINE</a> </b></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <div class="row">
                            <div class="col text-left">
                                REQUISITOS
                            </div>
                                
                            <div class="col-auto my-auto">                        
                                <i class="fas fa-caret-down fa-2x"></i>
                            </div>
                        </div>
                    </button>
                </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul>
                            <li>
                                <b>¿Cuáles son los requisitos de ingreso?</b><br>
                                Hacé click <a href="?p=requisitos">acá</a> para conocer los requisitos de ingreso.
                            </li>
                            <br>
                            <li>
                                <b>¿Cuál es el periodo de la inscripción?</b><br>
                                La inscripción finaliza el 31 de octubre del 2021.
                            </li>
                            <br>
                            <li>
                                <b>¿Hay preinscripción?</b><br>
                                ¡No! Este año solo se realizará una inscripción.
                            </li>
                            <br>
                            <li>
                                <b>¿Si soy de Formosa, me puedo inscribir?</b><br>
                                ¡Así es! La inscripción se encuentra habilitada para todo el país.
                            </li>
                            <br>
                            <li>
                                <b>¿Qué nivel de estudios necesito?</b><br>
                                Necesitás acreditar estudios de nivel secundario completos, con hasta un máximo de 1 (una) materia adeudada que deberás rendir antes del mes de agosto del año de ingreso.
                            </li>
                            <br>
                            <li>
                                <b>Estoy cursando el ultimo año, ¿me puedo inscribir?</b><br>
                                ¡Es posible! Pero recordá que al momento de incorporarte debés tener como máximo 1 (una) materia pendiente. <br>
                                Vas a poder inscribirte solo si tenés: <br>
                                <ul>
                                    <li>Secundario completo.</li>
                                    <li>Secundario incompleto, adeudando 1 (una) materia.</li>
                                    <li>Secundario incompleto, adeudando más de 1 (una) materia.</li>
                                    <li>Secundario incompleto, cursando el último año de este.</li>
                                </ul>
                            </li>
                            <br>
                            <li>
                                <b>Tengo 23 años y no puedo poner mi fecha de nacimiento en el formulario ¿qué hago?</b><br>
                                Es posible que excedas el <a href="?p=requisitos">limite de edad</a> para ingresar al instituto.
                            </li>
                            <br>
                            <li>
                                <b>¿Qué edad debo tener para ingresar?</b><br>
                                Podés ingresar si tenés entre 18 y 23 años para el 1 de febrero del año de ingreso.
                            </li>
                            <br>
                            <li>
                                <b>¿Se aceptan postulantes con tatuajes?</b><br>
                                Hacé click <a href="http://localhost/esesc_v2/esesc/assets/docs/tatuajes_permitidos.pdf">acá</a> para conocer los tatuajes permitidos.
                            </li>
                            <br>
                            <li>
                                <b>¿Me puede inscribir con dispensa por edad?</b><br>
                                No es posible, ya que el instituto no acepta dispensa por edad.
                            </li>
                            <br>
                            <li>
                                <b>¿Puedo reincorporarme?</b><br>
                                Este año el instituto no acepta reincorporados.
                            </li>
                            <br>
                            <li>
                                <b>Soy de Brasil pero vivo hace 10 años en Argentina. ¿Puedo inscribirme?</b><br>
                                ¡Así es! Podés hacerlo si contás con DNI argentino.
                            </li>
                            <br>
                            <li>
                                <b>Soy soltero pero tengo hijos ¿Puedo inscribirme?</b><br>
                                ¡Podés hacerlo! Pero, ¡recordá! tu estado civil debe ser soltero.
                            </li>
                            <br>
                            <li>
                                <b>Para mayor información sobre requisitos, mirá nuestros <a href="https://www.youtube.com/watch?v=M28Xf7MKrJo">REQUISITOS DE INGRESO</a></b>
                            </li>
                        </ul>                
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingThree">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <div class="row">
                            <div class="col text-left">
                                FORMULARIO
                            </div>
                                
                            <div class="col-auto my-auto">                        
                                <i class="fas fa-caret-down fa-2x"></i>
                            </div>
                        </div>
                    </button>
                </h2>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul>
                            <li>
                                <b>Tengo el DNI en trámite, ¿puedo enviar la foto del comprobante?</b><br>
                                Es necesario que envíes una fotografía del DNI original. Recorda que tenés tiempo de hacerlo hasta el 31 de octubre del 2021.
                            </li>
                            <br>
                            <li>
                                <b>¿Dónde veo las carreras que puedo elegir?</b><br>
                                ¡Contamos con una amplia oferta académica! Visita nuestras carreras de <a href="?p=armas">Armas</a> y <a href="?p=especialidades">Especialidades y Servicios</a> y conocé todas nuestras carreras.
                            </li>
                            <br>
                            <li>
                                <b>¿Qué son los centros de preselección que tengo que elegir en el formulario?</b><br>
                                El centro de preselección es la unidad destinada para que rindas tus exámenes de admisión ¡Podés elegir el que se encuentre más cerca de tu domicilio!
                            </li>
                            <br>
                            <li>
                                <b>Cuando quiero re-escribir el mail me dicen que no coincide pero cuando los reviso están iguales</b><br>
                                Es probable que no coincidan debido a que al final de alguno de los dos mails hay un espacio de más.
                            </li>
                            <br>
                            <li>
                                <b>Anoté mal algo y quiero cambiarlo, ¿puedo hacerlo?</b><br>
                                ¡Así es! Para solicitar la modificación de alguno de tus datos deberás comunicarte, telefónicamente, con el instituto indicando tu número de DNI y el motivo. 
                            </li>
                            <br>
                            <li>
                                <b>Soy soldado y no se el codigo de mi unidad</b><br>
                                Podes consultarlo en la Oficina de Personal de tu unidad o también en tu Recibo de Sueldo figura como Unidad. Tenés tiempo hasta el cierre de la inscripción.
                            </li>
                            <br>
                            <li>
                                <b>Llené el formulario pero me olvide la contraseña que me dieron. ¿Qué hago?</b><br>
                                Hacé click <a href="?p=recuperarcontrasenia">acá</a> para recuperar tu contraseña y ¡listo!
                            </li>
                            <br>
                            <li>
                                <b>DOCUMENTACIÓN A CARGAR</b><br>
                                La documentación a cargar es la siguiente: 
                                <ul>
                                    <li>DNI anverso</li>
                                    <li>DNI reverso</li>
                                    <li>Comprobante/boleta de pago</li>
                                    <li>Ticket de pago</li>
                                </ul>
                            </li>
                            <br>
                            <li>
                                <b>¿Apenas completo el formulario tengo que abonar o hasta cuándo tengo tiempo? /<br> 
                                Llené el formulario pero, ¿puedo enviar la documentación más tarde?</b><br>
                                Tenes tiempo hasta el cierre de la inscripción, el 31 de octubre.
                            </li>
                            <br>
                            <li>
                                <b>Me sale un error en la página cuando quiero cargar mis imágenes</b><br>
                                Es probable que las imágenes que desea cargar exceda el tamaño permitido.
                            </li>
                            <br>
                            <li>
                                <b>¿Cómo puedo achicar las imagenes a enviar?</b><br>
                                Los archivos cargados no deben exceder los 250kb. Si tus imágenes exceden el tamaño podes: 
                                <ul>
                                    <li>Ingresar al sitio web www.achicarimagenes.com.ar, seleccionar las imágenes que queres achicar y descargarlas. Luego, adjunta dichas imágenes en nuestro sitio web. </li>
                                    <li>Tomar las fotografías desde WhatsApp y corroborar que se almacenen en tu galería; esto permitirá que tu imagen sea de un tamaño menor a 250kb. Luego, adjunta dichas imágenes en nuestro sitio web. </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingFour">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        <div class="row">
                            <div class="col text-left">
                                PAGO
                            </div>
                                
                            <div class="col-auto my-auto">                        
                                <i class="fas fa-caret-down fa-2x"></i>
                            </div>
                        </div>
                    </button>
                </h2>
                </div>
                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul>
                            <li>
                                <b>¿Qué es la boleta de pago boleta de pago y cuándo la realizo?</b><br>
                                La boleta de pago se genera en el <a href="https://erecauda.mecon.gov.ar/erecauda/" target="_blank">sitio web e-Recuada</a> ¡Recordá! que sin ella no podrás abonar tu inscripción 
                            </li>
                            <br>
                            <li>
                                <b>¿Cuánto es el monto a pagar de la inscripción?</b><br>
                                El derecho a examen tiene un valor de $3000.
                            </li>
                            <br>
                            <li>
                                <b>¿Para qué es el pago de la inscripción?</b><br>
                                El pago equivale al derecho de rendir los exámenes de admisión.
                            </li>
                            <br>
                            <li>
                                <b>¿Qué es el ticket de pago?</b><br>
                                El ticket de pago es aquel que recibis una vez abonada la inscripción.
                            </li>
                            <br>
                            <li>
                                <b>¿Cuáles son las entidades en donde puedo pagar?</b><br>
                                Hacé click <a href="https://dgsiaf.mecon.gov.ar/wp-content/uploads/TE_EREC_ERECAUDA_Entidades_Habilitadas_para_el_Pago.pdf" target="_blank">acá</a> y conocé las entidades de pago habilitadas por e-Recauda
                            </li>
                            <br>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="headingFive">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                    
                        <div class="row">
                            <div class="col text-left">
                                FINALIZADA LA CARGA DE ARCHIVOS
                            </div>
                                
                            <div class="col-auto my-auto">                        
                                <i class="fas fa-caret-down fa-2x"></i>
                            </div>
                        </div>
                    </button>
                </h2>
                </div>
                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul>
                            <li>
                                <b>¿Cuánto tiempo tarda en aprobarme la inscripción?</b><br>
                                Una vez que se acredite el pago, será aprobada en un rango de 10 a 15 días. ¡Sea paciente!
                            </li>
                            <br>
                            <li>
                                <b>¿Me llega alguna notificación cuando me aprueban la documentación?</b><br>
                                ¡No! Ingresá <a href="ingreso.php">acá</a> con tu usuario y contraseña para hacer el seguimiento de tu inscripción.
                            </li>
                            <br>
                            <li>
                                <b>No encuentro los examenes intelectuales y físicos en la web</b><br>
                                Sólo aquellos postulantes que hayan aprobado la inscripción podrán acceder al contenido para los exámenes de admisión.
                            </li>
                            <br>
                            <li>
                                <b>¿Cuándo me dan el número de inscripto?</b><br>
                                El número de inscripto es generado una vez tu inscripción haya sido aprobada.
                            </li>
                            <br>
                            <li>
                                <b>¿Ya puedo comenzar con los estudios medicos?</b><br>
                                Podés comenzar con los estudios médicos solo si aprobaste la inscripción.
                            </li>
                            <br>
                            <li>
                                <b>¿Cuándo tenemos que presentar los estudios médicos?</b><br>
                                Deberás entregar los estudios médicos una vez que te presentes a rendir los exámenes de admisión.
                            </li>
                            <br>
                            <li>
                                <b>¿Cuándo son las fechas de los exámenes de admisión?</b><br>
                                Aún no se encuentran vigentes la fechas para rendir los exámenes de admisión, pronto las estaremos comunicando.
                            </li>
                            <br>
                            <li>
                                <b>¿Cuáles son los exámenes de ingreso?</b><br>
                                Los exámenes de admisión constan de cuatro partes:
                                <ul>
                                    <li>Intelectuales</li>
                                    <li>Médicos</li>
                                    <li>Físicos</li>
                                    <li>Psicotécnicos</li>
                                </ul>
                            </li>
                            <br>
                            <li>
                                <b>¿El DUPIE y el resto de los documentos cuando se entregan?</b><br>
                                La documentación solicitada deberá ser presentada una vez que hayas sido citado para ingresar en las vacantes.
                            </li>
                            <br>
                            <li>
                                <b>¿Los estudios médicos se realizan por cuenta propia?</b><br>
                                ¡Así es! Es responsabilidad de cada postulante. Podés realizarlos tanto en un hospital público como en un centro médico privado. 
                            </li>
                            <br>
                            <li>
                                <b>Una vez que estamos inscriptos, ¿cuál es el siguiente paso?</b><br>
                                Una vez aprobada tu inscripción deberás continuar con el proceso de incorporación, el cual consta de rendir los exámenes de admisión y presentar la documentación solicitada.
                            </li>
                            <br>
                        </ul>
                    </div>
                </div>
            </div>    
            <div class="card ">
                <div class="card-header" id="headingSix">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block text-left collapsed " type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                        <div class="row">
                            <div class="col text-left">
                                PREGUNTAS GENERALES
                            </div>
                                
                            <div class="col-auto my-auto">                        
                                <i class="fas fa-caret-down fa-2x"></i>
                            </div>
                        </div>
                    </button>
                        
                </h2>
                </div>
                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
                    <div class="card-body">
                        <ul>
                            <li>
                                <b>¿La inscripción es para ser soldados?</b><br>
                                La ESESC forma futuros suboficiales del Ejército Argentino, no soldados voluntarios.
                            </li>
                            <br>
                            <li>
                                <b>¿La inscripción es para ingresar en qué año?</b><br>
                                La inscripción es para el ingreso en el año 2022.
                            </li>
                            <br>
                            <li>
                                <b>¿Los aspirantes recibien una ayuda economica?</b><br>
                                Los aspirantes reciben una beca económica para los gastos personales. Además, a partir de este año tendrán la oportunidad de inscribirse en las becas estratégicas Progresar/Belgrano.
                            </li>
                            <br>
                            <li>
                                <b>¿Qué es el IMC? ¿Cómo se calcula?</b><br>
                                El IMC es una medida del peso para detectar sobrepeso o insuficiencia de peso a través del Índice de Masa Corporal.<br> Ingresá <a href="https://www.texasheart.org/heart-health/heart-information-center/topics/calculadora-del-indice-de-masa-corporal-imc/" target="_blank">acá</a> para calcular tu IMC.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>                    

        </div>
        <br><br>

</section>
