    <section id="cuerpo" style="color:#444444">
        <h1  class="text-center text-shadow">Contacto</h1>
        <ol style="text-align: center;">
            <ul class="contacto">
                <li>
                    <i class="redes"></i><br/>
                    <h5>Redes sociales</h5>
                    <p>
                    <a href="https://www.facebook.com/pg/EscueladeSuboficialesSargentoCabral/"><img src="assets/img/contacto/fb.png" style="width:40px;height:40px;" alt=""></a>
                    <a href="https://www.instagram.com/escueladesuboficiales.esesc/"><img src="assets/img/contacto/ig.png" style="width:40px;height:40px;" alt=""></a>
                    <a href="https://www.youtube.com/channel/UCGXnimUHuVG1_yWWWoqiz2g"><img src="assets/img/contacto/yt.png" style="width:40px;height:40px;" alt=""></a>
                    <a href="https://twitter.com/esesc_argentina"><img src="assets/img/contacto/tw.png" style="width:40px;height:40px;" alt=""></a>
                    </p>
                </li>
            </ul>
            <ul class="contacto">
                <li>
                    <i class="cont"></i><br/>
                    <h5>Teléfonos</h5>
                    <p>
                        <li>011 4664 1527</li>
                        <li>011 4664 5491</li>
                        <li>011 4664 0703</li>
                        <li>011 4664 5442</li>
                    </p>
                </li>
            </ul>
            <ul class="contacto">
                <li>
                    <i class="ubi"></i><br/>
                    <h5>Ubicación</h5>
                    <p>
                        <li>RP 202 S/N CP:1659<br/> Campo de Mayo</li>
                    </p>
                </li>
            </ul>
            <ul class="contacto">
                <li>
                    <i class="mails"></i><br/>
                    <h5>E-mails</h5>
                    <p>
                        <li><a href="mailto:esesc@ejercito.mil.ar" style="color:#444444">esesc@ejercito.mil.ar</a></li>
                        <li><a href="mailto:incorporacionesesc@ejercito.mil.ar" style="color:#444444">incorporacionesesc@ejercito.mil.ar</a></li>
                    </p>
                </li>
            </ul>
        </ol>
        <br>
        <h1  class="text-center text-shadow">¿Cómo llegar?</h1>
        <div class="text-center p-2">
            <iframe class="mw-100 w-100" height="400" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3287.0136807767017!2d-58.697600185198034!3d-34.52788136099552!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcbd1e46435711%3A0x4a5fa6d037eb5c0d!2sEscuela%20de%20Suboficiales%20del%20Ej%C3%A9rcito%20Sargento%20Cabral!5e0!3m2!1ses!2sar!4v1607428652173!5m2!1ses!2sar" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </section>