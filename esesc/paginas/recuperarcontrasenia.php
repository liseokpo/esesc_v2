<section id="cuerpo" style="min-height:25vh">
<?php
//4) El codigo de recuperación debe estar en la url, verificamos con $_GET["crec"] y si esta en nuestra
//bd, proseguimos
//SI EN LA URL HAY UN CODIGO, VERIFICAMOS



if(isset($_GET["crec"])){

    $pdo = conexion();
    
    $DBCandidato2 = new DBCandidato($pdo);
    

    //1)Que exista
    if($recuperar = $DBCandidato2 -> recuperarContrasenia($_GET["crec"])){

        $idsCodigo = $DBCandidato2 -> obtenerIdUsuariosCodigo($_GET["crec"]);
        $tipo = "";
        $id_codigorecuperar = 0;
        

        if($idsCodigo["id_candidato"] != null){
            $id_codigorecuperar = $idsCodigo["id_candidato"];
            $tipo = "candidato";
        }
        else{
            $id_codigorecuperar = $idsCodigo["id_usuario"];
            $tipo = "gestor";
        }

        

        //2) Que no esté expirado
        $current = date("Y-m-d H:i:s");
        if($current > $recuperar['fecha_expiracion'])
        {
            echo'
            
            <div class="container-fluid" style="height:25vh">
                <div class="row align-items-center h-100">
                    <div class="col-sm  h-100 w-100">
                        <div class="h-100 d-flex  align-content-center flex-wrap">
                            <div class="alert alert-warning m-0 mx-auto">El código de recuperación de contraseña ha expirado. Por favor solicite uno nuevo.</div>
                        </div>
                    </div>
                </div>
            </div>';
        }
        else{
            //3) Que no esté usado
            if($recuperar['usado'] == 1){

                echo'
                <div class="container-fluid" style="height:25vh">
                    <div class="row align-items-center h-100">
                        <div class="col-sm h-100 w-100">
                            <div class="h-100 d-flex  align-content-center flex-wrap">
                                <div class="alert alert-warning m-0 mx-auto">El código de recuperación de contraseña ya ha sido usado. Por favor solicite uno nuevo.</div>
                            </div>
                        </div>
                    </div>
                </div>
                ';
            }
            else{

                //4)Si el código no está expirado, no está usado, entonces puedo cambiar la contraseña.
                //Al cambiar la contraseña debo cambiar el código a usado.
?>
                <form action="paginas/metarecuperarcontrasenia.php" class="text-center" method="post">
                
                <div class="form">
                    <div class="col-lg-4 mt-5 mb-2 mx-auto">
                        <div class="input-group">
                            <input type="password" class="form-control" name="recupass" id="recupass" placeholder="Contraseña nueva" required>  
                            <div class="input-group-append">
                                <button id="show_password2" class="btn btn-primary" type="button" onclick="mostrarPassword('recupass','icono2')"> <span id="icono2" class="fa fa-eye-slash icon"></span> </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <div class="col-lg-4 mb-4 mx-auto">
                        
                        <div class="input-group">
                            <input type="password" class="form-control" name="rerecupass" id="rerecupass" placeholder="Repetir contraseña nueva" onfocus="validateMail(document.getElementById('recupass'), this, 'Las contraseñas no coinciden');" oninput="validateMail(document.getElementById('recupass'), this,'Las contraseñas no coinciden');">
                            <div class="input-group-append">
                                <button id="show_password3" class="btn btn-primary" type="button" onclick="mostrarPassword('rerecupass','icono3')"> <span id="icono3" class="fa fa-eye-slash icon"></span> </button>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" value="<?php echo $id_codigorecuperar; ?>" id="idusuariorec" name="idusuariorec">
                <input type="hidden" value="<?php echo $tipo; ?>" id="tipousuariorec" name="tipousuariorec">
                    
                <div class="form">
                    <div class="col-lg-2 mb-4 mx-auto p-0">
                        <input class="form-control btn btn-primary" type="submit" value="Cambiar contraseña">
                    </div>
                </div>
                </div>

            </form>
            
<?php

            }
        }
    }
    else{

        echo'
            
        <div class="container-fluid" style="height:25vh">
            <div class="row align-items-center h-100">
                <div class="col-sm  h-100 w-100">
                    <div class="h-100 d-flex  align-content-center flex-wrap">
                        <div class="alert alert-danger m-0 mx-auto">Código de restauración inválido o expirado, por favor vuelva a solicitar un código de recuperación.</div>
                    </div>
                </div>
            </div>
        </div>';
    }
}

else{
?>


<form action="paginas/metarecuperarcontrasenia.php" method="post">
    <h4 class="text-center">Utilice el email de registro para recuperar su contraseña</h4>
        <div class="form">
            <div class="col-lg-4 col-sm-auto   mb-4 mt-4 mx-auto">
                <input type="email" class="form-control" id="recupemail" name="recupemail" placeholder="Email" required>
            </div>
        </div>
        <div class="form text-center">
            <div class="col-lg-3  mx-auto">
                <input class="form-control btn btn-primary" type="submit" value="Recuperar Contraseña">
            </div>
            <?php
            if(isset($_GET['mensaje']) && isset($_GET['c'])){

                switch ($_GET['c']) {
                    case 1:
                        $color = "alert-info";
                        break;
                    case 2:
                        $color = "alert-warning";
                        break;
                    case 3:
                        $color = "alert-danger";
                        break;
                    case 4:
                        $color = "alert-success";
                        break;
                    default:
                        $color = "";
                        break;
                }
                echo"<br/><div class='d-inline-block m-0 text-center alert ".$color."'>".$_GET['mensaje'];
            }            
                
            ?>
        </div>
        </div>
    </form>

<?php
}
?>
</section>