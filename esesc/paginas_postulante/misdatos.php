<?php
if(!isset($_SESSION['usuario'])){
    header('Location: ../index.php');
}
else{
    $idcandidato = $_SESSION['usuario'];
    $conexion = conexion();
    $DBCandidato = new DBCandidato($conexion);
    $candidato = $DBCandidato -> obtenerCandidatoporId($idcandidato);
    $domicilio = $DBCandidato -> obtenerDomicilio($idcandidato);
    $plancarrera = $DBCandidato -> obtenerPlanCarrera($idcandidato);
    $soldvol = $DBCandidato -> obtenerSoldadoVoluntario($idcandidato);
    $secundario = $DBCandidato -> obtenerSecundario($idcandidato);
    $centro_preseleccion = $DBCandidato -> obtenerCentroPreseleccion($idcandidato);

    $DBEdicion = new DBEdicion($conexion);

    $existeediciondatospersonales = false;
    $existeedicionplancarrera = false;
    $existeedicionsoldvol = false;
    $existeedicionsecundario = false;
    
    $existeEdicionCandidato = $DBEdicion -> existeEdicionEnCandidato($idcandidato);



    if($existeEdicionCandidato){

        $DBEdicion -> validarEdicion($idcandidato,"DATOS_PERSONALES");
        $DBEdicion -> validarEdicion($idcandidato,"PLAN_CARRERA");
        $DBEdicion -> validarEdicion($idcandidato,"SOLD_VOL");
        $DBEdicion -> validarEdicion($idcandidato,"SECUNDARIO");

        if($DBEdicion -> existeEdicion($idcandidato,"DATOS_PERSONALES")){
            $existeediciondatospersonales = true;
        }
        if($DBEdicion -> existeEdicion($idcandidato,"PLAN_CARRERA")){
            $existeedicionplancarrera = true;
        }
        if($DBEdicion -> existeEdicion($idcandidato,"SOLD_VOL")){
            $existeedicionsoldvol = true;
        }
        if($DBEdicion -> existeEdicion($idcandidato,"SECUNDARIO")){
            $existeedicionsecundario = true;
        }
    }
?>
<section id="cuerpo">
    <?php

    if($existeediciondatospersonales || $existeedicionplancarrera || $existeedicionsecundario || $existeedicionsoldvol ){
        echo'<h5 class="text-center">'.$candidato["nombres"].' '.$candidato["apellidos"].'</h5>
            <h5>Se habilitó la edición de algunos de sus datos.<br><br>
            A partir de este momento tiene 48hs y por única vez habilitada la edición.<br>
            Antes de actualizar la información, verifique que los datos cargados sean los correctos.
            Gracias</h5>';

            if(isset($_GET['m'])){
                switch ($_GET['m']) {
                    case 'errordni':
                        echo'<div class="alert alert-danger">El DNI que usted ingresó se encuentra registrado. Intente nuevamente o contáctese con el instituto.</div>';
                        break;
                    case 'errormail':
                        echo'<div class="alert alert-danger">El email que usted ingresó se encuentra registrado. Intente nuevamente o contáctese con el instituto.</div>';
                        break;
    
                    default:
                        echo'';
                        break;
                }
            }

        echo'
            <form action="paginas_postulante/metamisdatos.php" method="post">
            <div class="jumbotron p-3">
        ';

        if($existeedicionsecundario){
            $_SESSION['secundario'] = 1;
        ?>
            <!-- FORMULARIO SECUNDARIO -->
            <h4>Estudio secundario</h4>
            <div class="alert alert-primary p-2" role="alert">
                Importante: Una vez haya aprobado el examen de admisión y tenga su fecha de incorporación, deberá acreditar el certificado  del <b>Secundario completo</b> o <b>Secundario incompleto</b>, adeudando UNA (1) materia sin excepciones.
            </div>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label for="plansecundario">Plan (*)</label>
                    <select class="custom-select" name="plansecundario" id="plansecundario" required>
                        <option value="<?php echo$secundario['plan']; ?>" selected><?php echo $secundario['plan']; ?>(Actual)</option>
                        <option value="TECNICO">TÉCNICO</option>
                        <option value="BACHILLER">BACHILLER</option>
                        <option value="FINES">FINES</option>
                    </select>
                </div>
               
                <div class="form-group col-md-6 pl-md-3">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="anio_cursado" id="exampleRadios1" value="COMPLETO" onchange="cambiarVisibilidad('debematerias', 'none');ponerMaterias(0);">
                        <label class="form-check-label" for="exampleRadios1">
                            Secundario completo
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="anio_cursado" id="exampleRadios2" value="INCOMPLETO" onchange="cambiarVisibilidad('debematerias', 'none');ponerMaterias(1);">
                        <label class="form-check-label" for="exampleRadios2">
                            Secundario incompleto, adeudando UNA (1)  materia.
                        </label>
                    </div>
                    <div class="form-check disabled">
                        <input class="form-check-input" type="radio" name="anio_cursado" id="exampleRadios3" value="INCOMPLETO" onchange="cambiarVisibilidad('debematerias', 'inline');ponerMaterias(2);">
                        <label class="form-check-label" for="exampleRadios3">
                            Secundario incompleto, adeudando más de UNA (1) materia.
                        </label>
                    </div>
                    <div class="form-check disabled">
                        <input class="form-check-input" type="radio" name="anio_cursado" id="exampleRadios4" value="EN_CURSO" onchange="cambiarVisibilidad('debematerias', 'none');ponerMaterias(0);">
                        <label class="form-check-label" for="exampleRadios4">
                            Secundario incompleto, cursando el último año del mismo.
                        </label>
                    </div>
                </div>
                
            </div>
            
            <div class="form-group" id="debematerias" style="display:none">
                    <label for="cantmateriassecundario">Indique la cantidad de materias adeudadas (*)</label>
                    <input type="number" class="form-control" id="cantmateriassecundario" name="cantmateriassecundario" min="0" placeholder="<?php echo$secundario["cant_materias_adeudadas"]."(Actual)"; ?>" required>
            
            </div>
            <br>
            <script>
            function ponerMaterias(numero){
                document.getElementById("cantmateriassecundario").value = numero;
            }
            
            </script>
            <div class="form-row">
                <div class="col">
                    <label for="tsecundario"><b>Título</b> con el que egresó/ Egresará (*)</label>
                    <input type="text" class="form-control text-uppercase" name="tsecundario" id="tsecundario" onchange="this.value = this.value.toUpperCase();" value="<?php echo$secundario["titulo"]; ?>" placeholder="<?php echo$secundario["titulo"]."(Actual)"; ?>" required>
                </div>       
            </div>
            <div class="form-group">
                <label for="nsecundario">Nombre del colegio/ Instituto (*)</label>
                <input type="text" class="form-control text-uppercase" name="nsecundario" id="nsecundario"  onchange="this.value = this.value.toUpperCase();" value="<?php echo$secundario["nombre"]; ?>" placeholder="<?php echo$secundario["nombre"]."(Actual)"; ?>" required>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="psecundario">Provincia del colegio/ Instituto (*)</label>
                    <input type="text" class="form-control text-uppercase" name="psecundario" id="psecundario" onchange="this.value = this.value.toUpperCase();" value="<?php echo$secundario["provincia"]; ?>" placeholder="<?php echo$secundario["provincia"]."(Actual)"; ?>" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="lsecundario">Localidad del colegio/ Instituto (*)</label>
                    <input type="text" class="form-control text-uppercase" name="lsecundario" id="lsecundario" onchange="this.value = this.value.toUpperCase();" value="<?php echo$secundario["localidad"]; ?>" placeholder="<?php echo$secundario["localidad"]."(Actual)"; ?>" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="dircolegio">Dirección del colegio/ Instituto (*)</label>
                    <input type="text" class="form-control text-uppercase" name="dircolegio" id="dircolegio" onchange="this.value = this.value.toUpperCase();" value="<?php echo$secundario["direccion"]; ?>" placeholder="<?php echo$secundario["direccion"]."(Actual)"; ?>" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="telcolegio">Teléfono del colegio/ Instituto (*)</label>
                    <input type="number" class="form-control text-uppercase" name="telcolegio" id="telcolegio" onchange="this.value = this.value.toUpperCase();" value="<?php echo$secundario["telefono"]; ?>" placeholder="<?php echo$secundario["telefono"]."(Actual)"; ?>" required>

                </div>
            </div>
            <hr>
            
            
        <?php      
        }
        if($existeedicionplancarrera){
            $_SESSION['plancarrera'] = 1;
        ?>

            <!-- FORMULARIO PLAN CARRERA -->
            <h4>Plan de Carrera (*)</h4>
            <div class="form-group">
                <h5><small class="form-text" style="color:#FF4848;">Importante: La elección final del Arma o Especialidad y Servicio se realiza una vez que el postulante está incorporado.</small></h5>

                <div class="form-group btn-group btn-group-toggle d-flex justify-content-center" id="plancarrera" data-toggle="buttons">            
                    <label class="btn btn-primary p-4">
                        <input type="radio" name="plancarrera" id="option1" value="ESPECIALIDADES" autocomplete="off" onchange="cambiarVisibilidad('planarmas','none');cambiarVisibilidad('planespecialidades', 'inline');prueba2()" required>ESPECIALIDADES
                    </label>
                    <label class="btn btn-primary p-4">
                        <input type="radio" name="plancarrera" id="option2" value="ARMAS" autocomplete="off" onchange="cambiarVisibilidad('planespecialidades','none');cambiarVisibilidad('planarmas','inline');prueba()">ARMAS
                    </label>
                </div>
            </div>

            <script>
            function prueba2(){
                $('#orientacionesp').attr('required','required');
                $('#centropreseleccionesp').attr('required','required');
            
                $('#orientacionarm').removeAttr('required');
                $('#centropreseleccionarm').removeAttr('required');
                $('#orientacionarm')[0].selectedIndex = 0;
                $('#centropreseleccionarm')[0].selectedIndex = 0;
            };
            function prueba(){
                $('#orientacionarm').attr('required','required');
                $('#centropreseleccionarm').attr('required','required');
                
                $('#orientacionesp').removeAttr('required');
                $('#centropreseleccionesp').removeAttr('required');
                $('#orientacionesp')[0].selectedIndex = 0;
                $('#centropreseleccionesp')[0].selectedIndex = 0;
            };
            </script>

            <div class="form-group" id="planespecialidades" style="display:none">

                <label for="orientacion">Elija orientación de especialidades (*)</label>
                <select class="custom-select mb-3" name="orientacion" id="orientacionesp">
                    <option value="" selected disabled>Seleccione una opción</option>
                    <option value="MÚSICO">MÚSICO</option>
                    <option value="CONDUCTOR MOTORISTA">CONDUCTOR MOTORISTA</option>
                    <option value="ENFERMERO">ENFERMERO</option>
                    <option value="MECÁNICO EN INFORMÁTICA">MECÁNICO EN INFORMÁTICA</option>
                    <option value="" disabled>ADMINISTRACIÓN</option>
                    <option value="OFICINISTA">OFICINISTA</option>
                    <option value="INTENDENCIA">INTENDENCIA</option>
                    <option value="" disabled>MECÁNICOS</option>
                    <option value="MECÁNICO MOTORISTA A RUEDA">MECÁNICO MOTORISTA A RUEDA</option>
                    <option value="MECÁNICO MOTORISTA ELECTRICISTA">MECÁNICO MOTORISTA ELECTRICISTA</option>
                    <option value="MECÁNICO MOTORISTA A ORUGA">MECÁNICO MOTORISTA A ORUGA</option>
                    <option value="MECÁNICO ARMERO">MECÁNICO ARMERO</option>
                    <option value="MECÁNICO DE MUNICIÓN Y EXPLOSIVOS">MECÁNICO DE MUNICIÓN Y EXPLOSIVOS</option>
                    <option value="MECÁNICO DE ARTILLERÍA">MECÁNICO DE ARTILLERÍA</option>
                    <option value="MECÁNICO DE INSTALACIONES">MECÁNICO DE INSTALACIONES</option>
                    <option value="MECÁNICO DE INGENIEROS">MECÁNICO DE INGENIEROS</option>
                    <option value="MECÁNICO DE AVIACIÓN">MECÁNICO DE AVIACIÓN</option>
                    <option value="MECÁNICO DE ÓPTICA Y APARATOS DE PRECISIÓN">MECÁNICO DE ÓPTICA Y APARATOS DE PRECISIÓN</option>
                    <option value="" disabled>ELECTRÓNICA</option>
                    <option value="MECÁNICO DE EQUIPO DE CAMPAÑA">MECÁNICO DE EQUIPO DE CAMPAÑA</option>
                    <option value="MECÁNICO DE EQUIPOS FIJOS">MECÁNICO DE EQUIPOS FIJOS</option>
                    <option value="MECÁNICO DE RADAR">MECÁNICO DE RADAR</option>

                </select>

                <label for="centropreseleccion">Elija un centro de preselección (*)</label>
                <select class="custom-select mb-3" name="centropreseleccion" id="centropreseleccionesp">
                <option value="" selected disabled>Seleccione una opción</option>
                    <option value='ESCUELA DE SUBOFICIALES DEL EJÉRCITO "SARGENTO CABRAL"'>ESCUELA DE SUBOFICIALES DEL EJÉRCITO "SARGENTO CABRAL" - BUENOS AIRES</option>
                    <option value='LICEO MILITAR "GENERAL ROCA"'>LICEO MILITAR "GENERAL ROCA" - CHUBUT</option>
                    <option value='LICEO MILITAR "GENERAL PAZ"'>LICEO MILITAR "GENERAL PAZ" - CÓRDOBA</option>
                    <option value='LICEO MILITAR "GENERAL LAMADRID"'>LICEO MILITAR "GENERAL LAMADRID" - TUCUMÁN</option>
                    <option value='COMANDO DE LA XIIda BRIGADA DE MONTE'>COMANDO DE LA XIIda BRIGADA DE MONTE - MISIONES</option>
                    
                    <option value='LICEO MILITAR "GENERAL ESPEJO"'>LICEO MILITAR "GENERAL ESPEJO" - MENDOZA</option>
                    <option value='LICEO MILITAR "GENERAL BELGRANO"'>LICEO MILITAR "GENERAL BELGRANO" - SANTA FÉ</option>
                    <option value='COMANDO DE LA Vta BRIGADA DE MONTAÑA'>COMANDO DE LA Vta BRIGADA DE MONTAÑA - SALTA</option>
                    <option value='REGIMIENTO DE INFANTERÍA DE MONTE 29 "CNL IGNACIO JOSÉ WARNES"'>REGIMIENTO DE INFANTERÍA DE MONTE 29 "CNL IGNACIO JOSÉ WARNES" - FORMOSA</option>
                </select>
            </div>
            <div class="form-group" id="planarmas" style="display:none">

                <label for="orientacion">Elija orientación de armas (*)</label>
                <select class="custom-select  mb-3" name="orientacion" id="orientacionarm">
                    <option value="" selected disabled>Seleccione una opción</option>
                    <option value="INFANTERÍA">INFANTERÍA</option>
                    <option value="CABALLERÍA">CABALLERÍA</option>
                    <option value="ARTILLERÍA">ARTILLERÍA</option>
                    <option value="INGENIEROS">INGENIEROS</option>
                    <option value="COMUNICACIONES">COMUNICACIONES</option>
                </select>

                <label for="centropreseleccion">Elija un centro de preselección (*)</label>
                <select class="custom-select mb-3" name="centropreseleccion" id="centropreseleccionarm">
                    <option value="" selected disabled>Seleccione una opción</option>
                    <option value='ESCUELA DE SUBOFICIALES DEL EJÉRCITO "SARGENTO CABRAL"'>ESCUELA DE SUBOFICIALES DEL EJÉRCITO "SARGENTO CABRAL" - BUENOS AIRES</option>
                    <option value='LICEO MILITAR "GENERAL ROCA"'>LICEO MILITAR "GENERAL ROCA" - CHUBUT</option>
                    <option value='LICEO MILITAR "GENERAL PAZ"'>LICEO MILITAR "GENERAL PAZ" - CÓRDOBA</option>
                    <option value='LICEO MILITAR "GENERAL LAMADRID"'>LICEO MILITAR "GENERAL LAMADRID" - TUCUMÁN</option>
                    <option value='COMANDO DE LA XIIda BRIGADA DE MONTE'>COMANDO DE LA XIIda BRIGADA DE MONTE - MISIONES</option>
                    
                    <option value='LICEO MILITAR "GENERAL ESPEJO"'>LICEO MILITAR "GENERAL ESPEJO" - MENDOZA</option>
                    <option value='LICEO MILITAR "GENERAL BELGRANO"'>LICEO MILITAR "GENERAL BELGRANO" - SANTA FÉ</option>
                    <option value='COMANDO DE LA Vta BRIGADA DE MONTAÑA'>COMANDO DE LA Vta BRIGADA DE MONTAÑA - SALTA</option>
                    <option value='REGIMIENTO DE INFANTERÍA DE MONTE 29 "CNL IGNACIO JOSÉ WARNES"'>REGIMIENTO DE INFANTERÍA DE MONTE 29 "CNL IGNACIO JOSÉ WARNES" - FORMOSA</option>
                </select>
            </div>  
            <hr>

        <?php            
        }
        if($existeediciondatospersonales){
            $_SESSION['datospersonales'] = 1;
        ?>

            <!-- FORMULARIO DATOS PERS -->
            <h4>Datos personales</h4>
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-6">
                        <label for="nombres">Nombres (*)</label>
                        <input type="text" class="form-control text-uppercase" name="nombres" id="nombres" value="<?php echo$candidato["nombres"]; ?>" placeholder="<?php echo$candidato["nombres"]."(Actual)"; ?>" onchange="this.value = this.value.toUpperCase();" required>
                    </div>
                    <div class="col-md-6">
                        <label for="apellidos">Apellidos (*)</label>
                        <input type="text" class="form-control text-uppercase" name="apellidos" id="apellidos" onchange="this.value = this.value.toUpperCase();" value="<?php echo$candidato["apellidos"]; ?>" placeholder="<?php echo$candidato["apellidos"]."(Actual)"; ?>" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-5">
                        <label for="domicilio">Calle domicilio (*)</label>
                        <input type="text" class="form-control text-uppercase" name="domicilio" id="domicilio" onchange="this.value = this.value.toUpperCase();" value="<?php echo$domicilio["calle"]; ?>" placeholder="<?php echo$domicilio["calle"]."(Actual)"; ?>" required>
                        <small class="form-text">Recuerde que esta es la calle del domicilio donde usted vive actualmente.</small>
                    </div>
                    <div class="col-md-2">
                        <label for="nro">Altura (*)</label>
                        <input type="number" min=0 class="form-control" name="nro" id="nro" value="<?php echo$domicilio["numero"]; ?>" placeholder="<?php echo$domicilio["numero"]."(Actual)"; ?>" required>
                    </div>
                    <div class="col">
                        <label for="piso">Piso</label>
                        <input type="text" class="form-control" name="piso" id="piso" value="<?php echo$domicilio["piso"]; ?>" placeholder="<?php echo$domicilio["piso"]."(Actual)"; ?>">
                    </div>
                    <div class="col">
                        <label for="depto">Dpto</label>
                        <input type="text" class="form-control" name="depto" id="depto" value="<?php echo$domicilio["depto"]; ?>" placeholder="<?php echo$domicilio["depto"]."(Actual)"; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-6 ">
                        <label for="selectprov">Provincia (*)</label>
                        <select id="selectprov" name="provincia" class="form-control" required>
                            <option value="" selected disabled><?php echo$candidato["provincia"]."(Actual)" ?></option>
                        </select>
                    </div>
                    <div class="col-md-6 ">
                        <label for="selectloc">Localidad (*)</label>
                        <select id="selectloc" name="localidad" class="form-control" required>
                            <option value="" selected disabled><?php echo$candidato["localidad"]."(Actual)" ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="codigopostal">Código postal (*)<a href="https://www.correoargentino.com.ar/formularios/cpa" target="_blank" rel="noopener noreferrer">(buscar)</a> </label>
                <input type="text" class="form-control text-uppercase" name="codigopostal" id="codigopostal" onchange="this.value = this.value.toUpperCase();" value="<?php echo$candidato["codigo_postal"]; ?>" placeholder="<?php echo$domicilio["codigo_postal"]."(Actual)"; ?>" required>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6 ">
                    <label for="telefono">Teléfono</label>
                    <input type="text" class="form-control" name="telefono" id="telefono" value="<?php echo$candidato["telefono"]; ?>" placeholder="<?php echo$candidato["telefono"]."(Actual)"; ?>">                
                </div>
                <div class="form-group col-md-6 ">
                    <label for="celular">Celular (*)</label>
                    <input type="number" min=0 class="form-control" name="celular" id="celular" value="<?php echo$candidato["celular"]; ?>" placeholder="<?php echo$candidato["celular"]."(Actual)"; ?>" required>
                </div>
            </div>
            <div class="form-row">
                <input type="hidden" name="emailanterior" value="<?php echo$candidato["email"]; ?>">
                <div class="form-group col-md-6">
                    <label for="email" >Email (*)</label>
                    <input type="email" class="form-control" name="email" id="email" value="<?php echo$candidato["email"]; ?>" placeholder="<?php echo$candidato["email"]."(Actual)"; ?>" required>             
                </div>
                <div class="form-group col-md-6">
                    <label for="reemail" >Re-escriba Email</label>
                    <input type="reemail" class="form-control" name="reemail" id="reemail" onfocus="validateMail(document.getElementById('email'), this, 'Los emails no coinciden');" 
                    oninput="validateMail(document.getElementById('email'), this,'Los emails no coinciden');" required>             
                </div>
            </div>    

            
            <div class="form-group">
                <label for="fnacmiento">Fecha de nacimiento (*)</label>
                <input type="date" class="form-control" name="fnacimiento" id="fnacimiento" max="<?php echo fechaNacimientoLimite('18','02','01'); ?>" min="<?php echo fechaNacimientoLimite('24','02','01'); ?>" value="<?php echo$candidato["fecha_nacimiento"]; ?>" required>
            </div>
            
            <div class="form-row">
                <input type="hidden" name="dnianterior" value="<?php echo$candidato["dni"]; ?>">
                <div class="form-group col-md-4">
                    <label for="dni">DNI(sin puntos) (*)</label>
                    <input type="number" min=0 class="form-control" name="dni" id="dni" value="<?php echo$candidato["dni"]; ?>" placeholder="<?php echo$candidato["dni"]."(Actual)"; ?>" required>
                </div>
                <div class="form-group col-md-4">
                    <label for="redni" >Re-escriba DNI</label>
                    <input type="number" min=0 class="form-control" name="redni" id="redni" onfocus="validateMail(document.getElementById('dni'), this, 'Los dni no coinciden');" 
                    oninput="validateMail(document.getElementById('dni'), this,'Los dni no coinciden');" required>             
                </div>
                <div class="form-group col-md-4">
                    <label for="cuil">Número de Cuil(con guiones) (*)<a href="http://mi-cuil.com.ar/" target="_blank" rel="noopener noreferrer">(buscar)</a></label>
                    <input type="text" class="form-control text-uppercase" name="cuil" id="cuil" onchange="this.value = this.value.toUpperCase();" value="<?php echo$candidato["cuil"]; ?>" placeholder="<?php echo$candidato["cuil"]."(Actual)"; ?>" required>
                </div>        
            </div>

            <div class="form-group">
                <label for="nacionalidad">Nacionalidad (*)</label>
                <select class="custom-select" name="nacionalidad" id="nacionalidad" required>
                    <option value="" selected disabled><?php echo$candidato["nacionalidad"] ?>(Actual)</option>
                    <option value="ARGENTINO_NAT">ARGENTINO (NATIVO)</option>
                    <option value="ARGENTINO_OP">ARGENTINO (POR OPCIÓN)</option>
                </select>
            </div>
            <!-- SEXO / GRUPO SANG / EST CIVIL -->
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="sexo">Sexo (*)</label>
                    <select class="custom-select" name="sexo" id="sexo" required>
                        <option value="" selected disabled><?php echo$candidato["sexo"]."(Actual)"; ?></option>
                        <option value="FEMENINO">FEMENINO</option>
                        <option value="MASCULINO">MASCULINO</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="gruposanguineo">Grupo Sanguíneo</label>
                    <select class="custom-select" name="gruposanguineo" id="gruposanguineo" required>
                        <option value="" selected disabled><?php echo$candidato["grupo_sanguineo"]."(Actual)"; ?></option>
                        <option value="0+">0+</option>
                        <option value="0-">0-</option>
                        <option value="A+">A+</option>
                        <option value="A-">A-</option>
                        <option value="AB+">AB+</option>
                        <option value="AB-">AB-</option>
                        <option value="B+">B+</option>
                        <option value="B-">B-</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="estadocivil">Estado Civil (*)</label>
                    <select class="custom-select" name="estadocivil" id="estadocivil" required>
                        <option value="" selected disabled><?php echo$candidato["estado_civil"]."(Actual)"; ?></option>
                        <option value="SOLTERO (SIN HIJOS)">SOLTERO (SIN HIJOS)</option>
                        <option value="SOLTERO (CON HIJOS)">SOLTERO (CON HIJOS)</option>
                    </select>
                </div>
            </div>
            <hr>
        <?php
        }
        if($existeedicionsoldvol){
            $_SESSION['soldvol'] = 1;
        ?>
            <!-- FORMULARIO SOLD VOL -->
            <h4>Soldado Voluntario</h4>
            <div class="form-group">
                <label class="d-flex justify-content-center" for="essoldvol">¿Es Soldado Voluntario? (*)</label>
                <div class="form-group btn-group btn-group-toggle d-flex justify-content-center" id="essoldvol" data-toggle="buttons">            
                    <label id="sisoldvol" class="btn btn-primary">
                        <input type="radio" name="soldvol" id="option1" value="1" autocomplete="off" onchange="cambiarVisibilidad('destino', 'inline');siSoldVol();" required>SI
                    </label>
                    <label id="nosoldvol" class="btn btn-primary">
                        <input type="radio" name="soldvol" id="option2" value="0" autocomplete="off" onchange="cambiarVisibilidad('destino', 'none');noSoldVol();">NO
                    </label>
                </div>
            </div>

            <div class="form-group" id="destino" style="display:none">
                <label for="destinosoldvol">Indique el código de la unidad (*)</label>
                <select id="destinosoldvol" name="destinosoldvol" class="form-control" required>
                    <option value="" selected disabled><?php echo$soldvol["destino"]."(Actual)"; ?></option>
                </select>
            </div>
            <script>
                function siSoldVol(){
                    $('#destinosoldvol').attr('required','required');
                };
                function noSoldVol(){
                    $('#destinosoldvol').removeAttr('required');
                    document.getElementById("destinosoldvol").value = "";

                };
            </script>
            <hr>
        <?php
        }
        echo'
        </div>
            <input type="submit" name="enviarformact" class="submit btn btn-primary" value="Actualizar información" />
        </form>';
    }
    else{
        if(isset($_GET['m1'])){
            echo'<div class="alert alert-success">Datos actualizados.</div>';
        }
    ?>
    <div class="row">
        <div class="col-lg-4 mb-5">
            <div class="list-group" id="list-tab" role="tablist">
            <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Datos personales</a>
            <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Domicilio</a>
            <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">Datos de contacto</a>
            <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">Estudios</a>
            <a class="list-group-item list-group-item-action" id="list-settings-list2" data-toggle="list" href="#list-settings2" role="tab" aria-controls="settings">Plan de carrera</a>

            </div>
        </div>
        <div class="col-lg-8">
            <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                    <?php
                    echo'
                        <h4>Datos personales</h4>
                        <hr>
                        <div class="row">
                            <div class="col-6">
                                Nombre y apellido
                                <p class="ml-2">'.$candidato["nombres"].' '.$candidato["apellidos"].'</p>
                                DNI
                                <p class="ml-2">'.$candidato["dni"].'</p>
                                Fecha de nacimiento
                                <p class="ml-2">'.$candidato["fecha_nacimiento"].'</p>    
                                Estado civil
                                <p class="ml-2">'.$candidato["estado_civil"].'</p>
                            </div>
                            <div class="col-6">
                                Sexo
                                <p class="ml-2">'.$candidato["sexo"].'</p>
                                Cuil
                                <p class="ml-2">'.$candidato["cuil"].'</p>
                                Grupo sanguíneo
                                <p class="ml-2">'.$candidato["grupo_sanguineo"].'</p>
                                Nacionalidad
                                <p class="ml-2">'.$candidato["nacionalidad"].'</p>    
                            </div>
                        </div>
                        ';
                    ?>
                    </div>
                </div>   
            </div>

            <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                    <?php
                    echo'
                        <h4>Domicilio</h4>
                        <table class="table">
                            <tr>

                                <td>Calle</td>
                                <td>'.$domicilio["calle"].'</td>

                            </tr>
                            <tr>
                                <td>Número</td>
                                <td>'.$domicilio["numero"].'</td>
                            </tr>
                            <tr>
                                <td>Piso y depto</td>
                                <td>'.$domicilio["piso"].' - '.$domicilio["depto"].'</td>
                            </tr>
                            <tr>
                                <td>Localidad</td>
                                <td>'.$candidato["localidad"].', '.$candidato["provincia"].'</td>
                            </tr>
                            <tr>
                                <td>Código postal</td>
                                <td>'.$candidato["codigo_postal"].'</td>
                            </tr>
                        </table>
                        ';
                    ?>
                    </div>
                </div> 
            </div>
            <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                    <?php
                    echo'
                        <h4>Datos de contacto</h4>
                        <table class="table">
                            <tr>
                                <th>Tipo</th>
                                <th>Contacto</th>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>'.$candidato["email"].'</td>
                            </tr>
                            <tr>
                                <td>Celular</td>
                                <td>'.$candidato["celular"].'</td>
                            </tr>
                            <tr>
                                <td>Teléfono</td>
                                <td>'.$candidato["telefono"].'</td>
                            </tr>
                        </table>
                        ';
                    ?>
                    </div>
                </div> 
            </div>
            <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                    <?php
                    echo'
                        <h4>Estudio secundario</h4>
                        <table class="table">
                            <tr>

                                <td>Instituto / Colegio</td>
                                <td>'.$secundario["nombre"].'</td>

                            </tr>
                            <tr>
                                <td>Título</td>
                                <td>'.$secundario["titulo"].'</td>
                            </tr>
                            <tr>
                                <td>Plan</td>
                                <td>'.$secundario["plan"].'</td>
                            </tr>
                            <tr>
                                <td>Materias adeudadas</td>
                                <td>'.$secundario["cant_materias_adeudadas"].'</td>
                            </tr>
                            <tr>
                                <td>Estado</td>
                                <td>'.$secundario["anio_cursado"].'</td>
                            </tr>
                        </table>
                        
                        <h4>Datos de la Institución / Colegio</h4>

                        <table class="table">
                            <tr>
                                <td>Nombre</td>
                                <td>'.$secundario["nombre"].'</td>
                            </tr>
                            <tr>
                                <td>Teléfono</td>
                                <td>'.$secundario["telefono"].'</td>
                            </tr>
                            <tr>
                                <td>Provincia</td>
                                <td>'.$secundario["provincia"].'</td>
                            </tr>
                            <tr>
                                <td>Localidad</td>
                                <td>'.$secundario["localidad"].'</td>
                            </tr>
                            <tr>
                                <td>Dirección</td>
                                <td>'.$secundario["direccion"].'</td>
                            </tr>
                        </table>
                        
                        ';
                    ?>
                    </div>
                </div> 
            </div>
            <div class="tab-pane fade" id="list-settings2" role="tabpanel" aria-labelledby="list-settings-list">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                    <?php
                    echo'
                        <h4>Centro de preselección</h4>
                        <table class="table">
                            <tr>
                                '.$centro_preseleccion["nombre"].'
                            </tr>
                            <tr>
                                <td>Elección inicial plan de carrera</td>
                                <td>'.$plancarrera["tipo_plan"].'</td>
                            </tr>
                            <tr>
                                <td>Orientación</td>
                                <td>'.$plancarrera["orientacion"].'</td>
                            </tr>
                            <tr>
                                <td>Soldado voluntario</td>';
                                if(!$soldvol){
                                    echo'<td>Usted indicó que no es soldado voluntario</td>';
                                }
                                else{
                                    echo'<td>Destino: '.$soldvol["destino"].'</td>';
                                }
                                echo'
                                
                            </tr>
                        </table>
                        ';
                    ?>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    <?php
    }
    ?>
</section>



<?php
}
?>