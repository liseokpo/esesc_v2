<?php
session_start();
if(!isset($_SESSION['usuario'])){
    header('Location: ../index.php');
}
else{
    include ('../assets/func/funciones.php');
    include ('../assets/func/class.DBCandidato.php');

    $anterior = $_POST['anterior'];
    $nueva = $_POST['nueva'];
    $idusuario = $_POST['idusuario'];
    
    $pdo = conexion();

    $DBCandidato = new DBCandidato($pdo);
    $candidato = $DBCandidato -> obtenerCandidatoporId($idusuario);

    // Si La contraseña anterior está bien, ponemos la nueva
    if(verificarhashPass($anterior,$candidato['clave_candidato']))
    {
        $clave = $nueva;
        $clavehasheada = hashearPass($clave);
        $DBCandidato -> cargarClaveCandidato($idusuario,$clavehasheada);
        $DBCandidato -> primerLogin(0,$idusuario);
        $candidatomodificado = $DBCandidato -> obtenerCandidatoporId($idusuario);
        $_SESSION['candidato'] = $candidatomodificado;
        header ('Location: ../index.php?p=miestado');
    }
    // Si no, informamos
    else{
        header('Location: ../index.php?p=miestado&mensaje=Contraseña incorrecta, intente nuevamente');
    }    

}

?>