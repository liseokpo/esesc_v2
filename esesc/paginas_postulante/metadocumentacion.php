
<?php
    session_start();


    if(!isset($_SESSION['usuario'])){
        header('Location: ../index.php');
    }
    else{
        include ('../assets/func/funciones.php');
        include ('../assets/func/class.DBCandidato.php');
        include ('../assets/func/class.DBEdicion.php');
        $pdo = conexion();

        $DBCandidato = new DBCandidato($pdo);
        $DBEdicion = new DBEdicion($pdo);

        $idcandidato = $_SESSION['usuario'];

        $candidato = $DBCandidato -> obtenerCandidatoporId($idcandidato);

        $inscripcion = $DBCandidato -> obtenerInscripcion($idcandidato);

        $path = "/assets/docs/inscripciones/".$candidato["dni"];
        $cargarImagenes = true;
        if(isset($_POST["submit"])){                
            foreach ($_FILES as $nombreArray => $archivo){
                if(!esExtension("jpg", $archivo) && !esExtension("pdf", $archivo) 
                && !esExtension("jpeg", $archivo) && !esExtension("png", $archivo)){
                    $cargarImagenes = false;
                    $header = 'Location: ../index.php?p=midocumentacion&a='.$nombreArray.'';
                    break;
                }
                if($archivo["size"] > 256311){
                    $cargarImagenes = false;
                    $header = 'Location: ../index.php?p=midocumentacion&a='.$nombreArray.'';
                    break;
                }
                else{
                    $cargarImagenes = true;
                    $header = 'Location: ../index.php?p=midocumentacion';
                }
            }
            if($cargarImagenes && !($DBEdicion -> existeEdicionFotosDNI_Comprobante($idcandidato))){
                if (!is_dir(".." . $path)) {
                    mkdir(".." . $path, 0777, true);
                }
                else{
                    $files = glob('..'.$path.'/*'); // get all file names
                    var_dump(count($files));
    
                    foreach($files as $file){ // iterate files
                        if(is_file($file)) {
                            unlink($file); // delete file
                        }
                    }
                }
    
                foreach ($_FILES as $nombreArray => $archivo){
                    $temp = explode(".", $archivo["name"]);
                    $newfilename = $nombreArray . '.' . end($temp);
                    $fichero_subido = ".." . $path . "/" . basename($newfilename);
    
                    if (move_uploaded_file($archivo['tmp_name'], $fichero_subido)) {
                        echo "El fichero es válido y se subió con éxito.<br>";
                    } else {
                        echo "¡Posible ataque de subida de ficheros!<br>";
                    }
                }
    
                if(!$DBCandidato -> folderDocumentosExiste($inscripcion["id"])){
                    $DBCandidato -> cargarFolderDocumentos($path, $inscripcion["id"]);
                }
            }
            else if($DBEdicion -> existeEdicionFotosDNI_Comprobante($idcandidato)){


                $files = glob('..'.$path.'/*'); // get all file names
                var_dump(count($files));

                

                foreach ($_FILES as $nombreArray => $archivo){
                    $file_parts = pathinfo($archivo["name"]);
                    $extensionArchivo = strtolower($file_parts["extension"]);
                    
                    //nombreArray es el nombre que llevará el archivo actual
                    $archivoAnterior = obtenerRutaDocumentoCandidato($nombreArray, $candidato["dni"]);
                    if($archivoAnterior){
                        unlink($archivoAnterior); // delete file
                    }


                    $temp = explode(".", $archivo["name"]);
                    $newfilename = $nombreArray . '.' . end($temp);
                    $fichero_subido = ".." . $path . "/" . basename($newfilename);
    
                    if (move_uploaded_file($archivo['tmp_name'], $fichero_subido)) {
                        echo "El fichero es válido y se subió con éxito.<br>";
                    } else {
                        echo "¡Posible ataque de subida de ficheros!<br>";
                    }
                }

                $consulta = "SELECT * FROM editardatos WHERE id_candidato = ".$inscripcion['id_candidato']." AND usado = '0' AND  ( permiso = 'FOTOS_DNI' OR permiso = 'FOTO_COMP_PAGO' OR permiso = 'FOTO_TICKET_PAGO');";
                $tabla = ejecutarConsulta($consulta,$pdo);
                
                foreach($tabla as $elemento){
                        $DBEdicion -> usarEdicion($idcandidato,$elemento["permiso"]);
                        if($elemento["permiso"] == "FOTOS_DNI"){
                            $DBCandidato -> modificarInscripcionExtConVal($idcandidato,"EN OBSERVACION");
                        }
                        else{
                            $DBCandidato -> modificarInscripcionSafConVal($idcandidato,"EN OBSERVACION");
                        }
                }

                

                $header = 'Location: ../index.php?p=midocumentacion&sc';
            }
            header($header);
        }
    }
?>
