
<section id="cuerpo">

<h3 class="text-center">Documentación</h3>


<br>
<?php
$pdo = conexion();
$DBCandidato = new DBCandidato($pdo);
$DBEdicion = new DBEdicion($pdo);

$idcandidato = $_SESSION['usuario'];

$inscripcion = $DBCandidato -> obtenerInscripcion($idcandidato);


$DBEdicion -> validarEdicion($idcandidato,"FOTOS_DNI");
$DBEdicion -> validarEdicion($idcandidato,"FOTO_COMP_PAGO");
$DBEdicion -> validarEdicion($idcandidato,"FOTO_TICKET_PAGO");

if($DBCandidato -> folderDocumentosExiste($inscripcion["id"]) && !($DBEdicion -> existeEdicionFotosDNI_Comprobante($inscripcion["id_candidato"]))){
    $folder_documentos = $DBCandidato -> obtenerFolderDocumentos($inscripcion["id"]);
    
    echo'
    <div class="alert alert-primary" role="alert">
        Usted ya ingresó la documentación.
    </div>
    ';

    $directorio = "." . $folder_documentos["path"];
    $ficheros1  = array_diff(scandir($directorio,1), array('..', '.'));
    echo'<div class="card-deck">';
    
    foreach($ficheros1 as $fichero){
        $remoteImage = $directorio . "/" .$fichero;
        if(strtolower(substr($fichero, -3)) == "pdf"){
            echo'
            <div class="card ">
                <a href="'.$remoteImage.'" target="_blank">
                    <div class="text-center">
                        <img src="assets/img/pdf_logo.png" class="card-img-top w-auto" style="max-height:100px;">
                    </div>
                </a>
                <div class="card-body">
                    <h5 class="card-title">'.$fichero.'</h5>
                    <p class="card-text"><small class="text-muted">'.date("F d Y H:i:s.",filectime($remoteImage)).'</small></p>
                </div>
            </div>
            ';
        }
        else{
            echo'
            <div class="card ">
                <a href="'.$remoteImage.'" data-toggle="lightbox" class="">
                    <img src="'.$remoteImage.'" class="card-img-top" style="object-fit: cover;max-height:100px;">
                </a>
                <div class="card-body">
                    <h5 class="card-title">'.$fichero.'</h5>
                    <p class="card-text"><small class="text-muted">'.date("F d Y H:i:s.",filectime($remoteImage)).'</small></p>
                </div>
            </div>
            ';
        }
    }
    echo"</div>";
    echo"<br><br>";
    
}

// SI HAY EDICION LA APLICO ACA
else if($DBEdicion -> existeEdicionFotosDNI_Comprobante($inscripcion["id_candidato"])){

    $consulta = "SELECT * FROM editardatos WHERE id_candidato = ".$inscripcion['id_candidato']." AND usado = '0' AND  ( permiso = 'FOTOS_DNI' OR permiso = 'FOTO_COMP_PAGO' OR permiso = 'FOTO_TICKET_PAGO');";
    $tabla = ejecutarConsulta($consulta,$conexion);
    
    
    echo'
    <div class="card ">
        <div class="p-3">
            <h4>Se detectó una novedad en su Documentación</h4>
            
            <div class="alert alert-warning border">
                <h4 class="alert-heading">División Extensión</h4>
                <p>'.$inscripcion["detalle"].'</p>
            </div>

            <div class="alert alert-warning border">
                <h4 class="alert-heading">Pagos</h4>
                <p>'.$inscripcion["detalle_saf"].'</p>
            </div>


            <ul>
                <li>Tipos de archivos admitidos JPG, JPEG, PNG y PDF.</li>
                <li>Tamaño máximo de archivos admitido 250kb.</li>
                <li>Enlaces de interés <a href="?p=instructivos#como_abonar" target="_blank" rel="noopener noreferrer">¿Cuánto debo pagar?</a></li>
            </ul>

            <h4>Por favor, ingrese nuevamente los siguientes documentos:</h4>
        </div>
        <form id="form-img" action="paginas_postulante/metadocumentacion.php" enctype="multipart/form-data" method="post"> 
            <div class="card-body">
    ';        
            

    $contador = 0;
    foreach($tabla as $elemento){
        $contador ++;
        if($elemento["permiso"] == "FOTOS_DNI"){
            $classcard = "far fa-address-card";
            $classcard2 = "fas fa-address-card";

            $texto = "DNI (de frente)";
            $texto2 = "DNI (de reverso)";

            $name = "dni_frente";
            $name2 = "dni_reverso";

            $label = "label_dni_frente";
            $label2 = "label_dni_reverso";
        }
        else if($elemento["permiso"] == "FOTO_TICKET_PAGO"){
            $classcard = "bi bi-file-earmark-text";
            $texto = "Ticket de pago";
            $name = "ticket_pago";
            $label = "label_ticket_pago";
        }
        else if($elemento["permiso"] == "FOTO_COMP_PAGO"){
            $classcard = "bi bi-file-earmark-text";
            $texto = "Comprobante de pago";
            $name = "comprobante_pago";
            $label = "label_comprobante_pago";
        }
        else{
            break;
        }

        echo'
        <div class="row mb-4">
            <div class="col align-self-center">
                <i class="'.$classcard.'" style="font-size: 1.5rem; color:#00652e"></i><b>'.$texto.'</b>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" id="'.$name.'" name="'.$name.'" class="custom-file-input" lang="es" onchange="readURL(this,\'blah-'.$contador.'\');" accept="image/jpeg,image/jpg,image/png,application/pdf" required/>
                        
                        <label class="custom-file-label" id="'.$label.'" for="'.$name.'">Adjunte una foto legible</label>
                    </div>
                    <div class="input-group-append">
                        <button type="button" class="btn" onclick="document.getElementById(\''.$name.'\').value = null;document.getElementById(\'blah-'.$contador.'\').src = \'\';"><img src="./assets/img/limpiar.png" style="max-heigth:20px;max-width:20px" ></button>
                    </div>
                </div>
            </div>
            <div class="col-auto">
                <img id="blah-'.$contador.'" src="#" class="rounded-lg" alt="" style="max-width:90px; max-height: 90px;"/>        
            </div>
        </div>
        ';
        if($elemento["permiso"] == "FOTOS_DNI"){
            $contador++;
            echo'
            <div class="row mb-4">
                <div class="col align-self-center">
                    <i class="'.$classcard2.'" style="font-size: 1.5rem; color:#00652e"></i><b>'.$texto2.'</b>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" id="'.$name2.'" name="'.$name2.'" class="custom-file-input" lang="es" onchange="readURL(this,\'blah-'.$contador.'\');" accept="image/jpeg,image/jpg,image/png,application/pdf" required/>
                            
                            <label class="custom-file-label" id="'.$label2.'" for="'.$name2.'">Adjunte una foto legible</label>
                        </div>
                        <div class="input-group-append">
                            <button type="button" class="btn" onclick="document.getElementById(\''.$name2.'\').value = null;document.getElementById(\'blah-'.$contador.'\').src = \'\';"><img src="./assets/img/limpiar.png" style="max-heigth:20px;max-width:20px" ></button>
                        </div>
                    </div>
                </div>
                <div class="col-auto">
                    <img id="blah-'.$contador.'" src="#" class="rounded-lg" alt="" style="max-width:90px; max-height: 90px;"/>        
                </div>
            </div>
            ';
        }
    }

    echo'
            </div>
            <div class="text-right ml-5 mr-5 mb-1 mt-0">
                <input type="submit" name="submit" class="btn btn-primary" value="Enviar">    
            </div>
        </form>
    </div>
    ';
}


// SI NO HAY EDICION ENTONCES MUESTRO LAS FOTOS CARGADAS
else{

    
    if(isset($_GET["a"])){
        echo'
        <div class="alert alert-danger" role="alert">
            Usted ingresó un archivo no admitido o que supera los 250kb en '.$_GET['a'].'
        </div>
        ';
    }
    else if(isset($_GET["sc"])){
        echo'
        <div class="alert alert-success" role="alert">
            Información actualizada correctamente. Deberá esperar a que la División Extensión revise los cambios.
        </div>
        ';
    }
    
?>

<ul>
    <li>Tipos de archivos admitidos JPG, JPEG, PNG y PDF.</li>
    <li>Tamaño máximo de archivos admitido 250kb.</li>
    <li>Enlaces de interés <a href="?p=instructivos#como_abonar" target="_blank" rel="noopener noreferrer">¿Cuánto debo pagar?</a></li>
</ul>
<h5 class="pb-2"></h5>
<!-- PERMITIR SELECCIÓN MÚLTIPLE DE FOTOS EN CADA ARCHIVO -->
<div class="card ">
    <form id="form-img" action="paginas_postulante/metadocumentacion.php" enctype="multipart/form-data" method="post"> 
        <div class="card-body">
            <div class="row mb-4">
                <div class="col align-self-center">
                    <i class="far fa-address-card" style="font-size: 1.5rem; color:#00652e"></i><b> DNI (de frente)</b>
                    <div class="input-group">
                        
                        <div class="custom-file">
                            <input type="file" id="dni_frente" name="dni_frente" class="custom-file-input" lang="es" onchange="readURL(this,'blah-1');" accept="image/jpeg,image/jpg,image/png,application/pdf" required/>
                            
                            <label class="custom-file-label" id="label_dni_frente" for="dni_frente">Adjunte una foto legible</label>
                        </div>
                        <div class="input-group-append">
                            <button type="button" class="btn" onclick="document.getElementById('dni_frente').value = null;document.getElementById('blah-1').src = '#';"><img src="./assets/img/limpiar.png" style="max-height:20px;max-width:20px" ></button>
                        </div>
                    </div>
                </div>
                <div class="col-auto">
                    <img id="blah-1" src="#" class="rounded-lg" alt="" style="max-width:90px; max-height: 90px;"/>        
                </div>
            </div>

            <div class="row mb-4">
                <div class="col">
                <i class="fas fa-address-card" style="font-size: 1.5rem; color:#00652e"></i><b> DNI (de reverso)</b>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" id="dni_reverso" name="dni_reverso" class="custom-file-input" lang="es" onchange="readURL(this,'blah-2');" accept="image/jpeg,image/jpg,image/png,application/pdf" required/>
                            
                            <label class="custom-file-label" id="label_dni_reverso" for="dni_reverso">Adjunte una foto legible</label>
                        </div>
                        <div class="input-group-append">
                            <button type="button" class="btn" onclick="document.getElementById('dni_reverso').value = null;document.getElementById('blah-2').src = '#';"><img src="./assets/img/limpiar.png" style="max-height:20px;max-width:20px" ></button>
                        </div>
                    </div>
                </div>
                <div class="col-auto">
                    <img id="blah-2" src="#" class="rounded-lg" alt="" style="max-width:90px; max-height: 90px;"/>        
                </div>
            </div>

            <div class="row mb-4">
                <div class="col">
                    <i class="bi bi-file-earmark-text" style="font-size: 1.5rem; color:#00652e"></i><b> Comprobante de pago </b>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" id="comprobante_pago" name="comprobante_pago" class="custom-file-input" lang="es" onchange="readURL(this,'blah-3');" accept="image/jpeg,image/jpg,image/png,application/pdf" required/>
                            
                            <label class="custom-file-label" id="label_comprobante_pago" for="comprobante_pago">Adjunte una foto legible</label>
                        </div>
                        <div class="input-group-append">
                            <button type="button" class="btn" onclick="document.getElementById('comprobante_pago').value = null;document.getElementById('blah-3').src = '#';"><img src="./assets/img/limpiar.png" style="max-height:20px;max-width:20px" ></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-auto col-sm-12">
                    <img id="blah-3" src="#" class="rounded-lg" alt="" style="max-width:90px; max-height: 90px;"/>        
                </div>
                
                
            </div>

            <div class="row mb-1">
                <div class="col">
                    <i class="bi bi-file-earmark-text" style="font-size: 1.5rem; color:#00652e"></i><b> Ticket de pago </b>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" id="ticket_pago" name="ticket_pago" class="custom-file-input" lang="es" onchange="readURL(this,'blah-4');" accept="image/jpeg,image/jpg,image/png,application/pdf" required/>
                            
                            <label class="custom-file-label" id="label_ticket_pago" for="ticket_pago">Adjunte una foto legible</label>
                        </div>
                        <div class="input-group-append">
                            <button type="button" class="btn" onclick="document.getElementById('ticket_pago').value = null;document.getElementById('blah-4').src = '#';document.getElementById('label_ticket_pago').value = null;"><img src="./assets/img/limpiar.png" style="max-height:20px;max-width:20px" ></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-auto col-sm-12">
                    <img id="blah-4" src="#" class="rounded-lg" alt="" style="max-width:90px; max-height: 90px;"/>        
                </div>
                
                
            </div>
        </div>
        <div class="text-right ml-5 mr-5 mb-1 mt-0">
            <input type="submit" name="submit" class="btn btn-primary" value="Enviar">    
        </div>
    </form>
</div>
<?php
}
?>
</section>


<script>
function readURL(input, idImagen) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
    
        reader.onload = function(e) {
            
        
            
            if(input.files[0].size > 256311 ){
                alert("La imagen ingresada excede los 250kb");
                
                document.getElementById(input.id).value = null;
            }
            else{
                $('#'+idImagen).attr('src', e.target.result);
                $('label[for='+input.id+']').html('<b>'+input.files[0].name+'</b>');
            }
        }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string    
  }
}


$(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});
</script>