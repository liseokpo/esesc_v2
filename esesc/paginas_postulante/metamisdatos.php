<?php
session_start();
// echo"<pre>";
// print_r($_POST);
// echo"</pre>";

include ('../assets/func/funciones.php');
include ('../assets/func/class.DBCandidato.php');
include ('../assets/func/class.DBIncorporacion.php');
include ('../assets/func/class.DBEdicion.php');

$conexion = conexion();
$DBCandidato = new DBCandidato($conexion);
$DBIncorporacion = new DBIncorporacion($conexion);
$DBEdicion = new DBEdicion($conexion);
//ID DEL CANDIDATO ACTUAL
$idcandidato = $_SESSION['usuario'];
$inscripcionAnterior =  $DBCandidato -> obtenerInscripcion($idcandidato);
// unset($_SESSION['cart']);

//SI NO EXISTE LO METO, SINO LO EDITO
//SECUNDARIO OK
if(isset($_SESSION['secundario'])){
    if($DBCandidato -> secundarioExiste($idcandidato)){
        $DBCandidato -> editarSecundario($idcandidato,$_POST);
    }
    else{
        $DBCandidato -> cargarSecundario($idcandidato,$_POST);
    }
    $DBEdicion -> usarEdicion($idcandidato,"SECUNDARIO");
    $DBCandidato -> modificarInscripcionExtConVal($idcandidato,"EN OBSERVACION");
    unset($_SESSION['secundario']);
}



//SI NO EXISTE LO METO, SINO LO EDITO
//SOLDVOL OK
if(isset($_SESSION['soldvol'])){
    if($DBCandidato -> soldadoVoluntarioExiste($idcandidato)){
        $DBCandidato -> editarDestino($idcandidato,$_POST);
    }
    else{
        $DBCandidato -> cargarDestino($idcandidato,$_POST);
    }
    $DBEdicion -> usarEdicion($idcandidato,"SOLD_VOL");
    $DBCandidato -> modificarInscripcionExtConVal($idcandidato,"EN OBSERVACION");
    unset($_SESSION['soldvol']);
}

//SI NO EXISTE LO METO, SINO LO EDITO
//PLAN DE CARRERA Y CENTRO DE PRESELECCION OK
if(isset($_SESSION['plancarrera'])){
    if($DBCandidato -> planCarreraExiste($idcandidato)){
        $DBCandidato -> editarPlanCarrera($idcandidato,$_POST);
    }
    else{
        $DBCandidato -> cargarPlanCarrera($idcandidato, $_POST);
    }
    $DBCandidato -> editarCentroPreseleccion($idcandidato,$_POST);    
    $DBEdicion -> usarEdicion($idcandidato,"PLAN_CARRERA");
    $DBCandidato -> modificarInscripcionExtConVal($idcandidato,"EN OBSERVACION");
    unset($_SESSION['plancarrera']);
}

//SI NO EXISTE DOMICILIO LO METO, SINO LO EDITO
//DATOS PERSONALES Y DOMICILIO OK
if(isset($_SESSION['datospersonales'])){
    //VERIFICAR QUE NO ESTÉ EL DNI EN LA BD, NI EL MAIL!!!
    //SI EL DNI EXISTE Y NO ES EL MISMO QUE EL SUYO.
    
    if($_POST['dnianterior'] != $_POST['dni'] && $DBCandidato -> dniExiste($_POST['dni']))
    {
        header ('Location: ../index.php?p=misdatos&m=errordni');    
    }
    else{
        if($_POST['emailanterior'] != $_POST['email'] && $DBCandidato -> emailExiste($_POST['email'])){
            header ('Location: ../index.php?p=misdatos&m=errormail');    
        }
        else{
            $DBCandidato -> editarDatosPersonales($idcandidato,$_POST);

            if($DBCandidato -> domicilioExiste($idcandidato)){
                $DBCandidato -> editarDomicilio($idcandidato,$_POST);
            }
            else{
                $DBCandidato -> cargarDomicilio($idcandidato, $_POST);
            }
            $DBEdicion -> usarEdicion($idcandidato,"DATOS_PERSONALES");
            $DBCandidato -> modificarInscripcionExtConVal($idcandidato,"EN OBSERVACION");
            unset($_SESSION['datospersonales']);
        }
    }
}

//cuando hago una modificación, seteo la inscripcion a "EN OBSERVACION"
if(!isset($_SESSION['datospersonales']) && !isset($_SESSION['plancarrera']) && !isset($_SESSION['soldvol']) && !isset($_SESSION['secundario'])){
    
    $DBCandidato -> modificarInscripcionConVal($idcandidato,'EN OBSERVACION');
    
    if($inscripcionAnterior['nro_inscripto'] != "0"){
        //asigno nroinscripto - si antes tenía uno
        $DBCandidato -> asignarNroInscripto("-", $idcandidato);
        
    }
    else{
        //asigno nroinscripto 0 si está en observación o desaprobado
        $DBCandidato -> asignarNroInscripto(0, $idcandidato);
    }
    //asigno fechanull si está en observacion o desaprobado
    $DBCandidato -> desasignarFechaAceptacionInscripcion($idcandidato);

    if($DBIncorporacion -> incorporacionExiste($idcandidato)){
        $DBIncorporacion -> eliminarIncorporacion($idcandidato);
    }
    
    
    header('Location: ../index.php?p=misdatos&m1');  
}
?>