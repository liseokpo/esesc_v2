
<?php
    if(!isset($_SESSION['usuario'])){
        header('Location: ../index.php');
    }
    else{
        include ('assets/func/class.DBIncorporacion.php');

        $pdo = conexion();
        // ACA tengo mi candidato logueado
        $idcandidato = $_SESSION['usuario'];
        $DBCandidato = new DBCandidato($pdo);
        $DBEdicion = new DBEdicion($pdo);
        $candidato = $DBCandidato -> obtenerCandidatoporId($idcandidato);

        $inscripcion = $DBCandidato -> obtenerInscripcion($candidato['id']);
        $centroPreseleccion = $DBCandidato -> obtenerCentroPreseleccion($idcandidato);
        $pathCredencial = obtenerPathCredencial($centroPreseleccion['nombre']);

        $DBIncorporacion = new DBIncorporacion($pdo);

        if($DBIncorporacion -> incorporacionExiste($candidato['id'])){
            $incorporacion = $DBIncorporacion -> obtenerIncorporacion($candidato['id']);
        }
        
        //$DBCandidato -> usarEdicion($idcandidato, "DATOS_PERSONALES");

        if($candidato['primerlogin'] == 1)
        {
            ?>
            <h5 class="text-center mt-4">Bienvenido, para ingresar por primera vez, por favor ingrese una contraseña nueva.</h5>

            <form action="paginas_postulante/metacambiarcontrasenia.php" class="text-center" method="post">
                <div class="form">
                    <div class="col-lg-4 col-sm-auto   mb-4 mt-3 mx-auto">
                        <div class="input-group">
                            <input type="password" class="form-control" id="anterior" name="anterior" placeholder="Contraseña anterior">
                            <div class="input-group-append">
                                <button id="show_password" class="btn btn-primary" type="button" onclick="mostrarPassword('anterior','icono1')"> <span id="icono1" class="fa fa-eye-slash icon"></span> </button>
                            </div>
                        </div>
                        
                    <?php
                        if(isset($_GET['mensaje']))
                            echo"<small style='color:#FF4848;'>".$_GET['mensaje']."</small>";
                    ?>
                    </div>
                </div>
                <div class="form">
                    <div class="col-lg-4 mb-2 mx-auto">
                        <div class="input-group">
                            <input type="password" class="form-control" name="nueva" id="nueva" placeholder="Contraseña nueva" required>  
                            <div class="input-group-append">
                                <button id="show_password2" class="btn btn-primary" type="button" onclick="mostrarPassword('nueva','icono2')"> <span id="icono2" class="fa fa-eye-slash icon"></span> </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form">
                    <div class="col-lg-4 mb-4 mx-auto">
                        <div class="input-group">
                            <input type="password" class="form-control" name="renueva" id="renueva" placeholder="Repetir contraseña nueva" onfocus="validateMail(document.getElementById('nueva'), this, 'Las contraseñas no coinciden');" oninput="validateMail(document.getElementById('nueva'), this,'Las contraseñas no coinciden');">
                            <div class="input-group-append">
                                <button id="show_password3" class="btn btn-primary" type="button" onclick="mostrarPassword('renueva','icono3')"> <span id="icono3" class="fa fa-eye-slash icon"></span> </button>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" value="<?php echo $idcandidato; ?>" id="idusuario" name="idusuario">
                    
                <div class="form">
                    <div class="col-lg-2 mb-4 mx-auto">
                        <input class="form-control btn btn-primary" type="submit" value="Cambiar contraseña">
                    </div>
                </div>
                </div>

            </form>
            <?php

            // $contrasenia = $DBCandidato -> verificarContrasenia($candidato[]);
        }
        
        else{
            echo"<section id ='cuerpo' class='mw-100'>";
            
            
            echo"<br><h3 class='text-center'>".$candidato['nombres']." ".$candidato['apellidos']."</h3>";
            ?>
                        
            

            <?php
            
            
            if($DBEdicion -> existeEdicionEnCandidato($inscripcion["id_candidato"])){
                $ediciones = $DBEdicion ->obtenerEdicionesEnCandidato($inscripcion["id_candidato"]);
                $existeDatos = $existeDocu = false;
                foreach($ediciones as $edicion){
                    if ($DBEdicion -> existeEdicionFotosDNI_Comprobante($inscripcion["id_candidato"]) && ($edicion["permiso"] == "FOTOS_DNI" || $edicion["permiso"] == "FOTO_COMP_PAGO" || $edicion["permiso"] == "FOTO_TICKET_PAGO" )){
                        $existeDocu = true;
                    }

                    if(!($edicion["permiso"] == "FOTOS_DNI" || $edicion["permiso"] == "FOTO_COMP_PAGO" || $edicion["permiso"] == "FOTO_TICKET_PAGO" )){
                        $existeDatos = true;
                    }
                }
                if($existeDocu){
                $consulta = "SELECT * FROM editardatos WHERE id_candidato = ".$inscripcion['id_candidato']." AND usado = '0' AND  ( permiso = 'FOTOS_DNI' OR permiso = 'FOTO_COMP_PAGO' OR permiso = 'FOTO_TICKET_PAGO');";
                        $tabla = ejecutarConsulta($consulta,$conexion);
                        echo'
                        <div class="alert alert-warning" role="alert">
                            Debe actualizar su documentación ingresada. Ingrese <a href="index.php?p=midocumentacion">aquí</a> para hacerlo ahora.
                        </div>
                        ';
                }
                if($existeDatos){
                    echo'
                    <div class="alert alert-warning" role="alert">
                        Debe actualizar sus datos ingresados. Ingrese <a href="index.php?p=misdatos">aquí</a> para hacerlo ahora.
                    </div>   
                        ';
                }
            }



            echo'
            
            <div id="accordion">
                ';
               
                    // --- Card Inscripción ---
                    echo'
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <button class="btn btn-link w-100" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                <div class="row">
                                        <div class="col text-left">
                                            <h5 class="mb-0">
                                                INSCRIPCIÓN
                                                ';

                                                if($inscripcion["estado"] == "EN OBSERVACION"){
                                                ?>
                                                <div class="text-white bg-info p-1" style="display:inline;"><?php echo$inscripcion['estado']; ?></div><div class="spinner-grow text-info" role="status">
                                                                <span class="sr-only">Loading...</span>
                                                                </div>
                                                
                                                <?php
                                                }

                                                if($inscripcion["estado"] == "INSCRIPTO" || $inscripcion["estado"] == "INSCRIPTO CONDICIONAL"){
                                                    ?>
                                                    <div class="text-white bg-success p-1" style="display:inline;"><?php echo$inscripcion['estado']; ?></div><br><div class="pt-1 ml-0"> Nro Inscripto : <b class="border border-dark text-dark"><?php echo$inscripcion["nro_inscripto"]; ?></b></div>
                                                    
                                                    <div class="position-fixed w-auto m-3" style="right: 0; bottom: 0;z-index:2000!important">
                                                        <div class="row d-flex align-items-center">
                                                            <div class="col p-0 m-0" style="z-index:2000">
                                                                <a id="popoverAyuda" type="button" tabindex="0"  
                                                                    role="button"
                                                                    data-html="true"
                                                                    data-toggle="popover"
                                                                    data-trigger="focus"
                                                                    data-content='<div class="contenidoPopover">¿Necesita ayuda?</div>'
                                                                    template="bg-primary">
                                                                </a>
                                                            </div>
                                                            <div class="col pl-1" >
                                                                <?php  
                                                                
                                                                $url = "¡Hola! Soy el/la Postulante ".$candidato["nombres"]." ".$candidato["apellidos"]." y quisiera saber:";
                                                                $urlencoded = urlencode($url);
                                                                echo'<a href="https://api.whatsapp.com/send?phone=+5491161234798&text='.$urlencoded.'" target="_blank" ><i class="fab fa-whatsapp-square fa-3x" style="color:#25D366;"></i></i></a>';

                                                                ?>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    
                                                    <script>
                                                    setTimeout(
                                                        function() {
                                                            $('#popoverAyuda').popover('toggle');
                                                    }, 3000);
                                                    $('#popoverAyuda').on('shown.bs.popover', function () {
                                                        var btn = document.getElementsByClassName("contenidoPopover");
                                                        btn[0].onclick = function() { $('[data-toggle="popover"]').popover('hide'); }
                                                    });

                                                    </script>

                                                    <?php
                                                }
                                                if($inscripcion["estado"] == "INSCRIPCION NO APROBADA"){
                                                    ?>
                                                    <div class="text-white bg-danger p-1" style="display:inline;"><?php echo$inscripcion['estado']; ?></div>
                                                    <?php
                                                }   
                                                echo'
                                                
                                            </h5>
                                        </div>
                                    
                                        <div class="col-auto my-auto">                        
                                            <i class="fas fa-caret-down fa-2x" style="color:#00652e"></i>
                                        </div>
                                </div>
                            </button>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    Recuerde que para que su inscripción sea aprobada, tanto la División Extensión como los Pagos deberán estar <b>SIN NOVEDAD</b>.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                            ';
                                if($DBCandidato -> folderDocumentosExiste($inscripcion["id"]) && !($DBEdicion -> existeEdicionFotosDNI_Comprobante($inscripcion["id_candidato"]))){
                                    echo'<div class="text-white bg-success p-1" style="display:inline;">DOCUMENTACIÓN CARGADA</div>';
                                }
                                else if($DBEdicion -> existeEdicionFotosDNI_Comprobante($inscripcion["id_candidato"])){
                                    echo'<div class="text-white bg-warning p-1" style="display:inline;">DEBE ACTUALIZAR SU DOCUMENTACIÓN</div>';
                                    echo'<br><br><p>Recuerde que su vacante de ingreso depende exclusivamente de completar la documentación correspondiente.
                                    <br>
                                    Para modificar su documentación ingrese <a href="?p=midocumentacion">AQUÍ</a>
                                    
                                    </p>';
                                }
                                else{
                                    echo'<div class="text-white bg-warning p-1" style="display:inline;">DEBE CARGAR SU DOCUMENTACIÓN</div>';
                                    echo'<br><br><p>Recuerde que su vacante de ingreso depende exclusivamente de completar la documentación correspondiente.
                                    <br>
                                    Para cargar su documentación ingrese <a href="?p=midocumentacion">AQUÍ</a>
                                    
                                    </p>';
                                }
                                if($inscripcion["detalle"] != ""){
                                    echo'<p><h6>Detalle División Extensión ('.$inscripcion["revision_ext"].'):</h6><pre style="white-space: pre-wrap;">'.$inscripcion['detalle'].'</pre></p>';
                                }
                                if($inscripcion["detalle_saf"] != ""){
                                    echo'<p><h6>Pagos ('.$inscripcion["revision_saf"].'):</h6><pre style="white-space: pre-wrap;">'.$inscripcion['detalle_saf'].'</pre></p>';
                                }
                                
                                if($inscripcion["estado"] == "INSCRIPTO" || $inscripcion["estado"] == "INSCRIPTO CONDICIONAL"){
                                ?>
                                <hr>
                                <div class="alert alert-warning" role="alert">
                                    Deberá tener en cuenta para continuar con su <b>Incorporación</b> los siguientes documentos
                                </div>
                                <h4 class="pb-1"><u>Contenidos examen intelectual</u></h4>
                                
                                <div class="row pl-3 pb-1">
                                    <div class="col">
                                        <h4>Matemática</h4>
                                    </div>
                                </div>
                                <div class="row pl-3">
                                    <div class="col">
                                        <h6>Programa</h6>
                                        <a class="descargapdf ml-3" download href="assets/docs/material_estudio/programa_mate.pdf"></a>    
                                    </div>
                                    <div class="col">
                                        <h6>Modelos de examen</h6>
                                        <a class="descargapdf ml-3" download href="assets/docs/material_estudio/modelo_examen_mate1.pdf"></a>    
                                        <a class="descargapdf ml-3" download href="assets/docs/material_estudio/modelo_examen_mate2.pdf"></a>    
                                    </div>
                                    <div class="col">
                                        <h6>Material de estudio</h6>
                                        <a class="descargapdf ml-3" download href="assets/docs/material_estudio/matematica_estudio.pdf"></a>    
                                    </div>
                                </div>
                                <div class="row pl-3 pb-1">
                                    <div class="col">
                                        <h4>Lengua</h4>
                                    </div>
                                </div>
                                <div class="row pl-3">
                                <div class="col">
                                        <h6>Programa</h6>
                                        <a class="descargapdf ml-3" download href="assets/docs/material_estudio/programa_lengua.pdf"></a>    
                                    </div>
                                    <div class="col">
                                        <h6>Modelos de examen</h6>
                                        <a class="descargapdf ml-3" download href="assets/docs/material_estudio/modelo_examen_leng1.pdf"></a>    
                                        <a class="descargapdf ml-3" download href="assets/docs/material_estudio/modelo_examen_leng2.pdf"></a>    
                                    </div>
                                    <div class="col">
                                        <h6>Material de estudio</h6>
                                        <a class="descargapdf ml-3" download href="assets/docs/material_estudio/lengua_estudio.pdf"></a>    
                                    </div>
                                </div>
                                <hr>
                                <h4 class="pb-1"><u>Exigencias físicas</u></h4>
                                <div class="row">
                                    <div class="col">
                                        <h6>Pruebas físicas</h6>
                                        <a class="descargapdf ml-3" download href="assets/docs/instructivo_pruebas_fisicas.pdf"></a>    
                                    </div>
                                    <div class="col">
                                        <h6>Certificado aptitud física</h6>
                                        <a class="descargapdf ml-3" download href="assets/docs/certificado_aptitud_fisica.pdf"></a>    
                                    </div>
                                    <div class="col"></div>
                                </div>
                                <hr>
                                <h4 class="pb-1"><u>Documentación</u></h4>
                                <div class="row">
                                    <div class="col">
                                        <h6>Dupie</h6>
                                        <a class="descargapdf ml-3" download href="assets/docs/dupie.pdf"></a>
                                    </div>
                                    <div class="col">
                                        <h6>Credencial de ingreso</h6>
                                        <a class="descargapdf ml-3" download href="<?php echo$pathCredencial ?>"></a>
                                    </div>
                                    <div class="col">
                                        <h6>Listado de efectos a traer</h6>
                                        <a class="descargapdf ml-3" download href="assets/docs/elementos.pdf"></a>    
                                    </div>
                                </div>
                                <hr>
                                


                                <?php
                                
                            
                                }
                                else if($inscripcion["estado"] == "EN OBSERVACION"){
                                    echo'<p>Una vez cargada la documentación, su inscripción será revisada. Esto puede demorar unos días. </p>';
                                }
                                

                                ?>
                                

                                <!-- SI NO CARGÓ NADA LE AVISO QUE LO TIENE QUE HACER PARA RESERVAR SU CUPO EN LA ESESC, REDIRIJO A "misdatos.php" -->
                               

                                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                    
                                    </div>
                                </div>
                                </div>

                                <?php
                                
                                echo'
                            </div>
                        </div>
                    </div>';

                if($inscripcion["estado"] == 'INSCRIPTO' || $inscripcion["estado"] == 'INSCRIPTO CONDICIONAL'){
                    // --- Card Incorporación ---
                    echo'
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <button class="btn btn-link collapsed w-100" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">

                                <div class="row">
                                    <div class="col text-left">
                                        <h5 class="mb-0">
                                            INCORPORACIÓN
                                        </h5>
                                    </div>
                                    <div class="col-auto my-auto">                        
                                        <i class="fas fa-caret-down fa-2x" style="color:#00652e"></i>
                                    </div>
                                </div>
                            </button>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                            
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                ¡Felicitaciones, avanzaste al siguiente paso! Recordá que este paso consiste en:
                                <ul>
                                    <li>Rendir los exámenes de admisión correctamente en tu centro de preselección.<br> Podes verlo en la sección Plan de Carrera: <a href="?p=misdatos">conocelo acá</a>.</li>
                                    <li>Entregar los estudios médicos solicitados en la <a href="assets/docs/guia_ingreso.pdf">guía de ingreso</a>. No tenés que enviar ninguna documentación por correo postal a la Escuela.</li>
                                    <li>Los exámenes de admisión serán a partir del mes de noviembre, se te informará oportunamente la fecha exacta establecida
                                    para las evaluaciones. ¡Preparate!</li>
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            ';
                            if($incorporacion["estado"] == "APROBADO"){
                                echo'<div class="text-white bg-success p-1" style="display:inline;">APROBADO</div>';
                                echo'
                                <br><br>Detalle: <pre>'.$incorporacion["detalle"].'</pre>
                                <p class="p-1"><i>
                                Usted se encuentra dentro de las vacantes para  el segundo proceso de admisión a la ESCUELA DE SUBOFICIALES DEL EJÉRCITO  “SARGENTO CABRAL”. Para incorporarse definitivamente deberá aprobar los exámenes psicológicos y el segundo reconocimiento médico, que incluyen: control del IMC, Test de embarazo para el personal femenino y examen toxicológico. <br><br> Además deberá presentar documentación que acredite sus estudios finalizados, de acuerdo a lo determinado en su Declaración Jurada, de no ser así no será admitido/a como Aspirante. <br><br> Examen psicológico del 15 al 19  de Marzo a las 07:30 AM en la Escuela de Suboficiales (Ruta 202 S/Nro Campo de Mayo). Vestuario:  •	camisa blanca, traje negro, corbata y zapatos negros (personal masculino). •	camisa blanca, pollera negra y zapatos negros (personal femenino). Útiles: lapicera negra, lápiz, goma de borrar. Tiempo estimado de los exámenes y permanencia será de 0800 a 1300 hs <br><br> Saludos cordiales la División Extensión - ESESC.

                                </i></p>';
                                echo'
                                <div class="row row-auto">
                                    <div class="col">
                                        <h6>Consentimiento drogas cuadros:<br><br>
                                        <a class="descargapdf ml-3" download href="assets/docs/consentimientos.pdf"></a>
                                    </div>
                                    <div class="col">
                                        <h6>Formulario seguro de vida:<br><br>
                                        <a class="descargapdf ml-3" download href="assets/docs/seguro_de_vida.pdf"></a>
                                    </div>
                                    
                                    <div class="col">
                                        <h6>Citación para incorporación 05 marzo 21:<br><br>
                                        <a class="descargapdf ml-3" download href="assets/docs/citacion.pdf"></a>
                                    </div>
                                </div>
                                <br>
                                <h3>Para menores de 18 años</h3>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <h6>Acta responsabilidad de ingreso y egreso de menores:<br><br>
                                        <a class="descargapdf ml-3" download href="assets/docs/ingreso_menores.pdf"></a>
                                    </div>
                                    <div class="col">
                                        <h6>Nombramiento de tutor:<br><br>
                                        <a class="descargapdf ml-3" download href="assets/docs/tutor.pdf"></a>
                                    </div>
                                </div>
                                
                                
                                
                                ';
                            }
                            else if($incorporacion["estado"] == "RESERVA"){
                                echo'<div class="text-white bg-warning p-1" style="display:inline;">RESERVA</div>';
                                echo'
                                <br><br>Detalle: <pre>'.$incorporacion["detalle"].'</pre>
                                <p class="p-1"><i>Usted no se encuentra dentro de las primeras vacantes para el ingreso a la ESCUELA DE SUBOFICIALES DEL EJÉRCITO "SARGENTO CABRAL", le informamos que será tenido en cuenta para eventuales vacantes en el instituto, desde el 05 de Abril hasta el 26 de Abril. <br>Atte "Div Extensión"</i></p>';
                            }
                            else if($incorporacion["estado"] == "DESAPROBADO"){
                                echo'<div class="text-white bg-danger p-1" style="display:inline;">DESAPROBADO</div>';
                                echo'
                                <br><br>Detalle: <pre>'.$incorporacion["detalle"].'</pre>
                                <p class="p-1"><i>Lamentamos  informarle que no ha ingresado dentro de las vacantes  para la ESCUELA DE SUBOFICIALES DEL EJÉRCITO  “SARGENTO CABRAL” <br><br> Saludos cordiales la División Extensión - ESESC </i></p>';
                            }
                            
                            else{
                                echo'<div class="text-info p-1">EN OBSERVACIÓN<div class="spinner-grow text-info" role="status">
                                <span class="sr-only">Loading...</span>
                                </div></div>
                                ';
                                echo'
                                <br><br>Detalle: <pre>'.$incorporacion["detalle"].'</pre>
                                <p class="p-1"><i>En estos momentos, su incorporación está siendo procesada y revisada. <br>Atte "Div Extensión"</i></p>';
                            }
                            
                            echo'
                            </div>
                        </div>
                    </div>';
                }
            echo'    
            </div>

            <br><br>
            Si desea saber más o tiene alguna duda, comuníquese a los números:
            <ul>
                <li>011 4664-0691 Internos 7525 y 7526</li>
                <li>Email incorporacionesesc@ejercito.mil.ar, esesc@ejercito.mil.ar</li>
            </ul>
            ';

            echo'</section>';
        }
    }
?>

<script>
$(document).ready(function(){
  $('[data-toggle="popover"]').popover();
});
</script>