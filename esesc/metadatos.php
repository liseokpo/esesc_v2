<?php
include ('assets/func/funciones.php');
session_start();

$user = $_POST['user'];
$pass = $_POST['pass'];

$conexion = conectar();
$consulta = "select * from usuario where user='$user'";
$resultado = ejecutarConsulta($consulta,$conexion);

if($resultado->num_rows == 1){//devuelve el numero de registros que devuelve la consulta. Si es uno, seguimos
    $usuario = $resultado->fetch_assoc();//metemos el resultado en una fila
    if(!verificarhashPass($pass,$usuario['pass']) && $pass != $usuario['pass']){
        $usuario = 0;  
    }

    if($usuario == 0){
        header('Location: login.php?mensaje=Error en usuario o contraseña');
    }
    else{
        
        $_SESSION['usuario'] = $usuario['id'];
        $_SESSION['cargo'] = $usuario['cargo'];//metemos la celda 'cargo' de la fila $usuario en una variable de SESSION
        $_SESSION['tipo_usuario'] = "gestor";
        header('Location: index.php');
    }

}
else if($user == null || $pass == null)
{
    header('Location: login.php?mensaje=Usuario o contraseña nulos');
}
else{
    header('Location: login.php?mensaje=Error en usuario o contraseña');
    session_destroy();
}
?>