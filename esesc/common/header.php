<?php
// ob_start();
?>
<!DOCTYPE html>
<head>
    <title>Escuela de Suboficiales del Ejército "Sargento Cabral"</title>

    <script async src="https://www.googletagmanager.com/gtag/js?id=G-S41FYGLXQV"></script>
    <script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'G-S41FYGLXQV');
    </script>
    
    <!-- FAVICON -->
    <link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="assets/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="assets/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <script src="assets/js/jquery-3.4.1.js" type="text/javascript"></script>
    <script src="assets/js/jquery.miranda.js"></script>
    <script src="assets/js/funciones.js"></script>
    <link rel="stylesheet" href="assets/css/pie.css">
    <link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css"> 
    

    
    <link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"></script>
    
    <!-- POPPER -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

    
    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="assets/css/estilos.css"/>
    <link rel="stylesheet" href="assets/css/menu.min.css">

   
    <!-- ekko-lightbox -->
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css"> 
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js.map"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js.map"></script>

    <!-- FONT AWESOME -->
    <script src="https://kit.fontawesome.com/e81d36029d.js" crossorigin="anonymous"></script>

    <!-- DATATABLES -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    
    <!-- BOOTSTRAP ICONS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

    <!-- FACEBOOK -->
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v10.0" nonce="RmTuiZRk"></script>
</head>
<body >
    <section id="enc1" >
        <div class="row align-items-center h-100 mw-100">
            <div class="col" >
                <div class="text-right my-auto w-100">
                    <img src="assets/img/cabecera/tituloesesc.png" class="mr-5 tituloesesc">
                </div>
            </div>
            <div class="col text-center position-absolute">
                <img  src="assets/img/cabecera/cabralesesc.png" class="cabralesesc">
            </div>
            <div class="col display-991-none w-100">
                <div class="row text-left">
                    <div class="col-auto redes1 my-auto">
                        <a href="https://www.facebook.com/pg/EscueladeSuboficialesSargentoCabral/"><img src="assets/img/cabecera/fb_v.png" height="55px"></a>
                    </div>
                    <div class="col-auto redes2 my-auto">
                        <a href="https://www.instagram.com/escueladesuboficiales.esesc/"><img src="assets/img/cabecera/ig_v.png" height="55px"></a>
                    </div>
                    <div class="col-auto redes2 my-auto">
                        <a href="https://www.youtube.com/channel/UCGXnimUHuVG1_yWWWoqiz2g"><img src="assets/img/cabecera/yt_v.png" height="55px"></a>
                    </div>
                    <div class="col-auto redes2 my-auto">
                        <a href="https://twitter.com/esesc_argentina"><img src="assets/img/cabecera/tw_v.png" height="55px"></a>
                    </div>
                    <div class="col-auto pl-3">
                        <a href="index.php?p=inicio"><img src="assets/img/cabecera/LOGO ESESC.png" class="logoesesc"></a>
                    </div>
                </div>
            </div>
            
        </div>
        
        

    </section>


