    
    <footer class="footer-distributed">
        <div class="footer-left">
            <div>
                <a href="?p=inicio"><img class="mt-3 logofooter" src="./assets/img/pie/esesc.png" alt="ESESC"></a><br>
                <a href="https://www.argentina.gob.ar/ejercito"><img class="mt-3 logofooter" src="./assets/img/pie/ea.png" alt="EA"></a>
            </div>
            <p class="footer-links">
                <a href="?p=inicio" class="hover-primary">Inicio</a>
                ·
                <a href="?p=historia" class="hover-primary">Reseña Histórica</a>
                ·
                <a href="?p=contacto" class="hover-primary">Contacto</a><br>
                <a href="?p=preguntas_frecuentes" class="hover-primary">Preguntas Frecuentes</a>
            </p>
            <p class="footer-company-name">Equipo de Comunicación Institucional - ESESC &copy; 2021</p>
        </div>
        <div class="footer-center">
            <div class="mb-3 mt-1">
                <a href="https://www.google.com.ar/maps/dir//Escuela+de+Suboficiales+del+Ejército+Sargento+Cabral"><i><img src="assets/img/pie/dir.png" onmouseover="this.src='assets/img/pie/dir_hover.png'"  onmouseout="this.src='assets/img/pie/dir.png'" style="width:37px;height:37px"></i></a>
                <p><span>RP 202	S/N CP:1659, Campo de Mayo</span> Buenos Aires, Argentina</p>
            </div>
            <div class="mb-2">
                <i><img src="assets/img/pie/tel.png" onmouseover="this.src='assets/img/pie/tel_hover.png'"  onmouseout="this.src='assets/img/pie/tel.png'" style="width:37px;height:37px"></i>
                <p>011 4664 1527</p>
            </div>
            <div class="mb-3">
                <i><img src="assets/img/pie/mail.png" onmouseover="this.src='assets/img/pie/mail_hover.png'"  onmouseout="this.src='assets/img/pie/mail.png'" style="width:37px;height:37px"></i>
                <p><a href="mailto:esesc@ejercito.mil.ar">esesc@ejercito.mil.ar</a></p>
            </div>
        </div>
        <div class="footer-right">
            <p class="footer-company-about">
                <span>Acerca de la escuela</span>
                La Escuela de Suboficiales del Ejército Sargento Cabral es el único Instituto que forma a los futuros Suboficiales del Ejército Argentino.
            </p>
            <div class="footer-icons mt-3">
                <a href="https://www.facebook.com/pg/EscueladeSuboficialesSargentoCabral/"><img src="assets/img/pie/fb.png" onmouseover="this.src='assets/img/pie/fb_hover.png'"  onmouseout="this.src='assets/img/pie/fb.png'" style="width:35px;height:35px"></a>
                <a href="https://www.instagram.com/escueladesuboficiales.esesc/"><img src="assets/img/pie/ig.png" onmouseover="this.src='assets/img/pie/ig_hover.png'"  onmouseout="this.src='assets/img/pie/ig.png'" style="width:35px;height:35px"></a>
                <a href="https://www.youtube.com/channel/UCGXnimUHuVG1_yWWWoqiz2g"><img src="assets/img/pie/yt.png" onmouseover="this.src='assets/img/pie/yt_hover.png'"  onmouseout="this.src='assets/img/pie/yt.png'" style="width:35px;height:35px"></a>
                <a href="https://twitter.com/esesc_argentina"><img src="assets/img/pie/tw.png" onmouseover="this.src='assets/img/pie/tw_hover.png'"  onmouseout="this.src='assets/img/pie/tw.png'" style="width:35px;height:35px"></a>
            </div>
            
        </div>
    </footer>
</body>
</html>
