<?php
if($_SESSION['cargo'] != 10){
    echo"<h2 class='text-center m-5'>Usted NO tiene acceso a este contenido.</h2>";
}
else{
    $DBCandidato = new DBCandidato($conexion);
    $DBEdicion = new DBEdicion($conexion);
    ?>
    <section id="cuerpofull" class="pt-4">

<?php
    $consultaCandidatoInscriptos = "SELECT candidato.dni, inscripcion.id as inscripcion_id, candidato.nombres, candidato.apellidos, inscripcion.revision_saf, inscripcion.id_candidato, inscripcion.estado FROM folder_documentos, candidato, inscripcion WHERE inscripcion.id_candidato = candidato.id AND inscripcion.id = folder_documentos.id_inscripcion ORDER BY folder_documentos.id ASC";
    $listaCandidatosInscriptos = ejecutarConsulta($consultaCandidatoInscriptos,$conexion);
    $listaCandidatosInscriptos2 = ejecutarConsulta($consultaCandidatoInscriptos,$conexion);
    $listaCandidatosInscriptos3 = ejecutarConsulta($consultaCandidatoInscriptos,$conexion);
    
    $listainscriptos = ejecutarConsulta($consultaCandidatoInscriptos,$conexion);
    $enobservacion = $connovedad = $sinnovedad = $desaprobados = $inscriptos = $doccargadas = 0;
        

    $doccargadas = count($listaCandidatosInscriptos3 -> fetchAll());
    foreach($listainscriptos as $inscripcion){
        if($inscripcion["revision_saf"] == "SIN NOVEDAD"){
            $sinnovedad++;
        }
        else if ($inscripcion["revision_saf"] == "CON NOVEDAD"){
            $connovedad++;
        }
        else if ($inscripcion["revision_saf"] == "DESAPROBADO"){
            $desaprobados++;
        }
        else if ($inscripcion["revision_saf"] == "EN OBSERVACION"){
            
            $enobservacion++;
        }
        if($inscripcion["estado"] == "INSCRIPTO"){
            $inscriptos++;
        }
    }

    
    echo'
        <div class="row">
            <div class="col text-center">
                <h5>Estadísticas</h5>
            </div>
        </div>
        <div class="row">
            
            <div class="col text-center">
                <button class=" border-0 bg-transparent text-left"><h3>'.$sinnovedad.'</h3>Sin novedad</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$connovedad.'</h3>Con novedad</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$desaprobados.'</h3>Desaprobados</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$enobservacion.'</h3>En observación</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$inscriptos.'</h3>Inscriptos</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$doccargadas.'</h3>Doc. Cargadas</button>
            </div>

        </div>';
    
    echo'
    <h4 class="">Seleccione un DNI para visualizar los documentos cargados.</h4><br>
    <div class="row mw-100">
        <div class="col mw-50">
            <table id="example" class="display table">
                <thead>
                    <tr class="w-25">
                        <th >DNI</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>ESTADO</th>
                    </tr>
                </thead>
                <tbody>
    
    ';
    

    foreach($listaCandidatosInscriptos as $candidatoInscripto){
        if($DBEdicion -> candidatoTieneEdicionSafUsadaHace72horas($candidatoInscripto["id_candidato"]) && $candidatoInscripto["revision_saf"] == "EN OBSERVACION"){
            $texto = "<br><span class='badge badge-secondary'>Edición efectuada</span>";
        }
        else{
            $texto = '';
        }
        echo'
            <tr>
                <td scope="row">
                    '.$candidatoInscripto["dni"].'
                </td>
                <td>
                    '.$candidatoInscripto["nombres"].'
                </td>
                <td>
                    '.$candidatoInscripto["apellidos"].'
                </td>
                <td>
                    '.$candidatoInscripto["revision_saf"].$texto.'
                </td>

            </tr>
        ';
                
    } 
    echo'
            <tbody>
        </table>
        </div>
    
        <div class="col mw-50">
            <div id="content-div"></div>

        </div>
    </div>
    ';
?>
    
    </section>
  
<script>
    $(document).ready(function() {
        var table = $('#example').DataTable({
            "order": [],
            language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            },

            
        }



        });
        
        $('#example tbody').on( 'click', 'tr', function () {
            var custid = (table.row(this).data());             
            var custString = custid.toString();
            
            $("#content-div").fadeIn(0);
            
            $("#content-div").load("./paginas_gestion/saf/docs_candidato.php?dni="+custid[0]);

            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
                
                $("#content-div").fadeOut(0);

                
            }
            else {
                table.$('tr.selected').removeClass('selected');
                
                $(this).addClass('selected');
                
            }
        } );
    
        $('#button').click( function () {
            table.row('.selected').remove().draw( false );
            
        } );
    } );


    function readURL(input, idImagen) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
        
            reader.onload = function(e) {
            $('#'+idImagen).attr('src', e.target.result);
        
            }
            
            reader.readAsDataURL(input.files[0]); // convert to base64 string

            $('label[for='+input.id+']').html('<b>'+input.files[0].name+'</b>');
        }
    }

   
    function readURL(input, idImagen) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
        
            reader.onload = function(e) {
            $('#'+idImagen).attr('src', e.target.result);
        
        }
        
        reader.readAsDataURL(input.files[0]); // convert to base64 string

        $('label[for='+input.id+']').html('<b>'+input.files[0].name+'</b>');
        
    }
    }

    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    $(function () {
        $('[data-toggle="popover"]').popover()
    })

    //TENGO QUE DESTILDAR TODO CUANDO QUEDE EN OCULTo.
    function editar(idcandidato){
        element = document.getElementById("tdedicion"+idcandidato);
        element2 = document.getElementById("recuerde"+idcandidato);
        if(element.style.display == "none"){
            element.removeAttribute("style");
            element2.removeAttribute("style");
        }
        else{
            element.style.display = "none";
            element2.style.display = "none";
            document.getElementById('customCheck1'+idcandidato).checked = false;
            document.getElementById('customCheck2'+idcandidato).checked = false;
        }
    };
</script>
<?php
}
?>