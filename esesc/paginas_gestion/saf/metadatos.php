<?php

echo"<pre>";
print_r($_POST);
echo"</pre>";


session_start();
if(!isset($_SESSION['usuario'])){
    header('Location: ../../index.php');
}
else{
    include ('../../assets/func/funciones.php');
    include ('../../assets/func/class.DBCandidato.php');
    include ('../../assets/func/class.DBEdicion.php');
    include ('../../assets/func/class.DBIncorporacion.php');

    $conexion = conexion();

    $DBCandidato = new DBCandidato($conexion);
    $DBIncorporacion = new DBIncorporacion($conexion);
    $DBEdicion = new DBEdicion($conexion);

    $idcandidato = $_POST["idcandidato"];
    $idinscripto = $_POST["idinscripto"];
    $inscripcionAnterior =  $DBCandidato -> obtenerInscripcion($idcandidato);
    $nro = $inscripcionAnterior['nro_inscripto'];


    //Modifico inscripcion sector saf
    //--- puede habilitar edicion de fotos
    //--- puede calificar 
    //--- puede poner detalle
    //--- lo que haga el saf no afectará a lo que haga la div incorp. La div incorp resuelve cuando tenga que aprobar o desaprobar una inscripción
    if(isset($_POST['enviarmodificacionestado'])){
        if($_POST['opciones_saf'] == 5){
            $DBCandidato -> modificarInscripcionSaf($idcandidato,$_POST);
        }
        else{
            $_POST['detallenuevo'] = obtenerSpeechSaf($_POST['opciones_saf']);
            $DBCandidato -> modificarInscripcionSaf($idcandidato,$_POST);
        }
        if($_POST["estadonuevo"] == "SIN NOVEDAD" && $inscripcionAnterior["revision_ext"] == "SIN NOVEDAD"){
            
            $DBCandidato -> asignarFechaAceptacionInscripcion($idcandidato);
            $DBCandidato -> modificarInscripcionConVal($idcandidato,"INSCRIPTO");

            if($inscripcionAnterior['nro_inscripto'] == "0" || $inscripcionAnterior['nro_inscripto'] == "-"){
                //asigno numero de inscripto si no tenía uno antes.
                $consulta = "SELECT nro_inscripto FROM inscripcion WHERE inscripcion.nro_inscripto != '0'";
                
                $nrosInscriptos = ejecutarConsulta($consulta,$conexion) ->fetchAll(PDO::FETCH_COLUMN, 0);
                $nroMaximo = 0;

                foreach ($nrosInscriptos as $nroInscripto){
                    $nroSinAnio = str_replace("/21", "", $nroInscripto);
                    if($nroSinAnio > $nroMaximo){
                        $nroMaximo = $nroSinAnio;
                    }
                }

                $inscriptoNro = $nroMaximo + 1;
                $nro = $inscriptoNro."/".date("y");
                echo$nro;
                
            }
            $DBCandidato -> asignarNroInscripto($nro, $idcandidato);
            if(!$DBIncorporacion -> incorporacionExiste($idcandidato)){
                $DBIncorporacion -> asignarIncorporacion($idcandidato);
            }

        }
        // con novedad significa que tiene que cambiar una foto
        else{
            if($inscripcionAnterior['estado'] != "EN OBSERVACION" || $_POST["estadonuevo"] == "DESAPROBADO"){

                if($inscripcionAnterior['nro_inscripto'] != "0"){
                    //asigno nroinscripto - si antes tenía uno
                    $DBCandidato -> asignarNroInscripto("-", $idcandidato);
                }
                else{
                    //asigno nroinscripto 0 si está en observación o desaprobado
                    $DBCandidato -> asignarNroInscripto(0, $idcandidato);
                }
                //asigno fechanull si está en observacion o desaprobado
                $DBCandidato -> desasignarFechaAceptacionInscripcion($idcandidato);
    

                if($_POST["estadonuevo"] == "DESAPROBADO"){
                    $DBCandidato -> modificarInscripcionConVal($idcandidato,"INSCRIPCION NO APROBADA");
                }
                else{
                    if($inscripcionAnterior['revision_ext'] != "DESAPROBADO"){
                        $DBCandidato -> modificarInscripcionConVal($idcandidato,"EN OBSERVACION");
                    }
                }

                if($DBIncorporacion -> incorporacionExiste($idcandidato)){
                    $DBIncorporacion -> eliminarIncorporacion($idcandidato);
                }
            }
            
        }
    }

    $candidato = $DBCandidato -> obtenerCandidatoporId($idcandidato);
    $url = 'ingresar.php';
    
    // habilito las ediciones
    if(isset($_POST["he_ticket_pago"])){
        $DBEdicion -> cargarEdicion($_SESSION["usuario"],$idcandidato,$_POST["he_ticket_pago"]);
        sendMail($candidato["email"], $candidato['nombres'].' '.$candidato['apellidos'], $url, "edicion_datos_habilitada", "Edición habilitada - ESESC",2);
    }
    if(isset($_POST["he_comprobante_pago"])){
        $DBEdicion -> cargarEdicion($_SESSION["usuario"],$idcandidato,$_POST["he_comprobante_pago"]);
        sendMail($candidato["email"], $candidato['nombres'].' '.$candidato['apellidos'], $url, "edicion_datos_habilitada", "Edición habilitada - ESESC",2);
    }

    header('Location: ../../index.php?p=inicio');

}
?>