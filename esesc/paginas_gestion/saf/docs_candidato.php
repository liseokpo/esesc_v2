<?php
include ('../../assets/func/funciones.php');
include ('../../assets/func/class.DBCandidato.php');
include ('../../assets/func/class.DBEdicion.php');

$conexion = conexion();   

$DBCandidato = new DBCandidato($conexion);
$DBEdicion = new DBEdicion($conexion);

$idCandidato = $DBCandidato -> obtenerIdCandidato($_GET["dni"]);
$candidato = $DBCandidato -> obtenerCandidatoporId($idCandidato);
$inscripcion = $DBCandidato -> obtenerInscripcion($idCandidato);

$existeEdicionCandidato = $DBEdicion -> existeEdicionEnCandidato($inscripcion["id_candidato"]);

$existeticketpago = $existecomppago = false;

if($existeEdicionCandidato)
{
    $DBEdicion -> validarEdicion($inscripcion["id_candidato"],"FOTO_TICKET_PAGO");
    $DBEdicion -> validarEdicion($inscripcion["id_candidato"],"FOTO_COMP_PAGO");
    
    if($DBEdicion -> existeEdicion($inscripcion["id_candidato"],"FOTO_TICKET_PAGO")){
        $existeticketpago = true;
    }
    if($DBEdicion -> existeEdicion($inscripcion["id_candidato"],"FOTO_COMP_PAGO")){
        $existecomppago = true;
    }
}




echo'<div id="'.$candidato["dni"].'">';
        
echo'
<form id="modificarInscripcion'.$idCandidato.'" class="myclass" action="paginas_gestion/saf/metadatos.php"  method="post" enctype="multipart/form-data">
    <div class="form-group">
        <table class="w-100">
            <tr>
                <label for="estadonuevo">Estado de documentación</label>
                <select class="form-control" name="estadonuevo" id="estadonuevo'.$idCandidato.'" required>
                    <option value="" selected disabled>'.$inscripcion["revision_saf"].'</option>
                    <option value="EN OBSERVACION">EN OBSERVACION</option>
                    <option value="CON NOVEDAD">CON NOVEDAD</option>
                    <option value="SIN NOVEDAD">SIN NOVEDAD</option>
                    <option value="DESAPROBADO">DESAPROBADO</option>
                </select>
            </tr>
            <tr>
                <td>
                    <label for="detallenuevo">Detalle 
                        <button type="button" tabindex="0" class="bg-transparent border fas fa-question-circle p-1" 
                            role="button"
                            data-toggle="popover"
                            data-content="Deberá colocar lo que quiera que le sea notificado tanto al postulante como a la División Incorporación.">
                        </button>
                    </label>
                    <div class="form-group">
                        <div class="form-check pb-2">
                            <input class="form-check-input" type="radio" name="opciones_saf" id="inlineRadio1'.$idCandidato.'" value="1" onchange="displayNoneById(\'detallenuevo'.$idCandidato.'\');">
                            <label class="form-check-label" for="inlineRadio1'.$idCandidato.'">
                                
                                1). Su comprobante/boleta  y ticket de pago, están aprobados..

                                Continúe el seguimiento de su estado de inscripción a través de nuestro sitio web, con su DNI y contraseña.
                            
                            </label>
                        </div>
                        <div class="form-check pb-2">
                            <input class="form-check-input" type="radio" name="opciones_saf" id="inlineRadio2'.$idCandidato.'" value="2" onchange="displayNoneById(\'detallenuevo'.$idCandidato.'\');">
                            <label class="form-check-label" for="inlineRadio2'.$idCandidato.'">

                                2). El importe abonado no es el que corresponde. El valor del derecho al examen de admisión es de $3000. Deberá volver a emitir la boleta de pago, por el valor parcial/diferencia y adjuntar un foto de ambos ticket de pago/Boleta de pago.
                            
                            </label>
                        </div>
                        <div class="form-check pb-2">
                            <input class="form-check-input" type="radio" name="opciones_saf" id="inlineRadio3'.$idCandidato.'" value="3" onchange="displayNoneById(\'detallenuevo'.$idCandidato.'\');">
                            <label class="form-check-label" for="inlineRadio3'.$idCandidato.'">
                            
                                3). No se cargo y/o no es legible el ticket de pago.

                            </label>
                        </div>
                        <div class="form-check pb-2">
                            <input class="form-check-input" type="radio" name="opciones_saf" id="inlineRadio4'.$idCandidato.'" value="4" onchange="displayNoneById(\'detallenuevo'.$idCandidato.'\');">
                            <label class="form-check-label" for="inlineRadio4'.$idCandidato.'">
                            
                                4). No se cargó y/o no es legible el comprobante de pago/boleta de pago

                            </label>
                        </div>

                        <div class="form-check pb-2">
                            <input class="form-check-input" type="radio" name="opciones_saf" id="inlineRadio5'.$idCandidato.'" value="5" onchange="removeDisplayNoneById(\'detallenuevo'.$idCandidato.'\');" ';if($inscripcion["detalle_saf"] != ""){echo'checked';}echo'>
                            <label class="form-check-label" for="inlineRadio5'.$idCandidato.'">
                                Otro
                            </label>
                        </div>
                    </div>
                    <textarea class="form-control" name="detallenuevo" id="detallenuevo'.$idCandidato.'" rows="4" style="display:';if($inscripcion["detalle_saf"] == ""){echo'none';}echo'">'.$inscripcion["detalle_saf"].'</textarea>
                </td>
                <td id="tdedicion'.$idCandidato.'" style="display:none">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="he_ticket_pago" value="FOTO_TICKET_PAGO" id="customCheck1'.$idCandidato.'" ';if($existeticketpago)echo'disabled';echo'>
                        <label class="custom-control-label p-0 m-0" for="customCheck1'.$idCandidato.'" ';if($existeticketpago)echo'title="Edición ya habilitada"';echo'>Foto de ticket de pago</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="he_comprobante_pago" value="FOTO_COMP_PAGO" id="customCheck2'.$idCandidato.'" ';if($existecomppago)echo'disabled';echo'>
                        <label class="custom-control-label p-0 m-0" for="customCheck2'.$idCandidato.'" ';if($existecomppago)echo'title="Edición ya habilitada"';echo'>Foto de comprobante de pago</label>
                    </div>
                </td>
            </tr>

            <tr>
                <td>
                    <input type="hidden" name="idcandidato" value="'.$idCandidato.'"/>
                    <input type="hidden" name="idinscripto" value="'.$inscripcion["id"].'"/>
                    <input type="submit" name="enviarmodificacionestado" class="submit btn btn-primary mt-2" value="MODIFICAR" />';
                    if($inscripcion["estado"] != "INSCRIPTO"){
                        echo'
                    <input type="button" name="submit2" class="submit btn btn-secondary mt-2" value="HABILITAR EDICIÓN" onclick="editar('.$idCandidato.')"/>
                    ';

                    }
                    echo'
                    <br><small id="recuerde'.$idCandidato.'" class="text-danger" style="display:none" >Al habilitar una edición, ésta sólo dura 48hs.</small>
                </td>
            </tr>
        </table>
    </div>        
</form>';




$folder_documentos = $DBCandidato -> obtenerFolderDocumentos($inscripcion["id"]);

$directorio = "../.." . $folder_documentos["path"];
$ficheros1  = array_diff(scandir($directorio,1), array('..', '.'));
 
echo'<div class="card-deck">';

foreach($ficheros1 as $fichero){
    $remoteImage = $directorio . "/" .$fichero;
    if(strtolower(substr($fichero, -3)) == "pdf"){
        echo'
        
        <div class="card">
            <a href="'.$remoteImage.'" target="_blank">
                <div class="text-center">
                    <img src="assets/img/pdf_logo.png" class="card-img-top w-auto" style="max-height:100px;">
                </div>
            </a>
            <div class="card-body">
                <h5 class="card-title">'.$fichero.'</h5>
                <p class="card-text"><small class="text-muted">'.date("F d Y H:i:s.",filectime($remoteImage)).'</small></p>
            </div>
        </div>
        ';
    }
    else{
        echo'
        <div class="card ">
            <a href="'.$remoteImage.'" data-toggle="lightbox" class="">
                <img src="'.$remoteImage.'" class="card-img-top" style="object-fit: cover;max-height:100px;">
            </a>
            <div class="card-body">
                <h5 class="card-title">'.$fichero.'</h5>
                <p class="card-text"><small class="text-muted">'.date("F d Y H:i:s.",filectime($remoteImage)).'</small></p>
            </div>
        </div>
        ';
    }
    
}
echo"
<script>
    $(function () {
        $('[data-toggle=\"popover\"]').popover()
    })
</script>


"
?>