<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<section id="cuerpo">
<?php

    $titulo = 'Calificados examen intelectual - Escuela de Suboficiales del Ejército "Sargento Cabral"';
    echo'
    <div class="text-center">
        <h2>En esta página podrá exportar los datos de los postulantes inscriptos y calificados a EXCEL.</h2>   
        <br>
    </div>';

    $pagina = "paginas_gestion/saf/exportarcalificadosjson.php";
    echo'
    <table id="example" class="display w-100">
        <thead>
            <tr>
                <th>NRO INSCRIPTO</th>
                <th>NOMBRES</th>
                <th>APELLIDOS</th>
                <th>DNI</th>
            </tr>
        </thead>
        
    </table>';
?>
</section>

<script>
    document.title = '<?php echo$titulo; ?>';
    $(document).ready(function(){
        $('#example').DataTable({

            "ajax": '<?php echo$pagina; ?>',
            "deferRender": true,
            "orderClasses": false,
            dom: 'Bfrtip',
            title: "export-title",
            "order": [],
            "columns": [
                { orderable: false},
                null,
                null,
                null,
            ],
            buttons:[
                {
                    extend: 'excel',
                    text: 'Exportar a excel',
                },
            ],
            
            responsive: true,
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
        });        
    });
</script>