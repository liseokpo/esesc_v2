<?php
if($_SESSION['cargo'] != 7){
    echo"<h2 class='text-center m-5'>Usted NO tiene acceso a este contenido.</h2>";
}
else{

    
    


?>

<section id="cuerpo" class="mt-4 w-100 mw-100">
<h2>Sin calificar</h2>
<form action="paginas_gestion/divedufis/metaexamen.php" method="post">
    <table id="example" class="display w-100 text-center">
    <thead>
        <tr >
            <th>NRO INSCRIPTO</th>
            <th>DNI</th>
            <th>FFBB</th>
            <th>ABDO</th>
            <th>TROTE 2000 MTS<br>(EJ 0900)</th>
            <th>Detalle</th>
            <th>AUSENTE</th>
            <th></th>
        </tr>
    </thead>

    <tfoot>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th><input type="submit" class="btn btn-sm btn-primary" value="CALIFICAR A TODOS"></th>
    </tfoot>
    </table>
</form>
<h2>Calificados</h2>
<form action="paginas_gestion/divestudio/metaexamen.php" method="post">
    <table id="example2" class="display w-100 text-center">
    <thead>
        <tr >
            <th>NRO INSCRIPTO</th>
            <th>DNI</th>
            <th>FFBB</th>
            <th>ABDO</th>
            <th>TROTE 2000 MTS<br>(EJ 0900)</th>
            <th>Detalle</th>
            <th>AUSENTE</th>
            <th></th>
        </tr>
    </thead>

    <tfoot>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th><input type="submit" name="modificar" class="btn btn-sm btn-primary" value="MODIFICAR A TODOS"></th>
    </tfoot>
    </table>
</form>
</section>
<script>
    
    $(document).ready(function(){
        
        $('#example').DataTable({

            "ajax": 'paginas_gestion/divedufis/inscriptosdivedufisjson.php',
            "deferRender": true,
            "orderClasses": false,
            "order": [],
            "columns": [
                { orderable: false, width: "8%" },
                null,
                { width: "12%" },
                { width: "12%" },
                null,
                null,
                null,
                null
            ],
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            },
        });

        $('#example2').DataTable({

            "ajax": 'paginas_gestion/divedufis/modificarinscriptosdivedufisjson.php',
            "deferRender": true,
            "orderClasses": false,
            "order": [],
            "columns": [
                { orderable: false, width: "8%" },
                null,
                { width: "12%" },
                { width: "12%" },
                null,
                null,
                null,
                null
            ],
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            },
            "drawCallback": function( settings ) {
                checkearRegistrosAusentes();
            },
        });
        

    });
    function autofilltitle(mytext2,mytext){
        var myText=document.getElementById(mytext);
        var myText2=document.getElementById(mytext2);
        var data =myText.value;
        myText2.value=myText.value;
        console.log(myText.value);
    }

    function ocultarTodosLosImput(id_candidato){
        var inputFFBB = document.getElementById(id_candidato+"ffbb");
        var inputABDO = document.getElementById(id_candidato+"abdo");
        var inputTROTE = document.getElementById(id_candidato+"trote");
        
        var inputFFBB2 = document.getElementById(id_candidato+"ffbb2");
        var inputABDO2 = document.getElementById(id_candidato+"abdo2");
        var inputTROTE2 = document.getElementById(id_candidato+"trote2");
        
        if (!inputFFBB.readOnly == true) {

            inputFFBB.readOnly = true;
            inputABDO.readOnly = true;
            inputTROTE.readOnly = true;
        
            inputFFBB.value = 0;
            inputFFBB2.value = 0;
            inputABDO.value = 0;
            inputABDO2.value = 0;
            inputTROTE.value = 0;
            inputTROTE2.value = 0;

        }
        else {

            inputFFBB.readOnly = false;
            inputABDO.readOnly = false;
            inputTROTE.readOnly = false;
            cleanInputs(id_candidato);

        }
    }

    function cleanInputs(id_candidato){
        var inputFFBB = document.getElementById(id_candidato+"ffbb");
        var inputABDO = document.getElementById(id_candidato+"abdo");
        var inputTROTE = document.getElementById(id_candidato+"trote");
        
        var inputFFBB2 = document.getElementById(id_candidato+"ffbb2");
        var inputABDO2 = document.getElementById(id_candidato+"abdo2");
        var inputTROTE2 = document.getElementById(id_candidato+"trote2");

        inputFFBB.value = null;
        inputFFBB2.value = null;
        inputABDO.value = null;
        inputABDO2.value = null;
        inputTROTE.value = null;
        inputTROTE2.value = null;
    }

    function checkearRegistrosAusentes(){
    document.querySelectorAll('input[type=checkbox]');
        $("input[type=checkbox]").each(function(){
            if(document.getElementById($(this).attr("id")).checked == true){
                
                var idInputCheck = $(this).attr("id");
                
                if(idInputCheck.includes("ausente")){
                    var inputIdCandidato = idInputCheck.split('ausente').join('');
                    ocultarTodosLosImput(inputIdCandidato);
                }
            }
        });
   }
</script>
<?php
}
?>