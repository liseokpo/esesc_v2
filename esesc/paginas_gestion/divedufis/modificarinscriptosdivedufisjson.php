
<?php
include ('../../assets/func/funciones.php');
include ('../../assets/func/class.DBCandidato.php');
include ('../../assets/func/class.DBExamenEduFis.php');
$conexion = conexion();   
$DBExamenEduFis = new DBExamenEduFis($conexion);
$consultaCandidatoInscriptos = "SELECT incorporacion.*, examen_fisico.*, inscripcion.nro_inscripto, candidato.dni  FROM incorporacion LEFT JOIN candidato ON candidato.id = incorporacion.id_candidato LEFT JOIN inscripcion ON inscripcion.id_candidato = candidato.id  LEFT JOIN examen_fisico ON examen_fisico.id_incorporacion = incorporacion.id LEFT JOIN examen_medico ON examen_medico.id_incorporacion = incorporacion.id WHERE examen_medico.estado != 'NO APTO' AND examen_medico.estado != '' ORDER BY inscripcion.fecha_aceptacion ASC";
$listaCandidatosInscriptos = ejecutarConsulta($consultaCandidatoInscriptos,$conexion);

echo'{"data" : ';
$array = array();
foreach($listaCandidatosInscriptos as $fila){

    if($fila['ausente'] == 1){
        $ausente = '
        <input type="text" id="'.$fila["id_candidato"].'ausente2" value="'.$fila["id_candidato"].'" name="ausente" form="form2'.$fila["id_candidato"].'" hidden>
        
        <div class="form-check">
            <input class="form-check-input mt-3" type="checkbox" name="ausente[]" value="'.$fila["id_candidato"].'" id="'.$fila["id_candidato"].'ausente" onchange="autofilltitle(this.id + 2,this.id);)" onclick="$(this).val(this.checked ? '.$fila["id_candidato"].' : 0);autofilltitle(this.id + 2,this.id);ocultarTodosLosImput('.$fila["id_candidato"].');" checked>
        
            <label class="form-check-label " for="'.$fila["id_candidato"].'ausente">
                AUSENTE
            </label>

        </div>';
    }
    else{
        $ausente = '
        <input type="text" id="'.$fila["id_candidato"].'ausente2" name="ausente" form="form2'.$fila["id_candidato"].'" hidden>
        
        <div class="form-check">
            <input class="form-check-input mt-3" type="checkbox" name="ausente[]" id="'.$fila["id_candidato"].'ausente" onchange="autofilltitle(this.id + 2,this.id);)" onclick="$(this).val(this.checked ? '.$fila["id_candidato"].' : 0);autofilltitle(this.id + 2,this.id);ocultarTodosLosImput('.$fila["id_candidato"].');">
        
            <label class="form-check-label " for="'.$fila["id_candidato"].'ausente">
                AUSENTE
            </label>

        </div>';
    }


    $datos_candidato = array();
    
    if($DBExamenEduFis -> examenExiste($fila[0])){
        $examen = $DBExamenEduFis -> obtenerExamen($fila[0]);
        array_push(
            $datos_candidato, 
            $fila["nro_inscripto"].'<form id="form2'.$fila["id_candidato"].'" action="paginas_gestion/divedufis/metaexamen.php" method="post"></form>',
            $fila["dni"].'<input type="text" id="'.$fila["id_candidato"].'id2" name="id" form="form2'.$fila["id_candidato"].'" value="'.$fila["id_candidato"].'" hidden> <input type="text" name="id[]" value="'.$fila["id_candidato"].'"  hidden class="form-control text-uppercase ">',
            '<input type="number" min="0" id="'.$fila["id_candidato"].'ffbb2" name="ffbb" form="form2'.$fila["id_candidato"].'" value="'.$examen["ffbb"].'" hidden> <input type="number" min="0" id="'.$fila["id_candidato"].'ffbb" class="form-control text-uppercase " name="ffbb[]" class="form-control text-uppercase" placeholder="FFBB" value="'.$examen["ffbb"].'" onchange="autofilltitle(this.id + 2,this.id)" required>',
            '<input type="number" min="0" id="'.$fila["id_candidato"].'abdo2" name="abdo" form="form2'.$fila["id_candidato"].'" value="'.$examen["abdo"].'" hidden> <input type="number" min="0" id="'.$fila["id_candidato"].'abdo" class="form-control text-uppercase " name="abdo[]" class="form-control text-uppercase" placeholder="ABDO" value="'.$examen["abdo"].'" onchange="autofilltitle(this.id + 2,this.id)" required>',
            '<input type="number" min="0" id="'.$fila["id_candidato"].'trote2" name="trote"  form="form2'.$fila["id_candidato"].'" value="'.$examen["trote"].'" hidden> <input type="number" min="0" id="'.$fila["id_candidato"].'trote" name="trote[]" class="form-control text-uppercase" placeholder="TROTE"  value="'.$examen["trote"].'" onchange="autofilltitle(this.id + 2,this.id)" required>',
            '<textarea class="form-control mt-2" name="detalle" id="'.$fila["id_candidato"].'detalle2" rows="2" form="form2'.$fila["id_candidato"].'" hidden>'.$examen["detalle"].'</textarea> <textarea class="form-control mt-2" name="detalle[]" id="'.$fila["id_candidato"].'detalle" rows="2" onchange="autofilltitle(this.id + 2,this.id)">'.$examen["detalle"].'</textarea> ',
            $ausente,
            '<input type="submit" class="btn btn-sm btn-primary" value="MODIFICAR" form="form2'.$fila["id_candidato"].'">',
        );
        array_push($array, $datos_candidato);   

    }    
}

echo $arr = json_encode($array);
echo"}";




?>