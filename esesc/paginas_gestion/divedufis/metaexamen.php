<?php
include ('../../assets/func/class.DBExamenEduFis.php');
include ('../../assets/func/class.DBExamenInt.php');
include ('../../assets/func/class.DBCandidato.php');
include ('../../assets/func/class.DBIncorporacion.php');
include ('../../assets/func/funciones.php');

// echo"<pre>";
// print_r($_POST);
// echo"</pre>";



if(is_array($_POST['ffbb'])){
    $ffbb = $_POST['ffbb'];
    $abdo = $_POST['abdo'];
    $trote = $_POST['trote'];
    $detalle = $_POST['detalle'];
    $id_candidato = $_POST['id'];
    if(is_array($_POST['ausente'])){
        $ausente = $_POST['ausente'];
    }
    $total = count($_POST['id']);
}
else{
    $ffbb = array($_POST['ffbb']);
    $abdo = array($_POST['abdo']);
    $trote = array($_POST['trote']);
    $detalle = array($_POST['detalle']);
    $id_candidato = array($_POST['id']);
    if(isset($_POST['ausente'])){
        $ausente = array($_POST['ausente']);
    }
    $total = 1;
}
$conexion = conexion();
$DBExamenEduFis = new DBExamenEduFis($conexion);
$DBExamenInt = new DBExamenInt($conexion);
$DBCandidato = new DBCandidato($conexion);
$DBIncorporacion = new DBIncorporacion($conexion);

for($i = 0; $i < $total; $i ++){
    $id_incorporacion = $DBIncorporacion -> obtenerIncorporacion($id_candidato[$i]);
    $sexo = $DBCandidato -> getSexo($id_candidato[$i]);
        
    // echo"Puntaje ffbb :".$puntajeffbb = $DBExamenEduFis -> calcularPuntajeFFBB($ffbb[$i], $sexo);
    // echo"Puntaje abdo :".$puntajeabdo = $DBExamenEduFis -> calcularPuntajeABDO($abdo[$i], $sexo);
    // echo"Puntaje trote :".$puntajetrote = $DBExamenEduFis -> calcularPuntajeTROTE($trote[$i], $sexo);
    
    
    if(isset($ausente) && in_array($id_candidato[$i], $ausente)){
        if($DBExamenEduFis -> examenExiste($id_incorporacion['id'])){
            $DBExamenEduFis -> modificarExamen($ffbb[$i], $abdo[$i], $trote[$i], $sexo, $detalle[$i], 1, $id_incorporacion["id"]);
        }
        else{
            $DBExamenEduFis -> cargarExamen($ffbb[$i], $abdo[$i], $trote[$i], $sexo, $detalle[$i], 1, $id_incorporacion["id"]);
        }
    }
    else{
        if($DBExamenEduFis -> examenExiste($id_incorporacion['id'])){
            $DBExamenEduFis -> modificarExamen($ffbb[$i], $abdo[$i], $trote[$i], $sexo, $detalle[$i], 0, $id_incorporacion["id"]);
        }
        else{
            $DBExamenEduFis -> cargarExamen($ffbb[$i], $abdo[$i], $trote[$i], $sexo, $detalle[$i], 0, $id_incorporacion["id"]);
        } 
    }
    

    if($DBExamenInt -> examenExiste($id_incorporacion['id'])){
        $examenFisico = $DBExamenEduFis -> obtenerExamen($id_incorporacion['id']);
        $examenIntelectual = $DBExamenInt -> obtenerExamen($id_incorporacion['id']);

        $notaFisico = $examenFisico['promedio'];
        $notaIntelectual = $examenIntelectual['promedio'];
        $DBIncorporacion -> cargarMerito($id_candidato[$i], $notaIntelectual,$notaFisico);
    }
}

header('Location: ../../index.php');

?>