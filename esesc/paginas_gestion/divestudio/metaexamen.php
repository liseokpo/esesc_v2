<?php
include ('../../assets/func/class.DBExamenInt.php');
include ('../../assets/func/class.DBExamenEduFis.php');
include ('../../assets/func/class.DBCandidato.php');
include ('../../assets/func/class.DBIncorporacion.php');
include ('../../assets/func/funciones.php');

if(is_array($_POST['lengua'])){
    $lengua = $_POST['lengua'];
    $mate = $_POST['mate'];
    $detalle = $_POST['detalle'];
    $id_candidato = $_POST['id'];
    if(is_array($_POST['tecnico'])){
        $tecnico = $_POST['tecnico'];
    }
    if(is_array($_POST['ausente'])){
        $ausente = $_POST['ausente'];
    }
    $total = count($_POST['id']);
}
else{
    $lengua = array($_POST['lengua']);
    $mate = array($_POST['mate']);
    $detalle = array($_POST['detalle']);
    
    $id_candidato = array($_POST['id']);
    
    if(isset($_POST['tecnico'])){
        $tecnico = array($_POST['tecnico']);
    }
    if(isset($_POST['ausente'])){
        $ausente = array($_POST['ausente']);
    }
    
    $total = 1;
}

$conexion = conexion();
$DBExamenInt = new DBExamenInt($conexion);
$DBExamenEduFis = new DBExamenEduFis($conexion);
$DBCandidato = new DBCandidato($conexion);
$DBIncorporacion = new DBIncorporacion($conexion);

for($i = 0; $i < $total; $i ++){
    $id_incorporacion = $DBIncorporacion -> obtenerIncorporacion($id_candidato[$i]);
    
    if(isset($ausente) && in_array($id_candidato[$i], $ausente)){
        if($DBExamenInt -> examenExiste($id_incorporacion['id'])){
            $DBExamenInt -> modificarExamen($lengua[$i], $mate[$i], $detalle[$i], 0, 1, $id_incorporacion["id"]);        
        }
        else{
            $DBExamenInt -> cargarExamen($lengua[$i], $mate[$i], $detalle[$i], 0, 1, $id_incorporacion["id"]);        
        }

    }
    else{
        if($DBExamenInt -> examenExiste($id_incorporacion['id'])){
            if(isset($tecnico) && in_array($id_candidato[$i], $tecnico)){
                $DBExamenInt -> modificarExamen($lengua[$i], $mate[$i], $detalle[$i], 1, 0, $id_incorporacion["id"]);        
            }
            else{
                $DBExamenInt -> modificarExamen($lengua[$i], $mate[$i], $detalle[$i], 0, 0, $id_incorporacion["id"]);
            }
        }
        else{
            if(isset($tecnico) && in_array($id_candidato[$i], $tecnico)){
                $DBExamenInt -> cargarExamen($lengua[$i], $mate[$i], $detalle[$i], 1, 0, $id_incorporacion["id"]);
            }
            else{
                $DBExamenInt -> cargarExamen($lengua[$i], $mate[$i], $detalle[$i], 0, 0, $id_incorporacion["id"]);
            }
        }
    }

    if($DBExamenEduFis -> examenExiste($id_incorporacion['id'])){
        $examenFisico = $DBExamenEduFis -> obtenerExamen($id_incorporacion['id']);
        $examenIntelectual = $DBExamenInt -> obtenerExamen($id_incorporacion['id']);

        $notaFisico = $examenFisico['promedio'];
        $notaIntelectual = $examenIntelectual['promedio'];
        $DBIncorporacion -> cargarMerito($id_candidato[$i], $notaIntelectual,$notaFisico);
    }

}

header('Location: ../../index.php');

?>