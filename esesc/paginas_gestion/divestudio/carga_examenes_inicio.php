<?php
if($_SESSION['cargo'] != 6){
    echo"<h2 class='text-center m-5'>Usted NO tiene acceso a este contenido.</h2>";
}
else{
?>

<section id="cuerpo" class="mt-4 w-100 mw-100">
    <h2>Sin calificar</h2>
    <form action="paginas_gestion/divestudio/metaexamen.php" method="post">
        <table id="example" class="display w-100 text-center">
        <thead>
            <tr >
                <th class="p-0 m-0">NRO INSCRIPTO</th>
                <th>DNI</th>
                <th>LENGUA<br>(0-100)</th>
                <th>MATE<br>(0-100)</th>
                <th></th>
                <th>PROMEDIO</th>
                <th>Detalle</th>
                <th>AUSENTE</th>
                <th></th>
            </tr>
        </thead>

        <tfoot>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th class="m-0 pl-0 pr-0"><input type="submit" name="calificar" class="btn btn-sm btn-primary" value="CALIFICAR A TODOS"></th>
        </tfoot>
        </table>
    </form>
    <hr>
    <h2>Calificados</h2>
    <form action="paginas_gestion/divestudio/metaexamen.php" method="post">
        <table id="example2" class="display w-100 text-center">
        <thead>
            <tr >
                <th class="p-0 m-0">NRO INSCRIPTO</th>
                <th>DNI</th>
                <th>LENGUA<br>(0-100)</th>
                <th>MATE<br>(0-100)</th>
                <th></th>
                <th>PROMEDIO</th>
                <th>Detalle</th>
                <th>AUSENTE</th>
                <th></th>
            </tr>
        </thead>

        <tfoot>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th class="m-0 pl-0 pr-0"><input type="submit" name="modificar" class="btn btn-sm btn-primary" value="MODIFICAR A TODOS"></th>
        </tfoot>
        </table>
    </form>
        
</section>
<script>
    
    $(document).ready(function(){

        
        $('#example').DataTable({
            "ajax": 'paginas_gestion/divestudio/inscriptosdivestudiojson.php',
            "deferRender": true,
            "orderClasses": false,
            "order": [],
            "columns": [
                { orderable: false},
                null,
                null,
                null,
                null,
                {width:"1%"},
                null,
                null,
                null
            ],
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            },
        });

        $('#example2').DataTable({

            "ajax": 'paginas_gestion/divestudio/modificarinscriptosdivestudiojson.php',
            "deferRender": true,
            "orderClasses": false,
            "order": [],
            "columns": [
                { orderable: false },
                null,
                null,
                null,
                null,
                {width:"1%"},
                null,
                null,
                null
            ],
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            },
            "drawCallback": function( settings ) {
                checkearRegistrosConSecundarioTecnico();
                checkearRegistrosAusentes();
            },
        });
        

    });
    function autofilltitle(mytext2,mytext){
        var myText=document.getElementById(mytext);
        var myText2=document.getElementById(mytext2);
        myText2.value=myText.value;
    }

    function promediar(idinput){
        var input = document.getElementById(idinput);

    }


    function ocultarInputs(id_candidato){

        var inputLengua = document.getElementById(id_candidato+"lengua");
        var inputMate = document.getElementById(id_candidato+"mate");
        if (!inputLengua.readOnly == true) {
            setearInputs100(id_candidato);
            inputLengua.readOnly = true;
            inputMate.readOnly = true;
        } else {

            inputLengua.readOnly = false;
            inputMate.readOnly = false;
            cleanInputs(id_candidato);


        }
    }
   
   function setearInputs100(id_candidato){
        var inputLengua = document.getElementById(id_candidato+"lengua");
        var inputMate = document.getElementById(id_candidato+"mate");
        var inputLengua2 = document.getElementById(id_candidato+"lengua2");
        var inputMate2 = document.getElementById(id_candidato+"mate2");
        var inputProm = document.getElementById(id_candidato+"prom");

        inputLengua.value = 100;
        inputLengua2.value = 100;
        inputMate.value = 100;
        inputMate2.value = 100;
        inputProm.value = inputProm.value = (parseInt(inputLengua.value) + parseInt(inputMate.value)) / 2;
   }

   function cleanInputs(id_candidato){
        var inputLengua = document.getElementById(id_candidato+"lengua");
        var inputMate = document.getElementById(id_candidato+"mate");
        var inputLengua2 = document.getElementById(id_candidato+"lengua2");
        var inputMate2 = document.getElementById(id_candidato+"mate2");
        var inputProm = document.getElementById(id_candidato+"prom");

        inputLengua.value = null;
        inputLengua2.value = null;
        inputMate.value = null;
        inputMate2.value = null;
        inputProm.value = null;
    }

   function calcularPromedio(id_candidato){
        var inputLengua = document.getElementById(id_candidato+"lengua");
        var inputMate = document.getElementById(id_candidato+"mate");
        var inputProm = document.getElementById(id_candidato+"prom");
        if(inputLengua.value != "" && inputMate.value != ""){
            inputProm.value = (parseInt(inputLengua.value) + parseInt(inputMate.value)) / 2;
        }
        else if(inputLengua.value == "" || inputMate.value == ""){
            inputProm.value = null;
        }
   }

   function checkearRegistrosConSecundarioTecnico(){
        document.querySelectorAll('input[type=checkbox]');
        $("input[type=checkbox]").each(function(){
            if(document.getElementById($(this).attr("id")).checked == true){
                
                var idInputCheck = $(this).attr("id");
                if(idInputCheck.includes("tecnico")){
                    var inputIdCandidato = idInputCheck.split('tecnico').join('');
                    calcularPromedio(inputIdCandidato);            
                }
            }
        });
        $("input[type=checkbox]:checked").each(function(){
            if(document.getElementById($(this).attr("id")).checked == true){
                var idInputCheckeado = $(this).attr("id");
                if(idInputCheckeado.includes("tecnico")){
                    var inputIdCandidato = idInputCheckeado.split('tecnico').join('');
                    ocultarInputs(inputIdCandidato);
                }
            }
        });
   }

   function checkearRegistrosAusentes(){
    document.querySelectorAll('input[type=checkbox]');
        $("input[type=checkbox]").each(function(){
            if(document.getElementById($(this).attr("id")).checked == true){
                
                var idInputCheck = $(this).attr("id");
                
                if(idInputCheck.includes("ausente")){
                    var inputIdCandidato = idInputCheck.split('ausente').join('');
                    calcularPromedio(inputIdCandidato);
                    ocultarTodosLosImput(inputIdCandidato);
                }
            }
        });
   }

   function ocultarTodosLosImput(id_candidato){
        var inputLengua = document.getElementById(id_candidato+"lengua");
        var inputMate = document.getElementById(id_candidato+"mate");
        var inputLengua2 = document.getElementById(id_candidato+"lengua2");
        var inputMate2 = document.getElementById(id_candidato+"mate2");
        if (!inputLengua.readOnly == true) {

            inputLengua.readOnly = true;
            inputMate.readOnly = true;
        
            inputLengua.value = 0;
            inputLengua2.value = 0;
            inputMate.value = 0;
            inputMate2.value = 0;

        }
        else {

            inputLengua.readOnly = false;
            inputMate.readOnly = false;
            cleanInputs(id_candidato);

        }
    }
</script>
<?php
}
?>