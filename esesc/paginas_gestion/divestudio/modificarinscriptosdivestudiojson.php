
<?php
include ('../../assets/func/funciones.php');
include ('../../assets/func/class.DBCandidato.php');
include ('../../assets/func/class.DBExamenInt.php');
$conexion = conexion();   
// $DBExamenInt = new DBExamenInt($conexion);
// $consultaCandidatoInscriptos = "SELECT * FROM incorporacion LEFT JOIN candidato ON candidato.id = incorporacion.id_candidato LEFT JOIN inscripcion ON inscripcion.id_candidato = candidato.id ORDER BY inscripcion.fecha_aceptacion ASC";
// $listaCandidatosInscriptos = ejecutarConsulta($consultaCandidatoInscriptos,$conexion);



$DBExamenInt = new DBExamenInt($conexion);
$consultaCandidatoInscriptos = "SELECT * FROM examen_intelectual LEFT JOIN incorporacion on incorporacion.id = examen_intelectual.id_incorporacion LEFT JOIN candidato ON candidato.id = incorporacion.id_candidato LEFT JOIN inscripcion ON inscripcion.id_candidato = candidato.id  ORDER BY inscripcion.fecha_aceptacion ASC";
$listaExamenesIntelectuales = ejecutarConsulta($consultaCandidatoInscriptos,$conexion);


echo'{"data" : ';
$array = array();
foreach($listaExamenesIntelectuales as $fila){
    if($fila['ausente'] == 1){
        $ausente = '
        <input type="text" id="'.$fila["id_candidato"].'ausente2" value="'.$fila["id_candidato"].'" name="ausente" form="form2'.$fila["id_candidato"].'" hidden>
        
        <div class="form-check">
            <input class="form-check-input mt-3" type="checkbox" name="ausente[]" value="'.$fila["id_candidato"].'" id="'.$fila["id_candidato"].'ausente" onchange="autofilltitle(this.id + 2,this.id);)" onclick="$(this).val(this.checked ? '.$fila["id_candidato"].' : 0);autofilltitle(this.id + 2,this.id);ocultarTodosLosImput('.$fila["id_candidato"].');" checked>
        
            <label class="form-check-label " for="'.$fila["id_candidato"].'ausente">
                AUSENTE
            </label>

        </div>';
    }
    else{
        $ausente = '
        <input type="text" id="'.$fila["id_candidato"].'ausente2" name="ausente" form="form2'.$fila["id_candidato"].'" hidden>
        
        <div class="form-check">
            <input class="form-check-input mt-3" type="checkbox" name="ausente[]" id="'.$fila["id_candidato"].'ausente" onchange="autofilltitle(this.id + 2,this.id);)" onclick="$(this).val(this.checked ? '.$fila["id_candidato"].' : 0);autofilltitle(this.id + 2,this.id);ocultarTodosLosImput('.$fila["id_candidato"].');">
        
            <label class="form-check-label " for="'.$fila["id_candidato"].'ausente">
                AUSENTE
            </label>

        </div>';
    }
    if($fila['secundario_tecnico'] == 0){
        $check = '
        <input type="text" id="'.$fila["id_candidato"].'tecnico2"  name="tecnico" form="form2'.$fila["id_candidato"].'" hidden>
        <div class="form-check">
            <input class="form-check-input mt-3" type="checkbox" name="tecnico[]" id="'.$fila["id_candidato"].'tecnico" onchange="autofilltitle(this.id + 2,this.id);ocultarInputs('.$fila["id_candidato"].')" onclick="$(this).val(this.checked ? '.$fila["id_candidato"].' : 0);autofilltitle(this.id + 2,this.id);">
            <label class="form-check-label " for="'.$fila["id_candidato"].'tecnico">
                Sec. Técnico
            </label>
        </div>';
    }
    else{
        $check = 
        '<input type="text" id="'.$fila["id_candidato"].'tecnico2" value="'.$fila["id_candidato"].'" name="tecnico" form="form2'.$fila["id_candidato"].'" hidden>
        <div class="form-check">
            <input class="form-check-input mt-3" type="checkbox" name="tecnico[]" id="'.$fila["id_candidato"].'tecnico" value="'.$fila["id_candidato"].'" onchange="autofilltitle(this.id + 2,this.id);ocultarInputs('.$fila["id_candidato"].')" onclick="$(this).val(this.checked ? '.$fila["id_candidato"].' : 0);autofilltitle(this.id + 2,this.id);" checked>
            <label class="form-check-label " for="'.$fila["id_candidato"].'tecnico">
                Sec. Técnico
            </label>
        </div>';
    }
    $datos_candidato = array();
        // $examen = $DBExamenInt -> obtenerExamen($fila[0]);
        array_push(
        $datos_candidato, 
        $fila["nro_inscripto"].'<form id="form2'.$fila["id_candidato"].'" action="paginas_gestion/divestudio/metaexamen.php" method="post"></form>',
        $fila["dni"].'<input type="text" id="'.$fila["id_candidato"].'id2" name="id" form="form2'.$fila["id_candidato"].'" value="'.$fila["id_candidato"].'" hidden> <input type="text" name="id[]" value="'.$fila["id_candidato"].'"  hidden class="form-control text-uppercase ">',
        '<input type="number" min="0" max="100" id="'.$fila["id_candidato"].'lengua2" name="lengua" form="form2'.$fila["id_candidato"].'" value="'.$fila["lengua_puntaje"].'" hidden required> <input type="number" min="0" max="100" id="'.$fila["id_candidato"].'lengua" class="form-control" name="lengua[]" placeholder="LENGUA" value="'.$fila["lengua_puntaje"].'" onchange="autofilltitle(this.id + 2,this.id);calcularPromedio('.$fila["id_candidato"].')" required>',
        '<input type="number" min="0" max="100" id="'.$fila["id_candidato"].'mate2" name="mate" form="form2'.$fila["id_candidato"].'" value="'.$fila["mate_puntaje"].'" hidden required> <input type="number" min="0" max="100" id="'.$fila["id_candidato"].'mate" class="form-control" name="mate[]" placeholder="MATE" value="'.$fila["mate_puntaje"].'" onchange="autofilltitle(this.id + 2,this.id);calcularPromedio('.$fila["id_candidato"].')" required>',
        $check,
        '<input type="text" class="form-control" id="'.$fila["id_candidato"].'prom" value='.($fila["promedio"]*10).' placeholder="PROM" disabled>',
        '<textarea class="form-control mt-2" name="detalle" id="'.$fila["id_candidato"].'detalle2" rows="2" form="form2'.$fila["id_candidato"].'" hidden>'.$fila["detalle"].'</textarea> <textarea class="form-control mt-2" name="detalle[]" id="'.$fila["id_candidato"].'detalle" rows="2" onchange="autofilltitle(this.id + 2,this.id)">'.$fila["detalle"].'</textarea> ',
        $ausente,
        '<input type="submit" class="btn btn-sm btn-primary" value="MODIFICAR" form="form2'.$fila["id_candidato"].'">',
        );
        array_push($array, $datos_candidato);
}

echo $arr = json_encode($array);
echo"}";




?>