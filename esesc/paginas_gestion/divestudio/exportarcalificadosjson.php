<?php
include ('../../assets/func/funciones.php');

include ('../../assets/func/class.DBTurnosExamenes.php');
ini_set('memory_limit', '5000M');
$conexion = conexion();   
    
    $consulta = "SELECT incorporacion.*, examen_fisico.*, examen_intelectual.*, examen_medico.*, 
    examen_psicologico.*, candidato.dni, candidato.nombres, candidato.apellidos, inscripcion.fecha_aceptacion, inscripcion.nro_inscripto, plan_carrera.tipo_plan, plan_carrera.orientacion 
    FROM incorporacion LEFT JOIN examen_fisico ON examen_fisico.id_incorporacion = incorporacion.id 
    LEFT JOIN examen_intelectual ON examen_intelectual.id_incorporacion = incorporacion.id LEFT JOIN 
    examen_medico ON examen_medico.id_incorporacion = incorporacion.id LEFT JOIN examen_psicologico ON
    examen_psicologico.id_incorporacion = incorporacion.id LEFT JOIN candidato ON 
    incorporacion.id_candidato = candidato.id LEFT JOIN inscripcion ON inscripcion.id_candidato = incorporacion.id_candidato LEFT JOIN plan_carrera ON plan_carrera.id_candidato = incorporacion.id_candidato WHERE  examen_medico.id IS NOT NULL AND examen_intelectual.fecha_carga IS NOT NULL ORDER BY inscripcion.fecha_aceptacion ASC";

$tabla = ejecutarConsulta($consulta,$conexion);
$DBTurnosExamenes = new DBTurnosExamenes($conexion);

echo'{"data" : ';
$array = array();
foreach($tabla as $fila){
    $ExamenActual = $DBTurnosExamenes -> obtenerTurnoExamenPorId($fila[5]);

    if($ExamenActual == null){
        $turnoExamenActual = "-";
    }
    else{
        $turnoExamenActual = $ExamenActual["nombre"];
    }

    $examenint = $fila[28];
    
    if($fila[26] == 1){
        $examenint = "AUSENTE";
    }

    $secundario = "BACHILLER";

    if($fila["secundario_tecnico"] == 1){
        $secundario = "TECNICO";
    }


    $datos_candidato = array();
    array_push(
        $datos_candidato,
        $fila['nro_inscripto'],
        $fila['dni'],
        $examenint,
        $fila[21],
        $fila[22],
        $fila[23],
        $fila[24],
        $secundario,
        $fila[27],
    );

    array_push($array, $datos_candidato);   
}
echo $arr = json_encode($array);
echo"}";

// '<div style="max-height:120px;overflow:auto;min-width:100px;">'.$fila['detalle'].'</div>'
?>