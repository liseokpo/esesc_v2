
<?php
include ('../../assets/func/funciones.php');
include ('../../assets/func/class.DBCandidato.php');
include ('../../assets/func/class.DBExamenInt.php');
$conexion = conexion();   
$DBExamenInt = new DBExamenInt($conexion);
$consultaCandidatoInscriptos = "SELECT * FROM incorporacion LEFT JOIN candidato ON candidato.id = incorporacion.id_candidato LEFT JOIN inscripcion ON inscripcion.id_candidato = candidato.id ORDER BY inscripcion.fecha_aceptacion ASC";
$listaCandidatosInscriptos = ejecutarConsulta($consultaCandidatoInscriptos,$conexion);

echo'{"data" : ';
$array = array();
foreach($listaCandidatosInscriptos as $fila){
    $datos_candidato = array();
    if(!$DBExamenInt -> examenExiste($fila[0])){
        array_push(
            $datos_candidato, 
            $fila["nro_inscripto"].'<form id="form2'.$fila["id_candidato"].'" action="paginas_gestion/divestudio/metaexamen.php" method="post"></form>',
            $fila["dni"].'<input type="text" id="'.$fila["id_candidato"].'id2" name="id" form="form2'.$fila["id_candidato"].'" value="'.$fila["id_candidato"].'" hidden> <input type="text" name="id[]" value="'.$fila["id_candidato"].'"  hidden class="form-control">',
            '<input type="number" min="0" max="100" id="'.$fila["id_candidato"].'lengua2" name="lengua" form="form2'.$fila["id_candidato"].'" hidden required> <input type="number" min="0" max="100" id="'.$fila["id_candidato"].'lengua" class="form-control" name="lengua[]" placeholder="LENGUA" onchange="autofilltitle(this.id + 2,this.id);calcularPromedio('.$fila["id_candidato"].')"  required>',
            '<input type="number" min="0" max="100" id="'.$fila["id_candidato"].'mate2" name="mate" form="form2'.$fila["id_candidato"].'" hidden required> <input type="number" min="0" max="100" id="'.$fila["id_candidato"].'mate" class="form-control" name="mate[]" placeholder="MATE" onchange="autofilltitle(this.id + 2,this.id);calcularPromedio('.$fila["id_candidato"].')" required>',
            '
            <input type="text" id="'.$fila["id_candidato"].'tecnico2" name="tecnico" form="form2'.$fila["id_candidato"].'" hidden>
            
            <div class="form-check">
                <input class="form-check-input mt-3" type="checkbox" name="tecnico[]" id="'.$fila["id_candidato"].'tecnico" onchange="autofilltitle(this.id + 2,this.id);ocultarInputs('.$fila["id_candidato"].')" onclick="$(this).val(this.checked ? '.$fila["id_candidato"].' : 0);autofilltitle(this.id + 2,this.id);">
            
                <label class="form-check-label " for="'.$fila["id_candidato"].'tecnico">
                    Sec. Técnico
                </label>

            </div>',
            '<input type="text" class="form-control" id="'.$fila["id_candidato"].'prom" placeholder="PROM" disabled>',
            '<textarea class="form-control mt-2" name="detalle" id="'.$fila["id_candidato"].'detalle2" rows="2" form="form2'.$fila["id_candidato"].'" hidden></textarea> <textarea class="form-control mt-2" name="detalle[]" id="'.$fila["id_candidato"].'detalle" rows="2" onchange="autofilltitle(this.id + 2,this.id)"></textarea> ',
            '<input type="text" id="'.$fila["id_candidato"].'ausente2" name="ausente" form="form2'.$fila["id_candidato"].'" hidden>
            
            <div class="form-check">
                <input class="form-check-input mt-3" type="checkbox" name="ausente[]" id="'.$fila["id_candidato"].'ausente" onchange="autofilltitle(this.id + 2,this.id);)" onclick="$(this).val(this.checked ? '.$fila["id_candidato"].' : 0);autofilltitle(this.id + 2,this.id);ocultarTodosLosImput('.$fila["id_candidato"].');">
            
                <label class="form-check-label " for="'.$fila["id_candidato"].'ausente">
                    AUSENTE
                </label>

            </div>',
            '<input type="submit" class="btn btn-sm btn-primary" value="CALIFICAR" form="form2'.$fila["id_candidato"].'">',
        );
        array_push($array, $datos_candidato);   
    }
    
    
}

echo $arr = json_encode($array);
echo"}";




?>