<?php
    if(!isset($_SESSION['usuario'])){
        header('Location: ../index.php');
    }
    else{
        ?>
<nav class="navbar navbar-expand-lg navbar-light bg-secondary sticky-top font-weight-bold" style="font-family:'Arial'">
    <a class="navbar-brand pull-left d-lg-none" href="?p=editarpreinscripcion">
        <img src="assets/img/cabecera/LOGO ESESC.png" width="50" height="50" alt="">
        
    </a>
    <button class="navbar-toggler pull-right border-0" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">      
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-center" id="navbarNavDropdown">
        <ul class="navbar-nav text-center">
        <?php
        if($_SESSION['cargo'] == 1){
?>

            <li class="nav-item dropdown">
                <a class="nav-link text-whitesmoke dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" data-hover="dropdown" aria-haspopup="true" aria-expanded="false">ADMINISTRAR
                </a>
                <div class="dropdown-menu bg-secondary rounded-0" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=crearusuario">Usuarios</a>
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="exportarBD.php">Exportar BD</a>
                </div>
            </li>
<?php
        }
        if($_SESSION['cargo'] == 10 ){
?>
        <a class="navbar-brand text-whitesmoke" href="?p=inicio">
                Saf / Inicio
        </a>
        <li class="nav-item">
            <a class="nav-link text-whitesmoke" href="?p=exportarcalificados">EXPORTAR</a>
        </li>

<?php
        }

        if($_SESSION['cargo'] == 9 || $_SESSION['cargo'] == 1){
?>
            <li class="nav-item dropdown">
                <a class="nav-link text-whitesmoke dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" data-hover="dropdown" aria-haspopup="true" aria-expanded="false">INCORPORACIÓN
                </a>
                <div class="dropdown-menu bg-secondary rounded-0" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=editarincorporacion">Estado de incorporación</a>
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=cargarturnosexamenes">Turnos de examen</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link text-whitesmoke dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" data-hover="dropdown" aria-haspopup="true" aria-expanded="false">INSCRIPCIÓN
                </a>
                <div class="dropdown-menu bg-secondary rounded-0" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=editarinscripcion">Estado de inscripción</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link text-whitesmoke dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" data-hover="dropdown" aria-haspopup="true" aria-expanded="false">EXPORTAR
                </a>
                <div class="dropdown-menu bg-secondary rounded-0" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=exportar&tipo=postulantes">Postulantes</a>
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=exportar&tipo=inscripciones">Inscripciones</a>
                    <a class="dropdown-item text-whitesmoke font-weight-bold" href="?p=exportar&tipo=incorporaciones">Incorporaciones</a>
                </div>
            </li>

<?php
    }
    if($_SESSION['cargo'] == 8){
        ?>
            <a class="navbar-brand text-whitesmoke" href="?p=carga_examenes_inicio">
                Sec Sanidad / Inicio
            </a>
            <a class="navbar-brand text-whitesmoke" href="?p=exportarcalificados">
                Exportar
            </a>
        <?php
    }
    if($_SESSION['cargo'] == 7){
        ?>
            <a class="navbar-brand text-whitesmoke" href="?p=carga_examenes_inicio">
                Div Educación Física / Inicio
            </a>
            <a class="navbar-brand text-whitesmoke" href="?p=exportarcalificados">
                Exportar
            </a>
        <?php
    }
    if($_SESSION['cargo'] == 6){
        ?>
        <a class="navbar-brand text-whitesmoke" href="?p=carga_examenes_inicio">
            Div Estudios / Inicio
        </a>
        <a class="navbar-brand text-whitesmoke" href="?p=exportarcalificados">
            Exportar
        </a>
        <?php   
    }
    if($_SESSION['cargo'] == 5){
        ?>
        <a class="navbar-brand text-whitesmoke" href="?p=carga_examenes_inicio">
            Div Evaluación / Inicio
        </a>
        <a class="navbar-brand text-whitesmoke" href="?p=exportarcalificados">
            Exportar
        </a>
        <?php
            }
    ?>
        <li class="nav-item">
            <a class="nav-link text-whitesmoke border" href="logout.php">CERRAR SESIÓN</a>
        </li>
        </ul>
    </div>
</nav>
    <?php
}
?>
