
<?php
include ('../../assets/func/funciones.php');
include ('../../assets/func/class.DBCandidato.php');
ini_set('memory_limit', '5000M');
$conexion = conexion();   

$consulta = "SELECT * FROM inscripcion LEFT JOIN domicilio ON domicilio.id_candidato = inscripcion.id_candidato 
LEFT JOIN plan_carrera ON plan_carrera.id_candidato = inscripcion.id_candidato LEFT JOIN centro_preseleccion ON
centro_preseleccion.id_candidato = inscripcion.id_candidato LEFT JOIN secundario ON
secundario.id_candidato = inscripcion.id_candidato LEFT JOIN soldado_voluntario ON
soldado_voluntario.id_candidato = inscripcion.id_candidato LEFT JOIN folder_documentos ON
inscripcion.id = folder_documentos.id_inscripcion LEFT JOIN candidato ON
candidato.id = inscripcion.id_candidato ORDER BY candidato.id ASC";

$tabla = ejecutarConsulta($consulta,$conexion);

echo'{"data" : ';
$array = array();
foreach($tabla as $fila){
    if($fila['destino'] != NULL){
        $essoldvol = "SI";
    }
    
    else{
        $essoldvol = "NO";
    }
    $datos_candidato = array();
    array_push(
        $datos_candidato,
        $fila['nro_inscripto'],
        $fila['dni'],
        $fila['nombres'],
        $fila['apellidos'],
        $fila['fecha_nacimiento'],
        $fila['sexo'],
        $fila['telefono'],
        $fila['celular'],
        $fila['email'],
        $fila['cuil'],
        $fila['calle'].' - '.$fila['numero'].' - '.$fila['piso'].' - '.$fila['depto'],
        $fila['codigo_postal'],
        $fila['localidad'],
        $fila['provincia'],
        $fila['nacionalidad'],
        $fila['estado_civil'],
        $fila['grupo_sanguineo'],
        $fila['tipo_plan'],
        $fila['orientacion'],
        $fila[20],
        $fila[23],
        $fila[24],
        $fila[28],
        $fila['plan'],
        $fila['anio_cursado'],
        $fila['cant_materias_adeudadas'],
        $essoldvol,
        $fila['destino'],
        $fila['fecha_aceptacion'],
        $fila[2],
        $fila["path"] != "" ? "CARGADA": "NO CARGADA",
        '<div style="max-height:120px;overflow:auto;min-width:100px;">'.$fila['detalle'].'</div>',
    );

    array_push($array, $datos_candidato);   
}
echo $arr = json_encode($array);
// echo $json_string = json_encode($array, JSON_PRETTY_PRINT);
echo"}";
?>