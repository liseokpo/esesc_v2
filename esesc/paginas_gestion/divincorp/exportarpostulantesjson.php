
<?php
include ('../../assets/func/funciones.php');
include ('../../assets/func/class.DBCandidato.php');
ini_set('memory_limit', '1000M');
$conexion = conexion();   

$consulta = "SELECT candidato.* , domicilio.*, plan_carrera.* , centro_preseleccion.*, secundario.titulo, secundario.nombre, secundario.telefono, secundario.plan, secundario.anio_cursado, secundario.cant_materias_adeudadas, soldado_voluntario.*
FROM candidato LEFT JOIN domicilio ON domicilio.id_candidato = candidato.id LEFT JOIN  plan_carrera ON plan_carrera.id_candidato = candidato.id LEFT JOIN  centro_preseleccion ON centro_preseleccion.id_candidato = candidato.id LEFT JOIN  secundario ON secundario.id_candidato = candidato.id LEFT JOIN  soldado_voluntario ON soldado_voluntario.id_candidato = candidato.id ORDER BY candidato.id ASC";
$tabla = ejecutarConsulta($consulta,$conexion);
$cont = 1;
echo'{"data" : ';
$array = array();
foreach($tabla as $fila){
    if($fila['destino'] != NULL){
        $essoldvol = "SI";
    }
    
    else{
        $essoldvol = "NO";
    }
    $datos_candidato = array();
    array_push(
        $datos_candidato,
        $cont,
        $fila['dni'],
        $fila['nombres'],
        $fila['apellidos'],
        $fila['fecha_nacimiento'],
        $fila['sexo'],
        $fila['telefono'],
        $fila['celular'],
        $fila['email'],
        $fila['cuil'],
        $fila['calle'].' - '.$fila['numero'].' - '.$fila['piso'].' - '.$fila['depto'],
        $fila['codigo_postal'],
        $fila['localidad'],
        $fila['provincia'],
        $fila['nacionalidad'],
        $fila['estado_civil'],
        $fila['grupo_sanguineo'],
        $fila['tipo_plan'],
        $fila['orientacion'],
        $fila[31],
        $fila[33],
        $fila[34],
        $fila[35],
        $fila['plan'],
        $fila['anio_cursado'],
        $fila['cant_materias_adeudadas'],
        $essoldvol,
        $fila['destino'],
    );

    array_push($array, $datos_candidato);   
    $cont++;
}

echo $arr = json_encode($array);
// echo $json_string = json_encode($array, JSON_PRETTY_PRINT);
echo"}";
?>