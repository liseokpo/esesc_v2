
<?php
include ('../../assets/func/funciones.php');
include ('../../assets/func/class.DBTurnosExamenes.php');
ini_set('memory_limit', '5000M');
$conexion = conexion();   
    
$consulta = "SELECT incorporacion.*, examen_fisico.*, examen_intelectual.*, examen_medico.*, 
examen_psicologico.*, candidato.dni, candidato.nombres, candidato.apellidos, inscripcion.fecha_aceptacion, inscripcion.nro_inscripto, plan_carrera.tipo_plan, plan_carrera.orientacion, candidato.sexo, soldado_voluntario.destino 
FROM incorporacion LEFT JOIN examen_fisico ON examen_fisico.id_incorporacion = incorporacion.id 
LEFT JOIN examen_intelectual ON examen_intelectual.id_incorporacion = incorporacion.id LEFT JOIN 
examen_medico ON examen_medico.id_incorporacion = incorporacion.id LEFT JOIN examen_psicologico ON
examen_psicologico.id_incorporacion = incorporacion.id LEFT JOIN candidato ON
incorporacion.id_candidato = candidato.id LEFT JOIN inscripcion ON inscripcion.id_candidato = incorporacion.id_candidato LEFT JOIN plan_carrera ON plan_carrera.id_candidato = incorporacion.id_candidato LEFT JOIN soldado_voluntario ON soldado_voluntario.id_candidato = incorporacion.id_candidato WHERE examen_medico.id IS NOT NULL AND examen_intelectual.id IS NOT NULL ORDER BY inscripcion.fecha_aceptacion ASC";



$tabla = ejecutarConsulta($consulta,$conexion);
$DBTurnosExamenes = new DBTurnosExamenes($conexion);

echo'{"data" : ';
$array = array();
foreach($tabla as $fila){
    $ExamenActual = $DBTurnosExamenes -> obtenerTurnoExamenPorId($fila[5]);

    if($ExamenActual == null){
        $turnoExamenActual = "-";
    }
    else{
        $turnoExamenActual = $ExamenActual["nombre"];
    }

    $examenmed = $fila[33];
    $examenint = $fila[28];
    $examenfis = $fila[18];
    $examenpsico = $fila[38];
    if($fila[34] == 1){
        $examenmed = "AUSENTE";
    }
    if($fila[26] == 1){
        $examenint = "AUSENTE";
    }
    if($fila[16] == 1){
        $examenfis = "AUSENTE";
    }
    if($fila[39] == 1){
        $examenpsico = "AUSENTE";
    }

    $datos_candidato = array();
    array_push(
        $datos_candidato,
        $fila['nro_inscripto'],
        $fila['dni'],
        $fila['nombres'],
        $fila['apellidos'],
        $fila['sexo'],
        $fila[46],
        $fila[47],
        $fila[3],
        $fila["tipo_plan"],
        $fila["destino"] == "" ? 'NO SOLD VOL' : 'SOLD VOL',
        $fila["destino"],
        $fila[4],
        $fila[2],
        $fila[1],
        $turnoExamenActual,
        $examenmed,
        $fila[31],
        $examenint,
        $fila[21],
        $fila[22],
        $fila[23],
        $fila[24],
        $examenfis,
        $fila[11],
        $fila[12],
        $fila[13],
        $fila[14],
        $fila[15],
        $examenpsico,
    );

    array_push($array, $datos_candidato);   
}
echo $arr = json_encode($array);
echo"}";


// '<div style="max-height:120px;overflow:auto;min-width:100px;">'.$fila['detalle'].'</div>'
?>