<?php
    session_start();
    if(!isset($_SESSION['usuario'])){
        header('Location: ../../index.php');
    }
    else{
        include ('../../assets/func/funciones.php');
        include ('../../assets/func/class.DBCandidato.php');
        include ('../../assets/func/class.DBEdicion.php');
        include ('../../assets/func/class.DBIncorporacion.php');

        $conexion = conexion();

        $DBCandidato = new DBCandidato($conexion);
        $DBIncorporacion = new DBIncorporacion($conexion);
        $DBEdicion = new DBEdicion($conexion);
        
        $idcandidato = $_POST["idcandidato"];
        $idinscripto = $_POST["idinscripto"];
        $inscripcionAnterior =  $DBCandidato -> obtenerInscripcion($idcandidato);
        $nro = $inscripcionAnterior['nro_inscripto'];
        //modifico inscripcion
        if(isset($_POST['enviarmodificacionestado'])){
            
            // $DBCandidato -> modificarInscripcion($idcandidato,$_POST);
            $DBCandidato -> modificarInscripcionExt($idcandidato,$_POST);

            if($_POST['estadonuevo'] == "SIN NOVEDAD" && $inscripcionAnterior['revision_saf'] == "SIN NOVEDAD"){
                $DBCandidato -> asignarFechaAceptacionInscripcion($idcandidato);
                $DBCandidato -> modificarInscripcionConVal($idcandidato,"INSCRIPTO");
                if($inscripcionAnterior['nro_inscripto'] == "0" || $inscripcionAnterior['nro_inscripto'] == "-"){
                    //asigno numero de inscripto si no tenía uno antes.
                    $consulta = "SELECT nro_inscripto FROM inscripcion WHERE inscripcion.nro_inscripto != '0'";
                    
                    $nrosInscriptos = ejecutarConsulta($consulta,$conexion) ->fetchAll(PDO::FETCH_COLUMN, 0);
                    $nroMaximo = 0;

                    foreach ($nrosInscriptos as $nroInscripto){
                        $nroSinAnio = str_replace("/21", "", $nroInscripto);
                        if($nroSinAnio > $nroMaximo){
                            $nroMaximo = $nroSinAnio;
                        }
                    }

                    $inscriptoNro = $nroMaximo + 1;
                    $nro = $inscriptoNro."/".date("y");
                    echo$nro;
                    
                }
                $DBCandidato -> asignarNroInscripto($nro, $idcandidato);
                if(!$DBIncorporacion -> incorporacionExiste($idcandidato)){
                    $DBIncorporacion -> asignarIncorporacion($idcandidato);
                }
            }
            else{
                if($inscripcionAnterior['estado'] != "EN OBSERVACION" || $_POST["estadonuevo"] == "DESAPROBADO"){
                
                    if($inscripcionAnterior['nro_inscripto'] != "0"){
                        //asigno nroinscripto - si antes tenía uno
                        $DBCandidato -> asignarNroInscripto("-", $idcandidato);
                    }
                    else{
                        //asigno nroinscripto 0 si está en observación o desaprobado
                        $DBCandidato -> asignarNroInscripto(0, $idcandidato);
                    }
                    //asigno fechanull si está en observacion o desaprobado
                    $DBCandidato -> desasignarFechaAceptacionInscripcion($idcandidato);

                    if($_POST["estadonuevo"] == "DESAPROBADO"){
                        $DBCandidato -> modificarInscripcionConVal($idcandidato,"INSCRIPCION NO APROBADA");
                    }
                    else{
                        if($inscripcionAnterior['revision_saf'] != "DESAPROBADO"){
                            $DBCandidato -> modificarInscripcionConVal($idcandidato,"EN OBSERVACION");
                        }
                    }

                    if($DBIncorporacion -> incorporacionExiste($idcandidato)){
                        $DBIncorporacion -> eliminarIncorporacion($idcandidato);
                    }
                }
            }
        }



        //modifico datos personales
        if(isset($_POST['enviardatospers'])){ 
            if($DBCandidato -> dniExiste($_POST['dni']))
            $DBCandidato -> editarDatosPersonales($idcandidato,$_POST);

            if($_POST['dnianterior'] != $_POST['dni'] && $DBCandidato -> dniExiste($_POST['dni'])){   
                header('Location: ../../index.php?p=verdetalleinscripto&inscripcion='.$idinscripto); 
            }
            else{
                if($_POST['emailanterior'] != $_POST['email'] && $DBCandidato -> emailExiste($_POST['email'])){
                    header('Location: ../../index.php?p=verdetalleinscripto&inscripcion='.$idinscripto);  
                }
                else{
                    $DBCandidato -> editarDatosPersonales($idcandidato,$_POST);
        
                    if($DBCandidato -> domicilioExiste($idcandidato)){
                        $DBCandidato -> editarDomicilio($idcandidato,$_POST);
                    }
                    else{
                        $DBCandidato -> cargarDomicilio($idcandidato, $_POST);
                    }
                }
            }
        }

        // modifico plan de carrera y centro preseleccion
        if(isset($_POST['enviarplancarrera'])){ 
            if($DBCandidato -> planCarreraExiste($idcandidato)){
                $DBCandidato -> editarPlanCarrera($idcandidato,$_POST);
            }
            else{
                $DBCandidato -> cargarPlanCarrera($idcandidato, $_POST);
            }
            if($DBCandidato -> centroPreseleccionExiste($idcandidato)){
                $DBCandidato -> editarCentroPreseleccion($idcandidato,$_POST);
            }
            else{
                $DBCandidato -> cargarCentroPreseleccion($idcandidato,$_POST);
            }
        }

        // modifico sold vol
        if(isset($_POST['enviarsoldvol'])){ 
            if($DBCandidato -> soldadoVoluntarioExiste($idcandidato)){
                if($_POST["destinosoldvol"] != ""){
                    $DBCandidato -> editarDestino($idcandidato,$_POST);
                }
                else{
                    $DBCandidato -> eliminarSoldadoVoluntario($idcandidato);
                }
            }
            else{
                if($_POST["destinosoldvol"] != ""){
                    $DBCandidato -> cargarDestino($idcandidato,$_POST);
                }
            }
        }

        // modifico secundario
        if(isset($_POST['enviarsecundario'])){ 
            if($DBCandidato -> secundarioExiste($idcandidato)){
                $DBCandidato -> editarSecundario($idcandidato,$_POST);
            }
            else{
                $DBCandidato -> cargarSecundario($idcandidato,$_POST);
            }
        }


        // habilito las ediciones
        $candidato = $DBCandidato -> obtenerCandidatoporId($idcandidato);
        $url = 'ingreso.php';
        $enviarEmail = false;

        if(isset($_POST["he_datospersonales"])){
            $DBEdicion -> cargarEdicion($_SESSION["usuario"],$idcandidato,$_POST["he_datospersonales"]);
            $enviarEmail = true;
        }
        if(isset($_POST["he_plancarrera"])){
            $DBEdicion -> cargarEdicion($_SESSION["usuario"],$idcandidato,$_POST["he_plancarrera"]);
            $enviarEmail = true;
        }
        if(isset($_POST["he_soldvol"])){
            $DBEdicion -> cargarEdicion($_SESSION["usuario"],$idcandidato,$_POST["he_soldvol"]);  
            $enviarEmail = true;
        }
        if(isset($_POST["he_secundario"])){
            $DBEdicion -> cargarEdicion($_SESSION["usuario"],$idcandidato,$_POST["he_secundario"]);            
            $enviarEmail = true;
        }
        if(isset($_POST["he_fotosdni"])){
            $DBEdicion -> cargarEdicion($_SESSION["usuario"],$idcandidato,$_POST["he_fotosdni"]); 
            $enviarEmail = true;
        }

        if($enviarEmail){
            sendMail($candidato["email"], $candidato['nombres'].' '.$candidato['apellidos'], $url, "edicion_datos_habilitada", "Edición habilitada - ESESC", 2);
        }

        header('Location: ../../index.php?p=verdetalleinscripto&inscripcion='.$idinscripto);
    }
?>

















    