
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <head>
        <title>HTML Elements Reference</title>
    </head>
    <section id="cuerpo">
<?php

if(isset($_GET["tipo"])){
    
    if($_GET["tipo"] == "postulantes"){
        $titulo = 'Postulantes - Escuela de Suboficiales del Ejército "Sargento Cabral"';
        echo'
        <div class="text-center">
            <h2>En esta página podrá exportar los datos de los postulantes a EXCEL.</h2>   
            <br>
        </div>';

        $pagina = "paginas_gestion/divincorp/exportarpostulantesjson.php";
        echo'
        
        
    
        <table id="example" class="display w-100">
            <thead>
                <tr>
                    <th></th>
                    <th>DNI</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Fecha de Nacimiento</th>
                    <th>Sexo</th>
                    <th>Teléfono</th>
                    <th>Celular</th>
                    <th>Email</th>
                    <th>Cuil</th>
                    
                    <!-- DOMICILIO -->
                    <th>Domicilio</th>
                    
                    <th>Codigo Postal</th>
                    <th>Localidad</th>
                    <th>Provincia</th>
                    <th>Nacionalidad</th>
                    <th>Estado Civil</th>
                    <th>Grupo Sanguineo</th>
    
                    <!-- PLAN DE CARRERA -->
                    <th>Plan</th>
                    <th>Orientación</th>
    
                    <!-- Centro de preseleccion -->
                    <th>Centro de Preselección</th>
    
                    <!-- Secundario -->
                    <th>Título del secundario</th>
                    <th>Nombre del secundario</th>
                    <th>Telefono del secundario</th>
                    <th>Tipo de plan</th>
                    <th>Año cursado</th>
                    <th>Materias adeudadas</th>
    
                    <!-- SOLDADO VOLUNTARIo SI/NO -->
                    <th>Soldado Voluntario</th>
                    <!-- SI -->
                    <th>Destino</th>
                </tr>
            </thead>
            
        </table>
    

        ';
    }
    
    if($_GET["tipo"] == "inscripciones"){
        $titulo = 'Inscripciones - Escuela de Suboficiales del Ejército "Sargento Cabral"';
        echo'
        <div class="text-center">
            <h2>En esta página podrá exportar los datos de las inscripciones a EXCEL.</h2>   
            <br>
        </div>';

        $pagina = "paginas_gestion/divincorp/exportarinscripcionesjson.php";
        echo'
        
        <table id="example" class="display w-100">
            <thead>
                <tr>
                    <th>NRO INSCRIPTO</th>
                    <th>DNI</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Fecha de Nacimiento</th>
                    <th>Sexo</th>
                    <th>Teléfono</th>
                    <th>Celular</th>
                    <th>Email</th>
                    <th>Cuil</th>
                    
                    <!-- DOMICILIO -->
                    <th>Domicilio</th>
                    
                    <th>Codigo Postal</th>
                    <th>Localidad</th>
                    <th>Provincia</th>
                    <th>Nacionalidad</th>
                    <th>Estado Civil</th>
                    <th>Grupo Sanguineo</th>
    
                    <!-- PLAN DE CARRERA -->
                    <th>Plan</th>
                    <th>Orientación</th>
    
                    <!-- Centro de preseleccion -->
                    <th>Centro de Preselección</th>
    
                    <!-- Secundario -->
                    <th>Título del secundario</th>
                    <th>Nombre del secundario</th>
                    <th>Telefono del secundario</th>
                    <th>Tipo de plan</th>
                    <th>Año cursado</th>
                    <th>Materias adeudadas</th>
    
                    <!-- SOLDADO VOLUNTARIo SI/NO -->
                    <th>Soldado Voluntario</th>
                    <!-- SI -->
                    <th>Destino</th>
                    
                    <th>Fecha de aceptacion</th>
                    <th>Estado de Inscripción</th>
                    <th>Estado de Documentación</th>
                    <th>Detalle Div Ext</th>
                </tr>
            </thead>
        </table>
        ';
    }
    if($_GET["tipo"] == "incorporaciones"){
        $titulo = 'Incorporaciones - Escuela de Suboficiales del Ejército "Sargento Cabral"';
        echo'
        <div class="text-center">
            <h2>En esta página podrá exportar los datos de las incorporaciones a EXCEL.</h2>   
            <br>
        </div>';

        $pagina = "paginas_gestion/divincorp/exportarincorporacionesjson.php";

        echo'
        
        <table id="example" class="display w-100">
            <thead>
                <tr>
                    <th>NRO INSCRIPTO</th>
                    <th>DNI</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Sexo</th>
                    <th>CARRERA</th>
                    <th>ORIENTACIÓN</th>
                    <th>Mérito</th>
                    <th>SECUNDARIO</th>
                    <th>SOLD VOL</th>
                    <th>DESTINO</th>
                    <th>Estado de Incorporacion</th>
                    <th>Fecha de modificacion</th>
                    <th>Detalle de Incorporacion</th>
                    <th>Turno de Examenes</th>
                    <th>EXAMEN MEDICO</th>
                    <th>Detalle EXAMEN MEDICO</th>
                    <th>EXAMEN INT</th>
                    <th>Nota lengua</th>
                    <th>Nota matemática</th>
                    <th>Promedio EXAMEN INT</th>
                    <th>Detalle EXAMEN INT</th>
                    <th>EXAMEN FIS</th>
                    <th>Puntaje FFBB</th>
                    <th>Puntaje ABDO</th>
                    <th>Puntaje TROTE</th>
                    <th>Promedio EXAMEN FIS</th>
                    <th>Detalle EXAMEN FIS</th>
                    <th>EXAMEN PSICO</th>
                </tr>
            </thead>
        </table>
        ';
    }
    
}

?>
    </section>

<script>

    document.title = '<?php echo$titulo; ?>';
    $(document).ready(function(){
        $('#example').DataTable({

            "ajax": '<?php echo$pagina; ?>',
            "deferRender": true,
            "orderClasses": false,
            dom: 'Bfrtip',
            title: "export-title",
            buttons:[
                {
                    extend: 'excel',
                    text: 'Exportar a excel',
                },
            ],
            responsive: true,
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
        });
    });
</script>