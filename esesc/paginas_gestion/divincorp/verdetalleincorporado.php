<?php
    if(!isset($_SESSION['usuario'])){
        header('Location: ../../index.php');
    }
    else{

?>
<?php
    include ('assets/func/class.DBTurnosExamenes.php');

    $idinsc = $_GET["idinc"];
    $conexion = conexion();   
    $consulta = "SELECT incorporacion.*, examen_fisico.*, examen_intelectual.*, examen_medico.*, 
    examen_psicologico.*, candidato.dni, inscripcion.fecha_aceptacion, inscripcion.nro_inscripto, plan_carrera.tipo_plan 
    FROM incorporacion LEFT JOIN examen_fisico ON examen_fisico.id_incorporacion = incorporacion.id 
    LEFT JOIN examen_intelectual ON examen_intelectual.id_incorporacion = incorporacion.id LEFT JOIN 
    examen_medico ON examen_medico.id_incorporacion = incorporacion.id LEFT JOIN examen_psicologico ON
    examen_psicologico.id_incorporacion = incorporacion.id LEFT JOIN candidato ON 
    incorporacion.id_candidato = candidato.id LEFT JOIN inscripcion ON inscripcion.id_candidato = incorporacion.id_candidato LEFT JOIN plan_carrera ON plan_carrera.id_candidato = incorporacion.id_candidato WHERE incorporacion.id = '$idinsc' ORDER BY inscripcion.fecha_aceptacion ASC";
    $DBCandidato = new DBCandidato($conexion);
    $DBTurnosExamenes = new DBTurnosEXamenes($conexion);
    $consultaSiguiente = "SELECT incorporacion.id FROM incorporacion LEFT JOIN inscripcion ON inscripcion.id_candidato = incorporacion.id_candidato WHERE inscripcion.fecha_aceptacion > (SELECT inscripcion.fecha_aceptacion FROM incorporacion LEFT JOIN inscripcion ON inscripcion.id_candidato = incorporacion.id_candidato WHERE incorporacion.id = '$idinsc' ORDER BY inscripcion.fecha_aceptacion ASC) AND incorporacion.merito IS NOT NULL ORDER BY inscripcion.fecha_aceptacion ASC LIMIT 1";
    $consultaAnterior = "SELECT incorporacion.id FROM incorporacion LEFT JOIN inscripcion ON inscripcion.id_candidato = incorporacion.id_candidato WHERE inscripcion.fecha_aceptacion < (SELECT inscripcion.fecha_aceptacion FROM incorporacion LEFT JOIN inscripcion ON inscripcion.id_candidato = incorporacion.id_candidato WHERE incorporacion.id = '$idinsc' ORDER BY inscripcion.fecha_aceptacion ASC) AND incorporacion.merito IS NOT NULL ORDER BY inscripcion.fecha_aceptacion DESC LIMIT 1";

    $tabla = ejecutarConsulta($consulta,$conexion);
    $siguienteRegistro = ejecutarConsulta($consultaSiguiente,$conexion) -> fetch(PDO::FETCH_ASSOC);
    
    $anteriorRegistro = ejecutarConsulta($consultaAnterior,$conexion) -> fetch(PDO::FETCH_ASSOC);
    
    foreach($tabla as $fila){      
        
        if($fila[5] != null){
            $turnoExamenActual = $DBTurnosExamenes -> obtenerTurnoExamenPorId($fila[6]);
            if($turnoExamenActual){
                $nombreTurnoExamen = $turnoExamenActual["nombre"];
            }
            else{
                $nombreTurnoExamen = "";
            }
        }
        else{
            $nombreTurnoExamen = "";
        }
        $candidato = $DBCandidato -> obtenerCandidatoporId($fila["id_candidato"]);
    ?>
    <section id="cuerpofull">

    <h3 class="shadow p-3 pb-5 pt-0 mt-3 bg-white w-auto d-none d-lg-block" style="position: -webkit-sticky;position: sticky;top: 52px;z-index:1000;"><?php echo$candidato["nombres"].' '.$candidato["apellidos"];?> <b class="border border-dark p-1"><?php echo"NRO INSC: ".$fila["nro_inscripto"]; ?></b> <b class="border border-dark p-1"><?php echo"MÉRITO: ".$fila[3]; ?></b> <b class="border border-dark p-1"><?php echo$fila["tipo_plan"]; ?></b> <?php if(isset($siguienteRegistro["id"])){echo'<br><br><a class="btn btn-primary float-right ml-1" href="?p=verdetalleincorporado&idinc='.$siguienteRegistro['id'].'">Siguiente registro</a>';}; if(isset($anteriorRegistro["id"])){echo'<a class="btn btn-primary float-right" href="?p=verdetalleincorporado&idinc='.$anteriorRegistro["id"].'">Anterior</a>';}?></h3>
    <div style="position: -webkit-sticky;position: sticky;top: 72px;z-index:1000;"><h5 class="shadow p-3 pt-0 mt-3 bg-white w-auto d-lg-none"><?php echo$candidato["nombres"].'<br>'.$candidato["apellidos"]?> <b class="border border-dark"><?php echo"NRO INSC: ".$fila["nro_inscripto"]; ?></b><b class="border border-dark p-1"><?php echo"MÉRITO: ".$fila[3]; ?></b> <b class="border border-dark p-1"><?php echo$fila["tipo_plan"]; ?></b>   <?php if(isset($siguienteRegistro["id"])){echo'<a class="btn btn-primary float-right ml-1" href="?p=verdetalleincorporado&idinc='.$siguienteRegistro['id'].'">Siguiente registro</a>';}; if(isset($anteriorRegistro["id"])){echo'<a class="btn btn-primary float-right" href="?p=verdetalleincorporado&idinc='.$anteriorRegistro["id"].'">Anterior</a>';}?>  </h5> </div>
                
    <div class="row mw-100">
        <div class="col-7">
            <div class="contenidoizquierda w-100">
                <h3>Examen Médico</h3>
                <div class="table-responsive">
                    <table class="table bg-white">
                        <?php if($fila[32] != ''){ ?>

                        <thead>
                            <tr>
                            <th scope="col">Estado</th>
                            <th scope="col">Presentismo</th>
                            <th scope="col">Fecha de carga</th>
                            <th scope="col">Detalle</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td><?php echo$fila[33]; ?></td>
                            <td><?php echo$presentismo = $fila[33] == 0 ? 'PRESENTE' : 'AUSENTE'; ?></td>
                            <td><?php echo$fila[32]; ?></td>
                            <td><div style="max-height:120px;overflow:auto;min-width:100px;"><?php echo$fila[31]; ?></div></td>
                            </tr>
                        </tbody>
                        <?php } 
                            else{
                                echo"<h5 class='ml-2'>Sin calificar</h5>";
                            }
                        ?>
                    </table>
                </div>
                <h3>Examen Intelectual</h3>
                <div class="table-responsive">
                    <table class="table bg-white overflow-auto">
                        <?php if($fila[27] != ''){ ?>
                        <thead>
                            <tr>
                            <th scope="col">Estado</th>
                            <th scope="col">Presentismo</th>
                            <th scope="col">Nota lengua</th>
                            <th scope="col">Nota mate<br>mática</th>
                            <th scope="col">Promedio</th>
                            <th scope="col">Secundario</th>
                            <th scope="col">Fecha de carga</th>
                            <th scope="col">Detalle</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                            <td><?php echo$fila[28]; ?></td>
                            <td><?php echo$presentismo = $fila[26] == 0 ? 'PRESENTE' : 'AUSENTE'; ?></td>
                            <td><?php echo$fila[21]; ?></td>
                            <td><?php echo$fila[22]; ?></td>
                            <td><?php echo$fila[23]; ?></td>
                            <td><?php echo$presentismo = $fila[25] == 0 ? 'NO TÉCNICO' : 'TÉCNICO'; ?></td>
                            <td><?php echo$fila[27]; ?></td>
                            <td><div style="max-height:120px;overflow:auto;min-width:100px;"><?php echo$fila[24]; ?></div></td>
                            </tr>
                        </tbody>
                        <?php } 
                            else{
                                echo"<h5 class='ml-2'>Sin calificar</h5>";
                            }
                        ?>
                    </table>
                </div>

                <h3>Examen Físico</h3>

                <div class="table-responsive">
                    <table class="table bg-white overflow-auto">
                    <?php if($fila[17] != ''){ ?>
                        <thead>
                            <tr>
                            <th scope="col">Estado</th>
                            <th scope="col">Presentismo</th>
                            <th scope="col">Pje FFBB</th>
                            <th scope="col">Pje ABDO</th>
                            <th scope="col">Pje TROTE</th>
                            <th scope="col">Promedio</th>
                            <th scope="col">Fecha de carga</th>
                            <th scope="col">Detalle</th>
                            </tr>
                        </thead>
                        <tbody>
                            

                            <tr>
                            <td><?php echo$fila[18]; ?></td>
                            <td><?php echo$presentismo = $fila[16] == 0 ? 'PRESENTE' : 'AUSENTE'; ?></td>
                            <td><?php echo$fila[11]; ?></td>
                            <td><?php echo$fila[12]; ?></td>
                            <td><?php echo$fila[13]; ?></td>
                            <td><?php echo$fila[14]; ?></td>
                            <td><?php echo$fila[17]; ?></td>
                            <td><div style="max-height:120px;overflow:auto;min-width:100px;"><?php echo$fila[15]; ?></div></td>
                            </tr>
                        </tbody>
                        <?php }
                        else{
                            echo"<h5 class='ml-2'>Sin calificar</h5>";
                        }
                        
                        ?>

                    </table>
                </div>

                <h3>Examen Psicológico</h3>

                <div class="table-responsive">
                    <table class="table bg-white overflow-auto">
                    <?php if($fila[37] != ''){ ?>

                        <thead>
                            <tr>
                            <th scope="col">Estado</th>
                            <th scope="col">Presentismo</th>
                            <th scope="col">Fecha de carga</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td><?php echo$fila[38]; ?></td>
                            <td><?php echo$presentismo = $fila[38] == 0 ? 'PRESENTE' : 'AUSENTE'; ?></td>
                            <td><?php echo$fila[37]; ?></td>
                            </tr>
                        </tbody>
                        <?php }
                        else{
                            echo"<h5 class='ml-2'>Sin calificar</h5>";
                        }
                        
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-5">
            <div class="contenidoderecha d-none d-lg-block w-100" style="top:118px">
                <form action="paginas_gestion/divincorp/metamodificarincorporacion.php" method="POST">
                    <div class="row">
                        <div class="col">
                            <h5>Estado de incorporación</h5>

                            <select class="custom-select" name="estadonuevo" id="estadonuevo" required onchange="showHide();">
                                <option value="" selected disabled><?php echo$fila[4]; ?></option>
                                <option value="EN OBSERVACION">EN OBSERVACION</option>
                                <option value="APROBADO">APROBADO</option>
                                <option value="RESERVA">RESERVA</option>
                                <option value="DESAPROBADO">DESAPROBADO</option>
                            </select>
                        </div>
                        
                        <div class="col">
                            <h5>Turno examen</h5>

                            <select class="custom-select" name="turnoexamennuevo" id="turnoexamennuevo" required>
                                <option value="" selected disabled><?php echo$nombreTurnoExamen; ?></option>
                                <?php
                                    $listaTurnosExamenes = $DBTurnosExamenes -> obtenerListaTurnosExamenes();
                                    echo$listaTurnosExamenes;
                                    foreach($listaTurnosExamenes as $turnoExamen){
                                        echo'<option value="'.$turnoExamen["id"].'">'.$turnoExamen["nombre"].'</option> ';
                                    }
                                ?>
                            </select>
                        </div>
                        

                    </div>
                    <textarea class="form-control mt-2" name="detallenuevo" id="detallenuevo" placeholder="Escriba un Detalle" rows="4"><?php echo$fila[1]; ?></textarea> 

                    <input type="hidden" name="idcandidato" value="<?php echo$candidato["id"]; ?>"/>
                    <input type="hidden" name="idincorp" value="<?php echo$fila[0]; ?>"/>
                    <input type="submit" name="enviarmodificacionestado" class="submit btn btn-primary mt-2" value="MODIFICAR" />
                    <a href="?p=editarinscripcion" class="btn btn-primary btn-md mt-2" title="Volver a pagina anterior">Volver</a>

                    <br><br>
                    <div id="speach" style="display:none;">
                        Speach que verá el postulante además del detalle <br>
                        <div class="p-1 bg-primary">
                            <i id="speach_contenido"></i>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
    </section>

<?php
    }

}
?>

<script>
    function showHide(){                
        if(document.getElementById('estadonuevo').value == ""){
            document.getElementById('speach').removeAttribute("style");   
            document.getElementById('speach').style.display = 'none';
        }
        else{
            document.getElementById('speach').removeAttribute("style");   
            document.getElementById('speach').style.display = 'block';
            if(document.getElementById('estadonuevo').value == "APROBADO"){
                document.getElementById('speach_contenido').innerHTML = '';
                document.getElementById('speach_contenido').innerHTML = 'Usted se encuentra dentro de las vacantes para  el segundo proceso de admisión a la ESCUELA DE SUBOFICIALES DEL EJÉRCITO  “SARGENTO CABRAL”. Para incorporarse definitivamente deberá aprobar los exámenes psicológicos y el segundo reconocimiento médico, que incluyen: control del IMC, Test de embarazo para el personal femenino y examen toxicológico. <br><br> Además deberá presentar documentación que acredite sus estudios finalizados, de acuerdo a lo determinado en su Declaración Jurada, de no ser así no será admitido/a como Aspirante. <br><br> Examen psicológico del 15 al 19  de Marzo a las 07:30 AM en la Escuela de Suboficiales (Ruta 202 S/Nro Campo de Mayo). Vestuario:  •	camisa blanca, traje negro, corbata y zapatos negros (personal masculino). •	camisa blanca, pollera negra y zapatos negros (personal femenino). Útiles: lapicera negra, lápiz, goma de borrar. Tiempo estimado de los exámenes y permanencia será de 0800 a 1300 hs <br><br> Saludos cordiales la División Extensión - ESESC.';
            }
            else if(document.getElementById('estadonuevo').value == "RESERVA"){
                document.getElementById('speach_contenido').innerHTML = 'Usted no se encuentra dentro de las primeras vacantes para el ingreso a la ESCUELA DE SUBOFICIALES DEL EJÉRCITO "SARGENTO CABRAL", le informamos que será tenido en cuenta para eventuales vacantes en el instituto, desde el 05 de Abril hasta el 26 de Abril. <br>Atte "Div Extensión"';
            }
            else if(document.getElementById('estadonuevo').value == "DESAPROBADO"){
                document.getElementById('speach_contenido').innerHTML = '';
                document.getElementById('speach_contenido').innerHTML = 'Lamentamos  informarle que no ha ingresado dentro de las vacantes  para la ESCUELA DE SUBOFICIALES DEL EJÉRCITO  “SARGENTO CABRAL” <br><br> Saludos cordiales la División Extensión - ESESC ';
            }
            else if(document.getElementById('estadonuevo').value == "EN OBSERVACION"){
                document.getElementById('speach_contenido').innerHTML = '';
                document.getElementById('speach_contenido').innerHTML = 'En estos momentos, su incorporación está siendo procesada y revisada. <br>Atte "Div Extensión"';

            }
        }
    }
</script>