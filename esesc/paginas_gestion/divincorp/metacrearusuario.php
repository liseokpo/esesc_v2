<?php

session_start();
if(!isset($_SESSION['usuario']) && $_SESSION['cargo'] != 1){
    header('Location: ../../index.php');
}
else{
    include ('../../assets/func/funciones.php');
    include ('../../assets/func/class.DBUsuario.php');
    
    $conexion = conexion();

    $DBUsuarios = new DBUsuario($conexion);  

    // print_r($_POST);

    if(isset($_POST["idUsuario"]) && $_POST["modifcargo"] == 0){
        $DBUsuarios -> deshabilitarUsuario($_POST["idUsuario"]);
        $result = "Usuario modificado exitosamente.";    
        header('Location: ../../index.php?p=crearusuario&success='.urlencode($result).'');
    }
    else if(isset($_POST["idUsuario"]) && filter_var($_POST["modifemail"], FILTER_VALIDATE_EMAIL)){
        $usuarioAnterior = $DBUsuarios -> obtenerUsuarioporId($_POST["idUsuario"]);
        
        if($_POST['modifusuario'] != $usuarioAnterior['user'] && $DBUsuarios -> nombreUsuarioExiste($_POST['modifusuario'])){
            //nombre de usuario ya existe
            $result = "El nombre de usuario que intenta colocar ya existe.";
            header('Location: ../../index.php?p=crearusuario&valid='.urlencode($result).''); 
        }
        
        else if($_POST['modifemail'] != $usuarioAnterior['email'] && ($DBUsuarios -> emailExiste($_POST['modifemail']) || $DBUsuarios -> emailPostulantesExiste($_POST['modifemail']))){
            //email ya existe
            $result = "El email que intenta colocar ya se encuentra registrado en otro usuario o en un postulante.";
            header('Location: ../../index.php?p=crearusuario&valid='.urlencode($result).''); 
        }
        
        else{
            $DBUsuarios -> modificarUsuario($_POST["modifusuario"], $_POST["modifemail"], $_POST["modifcargo"], $_POST["idUsuario"]);
            $result = "Usuario modificado exitosamente.";    
            header('Location: ../../index.php?p=crearusuario&success='.urlencode($result).'');
        }
        
    }
    else if(!is_null($result = $DBUsuarios -> validarUserYEmail($_POST["usuario"], $_POST["email"])) || !filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
        header('Location: ../../index.php?p=crearusuario&valid='.urlencode($result).''); 
    }
    else{
        $DBUsuarios -> cargarUsuario($_POST["usuario"],$_POST["email"],$_POST["cargo"],"us3r3s3sc");
        $result = "Usuario creado exitosamente.";    
        header('Location: ../../index.php?p=crearusuario&success='.urlencode($result).'');
    }
}


?>