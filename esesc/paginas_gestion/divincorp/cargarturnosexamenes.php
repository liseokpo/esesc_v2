<?php
include ('assets/func/class.DBTurnosExamenes.php');

if(!isset($_SESSION['usuario'])){
    header('Location: ../../index.php');
}
else{ 
    $DBTurnosExamenes = new DBTurnosExamenes($conexion);
    
    $listaTurnosExamenes = $DBTurnosExamenes -> obtenerListaTurnosExamenes();

    
?>



<section id="cuerpo">

<h3 class="text-center">Turnos de exámenes
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Agregar turno
</button></h3>
<br>


<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Turno</th>
      <th scope="col">Fecha desde</th>
      <th scope="col">Fecha hasta</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    

<?php
$cont = 0;
foreach($listaTurnosExamenes as $turno){
    $cont ++;
        ?>

    <tr>
      <th scope="row"><?php echo$cont; ?></th>
      <td><?php echo$turno["nombre"]; ?></td>
      <td><?php echo$turno["fecha_desde"]; ?></td>
      <td><?php echo$turno["fecha_hasta"]; ?></td>
      <td><?php echo'<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal'.$turno["id"].'">
        Modificar turno
      </button>

      <div class="modal fade" id="modal'.$turno["id"].'" tabindex="-1" role="dialog" aria-labelledby="modalLabel'.$turno["id"].'" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="modalLabel'.$turno["id"].'">Modificar '.$turno["nombre"].'</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form action="paginas_gestion/divincorp/metacargarturnosexamenes.php" method="POST">
                  <div class="modal-body">
                      
						  <div class="form-group">
							<input type="hidden" value="'.$turno["id"].'" name="idTurno">
                              <label for="exampleInputEmail1">Nombre</label>
                              <input type="text" name="nombre" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="'.$turno["nombre"].'" placeholder="Ejemplo 1er Turno">
                              <br>
                              <label for="fdesde">Fecha desde</label>
                              <input type="date" name="fdesde" class="form-control" id="fdesde" aria-describedby="emailHelp" value="'.$turno["fecha_desde"].'" placeholder="Ingrese nombre">
                              <br>
                              <label for="fhasta">Fecha hasta</label>
                              <input type="date" name="fhasta" class="form-control" id="fhasta" aria-describedby="emailHelp" value="'.$turno["fecha_hasta"].'" placeholder="Ingrese nombre">
                              <br>
                          </div>
                      
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                      <input type="submit" class="btn btn-primary" value="Guardar">
                      
                  </div>
              </form>
              </div>
          </div>
      </div>


      '; ?></td>
    </tr>

        <?php   
    }
?>
  </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Agregar turno</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="paginas_gestion/divincorp/metacargarturnosexamenes.php" method="POST">
            <div class="modal-body">
                
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nombre</label>
                        <input type="text" name="nombre" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ejemplo 1er Turno">
                        <br>
                        <label for="fdesde">Fecha desde</label>
                        <input type="date" name="fdesde" class="form-control" id="fdesde" aria-describedby="emailHelp" placeholder="Ingrese nombre">
                        <br>
                        <label for="fhasta">Fecha hasta</label>
                        <input type="date" name="fhasta" class="form-control" id="fhasta" aria-describedby="emailHelp" placeholder="Ingrese nombre">
                        <br>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <input type="submit" class="btn btn-primary" value="Guardar">
                
            </div>
        </form>
        </div>
    </div>
</div>









</section>
<?php
    }
?>
