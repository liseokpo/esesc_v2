<?php
    if(!isset($_SESSION['usuario'])){
        header('Location: ../../index.php');
    }
    else{
        
?>

<!-- <section id="cuerpo" style="color:#444444">
        <div class="container p-0 m-0 mw-100">
            <div class="row m-0 p-0">
                <div class="col-12 col-lg-9 m-0 p-0 pr-lg-2">
                    <h3 class="titulo fullancho text-left">NOVEDADES<a href="?p=agregarnoticia" class="btn text-white btn-sm" style="background-color:#007bff;">+</a></h3>
                    <div id="carouselNovedades" class="carousel slide carousel-fade" data-ride="carousel" >
                        <div class="carousel-inner">
                            <div class="carousel-item active carousel('pause')">
                                <div class="w-100 h-100 position-absolute d-flex justify-content-end align-items-end">
                                    <a href="?p=noticia11" class="btn-sm text-white mb-lg-4 mb-md-3 mb-sm-2 mb-2 mr-lg-7 mr-md-7 mr-sm-5 mr-3" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>
                                <img class="d-block w-100" src="assets/img/noticias/chile_portada.jpg" alt="...">
                            </div>
                            <div class="carousel-item">
                                <div class="w-100 h-100 position-absolute d-flex justify-content-end align-items-end">
                                    <a href="?p=noticia10" class="btn-sm text-white mb-lg-4 mb-md-3 mb-sm-2 mb-2 mr-lg-7 mr-md-7 mr-sm-5 mr-3" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>
                                <img class="d-block w-100" src="assets/img/noticias/cerezo_portada.jpg" alt="...">
                            </div>
                            <div class="carousel-item">
                                <div class="w-100 h-100 position-absolute d-flex justify-content-end align-items-end">
                                    <a href="?p=noticia9" class="btn-sm text-white mb-lg-4 mb-md-3 mb-sm-2 mb-2 mr-lg-7 mr-md-7 mr-sm-5 mr-3" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>
                                <img class="d-block w-100" src="assets/img/noticias/panameños_portada.jpg" alt="...">
                            </div>
                            <div class="carousel-item">
                                <div class="w-100 h-100 position-absolute d-flex justify-content-end align-items-end">
                                    <a href="?p=noticia8" class="btn-sm text-white mb-lg-4 mb-md-3 mb-sm-2 mb-2 mr-lg-7 mr-md-7 mr-sm-5 mr-3" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>
                                <img class="d-block w-100" src="assets/img/noticias/apto_condicional.jpg" alt="...">
                            </div>
                            <div class="carousel-item">
				                <div class="w-100 h-100 position-absolute d-flex justify-content-end align-items-start">
                                    <a href="?p=listaaprobados" class="btn-sm text-white mt-lg-4 mt-md-3 mt-sm-2 mt-2 mr-lg-7 mr-md-7 mr-sm-5 mr-3" style="background-color:#007bff;">VER LISTA</a>
                                </div>
                                <img class="d-block w-100" src="assets/img/yadisponible.jpg" alt="...">
                            </div>
                            <div class="carousel-item">
                                <div class="w-100 h-100 position-absolute d-flex justify-content-end align-items-end">
                                    <a href="?p=noticia7" class="btn-sm text-white mb-lg-4 mb-md-3 mb-sm-2 mb-2 mr-lg-7 mr-md-7 mr-sm-5 mr-3" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>
                                <img class="d-block w-100" src="assets/img/noticias/cepeda_portada.jpg" alt="...">
                            </div>
                            <div class="carousel-item">
                                <div class="w-100 h-100 position-absolute d-flex justify-content-end align-items-start">
                                    <a href="?p=noticia6" class="btn-sm text-white mt-lg-4 mt-md-3 mt-sm-2 mt-2 mr-lg-7 mr-md-7 mr-sm-5 mr-3" style="background-color:#007bff;">VER NOTICIA</a>
                                </div>
                                <img class="d-block w-100" src="assets/img/noticias/incorporacion_portada.jpg" alt="...">
                            </div>
                        </div>

                        <a class="carousel-control-prev" href="#carouselNovedades" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Anterior</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselNovedades" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Siguiente</span>
                        </a>
                    </div>
                    
                </div>
                
                <div class="col-12 col-lg-3 p-lg-0 m-0 p-0 pt-2 text-center position-relative" >
                <h3 class="titulo fullancho text-left">VIDEO</h3>
                    
                    <a href="#" onclick="lightbox_open();">      
                        <div class="position-absolute w-100 d-flex align-items-center justify-content-center my-auto portadavidlat">
                            <img src="assets/img/play.png" class="" style="max-width:30%;">                
                        </div>
                        <img src="assets/img/vid_lat_2.jpg" class="portadavidlat">
                    </a>
            
                    
                </div>
            </div>
        </div>    
        <div id="light">
                        <a class="boxclose" id="boxclose" onclick="lightbox_close();"></a>
                        <video id="VisaChipCardVideo" controls>
                            <source src="assets/img/esesc_video.mp4" type="video/mp4">
                        </video>
                    </div>

                    <div id="fadevid" onClick="lightbox_close();"></div>
        </div>
        <div class="pt-2"></div>
        <h3 class="titulo fullancho text-left">LA ESCUELA EN IMAGENES</h3>
        <div id="carouselGaleria" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselGaleria" data-slide-to="0" class="active"></li>
                <li data-target="#carouselGaleria" data-slide-to="1"></li>
                <li data-target="#carouselGaleria" data-slide-to="2"></li>
                <li data-target="#carouselGaleria" data-slide-to="3"></li>
                <li data-target="#carouselGaleria" data-slide-to="4"></li>
                <li data-target="#carouselGaleria" data-slide-to="5"></li>
                <li data-target="#carouselGaleria" data-slide-to="6"></li>
                <li data-target="#carouselGaleria" data-slide-to="7"></li>
                <li data-target="#carouselGaleria" data-slide-to="8"></li>
                <li data-target="#carouselGaleria" data-slide-to="9"></li>
                
            </ol>
            <div class="carousel-inner"style="max-height:500px">
                <div class="carousel-item active">
                    <img src="assets/img/galeria/01-galeria.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/02-galeria.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/03-galeria.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/04-galeria.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/05-galeria.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/06-galeria.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/07-galeria.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/08-galeria.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/09-galeria.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/img/galeria/10-galeria.jpg" class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselGaleria" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselGaleria" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

    </section> -->

<!-- <section id="cuerpo">
    <i class="historiap">
    <li>Bienvenido <?php //echo$usuario.". Usted tiene el cargo de: ".$cargo ?> </li>
    </i>
</section> -->
<?php
    };
?>
