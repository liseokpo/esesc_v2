<?php
include ('assets/func/class.DBTurnosExamenes.php');

if(!isset($_SESSION['usuario']) && $_SESSION['cargo'] != 1 ){
    header('Location: ../../index.php');
}
else{ 
    $DBUsuarios = new DBUsuario($conexion);
    
    $listaUsuarios = $DBUsuarios -> obtenerUsuarios();

    
?>

<section id="cuerpo">
<br>
<h3 class="text-center">Usuarios
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    <i class="fas fa-user-plus ml-1"></i>
    </button>
</h3>
<br>

<?php
    if(isset($_GET['valid'])){
        echo'<div class="alert alert-danger alert-dismissible fade show" role="alert">'.$_GET['valid'].'
        <button type="button" class="close" id="cartelalert" data-dismiss="alert" aria-label="Close" onclick="cleanUrlUntil(\'&\')">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>';
    }

    if(isset($_GET['success'])){
        echo'<div class="alert alert-success alert-dismissible fade show" role="alert">'.$_GET['success'].'
        <button type="button" class="close" id="cartelalert" data-dismiss="alert" aria-label="Close" onclick="cleanUrlUntil(\'&\')">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>';
    }
?>
<table class="table" id="myTable">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Usuario</th>
      <th scope="col">Email</th>
      <th scope="col">
          Cargo
        
      </th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    

<?php
$cont = 0;
foreach($listaUsuarios as $usuario){
    
        ?>

    <tr>
      <th scope="row"><?php echo$usuario["id"]; ?></th>
      <td><?php echo$usuario["user"]; ?></td>
      <td><?php echo$usuario["email"]; ?></td>
      <td>
        <?php 
            if($usuario["cargo"] != 0){
                echo $DBUsuarios -> obtenerCargo($usuario["cargo"]); 
            }
            else{
                echo"DESHABILITADO";
            }
            
        ?>
      
      
        </td>
        <td><?php echo'<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal'.$usuario["id"].'">
            <i class="fas fa-edit"></i>
        </button>';
        if($usuario["cargo"] != 0 && $usuario["cargo"] != 1){
            echo'
                <button type="button" title="Deshabilitar usuario" class="btn btn-danger" data-toggle="modal" data-target="#deshabilitarModal'.$usuario["id"].'">
                    <i class="fas fa-user-slash"></i>
                </button>';
        }
        else if($usuario["cargo"] == 1){}
        
        echo'
        <div class="modal fade" id="modal'.$usuario["id"].'" tabindex="-1" role="dialog" aria-labelledby="modalLabel'.$usuario["id"].'" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel'.$usuario["id"].'">Modificar '.$usuario["user"].'</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="paginas_gestion/divincorp/metacrearusuario.php" method="POST">
                    <div class="modal-body">
                        
                            <div class="form-group">
                                <input type="hidden" value="'.$usuario["id"].'" name="idUsuario" id="idUsuario">
                                <label for="modifusuario">Usuario</label>
                                <input type="text" name="modifusuario" class="form-control" id="modifusuario" aria-describedby="emailHelp" value="'.$usuario["user"].'" placeholder="Ejemplo 1er Turno" required>
                                <br>
                                <label for="modifemail">Email</label>
                                <input type="email" name="modifemail" class="form-control" id="modifemail" aria-describedby="emailHelp" value="'.$usuario["email"].'" placeholder="Ingrese email" required>
                                <br>
                                <label for="modifcargo">Cargo</label>
                                <select class="custom-select" name="modifcargo" id="modifcargo" required>
                                        <option value="'.$usuario["cargo"].'" selected>'. $show = ($usuario["cargo"] != 0) ? $usuario["cargo"].' - ACTUAL' : 'DESHABILITADO'; echo'</option>
                                        <option value="1" disabled>1 - SUPERUSUARIO</option>
                                        <option value="2" disabled>2 - ADMINISTRADOR</option>
                                        <option value="3" disabled>3 - PUBLICADOR</option>
                                        <option value="4" disabled>4 - VISUALIZADOR</option>
                                        <option value="5">5 - DIV EVAL</option>
                                        <option value="6">6 - DIV ESTUDIO</option>
                                        <option value="7">7 - DIV EDU FIS</option>
                                        <option value="8">8 - SEC SANIDAD</option>
                                        <option value="9">9 - DIV EXT</option>
                                        <option value="10">10 - SAF</option>
                                </select>
                                
                                <br>
                                
                            </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        
                    </div>
                </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="deshabilitarModal'.$usuario["id"].'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">¿Seguro que desea deshabilitar este usuario?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <form action="paginas_gestion/divincorp/metacrearusuario.php" method="POST">

                            <input type="hidden" name="idUsuario" id="idUsuario" value="'.$usuario['id'].'">
                            <input type="hidden" name="modifusuario" id="modifusuario" value="'.$usuario['user'].'">
                            <input type="hidden" name="modifemail" id="modifemail" value="'.$usuario['email'].'">
                            <input type="hidden" name="modifcargo" id="modifcargo" value="0">

                            <button type="submit" id="deshabilitar" class="btn btn-danger">Si, estoy seguro</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

      '; ?></td>
    </tr>

        <?php   
    }
?>
  </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Agregar usuario</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="paginas_gestion/divincorp/metacrearusuario.php" method="POST">
            <div class="modal-body">
                
                    <div class="form-group">
                        <label for="usuario">Usuario</label>
                        <input type="text" name="usuario" class="form-control" id="usuario" aria-describedby="emailHelp" placeholder="Ingrese nombre de usuario" required>
                        <br>
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Ingrese email" required>
                        <br>
                        <label for="cargo">Cargo</label>

                        <select class="custom-select" name="cargo" id="cargo" required>
                            <option value="" selected></option>
                            <option value="1">SUPERUSUARIO</option>
                            <option value="2" disabled>ADMINISTRADOR</option>
                            <option value="3" disabled>PUBLICADOR</option>
                            <option value="4" disabled>VISUALIZADOR</option>
                            <option value="5">DIV EVAL</option>
                            <option value="6">DIV ESTUDIO</option>
                            <option value="7">DIV EDU FIS</option>
                            <option value="8">SEC SANIDAD</option>
                            <option value="9">DIV EXT</option>
                            <option value="10">SAF</option>
                        </select>

                        <br><br>
                        <small>
                            Recuerde que la contraseña del usuario será generada automáticamente y será "us3r3s3sc", notifíquela al usuario.</br>
                            El usuario será el responsable de modificarla en primera instancia con su email de recuperación para no comprometer la seguridad del sistema.
                        </small>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary" >Guardar</button>
                
            </div>
        </form>
        </div>
    </div>
</div>





</section>

<script type="text/javascript">
function cleanUrlUntil(character)
{
    var url = window.location.href;
    url = url.substr(0,url.lastIndexOf(character));
    var new_url = url;
    window.history.pushState("data","Title",new_url);
    document.title=url;
}
$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>

<?php
    }
?>
