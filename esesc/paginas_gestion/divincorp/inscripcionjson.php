
<?php

include ('../../assets/func/funciones.php');
include ('../../assets/func/class.DBEdicion.php');
$conexion = conexion();   
$DBEdicion = new DBEdicion($conexion);
$consulta = "SELECT inscripcion.id,candidato.dni,candidato.nombres,candidato.apellidos,inscripcion.estado,
folder_documentos.id,inscripcion.detalle, inscripcion.revision_ext, candidato.id

FROM inscripcion 
LEFT JOIN candidato ON inscripcion.id_candidato = candidato.id 
LEFT JOIN folder_documentos ON folder_documentos.id_inscripcion = inscripcion.id  ORDER BY candidato.id ASC;";
$tabla = ejecutarConsulta($consulta,$conexion);
$cont = 1;
echo'{"data" : ';
$array = array();
foreach($tabla as $fila){
    if($DBEdicion -> candidatoTieneEdicionExtUsadaHace72horas($fila[8]) && $fila["revision_ext"] == "EN OBSERVACION"){
        $texto = '<br><span class="badge badge-secondary">Edición efectuada</span>';
    }
    else{
        $texto = '';
    }
    $datos_candidato = array();
    array_push(
        $datos_candidato,
        $cont,
        '<div style="max-width: 200px;overflow-wrap: break-word;">'.$fila[1].$texto.'</div>',
        '<div style="max-width: 200px;overflow-wrap: break-word;">'.$fila[2].'</div>',
        '<div style="max-width: 200px;overflow-wrap: break-word;">'.$fila[3].'</div>',
        '<div style="max-width: 200px;overflow-wrap: break-word;">'.$fila[7].'</div>',
        '<div style="max-height:120px;overflow:auto;min-width:100px;">'.$fila[6].'</div>',
        $fila[5] ? 'CARGADA' : 'NO CARGADA',
        '<div style="max-width: 200px;overflow-wrap: break-word;">'.$fila[4].'</div>',
        '<a href="?p=verdetalleinscripto&inscripcion='.$fila[0].'" class="btn btn-sm btn-primary" title="Editar datos del postulante">Editar<a>'
    );

    array_push($array, $datos_candidato);   
    $cont++;
}

echo $arr = json_encode($array);
// echo $json_string = json_encode($array, JSON_PRETTY_PRINT);
echo"}";
?>