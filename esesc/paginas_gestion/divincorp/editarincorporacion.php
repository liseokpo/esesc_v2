<?php
include ('assets/func/class.DBIncorporacion.php');


if(!isset($_SESSION['usuario'])){
    header('Location: ../../index.php');
}
else{ 


/*
$consulta = "INSERT INTO incorporacion(id, detalle, fecha_modificacion, merito, estado, 		id_turno_examen, id_candidato) VALUES (NULL, '', NULL, NULL, 'EN OBSERVACION', NULL, 44)";
echo"asd<br>";
$con = ejecutarConsulta($consulta,$conexion);
print_r($conexion);
echo"<br>aaaAAAAAAAAAAAAA";
*/

    $busqueda="";
    $DBIncorporacion = new DBIncorporacion($conexion);
    
    $enobservacion = $incorporados =  $noaprobados = $condicionales = 0;
    
    $DBCandidato = new DBCandidato($conexion);
	
    $listainscriptos = $DBCandidato -> obtenerListaInscriptos();


    foreach($listainscriptos as $inscripto){
        if($inscripto['estado'] == "INSCRIPTO" || $inscripto['estado'] == "INSCRIPTO CONDICIONAL"){
            if(!$DBIncorporacion -> incorporacionExiste($inscripto['id_candidato'])){
                $DBIncorporacion -> asignarIncorporacion($inscripto['id_candidato']);		

		echo"no existe incorp";        
		// SE AGREGA INCORPORACION NO ASIGNADA
            }
        }

        else if($DBIncorporacion -> incorporacionExiste($inscripto['id_candidato'])){
            $DBIncorporacion -> eliminarIncorporacion($inscripto['id_candidato']);
            // SE ELIMINA INCORPORACION ASIGNADA SI NO ESTÁ INSCRIPTO / INSCRIPTO CONDICIONAL
        }

    }


    $listaincorporados = $DBIncorporacion -> obtenerListaIncorporados();

    foreach($listaincorporados as $incorporacion){
        if($incorporacion["estado"] == "EN OBSERVACION"){
            $enobservacion++;
        }
        
        else if ($incorporacion["estado"] == "RESERVA"){
            $condicionales++;
        }
        else if ($incorporacion["estado"] == "DESAPROBADO"){
            $noaprobados++;
        }
        else{
            $incorporados++;
        }            
    }



    // ------------------------------------------------
    // CONSULTA QUE DEVUELVE LA CANTIDAD DE MERITOS CALIFICADOS DE ESPECIALIDADES
    $consultameritoesp = 'SELECT COUNT(incorporacion.id) from incorporacion, plan_carrera WHERE incorporacion.merito IS NOT NULL AND incorporacion.id_candidato = plan_carrera.id_candidato AND plan_carrera.tipo_plan = "ESPECIALIDADES"';
    // ------------------------------------------------
    // CONSULTA QUE DEVUELVE LA CANTIDAD DE MERITOS CALIFICADOS DE ARMAS
    $consultameritoarmas = 'SELECT COUNT(incorporacion.id) from incorporacion, plan_carrera WHERE incorporacion.merito IS NOT NULL AND incorporacion.id_candidato = plan_carrera.id_candidato AND plan_carrera.tipo_plan = "ARMAS"';
    // ------------------------------------------------
    // CONSULTA QUE DEVUELVE LA CANTIDAD DE MERITOS CALIFICADOS DE FEMENINAS
    $consultameritofem = 'SELECT COUNT(incorporacion.id) FROM incorporacion, candidato WHERE incorporacion.merito IS NOT NULL AND incorporacion.id_candidato = candidato.id AND candidato.sexo = "FEMENINO"';
    // ------------------------------------------------
    // CONSULTA QUE DEVUELVE LA CANTIDAD DE MERITOS CALIFICADOS DE MASCULINOS
    $consultameritomasc = 'SELECT COUNT(incorporacion.id) FROM incorporacion, candidato WHERE incorporacion.merito IS NOT NULL AND incorporacion.id_candidato = candidato.id AND candidato.sexo = "MASCULINO"';
    
    $meritoesp = ejecutarConsulta($consultameritoesp,$conexion) -> fetchAll(PDO::FETCH_COLUMN, 0);
    $meritoarmas = ejecutarConsulta($consultameritoarmas,$conexion) -> fetchAll(PDO::FETCH_COLUMN, 0);
    $meritofem = ejecutarConsulta($consultameritofem,$conexion) -> fetchAll(PDO::FETCH_COLUMN, 0);
    $meritomasc = ejecutarConsulta($consultameritomasc,$conexion) -> fetchAll(PDO::FETCH_COLUMN, 0);

    $totalmerito = $meritoarmas[0] + $meritoesp[0];


    $consultaExamenMed = 'SELECT COUNT(examen_medico.id) from examen_medico;';
    $consultaExamenInt = 'SELECT COUNT(examen_intelectual.id) from examen_intelectual;';
    $consultaExamenFis = 'SELECT COUNT(examen_fisico.id) from examen_fisico;';
 
    $cantExamenMed = ejecutarConsulta($consultaExamenMed,$conexion) -> fetchAll(PDO::FETCH_COLUMN, 0);
    $cantExamenInt = ejecutarConsulta($consultaExamenInt,$conexion) -> fetchAll(PDO::FETCH_COLUMN, 0);
    $cantExamenFis = ejecutarConsulta($consultaExamenFis,$conexion) -> fetchAll(PDO::FETCH_COLUMN, 0);
?>

<section id="cuerpofull">
    <ul class="p-2 m-1">
    <?php
    echo '
        <div class="row">
            <div class="col text-center">
                Carga de exámenes
            </div>
            <div class="col text-center">
                Méritos calificados para<br>
            </div>
            
        </div>
        <div class="row">
            
            <div class="col text-center">
                <button class=" border-0 bg-transparent text-left"><h3>'.$cantExamenMed[0].'</h3>Médicos</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$cantExamenFis[0].'</h3>Físicos</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$cantExamenInt[0].'</h3>Intelectuales</button>
            </div>
            <div class="col text-center">
                <button class=" border-0 bg-transparent text-left"><h3>'.$meritoarmas[0].'</h3>Armas</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$meritoesp[0].'</h3>Esp y Serv</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$meritomasc[0].'</h3>Masculinos</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$meritofem[0].'</h3>Femeninas</button>    
                <button class=" border-0 bg-transparent text-left"><h3>'.$totalmerito.'</h3><b>Totales</b></button>    
            </div>
        </div>';
    ?>
    </ul>
    
    <h2 class="text-center text-shadow">Estado de Incorporación</h2>

    <div class="row p-0 m-0">
        <div class="col col-sm-12 col-lg-6 col-md-12 p-0 ">
            <h3 class="text-center">Armas</h3>            
            <table id="example" class="display border border-dark w-100 p-0 m-0">
                <thead>
                    <tr>
                    <th class="p-0">NRO<br> INSCRIPTO</th>
                    <th>ESTADO INSCRIPCIÓN</th>
                    <th>DNI</th>
                    <th class="p-0">MÉRITO</th>
                    <th class="pl-1">ESTADO<br>INCORPORACIÓN</th>
                    <th></th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="col col-sm-12 col-lg-6 col-md-12 p-0 pl-1 ">
            <h3 class="text-center">Especialidades y Servicios</h3>
            <table id="example2" class="display border border-dark w-100">
                <thead>
                    <tr>
                    <th class="p-0">NRO<br> INSCRIPTO</th>
                    <th>ESTADO INSCRIPCIÓN</th>
                    <th>DNI</th>
                    <th class="p-0">MÉRITO</th>
                    <th class="pl-1">ESTADO<br>INCORPORACIÓN</th>
                    <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    
    
<?php
    echo '<div class=" pt-2"><button class="bg-info border-0 rounded">EN OBSERVACION: '.$enobservacion.'</button> | <button class="bg-success border-0 rounded">INCORPORADOS: '.$incorporados.'</button> | <button class="bg-warning border-0 rounded">INCORPORADOS RESERVA: '.$condicionales.'</button> |<button class="bg-danger border-0 rounded">DESAPROBADOS: '.$noaprobados.'</button></div>';
?>
<br>
</section>
<script>
    $(document).ready(function(){
        var table = $('#example').DataTable({
            "ajax": 'paginas_gestion/divincorp/incorporacionjson.php?plan=armas',
            "deferRender": true,
            "orderClasses": false,
            "order": [],
            "columns": [
                { orderable: false},
                null,
                null,
                null,
                null,
                {width:"1%"},
            ],
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            }
        });

        var table = $('#example2').DataTable({
            "ajax": 'paginas_gestion/divincorp/incorporacionjson.php?plan=especialidades',
            "deferRender": true,
            "orderClasses": false,
            "order": [],
            "columns": [
                { orderable: false},
                null,
                null,
                null,
                null,
                {width:"1%"},
            ],
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            }
        });
    });
</script>

<?php


}
?>