<?php
    if(!isset($_SESSION['usuario'])){
        header('Location: ../../index.php');
    }
    else{
        
?>
<?php
    $conexion = conexion();
    $conexion_mysql = conectar();
    $DBCandidato = new DBCandidato($conexion);
    $DBEdicion = new DBEdicion($conexion);
    $lista = $DBCandidato -> obtenerListaInscriptos();

    for($i = 0;$i < sizeof($lista); $i ++)
        {

            if($lista[$i]["id"] == $_GET["inscripcion"]){

                $existeEdicionCandidato = $DBEdicion -> existeEdicionEnCandidato($lista[$i]["id_candidato"]);

                $existedatospersonales = $existeplancarrera = $existesecundario = $existesoldvol = $existedocumentacion = false;
            
                if($existeEdicionCandidato)
                {
                    $DBEdicion -> validarEdicion($lista[$i]["id_candidato"],"DATOS_PERSONALES");
                    $DBEdicion -> validarEdicion($lista[$i]["id_candidato"],"PLAN_CARRERA");
                    $DBEdicion -> validarEdicion($lista[$i]["id_candidato"],"SOLD_VOL");
                    $DBEdicion -> validarEdicion($lista[$i]["id_candidato"],"SECUNDARIO");
                    $DBEdicion -> validarEdicion($lista[$i]["id_candidato"],"FOTOS_DNI");
            
                    if($DBEdicion -> existeEdicion($lista[$i]["id_candidato"],"DATOS_PERSONALES")){
                        $existedatospersonales = true;
                    }
                    if($DBEdicion -> existeEdicion($lista[$i]["id_candidato"],"PLAN_CARRERA")){
                        $existeplancarrera = true;
                    }
                    if($DBEdicion -> existeEdicion($lista[$i]["id_candidato"],"SOLD_VOL")){
                        $existesoldvol = true;
                    }
                    if($DBEdicion -> existeEdicion($lista[$i]["id_candidato"],"SECUNDARIO")){
                        $existesecundario = true;
                    }
                    if($DBEdicion -> existeEdicion($lista[$i]["id_candidato"],"FOTOS_DNI")){
                        $existedocumentacion = true;
                    }
                }

                $candidato = $DBCandidato -> obtenerCandidatoporId($lista[$i]["id_candidato"]);
                $domicilio = $DBCandidato -> obtenerDomicilio($lista[$i]["id_candidato"]);
                $plancarrera = $DBCandidato -> obtenerPlanCarrera($lista[$i]["id_candidato"]);
                $soldvol = $DBCandidato -> obtenerSoldadoVoluntario($lista[$i]["id_candidato"]);
                $secundario = $DBCandidato -> obtenerSecundario($lista[$i]["id_candidato"]);
                $centro_preseleccion = $DBCandidato -> obtenerCentroPreseleccion($lista[$i]["id_candidato"]);
                $inscripcion = $DBCandidato -> obtenerInscripcion($lista[$i]["id_candidato"]);
                $folder_documentos = "";
                if($DBCandidato -> folderDocumentosExiste($inscripcion["id"])){
                    $folder_documentos = $DBCandidato -> obtenerFolderDocumentos($inscripcion["id"]);
                }
                echo'
                
                ';

                ?>
                
                <?php
                
                echo'
                <section id="cuerpofull">
                <h2 class="text-center text-shadow mt-2">Detalle Inscripto</h2>
                <h3 class="shadow p-3 pt-0 mt-3 bg-white w-auto d-none d-lg-block" style="position: -webkit-sticky;position: sticky;top: 52px;z-index:1000;">'.$candidato["nombres"].' '.$candidato["apellidos"].' | '.$inscripcion["estado"];if($inscripcion["nro_inscripto"] != 0){echo' <b class="border border-dark p-1"> '.$inscripcion["nro_inscripto"].' </b>';} if(isset($lista[$i+1])){echo'<a class="btn btn-primary float-right ml-1" href="?p=verdetalleinscripto&inscripcion='.$lista[$i+1]['id'].'">Siguiente registro</a>';}if(isset($lista[$i-1])){echo'<a class="btn btn-primary float-right" href="?p=verdetalleinscripto&inscripcion='.$lista[$i-1]['id'].'">Anterior</a>';}echo'</h3>
                <div style="position: -webkit-sticky;position: sticky;top: 72px;z-index:1000;"><h5 class="shadow p-3 pt-0 mt-3 bg-white w-auto d-lg-none">'.$candidato["nombres"].'<br>'.$candidato["apellidos"].' | '.$inscripcion["estado"];if($inscripcion["nro_inscripto"] != 0){echo' <b class="border border-dark"> '.$inscripcion["nro_inscripto"].' </b>';} if(isset($lista[$i+1])){echo'<a class="btn-sm btn-primary float-right ml-1" href="?p=verdetalleinscripto&inscripcion='.$lista[$i+1]['id'].'">Siguiente registro</a>';}if(isset($lista[$i-1])){echo'<a class="btn-sm btn-primary float-right" href="?p=verdetalleinscripto&inscripcion='.$lista[$i-1]['id'].'">Anterior</a>';}echo'</h5></div>
                
                <div class="w-100 cf">

                    <!-- PARTE IZQUIERDA -->   
                    <div class="row w-100">
                    
                        <div class="col">
                            <div class="contenidoizquierda w-100" style="background-color:whitesmoke;">
                                <div class="container border w-100">
                                
                                    <h3 class="text-center mt-2">Datos del postulante</h3>
                                   <h3 class="mt-3">Datos Personales<input type="button" class="btn btn-primary ml-3" onclick="intercambiarDisplay(\'datospersonales\',\'datospersonaleseditar\')" value="Editar"/></h3>
                                    <div class="row" id="datospersonales">
                                        <div class="col">
                                            <p>Nombre: <b>'.$candidato["nombres"].' '.$candidato["apellidos"].'</b></p>
                                            <p>Sexo: <b>'.$candidato["sexo"].'</b></p>
                                            <p>Dni: <b>'.$candidato["dni"].'</b></p>
                                            <p>Cuil: <b>'.$candidato["cuil"].'</b></p>
                                            <p>Fecha de nacimiento: <b>'.$candidato["fecha_nacimiento"].'</b></p>
                                            <p>Email: <b>'.$candidato["email"].'</b></p>
                                        </div>
                                        <div class="col">
                                            <p>Proveniente de: <b>'.$domicilio["calle"].' '.$domicilio["numero"].' '.$domicilio["piso"].' '.$domicilio["depto"].' - '.$candidato["localidad"].' - '.$candidato["provincia"].'</b></p>
                                            <p>Código postal: <b>'.$candidato["codigo_postal"].'</b></p>
                                            <p>Nacionalidad: <b>'.$candidato["nacionalidad"].'</b></p>
                                            <p>Estado civil: <b>'.$candidato["estado_civil"].'</b></p>
                                            <p>Grupo sanguineo: <b>'.$candidato["grupo_sanguineo"].'</b></p>
                                            <p>Celular: <b>'.$candidato["celular"].'</b></p>
                                            <p>Teléfono: <b>'.$candidato["telefono"].'</b></p>
                                        </div>
                                    </div>

                                    <form id="datospersonaleseditar" class="myclass" action="paginas_gestion/divincorp/metamodificarinscripcion.php"  method="post" enctype="multipart/form-data" style="display:none">
                                        <div class="row">
                                                <div class="col">
                                                    <p>Nombre: <input type="text" class="form-control text-uppercase" name="nombres" value="'.$candidato["nombres"].'" onchange="this.value = this.value.toUpperCase();"></p>
                                                    <p>Apellido: <input type="text" class="form-control text-uppercase" name="apellidos" value="'.$candidato["apellidos"].'" onchange="this.value = this.value.toUpperCase();"></p>
                                                    <p>
                                                        <label for="sexo">Sexo</label>
                                                        <select class="custom-select" name="sexo" id="sexo" required>
                                                            <option value="" selected disabled>'.$candidato["sexo"].'</option>
                                                            <option value="FEMENINO">FEMENINO</option>
                                                            <option value="MASCULINO">MASCULINO</option>
                                                        </select>
                                                    </p>
                                                    <input type="hidden" class="form-control text-uppercase" name="dnianterior" value="'.$candidato["dni"].'">
                                                    <p>Dni: <input type="number" class="form-control text-uppercase" name="dni" value="'.$candidato["dni"].'"></p>
                                                    <p>Cuil: <input type="text" class="form-control text-uppercase" name="cuil" value="'.$candidato["cuil"].'" onchange="this.value = this.value.toUpperCase();"></p>
                                                    <p> 
                                                        <label for="fnacmiento">Fecha de nacimiento (*)</label>
                                                        <input type="date" class="form-control" name="fnacimiento" id="fnacimiento" value="'.$candidato["fecha_nacimiento"].'" required>                                                
                                                    </p>
                                                    <input type="hidden" class="form-control" name="emailanterior" value="'.$candidato["email"].'">
                                                    <p>Email: <input type="email" class="form-control" name="email" value="'.$candidato["email"].'"></p>
                                                    
                                                </div>
                                                <div class="col">
                                                    
                                                    <p>Calle: <input type="text" class="form-control text-uppercase" value="'.$domicilio["calle"].'" name="domicilio" onchange="this.value = this.value.toUpperCase();"></p> 
                                                    <div class="form-row">
                                                        <div class="col-md-4">
                                                            <label for="nro">Altura</label>
                                                            <input type="number" min=0 class="form-control" value="'.$domicilio["numero"].'" name="nro" id="nro" required>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label for="piso">Piso</label>
                                                            <input type="text" class="form-control" value="'.$domicilio["piso"].'" name="piso" id="piso">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label for="depto">Dpto</label>
                                                            <input type="text" class="form-control" value="'.$domicilio["depto"].'" name="depto" id="depto">
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-md-6 ">
                                                            <label for="selectprov">Provincia (*)</label>
                                                            <select id="selectprov" name="provincia" class="form-control" required>
                                                                <option value="" selected disabled>'.$candidato["provincia"].'</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-6 ">
                                                            <label for="selectloc">Localidad (*)</label>
                                                            <select id="selectloc" name="localidad" class="form-control" required>
                                                                <option value="" selected disabled>'.$candidato["localidad"].'</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <p>Código postal: <input type="text" class="form-control text-uppercase" name="codigopostal" value="'.$candidato["codigo_postal"].'"></p> 
                                                    <label for="nacionalidad">Nacionalidad (*)</label>
                                                    <select class="custom-select" name="nacionalidad" id="nacionalidad" required>
                                                        <option value="" selected disabled>'.$candidato["nacionalidad"].'</option>
                                                        <option value="ARGENTINO_NAT">ARGENTINO (NATIVO)</option>
                                                        <option value="ARGENTINO_OP">ARGENTINO (POR OPCIÓN)</option>
                                                    </select>
                                                    <p class="mt-3">Estado civil: <b>'.$candidato["estado_civil"].'</b></p>
                                                    <input type="hidden" class="form-control text-uppercase" name="estadocivil" value="'.$candidato["estado_civil"].'"></p> 

                                                    <label for="gruposanguineo">Grupo Sanguíneo</label>
                                                    <select class="custom-select" name="gruposanguineo" id="gruposanguineo" required>
                                                        <option value="" selected disabled>'.$candidato["grupo_sanguineo"].'</option>
                                                        <option value="0+">0+</option>
                                                        <option value="0-">0-</option>
                                                        <option value="A+">A+</option>
                                                        <option value="A-">A-</option>
                                                        <option value="AB+">AB+</option>
                                                        <option value="AB-">AB-</option>
                                                        <option value="B+">B+</option>
                                                        <option value="B-">B-</option>
                                                    </select>
                                                    <p>Celular: <input type="number" class="form-control text-uppercase" name="celular" value="'.$candidato["celular"].'"></p> 
                                                    <p>Teléfono: <input type="number" class="form-control text-uppercase" name="telefono" value="'.$candidato["celular"].'"></p> 
                                                </div>
                                                
                                        </div>
                                        <input type="hidden" name="idcandidato" value="'.$candidato["id"].'"/>
                                        <input type="hidden" name="idinscripto" value="'.$lista[$i]["id"].'"/>
                                        <input type="submit" name="enviardatospers" class="submit btn btn-primary m-3" value="Guardar cambios" />
                                        <a href="#" class="btn btn-secondary" onclick="window.location.reload(true);">Cancelar</a>
                                    </form>

                                </div>
                                <div class="container border " style="background-color:whitesmoke;">
                                    <h3 class="mt-2">Plan de carrera<input type="button" class="btn btn-primary ml-3" onclick="intercambiarDisplay(\'plancarrera\',\'plancarreraeditar\')" value="Editar"/></h3>
                                    
                                    <div class="row" id="plancarrera">
                                        <div class="col">';
                                    if($plancarrera != null){
                                        echo'
                                            <p>Plan de carrera: <b>'.$plancarrera["tipo_plan"].'</b></p>
                                            
                                        </div>
                                        <div class="col">
                                            <p>Orientacion: <b>'.$plancarrera["orientacion"].'</b></p>
                                        ';
                                    }
                                    else{
                                        echo'<p class="text-danger"><b>El postulante no tiene datos del plan de carrera registrados.</b></p>';
                                    }
                                    echo'
                                        </div>
                                        ';
                                        if($centro_preseleccion != null){
                                            echo'
                                                <div class="col-auto">
                                                    <p>Centro de preselección: <b>'.$centro_preseleccion["nombre"].'</b></p>
                                                </div>
                                            ';
                                        }
                                        else{
                                            echo'<p class="text-danger"><b>El postulante no tiene datos del centro de preselección registrados.</b></p>';
                                        }

                                    echo'
                                    </div>
                                    <form id="plancarreraeditar" class="myclass" action="paginas_gestion/divincorp/metamodificarinscripcion.php"  method="post" enctype="multipart/form-data" style="display:none">
                                        <div class="row">
                                            <div class="col">
                                                <label for="plancarrera">Plan</label>
                                                <select class="custom-select" name="plancarrera" id="plancarrera" required>
                                                    <option value="" selected disabled>'.$plancarrera["tipo_plan"].'</option>
                                                    <option value="ARMAS">ARMAS</option>
                                                    <option value="ESPECIALIDADES">ESPECIALIDADES</option>
                                                </select>
                                            </div>
                                            <div class="col">
                                                <label for="orientacion">Orientacion</label>
                                                <select class="custom-select" name="orientacion" id="orientacion" required>
                                                    <option value="" selected disabled>'.$plancarrera["orientacion"].'</option>
                                                    <option value="INFANTERÍA">INFANTERÍA</option>
                                                    <option value="CABALLERÍA">CABALLERÍA</option>
                                                    <option value="ARTILLERÍA">ARTILLERÍA</option>
                                                    <option value="INGENIEROS">INGENIEROS</option>
                                                    <option value="COMUNICACIONES">COMUNICACIONES</option>
                                                    <option value="MÚSICO">MÚSICO</option>
                                                    <option value="CONDUCTOR MOTORISTA">CONDUCTOR MOTORISTA</option>
                                                    <option value="ENFERMERO">ENFERMERO</option>
                                                    <option value="MECÁNICO EN INFORMÁTICA">MECÁNICO EN INFORMÁTICA</option>
                                                    <option value="" disabled>ADMINISTRACIÓN</option>
                                                    <option value="OFICINISTA">OFICINISTA</option>
                                                    <option value="INTENDENCIA">INTENDENCIA</option>
                                                    <option value="" disabled>MECÁNICOS</option>
                                                    <option value="MECÁNICO MOTORISTA A RUEDA">MECÁNICO MOTORISTA A RUEDA</option>
                                                    <option value="MECÁNICO MOTORISTA ELECTRICISTA">MECÁNICO MOTORISTA ELECTRICISTA</option>
                                                    <option value="MECÁNICO MOTORISTA A ORUGA">MECÁNICO MOTORISTA A ORUGA</option>
                                                    <option value="MECÁNICO ARMERO">MECÁNICO ARMERO</option>
                                                    <option value="MECÁNICO DE MUNICIÓN Y EXPLOSIVOS">MECÁNICO DE MUNICIÓN Y EXPLOSIVOS</option>
                                                    <option value="MECÁNICO DE ARTILLERÍA">MECÁNICO DE ARTILLERÍA</option>
                                                    <option value="MECÁNICO DE INSTALACIONES">MECÁNICO DE INSTALACIONES</option>
                                                    <option value="MECÁNICO DE INGENIEROS">MECÁNICO DE INGENIEROS</option>
                                                    <option value="MECÁNICO DE AVIACIÓN">MECÁNICO DE AVIACIÓN</option>
                                                    <option value="MECÁNICO DE ÓPTICA Y APARATOS DE PRECISIÓN">MECÁNICO DE ÓPTICA Y APARATOS DE PRECISIÓN</option>
                                                    <option value="" disabled>ELECTRÓNICA</option>
                                                    <option value="MECÁNICO DE EQUIPO DE CAMPAÑA">MECÁNICO DE EQUIPO DE CAMPAÑA</option>
                                                    <option value="MECÁNICO DE EQUIPOS FIJOS">MECÁNICO DE EQUIPOS FIJOS</option>
                                                    <option value="MECÁNICO DE RADAR">MECÁNICO DE RADAR</option>
                                                </select>
                                            </div>
                                            <div class="col-auto">
                                                <label for="centropreseleccion">Centro de preselección</label>
                                                <select class="custom-select" name="centropreseleccion" id="centropreseleccionesp" required>
                                                    <option value="" selected disabled>'.$centro_preseleccion["nombre"].'</option>
                                                    <option value=\'ESCUELA DE SUBOFICIALES DEL EJÉRCITO "SARGENTO CABRAL"\'>ESCUELA DE SUBOFICIALES DEL EJÉRCITO "SARGENTO CABRAL" - BUENOS AIRES</option>
                                                    <option value=\'LICEO MILITAR "GENERAL ROCA"\'>LICEO MILITAR "GENERAL ROCA" - CHUBUT</option>
                                                    <option value=\'LICEO MILITAR "GENERAL LAMADRID"\'>LICEO MILITAR "GENERAL LAMADRID" - TUCUMÁN</option>
                                                    <option value=\'LICEO MILITAR "GENERAL PAZ"\'>LICEO MILITAR "GENERAL PAZ" - CÓRDOBA</option>
                                                    <option value=\'COMANDO DE LA XIIda BRIGADA DE MONTE\'>COMANDO DE LA XIIda BRIGADA DE MONTE - MISIONES</option>
                                                    <option value=\'LICEO MILITAR "GENERAL ESPEJO"\'>LICEO MILITAR "GENERAL ESPEJO" - MENDOZA</option>
                                                    <option value=\'LICEO MILITAR "GENERAL BELGRANO"\'>LICEO MILITAR "GENERAL BELGRANO" - SANTA FÉ</option>
                                                    <option value=\'COMANDO DE LA Vta BRIGADA DE MONTAÑA\'>COMANDO DE LA Vta BRIGADA DE MONTAÑA - SALTA</option>
                                                    <option value=\'REGIMIENTO DE INFANTERÍA DE MONTE 29 "CNL IGNACIO JOSÉ WARNES"\'>REGIMIENTO DE INFANTERÍA DE MONTE 29 "CNL IGNACIO JOSÉ WARNES" - FORMOSA</option>
                                                </select>
                                            </div>
                                        </div>
                                        <input type="hidden" name="idcandidato" value="'.$candidato["id"].'"/>
                                        <input type="hidden" name="idinscripto" value="'.$lista[$i]["id"].'"/>
                                        <input type="submit" name="enviarplancarrera" class="submit btn btn-primary m-3" value="Guardar cambios" />
                                        <a href="#" class="btn btn-secondary" onclick="window.location.reload(true);">Cancelar</a>
                                    </form>

                                </div>

                                

                                <div class="container border" style="background-color:whitesmoke;">
                                    <h3 class="mt-2">Soldado voluntario<input type="button" class="btn btn-primary ml-3" onclick="intercambiarDisplay(\'soldvol\',\'soldvoleditar\')" value="Editar"/></h3>
                                    <div class="row" id="soldvol">
                                        <div class="col">';
                                    if($soldvol != null){
                                        echo'
                                            <p>Destino: <b>'.$soldvol["destino"].'</b></p>
                                        ';
                                    }
                                    else{
                                        echo'<p>El '.$candidato["tipo_candidato"].' <b>no es soldado voluntario</b>.</p>';
                                    }
                                echo'
                                        </div>
                                    </div>    
                                    <form id="soldvoleditar" class="myclass" action="paginas_gestion/divincorp/metamodificarinscripcion.php"  method="post" enctype="multipart/form-data" style="display:none">
                                        <div class="row">

                                            <div class="col">
                                                <div class="form-group" id="destino">
                                                    <label for="destinosoldvol">Indique el código de la unidad (*)</label>
                                                    <select id="destinosoldvol" name="destinosoldvol" class="form-control" required>
                                                        <option value="" selected disabled>';if($soldvol != null){echo$soldvol["destino"];}echo'</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="idcandidato" value="'.$candidato["id"].'"/>
                                        <input type="hidden" name="idinscripto" value="'.$lista[$i]["id"].'"/>
                                        <input type="submit" name="enviarsoldvol" class="submit btn btn-primary m-3" value="Guardar cambios" />
                                        <a href="#" class="btn btn-secondary" onclick="window.location.reload(true);">Cancelar</a>
                                    </form>
                                </div>
                            
                                <div class="container border" style="background-color:whitesmoke;">
                                    <h3 class="mt-2">Datos del secundario <input type="button" class="btn btn-primary ml-3" onclick="intercambiarDisplay(\'secundario\',\'secundarioeditar\')" value="Editar"/></h3>
                                    <div class="row" id="secundario">
                                        <div class="col">';
                                    if($secundario != null){
                                        echo'
                                    
                                        <p>Titulo del secundario: <b>'.$secundario["titulo"].'</b></p>
                                        <p>Nombre del secundario: <b>'.$secundario["nombre"].'</b></p>
                                        <p>Provincia del colegio: <b>'.$secundario["provincia"].'</b></p>
                                        <p>Localidad del colegio: <b>'.$secundario["localidad"].'</b></p>
                                    </div>
                                    <div class="col">
                                        <p>Direccion del colegio: <b>'.$secundario["direccion"].'</b></p>
                                        <p>Telefono del colegio: <b>'.$secundario["telefono"].'</b></p>
                                        <p>Plan del secundario: <b>'.$secundario["plan"].'</b></p>
                                        <p>Año cursado: <b>'.$secundario["anio_cursado"].'</b></p>
                                        <p>Cantidad de materias que adeuda: <b>'.$secundario["cant_materias_adeudadas"].'</b></p>
                                    
                                    ';  
                                    } 
                                    else{
                                        echo'<p class="text-danger"><b>El postulante no tiene datos del secundario registrados.</b></p>';
                                    }
                                    echo'
                                        </div>

                                    </div>
                                    
                                    <form id="secundarioeditar" class="myclass" action="paginas_gestion/divincorp/metamodificarinscripcion.php"  method="post" enctype="multipart/form-data" style="display:none">
                                        <div class="row">
                                            <div class="col">
                                                <p>Titulo: <input type="text" class="form-control text-uppercase" name="tsecundario" value="'.$secundario["titulo"].'" onchange="this.value = this.value.toUpperCase();"></p>
                                                <p>Nombre: <input type="text" class="form-control text-uppercase" name="nsecundario" value="'.$secundario["nombre"].'" onchange="this.value = this.value.toUpperCase();"></p>
                                                
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <p>Provincia: <input type="text" class="form-control text-uppercase" name="psecundario" value="'.$secundario["provincia"].'" onchange="this.value = this.value.toUpperCase();"></p>
                                            </div>
                                            <div class="col">
                                                <p>Localidad: <input type="text" class="form-control text-uppercase" name="lsecundario" value="'.$secundario["localidad"].'" onchange="this.value = this.value.toUpperCase();"></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <p>Dirección: <input type="text" class="form-control text-uppercase" name="dircolegio" value="'.$secundario["direccion"].'" onchange="this.value = this.value.toUpperCase();"></p>
                                            </div>
                                            <div class="col">
                                                <p>Teléfono: <input type="text" class="form-control text-uppercase" name="telcolegio" value="'.$secundario["telefono"].'"></p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col">
                                                <label for="plansecundario">Plan</label>
                                                <select class="custom-select" name="plansecundario" id="plansecundario">
                                                    <option value="" selected disabled>'.$secundario["plan"].'</option>
                                                    <option value="TECNICO">TÉCNICO</option>
                                                    <option value="BACHILLER">BACHILLER</option>
                                                    <option value="FINES">FINES</option>
                                                </select>
                                            </div>
                                            <div class="col">
                                                <label for="estadosecundario">Año cursado</label>
                                                <select class="custom-select" name="estadosecundario" id="estadosecundario" required>
                                                    <option value="" selected disabled>'.$secundario["anio_cursado"].'</option>
                                                    <option value="COMPLETO">COMPLETO</option>
                                                    <option value="INCOMPLETO">INCOMPLETO</option>
                                                    <option value="EN_CURSO">EN CURSO</option>
                                                </select>
                                            </div>
                                            <div class="col">
                                                <label for="cantmateriassecundario">Cant materias que debe</label>
                                                <input type="number" class="form-control" id="cantmateriassecundario" name="cantmateriassecundario" min="0" value="'.$secundario["cant_materias_adeudadas"].'">
                                            </div>
                                        </div>
                                        
                                        <input type="hidden" name="idcandidato" value="'.$candidato["id"].'"/>
                                        <input type="hidden" name="idinscripto" value="'.$lista[$i]["id"].'"/>
                                        <input type="submit" name="enviarsecundario" class="submit btn btn-primary m-3" value="Guardar cambios" />
                                        <a href="#" class="btn btn-secondary" onclick="window.location.reload(true);">Cancelar</a>
                                    </form>
                                    ';
                                    ?>
                                    <script>

                                        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                                            event.preventDefault();
                                            $(this).ekkoLightbox();
                                        });

                                        //TENGO QUE DESTILDAR TODO CUANDO QUEDE EN OCULTo.
                                        function editar(){
                                            element = document.getElementById("tdedicion");
                                            element2 = document.getElementById("recuerde");
                                            if(element.style.display == "none"){
                                                element.removeAttribute("style");
                                                element2.removeAttribute("style");
                                            }
                                            else{
                                                element.style.display = "none";
                                                element2.style.display = "none";
                                                document.getElementById('customCheck1').checked = false;
                                                document.getElementById('customCheck2').checked = false;
                                                document.getElementById('customCheck3').checked = false;
                                                document.getElementById('customCheck4').checked = false;
                                                document.getElementById('customCheck5').checked = false;
                                            }
                                        };
                                    </script>
                                    
                                    
                                    <?php
                                    
                                    echo'
                                </div>
                            </div>
                        </div>
                        <div class="col col-lg-6 col-12">
                            <!-- parte DERECHA -->
                            <div class="contenidoderecha w-100" style="top:118px">
                                <h3 class="pl-3">Editar inscripción</h3>
                                <form id="modificarInscripcion" class="myclass" action="paginas_gestion/divincorp/metamodificarinscripcion.php"  method="post" enctype="multipart/form-data">
                                <row class="w-100">
                                    <div class="row">
                                            <select class="custom-select" name="estadonuevo" id="estadonuevo" required>
                                                <option value="" selected disabled>'.$inscripcion["revision_ext"].'</option>
                                                <option value="EN OBSERVACION">EN OBSERVACION</option>
                                                <option value="CON NOVEDAD">CON NOVEDAD</option>
                                                <option value="SIN NOVEDAD">SIN NOVEDAD</option>
                                                <option value="DESAPROBADO">DESAPROBADO</option>
                                            </select>
                                    </div>
                                    <div class="row p-0">
                                        <div class="col m-0 p-0">
                                            <textarea class="form-control mt-2" name="detallenuevo" id="detallenuevo" rows="4">'.$inscripcion["detalle"].'</textarea> 
                                        </div>                                        
                                        <div class="col col-4" id="tdedicion" style="display:none">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="he_datospersonales" value="DATOS_PERSONALES" id="customCheck1" ';if($existedatospersonales)echo'disabled';echo'>
                                                <label class="custom-control-label p-0 m-0" for="customCheck1" ';if($existedatospersonales)echo'title="Edición ya habilitada"';echo'>Datos personales</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="he_plancarrera" value="PLAN_CARRERA" id="customCheck2" ';if($existeplancarrera)echo'disabled';echo'>
                                                <label class="custom-control-label" for="customCheck2" ';if($existeplancarrera)echo'title="Edición ya habilitada"';echo'>Plan de carrera</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="he_soldvol" value="SOLD_VOL" id="customCheck3" ';if($existesoldvol)echo'disabled';echo'>
                                                <label class="custom-control-label" for="customCheck3" ';if($existesoldvol)echo'title="Edición ya habilitada"';echo'>Soldado voluntario</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="he_secundario" value="SECUNDARIO" id="customCheck4" ';if($existesecundario)echo'disabled';echo'>
                                                <label class="custom-control-label" for="customCheck4" ';if($existesecundario)echo'title="Edición ya habilitada"';echo'>Datos del secundario</label>
                                            </div>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="he_fotosdni" value="FOTOS_DNI" id="customCheck5" ';if($existedocumentacion)echo'disabled';echo'>
                                                <label class="custom-control-label" for="customCheck5" ';if($existedocumentacion)echo'title="Edición ya habilitada"';echo'>Fotos DNI</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col pb-2 m-0 p-0">
                                            <input type="hidden" name="idcandidato" value="'.$candidato["id"].'"/>
                                            <input type="hidden" name="idinscripto" value="'.$lista[$i]["id"].'"/>
                                            <input type="submit" name="enviarmodificacionestado" class="submit btn btn-primary mt-2" value="MODIFICAR" />';
                                            if($inscripcion["estado"] != "INSCRIPTO"){
                                                echo'
                                                <input type="button" name="submit2" class="submit btn btn-secondary mt-2" value="HABILITAR EDICIÓN" onclick="editar()"/>
                                                ';

                                            }
                                            echo'
                                            <a href="?p=editarinscripcion" class="btn btn-primary btn-md mt-2" title="Volver a pagina anterior">Volver</a>
                                            <br><small id="recuerde" class="text-danger" style="display:none" >Recuerde que al habilitar una edición, ésta sólo dura 48hs. Asegúrese de comunicarse con el postulante.</small>

                                        </div>
                                    </div>
                                    <div class="row p-2">
                                        <div class="alert alert-primary w-100" role="alert">
                                    ';

                                    if($folder_documentos != ""){
                                        echo'<h4>Documentación<div class="text-white bg-success ml-3 p-1" style="display:inline;">CARGADA</div></h4>';
                                        $directorio = "." . $folder_documentos["path"];
                                        $ficheros1  = array_diff(scandir($directorio,1), array('..', '.'));
                                    }
                                    else{
                                        echo'<h4 >Documentación<div class="text-white bg-danger ml-3 p-1" style="display:inline;">NO CARGADA</div></h4>';
                                    }
                                    
                                    echo'
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="row">
                                                
                                            ';

                                            
                                                echo'
                                                
                                                ';
                                                if(isset($ficheros1)){
                                                    foreach($ficheros1 as $fichero){
                                                        $remoteImage = $directorio . "/" .$fichero;
                                                        if(strtolower(substr($fichero, -3)) == "pdf"){
                                                            echo'
                                                            <div class="col-6 p-1 m-0">
                                                                <div class="card">
                                                                    
                                                                    <a href="'.$remoteImage.'" target="_blank">
                                                                        <div class="text-center">
                                                                            <img src="assets/img/pdf_logo.png" class="card-img-top w-auto" style="max-height:100px;">
                                                                        </div>
                                                                    </a>
                                                                    <div class="card-body p-1">
                                                                        <h5 class="card-title">'.$fichero.'</h5>
                                                                        <p class="card-text"><small class="text-muted">'.date("F d Y H:i:s.",filectime($remoteImage)).'</small></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                            ';
                                                        }
                                                        else{
                                                            
                                                            echo'
                                                            <div class="col-6 p-1 m-0">
                                                                <div class="card">
                                                                    <a href="'.$remoteImage.'" data-toggle="lightbox" class="">
                                                                        <img src="'.$remoteImage.'" class="card-img-top" style="object-fit: cover;max-height:100px;">
                                                                    </a>
                                                                    <div class="card-body p-1">
                                                                        <h5 class="card-title">'.$fichero.'</h5>
                                                                        <p class="card-text"><small class="text-muted">'.date("F d Y H:i:s.",filectime($remoteImage)).'</small></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            ';
                                                        }
                                                        
                                                    }
                                                }
                                                echo'
                                                ';
                                            

                                            echo'
                                            </div>
                                        </div>
                                        
                                        <div class="col p-1">
                                            <div class="card text-white mb-3 mw-100 h-100" style="background-color:rgba(0, 101, 46, 0.5);">
                                                <div class="text-center">
                                                    <img src="assets/img/saf.png" class="w-50">
                                                </div>
                                                <div class="card-body opacity-5 text-body">
                                                    <h5 class="card-title">Calificación SAF</h5>
                                                    <p class="card-text">
                                                        ESTADO: 
                                                        <select class="custom-select" name="estadonuevo" id="estadonuevo" disabled>
                                                            <option value="" selected disabled>'.$inscripcion["revision_saf"].'</option>
                                                        </select>
                                                        <br>DETALLE: 
                                                        <textarea class="form-control mt-2" disabled>'.$inscripcion["detalle_saf"].'</textarea>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    ';
                                    ?>
                                    
                                    <?php
                                    echo'
                                </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                ';
            }
        }
        ?>
</section>



<?php
};
?>