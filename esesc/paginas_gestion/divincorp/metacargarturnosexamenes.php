<?php

session_start();
if(!isset($_SESSION['usuario'])){
    header('Location: ../../index.php');
}
else{
    include ('../../assets/func/funciones.php');
    include ('../../assets/func/class.DBTurnosExamenes.php');
    
    $conexion = conexion();

    // print_r($_POST);

    $DBTurnosExamenes = new DBTurnosExamenes($conexion);
    if(isset($_POST["idTurno"])){
        $DBTurnosExamenes -> modificarTurnoExamen($_POST["nombre"], $_POST["fdesde"], $_POST["fhasta"], $_POST["idTurno"]);
        
    }
    else{
        $DBTurnosExamenes -> cargarTurno($_POST["nombre"], $_POST["fdesde"], $_POST["fhasta"]);
    }
             
    header('Location: ../../index.php?p=cargarturnosexamenes');
}


?>