<?php
    if(!isset($_SESSION['usuario'])){
        header('Location: ../../index.php');
    }
    else{ 
        $busqueda="";
        $DBCandidato = new DBCandidato($conexion);
        $listainscriptos = $DBCandidato -> obtenerListaInscriptos();
        $enobservacion = $inscriptos =  $noaprobados = $connovedad = $sinnovedad = $desaprobados = $doc_cargada = $total = 0;
        
	    foreach($listainscriptos as $inscripcion){
            $total++;
            if($DBCandidato -> folderDocumentosExiste($inscripcion["id"])){
                $doc_cargada++;
            }
            if($inscripcion["revision_ext"] == "SIN NOVEDAD"){
                $sinnovedad++;
            }
            else if ($inscripcion["revision_ext"] == "CON NOVEDAD"){
                $connovedad++;
            }
            else if ($inscripcion["revision_ext"] == "DESAPROBADO"){
                $desaprobados++;
            }

            if($inscripcion["revision_ext"] == "EN OBSERVACION"){
                $enobservacion++;
            }
            
            if ($inscripcion["estado"] == "INSCRIPCION NO APROBADA"){
                $noaprobados++;
            }
            
            if($inscripcion["estado"] == "INSCRIPTO"){
                $inscriptos++;
            }            
        }
             
?>  


<section id="cuerpofull" class="w-100 mw-100">
    <?php
    echo'
        <div class="row">
            <div class="col text-center">
                <h5>Estadísticas generales</h5>
            </div>
            <div class="col text-center">
                <h5>Estadísticas Div. Ext.</h5>
            </div>
        </div>
        <div class="row">
            
            <div class="col text-center">
                <button class=" border-0 bg-transparent text-left"><h3>'.$inscriptos.'</h3>Inscriptos</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$noaprobados.'</h3>Insc. desaprobadas</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$doc_cargada.'</h3>Doc. Cargadas</button>
            </div>
            <div class="col text-center">
                <button class=" border-0 bg-transparent text-left"><h3>'.$sinnovedad.'</h3>Sin novedad</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$connovedad.'</h3>Con novedad</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$desaprobados.'</h3>Desaprobados</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$enobservacion.'</h3>En observación</button>
                <button class=" border-0 bg-transparent text-left"><h3>'.$total.'</h3><b>Totales</b></button>
            </div>

        </div>';
    ?>
    <h2 class="text-center text-shadow pt-3">Estado de Inscripción</h2>
    <table id="example" class="display w-100">
        <thead>
            <tr>
            <th>ORD<br>LLEGADA</th>
            <th>DNI</th>
            <th>NOMBRE</th>
            <th>APELLIDO</th>
            <th>ESTADO DIV EXT</th>
            <th>DETALLE</th>
            <th>DOCUMENTACIÓN</th>
            <th>INSCRIPCIÓN</th>
            <th></th>
            </tr>
        </thead>
    </table>
    
</section>
<script>
    $(document).ready(function(){
        var table = $('#example').DataTable({
            "ajax": 'paginas_gestion/divincorp/inscripcionjson.php',
            "deferRender": true,
            "orderClasses": false,
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            }
        });
    });
</script>
<?php
    }
?>

