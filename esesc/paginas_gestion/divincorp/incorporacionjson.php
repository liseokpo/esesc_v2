<?php
include ('../../assets/func/funciones.php');
include ('../../assets/func/class.DBIncorporacion.php');
$plan = strtoupper ($_GET["plan"]);

$conexion = conexion();   
$consulta = "SELECT incorporacion.*, inscripcion.*, candidato.id, candidato.dni, plan_carrera.tipo_plan FROM incorporacion LEFT JOIN inscripcion ON incorporacion.id_candidato = inscripcion.id_candidato LEFT JOIN candidato ON inscripcion.id_candidato = candidato.id LEFT JOIN plan_carrera ON plan_carrera.id_candidato = candidato.id WHERE plan_carrera.tipo_plan = '$plan' ORDER BY inscripcion.fecha_aceptacion ASC";

$tabla = ejecutarConsulta($consulta,$conexion);

echo'{"data" : ';
$array = array();
foreach($tabla as $fila){
    $datos_candidato = array();
    array_push(
        $datos_candidato,
        $fila['nro_inscripto'],
        $fila[9],
        $fila['dni'],
        '<b>'.$fila['merito'].'</b>',
        $fila[4],
        '<a href="?p=verdetalleincorporado&idinc='.$fila[0].'" class="btn btn-sm btn-primary" title="Editar datos del postulante">Editar<a>'
    );

    array_push($array, $datos_candidato);   
}

echo $arr = json_encode($array);
// echo $json_string = json_encode($array, JSON_PRETTY_PRINT);
echo"}";
?>