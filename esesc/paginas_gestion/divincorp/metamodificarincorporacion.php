<?php
    session_start();
    if(!isset($_SESSION['usuario'])){
        header('Location: ../../index.php');
    }
    else{
        include ('../../assets/func/funciones.php');
        include ('../../assets/func/class.DBIncorporacion.php');
        include ('../../assets/func/class.DBEdicion.php');
        include ('../../assets/func/class.DBTurnosExamenes.php');
        
        $conexion = conexion();

        $DBIncorporacion = new DBIncorporacion($conexion);
        $DBEdicion = new DBEdicion($conexion);
        
        $idcandidato = $_POST["idcandidato"];
        $idincorp = $_POST["idincorp"];
        $incorporacionAnterior =  $DBIncorporacion -> obtenerIncorporacion($idcandidato);            
            
        $DBIncorporacion -> modificarIncorporacion($idcandidato,$_POST);
            
        $DBIncorporacion -> asignarFechaModificacionIncorporacion($idcandidato);
    
        header('Location: ../../index.php?p=verdetalleincorporado&idinc='.$idincorp);
    }
?>