<?php
if($_SESSION['cargo'] != 5){
    echo"<h2 class='text-center m-5'>Usted NO tiene acceso a este contenido.</h2>";
}
else{
?>

<?php

?>
<section id="cuerpo" class="mt-4">
<h2>Sin calificar</h2>
    <form action="paginas_gestion/diveval/metaexamen.php" method="post">
        <table id="example" class="display">
        <thead>
            <tr>
                <th>NRO INSCRIPTO</th>
                <th>DNI</th>
                <th>TEST PSICOLÓGICO</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th class="m-0 pl-0 pr-0"><input type="submit" name="calificar" class="btn btn-sm btn-primary" value="CALIFICAR A TODOS"></th>
            </tr>
        </tfoot>
        </table>
    </form>
<hr>
<h2>Calificados</h2>
<form action="paginas_gestion/diveval/metaexamen.php" method="post">
    <table id="example2" class="display">
    <thead>
        <tr>
            <th>NRO INSCRIPTO</th>
            <th>DNI</th>
            <th>ESTADO ACTUAL</th>
            <th>MODIFICAR TEST PSICOLÓGICO</th>
            <th></th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th class="m-0 pl-0 pr-0"><input type="submit" name="modificar" class="btn btn-sm btn-primary" value="MODIFICAR A TODOS"></th>
        </tr>
    </tfoot>
    </table>
</form>
</section>
<script>
    
    $(document).ready(function(){
        
        $('#example').DataTable({

            "ajax": 'paginas_gestion/diveval/inscriptosdivevaljson.php',
            "deferRender": true,
            "orderClasses": false,
            "order": [],
            "columns": [
                { orderable: false,width: "12%" },
                null,
                { width: "25%" },
                { width: "1%" },
            ],
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            }
        });

        $('#example2').DataTable({

            "ajax": 'paginas_gestion/diveval/modificarinscriptosdivevaljson.php',
            "deferRender": true,
            "orderClasses": false,
            "order": [],
            "columns": [
                { orderable: false, width: "12%" },
                null,
                null,
                { width: "25%" },
                { width: "1%" },
            ],
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            }
        });
    });
    function autofilltitle(mytext2,mytext){
        var myText=document.getElementById(mytext);
        var myText2=document.getElementById(mytext2);
        myText2.value=myText.value;
    }
    
</script>
<?php
}
?>