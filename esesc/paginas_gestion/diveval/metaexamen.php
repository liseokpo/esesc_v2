<?php
include ('../../assets/func/class.DBExamenPsico.php');
include ('../../assets/func/class.DBCandidato.php');
include ('../../assets/func/class.DBIncorporacion.php');
include ('../../assets/func/funciones.php');

echo"<pre>";
print_r($_POST);
echo"</pre>";

if(isset($_POST['estado'])){
    $estado = array($_POST['estado']);
    $id_candidato = array($_POST['id']);
    $total = 1;
}
else{
    $estado = array();
    foreach(array_keys($_POST) AS $val){
        if(strpos($val, "estado") !== false){
            $valueinput = $_POST[$val];
            array_push($estado, $valueinput);
        }
    }
    $id_candidato = $_POST['id'];
    
    $total = count($_POST['id']);
}

$conexion = conexion();
$DBExamenPsico = new DBExamenPsico($conexion);
$DBCandidato = new DBCandidato($conexion);
$DBIncorporacion = new DBIncorporacion($conexion);

for($i = 0; $i < $total; $i ++){
    $id_incorporacion = $DBIncorporacion -> obtenerIncorporacion($id_candidato[$i]);
    if($estado[$i] == 'AUSENTE'){
        $ausente = 1;
        $estado[$i] = '';
    }
    else{
        $ausente = 0;
    }
    if($DBExamenPsico -> examenExiste($id_incorporacion['id'])){
        $DBExamenPsico -> modificarExamen($estado[$i], $ausente, $id_incorporacion["id"]);
    }
    else{
        $DBExamenPsico -> cargarExamen($estado[$i], $ausente, $id_incorporacion["id"]);
    }
}

header('Location: ../../index.php');

?>