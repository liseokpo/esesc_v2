
<?php
include ('../../assets/func/funciones.php');
include ('../../assets/func/class.DBCandidato.php');
include ('../../assets/func/class.DBExamenPsico.php');

$conexion = conexion();   
$DBExamenPsico = new DBExamenPsico($conexion);
$consultaCandidatoInscriptos = "SELECT * FROM incorporacion LEFT JOIN candidato ON candidato.id = incorporacion.id_candidato LEFT JOIN inscripcion ON inscripcion.id_candidato = candidato.id LEFT JOIN examen_intelectual ON examen_intelectual.id_incorporacion = incorporacion.id LEFT JOIN examen_fisico ON examen_fisico.id_incorporacion = incorporacion.id WHERE examen_fisico.fecha_carga IS NOT NULL AND examen_intelectual.fecha_carga IS NOT NULL ORDER BY inscripcion.fecha_aceptacion ASC";
$listaCandidatosInscriptos = ejecutarConsulta($consultaCandidatoInscriptos,$conexion);

echo'{"data" : ';
$array = array();
foreach($listaCandidatosInscriptos as $fila){
    $datos_candidato = array();
    if(!$DBExamenPsico -> examenExiste($fila[0])){
        array_push(
            $datos_candidato, 
            $fila["nro_inscripto"].'<form id="form2'.$fila["id_candidato"].'" action="paginas_gestion/diveval/metaexamen.php" method="post"></form>',
            $fila["dni"].'<input type="text" name="id" value="'.$fila["id_candidato"].'" form="form2'.$fila["id_candidato"].'" hidden required><input type="text" name="id[]" value="'.$fila["id_candidato"].'"  hidden class="form-control">',
            
            '<input type="text" name="estado" id="'.$fila["id_candidato"].'estado2" form="form2'.$fila["id_candidato"].'" hidden required>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="estado'.$fila["id_candidato"].'" id="'.$fila["id_candidato"].'estadoA" value="APTO" onclick="autofilltitle(this.id.replace(\'A\', \'2\'),this.id);" required>
                <label class="form-check-label" for="'.$fila["id_candidato"].'estadoA">
                    <b>APTO</b>
                </label>
                </div>
                <div class="form-check">
                <input class="form-check-input" type="radio" name="estado'.$fila["id_candidato"].'" id="'.$fila["id_candidato"].'estadoB" value="NO APTO" onclick="autofilltitle(this.id.replace(\'B\', \'2\'),this.id);">
                <label class="form-check-label" for="'.$fila["id_candidato"].'estadoB">
                    <b>NO APTO</b>
                </label>
                </div>
                <div class="form-check">
                <input class="form-check-input" type="radio" name="estado'.$fila["id_candidato"].'" id="'.$fila["id_candidato"].'estadoC" value="AUSENTE" onclick="autofilltitle(this.id.replace(\'C\', \'2\'),this.id);">
                <label class="form-check-label" for="'.$fila["id_candidato"].'estadoC">
                    <b>AUSENTE</b>
                </label>
            </div>',
            '<input type="submit" class="btn btn-sm btn-primary" value="CALIFICAR" form="form2'.$fila["id_candidato"].'">',
        );
        array_push($array, $datos_candidato); 
    }  
}

echo $arr = json_encode($array);
echo"}";
?>