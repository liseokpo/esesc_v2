
<?php
include ('../../assets/func/funciones.php');
include ('../../assets/func/class.DBCandidato.php');
include ('../../assets/func/class.DBExamenMed.php');
$conexion = conexion();   
$DBExamenMed = new DBExamenMed($conexion);
$consultaCandidatoInscriptos = "SELECT * FROM incorporacion LEFT JOIN candidato ON candidato.id = incorporacion.id_candidato LEFT JOIN inscripcion ON inscripcion.id_candidato = candidato.id ORDER BY inscripcion.fecha_aceptacion ASC";
$listaCandidatosInscriptos = ejecutarConsulta($consultaCandidatoInscriptos,$conexion);

echo'{"data" : ';
$array = array();
foreach($listaCandidatosInscriptos as $fila){
    $datos_candidato = array();
    if($DBExamenMed -> examenExiste($fila[0])){
        $examen = $DBExamenMed -> obtenerExamen($fila[0]);
        $estadoExamen = ($examen['estado'] != '') ? $examen['estado'] : 'AUSENTE';

        array_push(
            $datos_candidato, 
            $fila["nro_inscripto"].'<form id="form2'.$fila["id_candidato"].'" action="paginas_gestion/secsanidad/metaexamen.php" method="post"></form>',
            $fila["dni"].'<input type="text" id="'.$fila["id_candidato"].'id2" name="id" form="form2'.$fila["id_candidato"].'" value="'.$fila["id_candidato"].'" hidden> <input type="text" name="id[]" value="'.$fila["id_candidato"].'"  hidden class="form-control text-uppercase ">',
            '<select class="form-control" id="'.$fila["id_candidato"].'estado2" name="estado" form="form2'.$fila["id_candidato"].'" required hidden>
                <option value="" selected disabled>'.$examen["estado"].'</option>
                <option value="APTO">APTO</option>
                <option value="APTO CONDICIONAL">APTO CONDICIONAL</option>
                <option value="NO APTO">NO APTO</option>
                <option value="AUSENTE">AUSENTE</option>
            </select>
            <b>'.$estadoExamen.'</b>
            <select class="form-control" id="'.$fila["id_candidato"].'estado" name="estado[]" onchange="autofilltitle(this.id + 2,this.id)" required>
                <option value="" selected disabled>Indique Nuevo</option>
                <option value="APTO">APTO</option>
                <option value="APTO CONDICIONAL">APTO CONDICIONAL</option>
                <option value="NO APTO">NO APTO</option>
                <option value="AUSENTE">AUSENTE</option>
            </select>
            ',
            
            '<textarea class="form-control mt-2" name="detalle" id="'.$fila["id_candidato"].'detalle2" rows="2" form="form2'.$fila["id_candidato"].'" hidden>'.$examen["detalle"].'</textarea> <textarea class="form-control mt-2" name="detalle[]" id="'.$fila["id_candidato"].'detalle" rows="2" onchange="autofilltitle(this.id + 2,this.id)">'.$examen["detalle"].'</textarea> ',
            '<input type="submit" class="btn btn-sm btn-primary" value="MODIFICAR" form="form2'.$fila["id_candidato"].'">',
        );
        array_push($array, $datos_candidato);   
    }
    
    
}

echo $arr = json_encode($array);
echo"}";




?>
