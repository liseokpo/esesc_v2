<?php
if($_SESSION['cargo'] != 8){
    echo"<h2 class='text-center m-5'>Usted NO tiene acceso a este contenido.</h2>";
}
else{
?>

<section id="cuerpo" class="mt-4">
<h2>Sin calificar</h2>
<form action="paginas_gestion/secsanidad/metaexamen.php" method="post">
    <table id="example" class="display w-100 text-center">
    <thead>
        <tr >
            <th>NRO INSCRIPTO</th>
            <th>DNI</th>
            <th>ESTADO</th>
            <th>Detalle</th>
            <th></th>
        </tr>
    </thead>

    <tfoot>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th><input type="submit" name="calificar" class="btn btn-sm btn-primary" value="CALIFICAR A TODOS"></th>
    </tfoot>
    </table>
</form>
<hr>
<h2>Calificados</h2>
<form action="paginas_gestion/secsanidad/metaexamen.php" method="post">
    <table id="example2" class="display w-100 text-center">
    <thead>
        <tr >
            <th>NRO INSCRIPTO</th>
            <th>DNI</th>
            <th>ESTADO</th>
            <th>Detalle</th>
            <th></th>
        </tr>
    </thead>

    <tfoot>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th><input type="submit" name="modificar" class="btn btn-sm btn-primary" value="MODIFICAR A TODOS"></th>
    </tfoot>
    </table>
</form>
        
</section>
<script>
    
    $(document).ready(function(){
        $('#example2').DataTable({

            "ajax": 'paginas_gestion/secsanidad/modificarinscriptossanidadjson.php',
            "deferRender": true,
            "orderClasses": false,
            "order": [],
            "columns": [
                { orderable: false, width: "8%" },
                null,
                { width: "20%" },
                { width: "30%" },
                null
            ],
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            },
        });
        $('#example').DataTable({

            "ajax": 'paginas_gestion/secsanidad/inscriptossanidadjson.php',
            "deferRender": true,
            "orderClasses": false,
            "order": [],
            "columns": [
                { orderable: false, width: "8%" },
                null,
                { width: "20%" },
                { width: "30%" },
                null
            ],
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                },
            },
        });
        

    });
    function autofilltitle(mytext2,mytext){
        var myText=document.getElementById(mytext);
        var myText2=document.getElementById(mytext2);
        var data =myText.value;
        myText2.value=myText.value;
        console.log(myText.value);
    }

    function promediar(idinput){
        var input = document.getElementById(idinput);

    }

    function funcc(idinput,idinput2){
        alert("id 1 : " + idinput);
        alert("id 2 : " + idinput2);
    }

</script>
<?php
}
?>