﻿<?php
include ('assets/func/funciones.php');
include ('assets/func/class.DBCandidato.php');
include ('assets/func/class.DBUsuario.php');
include ('assets/func/class.DBEdicion.php');
include ('assets/func/class.DBMantenimiento.php');


date_default_timezone_set('America/Argentina/Buenos_Aires');

session_start();
$conexion = conexion(); 
$DBMantenimiento = new DBMantenimiento($conexion);

$mantenimiento = $DBMantenimiento -> enMantenimiento();

if(!$mantenimiento){

    if(isset($_SESSION['usuario']) && $_SESSION['tipo_usuario'] == "postulante"){
        $now = time();
        // checking the time now when home page starts
        if($now > $_SESSION['expire']){
            header ("Location: logout.php");
        }
        $menulocacion = 'paginas_postulante/';
        $locacion = 'paginas_postulante/';
        $locacion2 = ''; 
        $candidatoid = $_SESSION['usuario'];
    
        if (isset($_GET['p'])){

            $pagina = strtolower($_GET['p']);
            
            if(!is_file('paginas_postulante/'.$pagina.'.php')){
                
                $comparacion = substr($pagina, 0, 7);

                if($comparacion == "noticia"){
                    if(!is_file('paginas/noticias/'.$pagina.'.php')){
                        $pagina = 'inicio';
                    }
                    else{
                        $locacion2 = 'paginas/noticias/';
                    }
                }
                else if(!is_file('paginas/'.$pagina.'.php')){
                    $pagina = 'miestado';
                }
                else{
                    $locacion2 = 'paginas/';
                }
            }
        }
        else{
            $pagina = 'miestado';
        }
    }


    if(isset($_SESSION['usuario']) && $_SESSION['tipo_usuario'] == "gestor"){
        $menulocacion = 'paginas_gestion/';
        if($_SESSION['cargo'] == 9){
            $locacion = 'paginas_gestion/divincorp/';
            $locacion2 = ''; 
            $usuario = $_SESSION['usuario'];
            $cargo = $_SESSION['cargo'];
            $permiso = $cargo;
            if (isset($_GET['p'])){
                $pagina = strtolower($_GET['p']);
                if(!is_file('paginas_gestion/divincorp/'.$pagina.'.php')){
                    $pagina = 'editarinscripcion';
                }
            }
            else{
                $pagina = 'editarinscripcion';
            }
        }

        else if($_SESSION['cargo'] == 10){
            $locacion = 'paginas_gestion/saf/';
            $locacion2 = ''; 
            $usuario = $_SESSION['usuario'];
            $cargo = $_SESSION['cargo'];
            $permiso = $cargo;
            if (isset($_GET['p'])){
                $pagina = strtolower($_GET['p']);
                if(!is_file('paginas_gestion/saf/'.$pagina.'.php')){
                    $pagina = 'inicio';
                }
            }
            else{
                $pagina = 'inicio';
            }
            
        }

        else if($_SESSION['cargo'] == 5){
            $locacion = 'paginas_gestion/diveval/';
            $locacion2 = ''; 
            $usuario = $_SESSION['usuario'];
            $cargo = $_SESSION['cargo'];
            $permiso = $cargo;
            if (isset($_GET['p'])){
                $pagina = strtolower($_GET['p']);
                if(!is_file('paginas_gestion/diveval/'.$pagina.'.php')){
                    $pagina = 'carga_examenes_inicio';
                }
            }
            else{
                $pagina = 'carga_examenes_inicio';
            }
            
        }

        else if($_SESSION['cargo'] == 6){
            $locacion = 'paginas_gestion/divestudio/';
            $locacion2 = ''; 
            $usuario = $_SESSION['usuario'];
            $cargo = $_SESSION['cargo'];
            
            $permiso = $cargo;
            if (isset($_GET['p'])){
                $pagina = strtolower($_GET['p']);
                if(!is_file('paginas_gestion/divestudio/'.$pagina.'.php')){
                    $pagina = 'carga_examenes_inicio';
                }
            }
            else{
                $pagina = 'carga_examenes_inicio';
            }
            
        }
        else if($_SESSION['cargo'] == 7){
            $locacion = 'paginas_gestion/divedufis/';
            $locacion2 = ''; 
            $usuario = $_SESSION['usuario'];
            $cargo = $_SESSION['cargo'];
            $permiso = $cargo;
            if (isset($_GET['p'])){
                $pagina = strtolower($_GET['p']);
                if(!is_file('paginas_gestion/divedufis/'.$pagina.'.php')){
                    $pagina = 'carga_examenes_inicio';
                }
            }
            else{
                $pagina = 'carga_examenes_inicio';
            }
            
        }
        else if($_SESSION['cargo'] == 8){
            $locacion = 'paginas_gestion/secsanidad/';
            $locacion2 = ''; 
            $usuario = $_SESSION['usuario'];
            $cargo = $_SESSION['cargo'];
            $permiso = $cargo;
            if (isset($_GET['p'])){
                $pagina = strtolower($_GET['p']);
                if(!is_file('paginas_gestion/secsanidad/'.$pagina.'.php')){
                    $pagina = 'carga_examenes_inicio';
                }
            }
            else{
                $pagina = 'carga_examenes_inicio';
            }
            
        }
        else if($_SESSION['cargo'] == 1){
            $locacion = 'paginas_gestion/divincorp/';
            $locacion2 = ''; 
            $usuario = $_SESSION['usuario'];
            $cargo = $_SESSION['cargo'];
            $permiso = $cargo;

            if (isset($_GET['p'])){
                $pagina = strtolower($_GET['p']);
                if(!is_file('paginas_gestion/divincorp/'.$pagina.'.php')){
                    $pagina = 'editarinscripcion';
                }
            }
            else{
                $pagina = 'editarinscripcion';
            }
        }
        else{
            header ("Location: logout.php");
        }
    }

    else if(!isset($_SESSION['usuario'])){
        $menulocacion = 'paginas/';
        $locacion = 'paginas/';
        $locacion2 = '';
        if (isset($_GET['p'])){
            $pagina = strtolower($_GET['p']);

            $comparacion = substr($pagina, 0, 7);

            if($comparacion == "noticia"){
                if(!is_file('paginas/noticias/'.$pagina.'.php')){
                    $pagina = 'inicio';
                }
                else{
                    $locacion2 = 'paginas/noticias/';
                }
            }
            else if(!is_file('paginas/'.$pagina.'.php')){
                $pagina = 'inicio';
            }
        }
        else{
            $pagina = 'inicio';
        }
    }

    require_once 'common/header.php';

    require_once $menulocacion . '/menu.php';

    if(is_file($locacion2.$pagina.'.php')){
        require_once $locacion2 . $pagina . '.php';
    }
    else{
        require_once $locacion . $pagina . '.php';
    }

    require_once 'common/footer.php';

}

else{

    require_once 'mantenimiento.php';

}
?>
