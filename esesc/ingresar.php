<title>Escuela de Suboficiales del Ejército "Sargento Cabral"</title>
<!-- FAVICON -->
<link rel="apple-touch-icon" sizes="57x57" href="assets/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="assets/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="assets/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="assets/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="assets/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="assets/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="assets/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="assets/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="assets/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="assets/img/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="assets/img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<meta charset="UTF-8">
<link href="assets/css/estilosusuarios.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<body id="bglogin" style="height:100%;width:100%">
    <section>
        <div class="container-fluid">
            <div class="row align-items-center h-100">
                <div class="col-sm d-none d-sm-block h-100 w-100" style="background:url(assets/img/Degrade.jpg);background-repeat:repeat-x;background-size: cover;">
                    <div class="h-100 d-flex  align-content-center flex-wrap">
                        <a  class="mx-auto" href="index.php"><img src="assets/img/pie/logo.png" width="200px" alt="Al inicio"></a>
                    </div>
                </div>
                <div class="col-sm w-100">
                    <h1 class="text-center mb-3">Escuela de Suboficiales del Ejército<br>"Sargento Cabral"</h1>
                    <div class="text-center mb-3">
                        <a class="btn btn-md btn-primary" style="width:150px;" href="login.php">Gestión</a>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-md btn-warning" style="width:150px;" href="ingreso.php">Postulantes</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</body>