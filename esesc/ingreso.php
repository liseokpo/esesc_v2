<?php
session_start();


if(!isset($_SESSION['candidatoid'])){
    require_once 'common/header.php';
    
?>
<!DOCTYPE html>
    <head>
        <meta charset="UTF-8">
        <link href="assets/css/estilosusuarios.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
    </head>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <body id="bglogin">
        <section>
        <div class="pt-4">
            
            <form action="metaingreso.php" method="post">
            
                <div class="form">
                    <h3 class="text-center mt-4">Postulantes</h3>
                    <div class="col-lg-4 col-sm-auto   mb-4 mx-auto">
                        <input type="text" class="form-control" id="dni" name="dni" placeholder="Dni (sin puntos)">
                    </div>
                </div>
                <div class="form">
                    <div class="col-lg-4 mb-4 mx-auto">
                        <div class="input-group">
                            <input type="password" class="form-control" id="clave" name="clave" placeholder="Contraseña">                    
                            <div class="input-group-append">
                                <button id="show_password3" class="btn btn-primary" type="button" onclick="mostrarPassword('clave','icono3')"> <span id="icono3" class="fa fa-eye-slash icon"></span> </button>
                            </div>
                        </div>
                    </div>
                </div>
                    
                <div class="form">
                    <div class="col-lg-2  mx-auto">
                        <input class="form-control btn btn-primary" type="submit" value="Ingresar">
                    </div>
                    <div class="text-center">
                    <small><a href="index.php?p=recuperarcontrasenia">¿Olvidó su contraseña?</a></small> <br>                   
                    <small><a href="index.php?p=inscripcion">¿Aún no se inscribió? Ingrese aquí</a></small>                    
                    </div>
                    <div class="text-center pt-2">
                        <div class="g-recaptcha" data-sitekey="6LeHvckUAAAAAJH76Onq-CfyiOGjWevzyd11fAtM"></div>
                    </div>
                    <?php
                    if(isset($_GET['mensaje']))
                        echo"<div class='mt-0 text-center' style='color:#FF4848;'>".$_GET['mensaje'];
                    ?>
                </div>
                </div>
            </form>

        </div>
        </section>
        
    </body>
</html>

<?php
}
else{
    header('Location: index.php?p=miestado');
}
?>