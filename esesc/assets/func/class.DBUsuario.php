<?php

class DBUsuario{

    private $DBConexion;

    function __construct($Conexion){
        $this -> DBConexion = $Conexion;
    }

    public function obtenerIdUsuario($nombre){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT id FROM usuario WHERE user =".$nombre);            
            $stmt -> execute(); 
            $idPadre = $stmt->fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo id del usuario.";
            echo"<br>";
        } 
        return $idPadre['id'];
    }

    public function obtenerUsuarioporId($idusuario){
        $data = [
            'idusuario' => $idusuario,
        ];
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM usuario WHERE usuario.id = :idusuario LIMIT 1");            
            $stmt -> execute($data);
            $usuario = $stmt->fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo candidato por id.";
        } 
        return $usuario;   
    }

    public function obtenerUsuarioPorEmail($correo){
        $data = [
            'correo' => $correo,
        ];
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM usuario WHERE usuario.email = :correo LIMIT 1");            
            $stmt -> execute($data);
            $usuario = $stmt->fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            return false;
        } 
        return $usuario;   
    }

    public function recoverPassword($id, $codigoRecuperacion, $fechaRecuperacion){
        $data = [
            'id' => NULL,
            'codigo' => $codigoRecuperacion,
            'fecha_expiracion' => $fechaRecuperacion,
            'usado' => 0,
            'id_usuario' => $id,
        ];
        try{                                        
            $stmt = $this -> DBConexion -> prepare("INSERT INTO recuperar_pass(id, codigo, fecha_expiracion, usado, id_usuario) VALUES (:id, :codigo, :fecha_expiracion, :usado, :id_usuario)");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            return false;
        }
        catch(Exception $e){
            return false;
        }
        $stmt = null;
        return true;
    }

    public function cargarClaveUsuario($idusuario,$clave){
        $data = [
            'clave' => $clave,
            'idusuario' => $idusuario,
        ];
        try {                                     

            $stmt = $this -> DBConexion -> prepare("UPDATE usuario SET pass = :clave WHERE usuario.id = :idusuario") ;
                                                    
            $stmt -> execute($data);
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar Clave del usuario: ".$e->getMessage();
        }
    }

    public function codigoUsado($usado,$idusuario){
        $data = [
            'usado' => $usado,
            'id_usuario' => $idusuario,
        ];
        try{        
            $stmt = $this -> DBConexion -> prepare("UPDATE recuperar_pass SET usado = :usado WHERE recuperar_pass.id_usuario = :id_usuario");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al modificar uso de codigo re recuperacion : ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

    public function obtenerUsuarios(){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM usuario WHERE TRUE");            
            $stmt -> execute(); 
            $lista = $stmt -> fetchAll();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo lista de usuarios.";
        }
        return $lista;
    }

    public function nombreUsuarioExiste($user){
        $data = [
            'user' => $user,
        ];
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT user FROM usuario WHERE usuario.user = :user LIMIT 1");
            $stmt -> execute($data);
            $resultado = $stmt -> rowCount();
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar existencia de nombre de usuario: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar existencia de nombre de usuario: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function emailExiste($email){
        $data = [
            'email' => $email,
        ];
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT email FROM usuario WHERE usuario.email = :email LIMIT 1");
            $stmt -> execute($data);
            $resultado = $stmt -> rowCount();
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar existencia de email: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar existencia de email: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function emailPostulantesExiste($email){
        $data = [
            'email' => $email,
        ];
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT email FROM candidato WHERE candidato.email = :email LIMIT 1");
            $stmt -> execute($data);
            $resultado = $stmt -> rowCount();
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar existencia de email: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar existencia de email: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function modificarUsuario($user,$email,$cargo, $idusuario){
        try {
            $stmt = $this -> DBConexion -> prepare("UPDATE usuario SET user = '$user', email = '$email', cargo = '$cargo' WHERE usuario.id = '$idusuario'");  

            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar modificacion usuario: ".$e->getMessage();
        }
    }

    public function cargarUsuario($user,$email,$cargo,$pass){
        try {
            $stmt = $this -> DBConexion -> prepare("INSERT INTO usuario(user,pass,email,cargo) VALUES('$user','$pass','$email','$cargo')");  

            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar usuario: ".$e->getMessage();
        }
    }

    public function validarUserYEmail($user,$email){
        $userExiste = $this -> nombreUsuarioExiste($user);
        $emailExiste = $this -> emailExiste($email);
        $emailPostulanteExiste = $this -> emailPostulantesExiste($email);

        if($userExiste){
            return "El usuario ingresado ya se encuentra registrado. Intente con uno nuevo.";
        }
        if($emailExiste || $emailPostulanteExiste){
            return "El email ingresado ya se encuentra registrado. Intente con uno nuevo.";
        }
        return null;
    }

    public function obtenerCargo($cargo){
        try {
            $stmt = $this -> DBConexion -> prepare("SELECT descripcion FROM cargo WHERE cargo.id = '$cargo'");  

            $stmt -> execute();
            $cargo = $stmt -> fetchColumn();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar usuario: ".$e->getMessage();
        }
        return $cargo;
    }

    public function deshabilitarUsuario($idusuario){
        try {
            $stmt = $this -> DBConexion -> prepare("UPDATE usuario SET cargo = '0' WHERE usuario.id = '$idusuario'");  

            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al deshabilitar el usuario: ".$e->getMessage();
        }
    }
}

?>