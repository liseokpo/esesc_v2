<?php
const troteMetros = 2000;
const calif_MAX = 10;
const calif_MIN = 4;

//FEMENINAS
const ffbb_F_MAX = 24;
const ffbb_F_MIN = 5;
const ffbb_F_CERO = 1;

const abdo_F_MAX = 42;
const abdo_F_MIN = 18;
const abdo_F_CERO = 10;


const tiempoTrote_F_MAX = 900; 
const tiempoTrote_F_MIN = 1300; 
const tiempoTrote_F_CERO = 1540; 

//MASCULINOS
const ffbb_M_MAX = 34;
const ffbb_M_MIN = 10;
const ffbb_M_CERO = 2;

const abdo_M_MAX = 52;
const abdo_M_MIN = 28;
const abdo_M_CERO = 20;

const tiempoTrote_M_MAX = 800;
const tiempoTrote_M_MIN = 1200;
const tiempoTrote_M_CERO = 1440;

class DBExamenEduFis{
    private $DBConexion;
   


    function __construct($Conexion){
        $this -> DBConexion = $Conexion;
    }

    public function cargarExamen($ffbb, $abdo, $trote, $sexo, $detalle, $ausente, $id_incorporacion){
        $puntajeffbb = $this -> calcularPuntajeFFBB($ffbb, $sexo);
        $puntajeabdo = $this -> calcularPuntajeABDO($abdo, $sexo);
        $puntajetrote = $this -> calcularPuntajeTROTE($trote, $sexo, $ausente);
        $promedio = $this -> calcularPromedio($puntajeffbb, $puntajeabdo, $puntajetrote);
        $estado = $this -> calcularEstado($puntajeffbb, $puntajeabdo, $puntajetrote);
        
        try {
            $stmt = $this -> DBConexion -> prepare("INSERT INTO examen_fisico(id, ffbb, abdo, trote, ffbb_puntaje, abdo_puntaje, trote_puntaje, promedio, detalle, ausente, estado, id_incorporacion) 
                                                    VALUES (NULL,'$ffbb','$abdo','$trote','$puntajeffbb','$puntajeabdo','$puntajetrote','$promedio','$detalle','$ausente','$estado','$id_incorporacion')");
            # Otro Ejemplo de error ! DELECT en lugar de SELECT!
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar examen fisico: ".$e->getMessage();
        }

    }

    public function calcularPuntajeFFBB($ffbb, $sexo){
        $puntajeffbb = 0;
        
        if($sexo == "MASCULINO"){
            if($ffbb >= ffbb_M_CERO && $ffbb <= ffbb_M_MIN){
                $puntajeffbb = ($ffbb - 2) * 0.5;
            }
            
            else if($ffbb > ffbb_M_MIN && $ffbb <= ffbb_M_MAX){
                if($ffbb % 2 == 0){
                    $puntajeffbb = 4 + (($ffbb - 10) * 0.25);
                }
                else{
                    $puntajeffbb = 4 + (($ffbb - 11) * 0.25); 
                }
            }
            else if($ffbb > ffbb_M_MAX){
                $puntajeffbb = 10;
            }

            else if($ffbb <= ffbb_M_CERO){
                $puntajeffbb = 0;
            }
        }

        else if($sexo == "FEMENINO"){
            if($ffbb >= ffbb_F_CERO && $ffbb <= ffbb_F_MIN){
                $puntajeffbb = $ffbb - 1;
            }
            
            else if($ffbb > ffbb_F_MIN && $ffbb <= 10){
                $puntajeffbb = 4 + (($ffbb - ffbb_F_MIN) * 0.5);
            }
            
            else if($ffbb > 10 && $ffbb <= ffbb_F_MAX){
                if($ffbb % 2 == 0){
                    $puntajeffbb = 6.5 + (($ffbb - 10) * 0.25);
                }
                else{
                    $puntajeffbb = 6.5 + (($ffbb - 11) * 0.25); 
                }
            }

            else if($ffbb > ffbb_F_MAX){
                $puntajeffbb = 10;
            }
        }

        return $puntajeffbb;
    }

    public function calcularPuntajeABDO($abdo, $sexo){
        $puntajeabdo = 0;
        
        if($sexo == "MASCULINO"){
            if($abdo >= abdo_M_CERO && $abdo <= abdo_M_MIN){
                $puntajeabdo = ($abdo - abdo_M_CERO) * 0.5;
            }
            else if($abdo > abdo_M_MIN && $abdo <= abdo_M_MAX){
                if($abdo % 2 == 0){
                    $puntajeabdo = 4 + (($abdo - abdo_M_MIN) * 0.25);
                }
                else{
                    $puntajeabdo = 4 + (($abdo - abdo_M_MIN - 1) * 0.25);
                }
            }
            else if($abdo > abdo_F_MAX){
                $puntajeabdo = 10;
            }
        }

        else if($sexo == "FEMENINO"){
            if($abdo >= abdo_F_CERO && $abdo <= abdo_F_MIN){
                $puntajeabdo = ($abdo - abdo_F_CERO) * 0.5;
            }
        
            else if($abdo > abdo_F_MIN && $abdo <= abdo_F_MAX){
                if($abdo % 2 == 0){
                    $puntajeabdo = 4 + (($abdo - abdo_F_MIN) * 0.25);
                }
                else{
                    $puntajeabdo = 4 + (($abdo - abdo_F_MIN - 1) * 0.25);
                }
            }
            else if($abdo > abdo_F_MAX){
                $puntajeabdo = 10;
            }
        }

        return $puntajeabdo;
    }

    public function calcularPuntajeTROTE($trote, $sexo, $ausente){
        $ultimos2numerosTrote = substr($trote, -2);

        $puntajetrote = 0;
        $diferencia = 0;

        if($ultimos2numerosTrote > 0 && $ultimos2numerosTrote <= 20){
            //hacer que termine en 20.
            $diferencia = 20 - $ultimos2numerosTrote;
        }
        else if($ultimos2numerosTrote > 20 && $ultimos2numerosTrote <= 40){
            //hacer que termine en 40
            $diferencia = 40 - $ultimos2numerosTrote;
        }
        else if($ultimos2numerosTrote > 40 && $ultimos2numerosTrote < 60){
            //hacer que termine en 00
            $diferencia = 100 - $ultimos2numerosTrote;
        }

        $equivalenteTrote = $trote + $diferencia;

        if($sexo == "MASCULINO"){

            if($equivalenteTrote >= tiempoTrote_M_MAX && $equivalenteTrote <= tiempoTrote_M_CERO){
                $x = ceil($equivalenteTrote / 20);
                $y = round($equivalenteTrote / 100);
                $puntajetrote = 10 - (($x - (40 + (($y - 8) * 2))) * 0.5);
            }

            else if($equivalenteTrote < tiempoTrote_M_MAX){
                $puntajetrote = 10;
            }
            
        }

        else if($sexo == "FEMENINO"){

            if($equivalenteTrote >= tiempoTrote_F_MAX && $equivalenteTrote <= tiempoTrote_F_CERO){
                $x = ceil($equivalenteTrote / 20);
                $y = round($equivalenteTrote / 100);
                $puntajetrote = 10 - (($x - (45 + (($y - 9) * 2))) * 0.5);
            }

            else if($equivalenteTrote < tiempoTrote_F_MAX){
                $puntajetrote = 10;
            }

        }

        if($ausente == 1){
            $puntajetrote = 0;
        }
        return $puntajetrote;
    }

    public function calcularPromedio($puntajeffbb, $puntajeabdo, $puntajetrote){
        return ($puntajeffbb + $puntajeabdo + $puntajetrote) / 3;
    }

    public function calcularEstado($puntajeffbb, $puntajeabdo, $puntajetrote){
        if($puntajetrote < 4 || $puntajeffbb < 4 || $puntajeabdo < 4){
            return "DESAPROBADO";
        }
        return "APROBADO";
    }

    public function examenExiste($id_incorporacion){
        $data = [
            'incorporacion' => $id_incorporacion,
        ];
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT id_incorporacion FROM examen_fisico WHERE examen_fisico.id_incorporacion = :incorporacion LIMIT 1");
            $stmt -> execute($data);
            $resultado = $stmt -> rowCount();
        }
        catch(PDOExeption $e){
            echo "Se ha producido un error al verificar examen edu fis: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar examen edu fis: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }
    
    public function obtenerExamen($id_incorporacion){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM examen_fisico WHERE id_incorporacion = '$id_incorporacion' LIMIT 1");            
            $stmt -> execute(); 
            $examen = $stmt -> fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error: obteniendo Examen edu fis.";
        }
        return $examen;
    }

    public function modificarExamen($ffbb, $abdo, $trote, $sexo, $detalle, $ausente, $id_incorporacion){
        $puntajeffbb = $this -> calcularPuntajeFFBB($ffbb, $sexo);
        $puntajeabdo = $this -> calcularPuntajeABDO($abdo, $sexo);
        $puntajetrote = $this -> calcularPuntajeTROTE($trote, $sexo,$ausente);
        $promedio = $this -> calcularPromedio($puntajeffbb, $puntajeabdo, $puntajetrote);
        $estado = $this -> calcularEstado($puntajeffbb, $puntajeabdo, $puntajetrote);

        try {
            $fecha = $this -> fechaActual();
            $stmt = $this -> DBConexion -> prepare("UPDATE examen_fisico SET ffbb = '$ffbb', abdo = '$abdo', trote = '$trote', ffbb_puntaje = '$puntajeffbb', abdo_puntaje = '$puntajeabdo', trote_puntaje = '$puntajetrote', promedio = '$promedio', estado = '$estado', fecha_carga = '$fecha', detalle = '$detalle', ausente = '$ausente' WHERE examen_fisico.id_incorporacion = '$id_incorporacion'");  

            # Otro Ejemplo de error ! DELECT en lugar de SELECT!
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar modificacion de examen fisico: ".$e->getMessage();
        }
    }
    public function fechaActual(){
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fechaActual = date("Y-m-d H:i:s", strtotime('+0 hours'));
        return $fechaActual;
    }

    public function obtenerListaExamenes(){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM examen_fisico WHERE TRUE");            
            $stmt -> execute(); 
            $lista = $stmt -> fetchAll();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo lista examenes fisicos.";
        }
        return $lista;
    }

    public function eliminarExamen($id_incorporacion){
        $examen = $this -> obtenerExamen($id_incorporacion);
        $id_examen = $examen['id'];
        $data = [
            'id_examen' => $id_examen,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("DELETE FROM examen_fisico WHERE id = :id_examen");
            $stmt -> execute($data);
        }
        catch(PDOExeption $e){
            echo "Se ha producido un error al eliminar examen fisico: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

}
?>