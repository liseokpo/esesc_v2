<?php

class DBEdicion{
    private $DBConexion;

    function __construct($Conexion){
        $this -> DBConexion = $Conexion;
    }

    //antes de cargar edicion se necesita verificar que no haya una existente sin usar.
    //carga una edicion, necesita el gestor, el candidato y el enum del permiso.
    public function cargarEdicion($idgestor,$idcandidato,$permiso){
        $fechaActual = $this -> fechaActual();

        try {
            $stmt = $this -> DBConexion -> prepare("INSERT INTO editardatos(id, id_usuario, id_candidato, permiso, fecha_emision) 
                                                    VALUES (NULL,'$idgestor','$idcandidato','$permiso','$fechaActual')");
            # Otro Ejemplo de error ! DELECT en lugar de SELECT!
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar la edición del candidato: ".$e->getMessage();
        }
    }

    //elimina una edicion. Necesita el id del candidato, del gestor y el enum del permiso
    public function cancelarEdicion($idcandidato,$permiso){
        try {
            $stmt = $this -> DBConexion -> prepare("DELETE FROM editardatos WHERE id_candidato = '$idcandidato' AND permiso = '$permiso' AND usado = '0'");
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cancelar la edición: ".$e->getMessage();
        }
    }

    public function usarEdicion($idcandidato, $permisoparaeditar){
        $fechaActual = $this -> fechaActual();
        
        try {
            $stmt = $this -> DBConexion -> prepare("UPDATE editardatos SET fecha_uso = '$fechaActual', usado = '1' WHERE editardatos.id_candidato = '$idcandidato' AND permiso = '$permisoparaeditar' AND usado = '0'");
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al usar una edicion: ".$e->getMessage();
            echo"<br>";            
        }
    }

    //obtiene los datos de una edicion en particular idcandidato, permiso.
    public function existeEdicion($idcandidato,$permiso){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT id FROM editardatos WHERE id_candidato = '$idcandidato' AND permiso = '$permiso' AND usado = '0' ");            
            $stmt -> execute(); 
            $resultado = $stmt -> rowCount();
            $stmt = null;
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar email: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar email: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }


    public function existeEdicionEnCandidato($idcandidato){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT id FROM editardatos WHERE id_candidato = '$idcandidato' AND usado = '0' ");            
            $stmt -> execute(); 
            $resultado = $stmt -> rowCount();
            $stmt = null;
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar email: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar email: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function obtenerEdicionesEnCandidato($idcandidato){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM editardatos WHERE id_candidato = '$idcandidato' AND usado = '0' ");            
            $stmt -> execute(); 
            $resultado = $stmt -> fetchAll();
            $stmt = null;
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar email: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar email: ".$e->getMessage();
            echo"<br>";   
        }
        return $resultado;
    }

    public function existeEdicionFotosDNI_Comprobante($idcandidato){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT id FROM editardatos WHERE id_candidato = '$idcandidato' AND usado = '0' AND  ( permiso = 'FOTOS_DNI' OR permiso = 'FOTO_COMP_PAGO' OR permiso = 'FOTO_TICKET_PAGO')");            
            $stmt -> execute(); 
            $resultado = $stmt -> rowCount();
            $stmt = null;
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar email: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar email: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function fechaActual(){
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fechaActual = date("Y-m-d H:i:s", strtotime('+0 hours'));
        return $fechaActual;
    }

    //PARA VALIDAR ESTA EDICION TENGO QUE OBTENER LA FECHA DE EMISION DEL PERMISO
    //DEBO SABER SI LA FECHA DE EMISIÓN TIENE 48 HORAS O MÁS ENCIMA.
    //SI LA FECHA actual TIENE 48HS O MÁS que la fecha de emision, ENTONCES PASO LA EDICIÓN A "USADA" SIN QUE TENGA FECHA DE USO.
    //SI UNA EDICIÓN ESTÁ USADA SIN FECHA DE USO, ENTONCES SE VENCIÓ.

    public function validarEdicion($idcandidato,$permisoparaeditar){
        $idEdicion = $this -> obtenerIdEdicion($idcandidato,$permisoparaeditar);
        $fechaEmisionEdicion = $this -> obtenerFechaEmisionEdicion($idEdicion);
        $fechaActual = $this -> fechaActual();
        $fechaEmision48 = date("Y-m-d H:i:s", strtotime($fechaEmisionEdicion.'+48 hours'));

        if($fechaActual >= $fechaEmision48){
            try {
                $stmt = $this -> DBConexion -> prepare("UPDATE editardatos SET usado = '1' WHERE editardatos.id = '$idEdicion'");
                $stmt -> execute();
                $stmt = null;
            }
            catch(PDOException $e) {
                echo "Se ha producido un error al usar una edicion: ".$e->getMessage();
                echo"<br>";            
            }
        }

    }

    public function obtenerIdEdicion($idcandidato,$permisoparaeditar){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT id FROM editardatos WHERE id_candidato ='$idcandidato' AND permiso = '$permisoparaeditar' AND usado = '0' LIMIT 1");            
            $stmt -> execute(); 
            $idEdicion = $stmt->fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo ID de la edición.";
        }
        if(isset($idEdicion['id'])){
            return $idEdicion['id'];
        } 
        return null;
    }

    public function obtenerFechaEmisionEdicion($idEdicion){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM editardatos WHERE id ='$idEdicion'");            
            $stmt -> execute(); 
            $edicion = $stmt->fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo ID de la edición.";
        } 
        if(isset($edicion['fecha_emision'])){
          return $edicion['fecha_emision'];
        } 
        return null;
    }

    public function obtenerEdicionesCandidato($idcandidato){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM editardatos WHERE id_candidato ='$idcandidato' AND usado = '1' AND fecha_uso IS NOT NULL");            
            $stmt -> execute(); 
            $idEdiciones = $stmt->fetchAll();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo ID de la edición.";
        }
        if(isset($idEdiciones)){
            return $idEdiciones;
        } 
        return null;
    }


    public function candidatoTieneEdicionSafUsadaHace72horas($idcandidato){
        $ediciones = $this -> obtenerEdicionesCandidato($idcandidato);
        $fechaActual = $this -> fechaActual();
        foreach($ediciones as $edicion){
            if($edicion["permiso"] == "FOTO_COMP_PAGO" || $edicion["permiso"] == "FOTO_TICKET_PAGO"){
                $fechaEmision72 = date("Y-m-d H:i:s", strtotime($edicion["fecha_uso"].'+72 hours'));
                if($fechaEmision72 >= $fechaActual){
                    return true;
                }
            }           
        }
        
        return false;
    }

    public function candidatoTieneEdicionExtUsadaHace72horas($idcandidato){
        $ediciones = $this -> obtenerEdicionesCandidato($idcandidato);
        $fechaActual = $this -> fechaActual();
        foreach($ediciones as $edicion){
            if($edicion["permiso"] != "FOTO_COMP_PAGO" && $edicion["permiso"] != "FOTO_TICKET_PAGO"){
                $fechaEmision72 = date("Y-m-d H:i:s", strtotime($edicion["fecha_uso"].'+72 hours'));
                if($fechaEmision72 >= $fechaActual){
                    return true;
                }
            }           
        }
        
        return false;
    }
}
?>