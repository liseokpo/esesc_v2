    <?php


    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    ini_set('memory_limit', '2000M');
    function conectar(){
        //MySQLi
        $hostname = 'localhost';
        $database = 'bd_esesc_2021';
        $username = 'root';
        $password = 'bd3s3sc2019';
        mysqli_report(MYSQLI_REPORT_STRICT); 
        $mysqli = new mysqli($hostname, $username,$password, $database);
        if ($mysqli -> connect_errno) {
            die( "Fallo la conexión a MySQL: (" . $mysqli -> connect_error
            . ") " . $mysqli -> connect_error);
        }
        $mysqli -> set_charset("utf8");
        return $mysqli;
    }

    function conexion(){
        $hostname = 'localhost';
        $database = 'bd_esesc_2021';
        $username = 'root';
        $password = 'bd3s3sc2019';
        
        try{
            $pdo = new PDO("mysql:host=$hostname;dbname=$database;charset=utf8", $username, $password);
            $pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            return $pdo;
        }
        catch(PDOException $error){
            echo $error -> getMessage();
        }
    }


    function ejecutarConsulta($consulta,$conexion){
        $resultado = $conexion -> query($consulta) or die ("error en consulta");
        return $resultado;
    }

    function fechaNacimientoLimite($edad,$mes,$dia){
    return (date("Y") - $edad + 1)."-".$mes."-".$dia;
    }

    function mostrarNovedades(){
        echo'<img src="assets/img/resultados.jpg" style="z-index:-1;" class="d-block w-100"alt="...">';
    }

    function generarClaveAleatoria($longitud = 8, $opcLetra = TRUE, $opcNumero = TRUE, $opcMayus = TRUE, $opcEspecial = FALSE){
        $letras ="abcdefghijklmnopqrstuvwxyz";
        $numeros = "1234567890";
        $letrasMayus = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
        $especiales ="|@#~$%()=^*+[]{}-_";
        $listado = "";
        $password = "";
        if ($opcLetra == TRUE) $listado .= $letras;
        if ($opcNumero == TRUE) $listado .= $numeros;
        if($opcMayus == TRUE) $listado .= $letrasMayus;
        if($opcEspecial == TRUE) $listado .= $especiales;
        
        for( $i=1; $i<=$longitud; $i++) {
        $caracter = $listado[rand(0,strlen($listado)-1)];
        $password.=$caracter;
        $listado = str_shuffle($listado);
        }
        return $password;
    }

    function hashearPass($password){
                //password_hash
                $hash = password_hash($password, PASSWORD_DEFAULT, ['cost' => 10]);
                return $hash;
    }


    function verificarhashPass($password,$passwordhash){
            //password_verify()
            if(password_verify($password,$passwordhash)){
                return TRUE;
            }
            return FALSE;
    }
        
    function createRandomCode()
    {
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz0123456789";
        srand((double)microtime()*1000000);
        $i = 0;
        $pass = '' ;

        while ($i <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }

        return time().$pass;
    }

    function sendMail($correoElectronico, $nombre, $url, $nombreTemplate, $tituloCorreo, $distanciadelHost)
    {
        define('URL_PUBLIC_FOLDER', 'public');
        define('URL_PROTOCOL', '//');
        define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
        define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));
        define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER);
        $distanciaHost = "";
        for ($i = 0; $i < $distanciadelHost; ++$i){
            $distanciaHost = $distanciaHost."../";
        }

        $template = file_get_contents($distanciaHost.'assets/view/'.$nombreTemplate.'.php');
        $template = str_replace("{{name}}", $nombre, $template);
        $template = str_replace("{{action_url_2}}", '<b>https:'.URL_PROTOCOL.URL_DOMAIN.'/'.$url.'</b>', $template);
        $template = str_replace("{{action_url_1}}", 'https:'.URL_PROTOCOL.URL_DOMAIN.'/'.$url, $template);
        $template = str_replace("{{year}}", date('Y'), $template);
        $template = str_replace("{{operating_system}}", getOS(), $template);
        $template = str_replace("{{browser_name}}", getBrowser(), $template);
            
        require_once $distanciaHost."vendor/autoload.php";
        $mail = new PHPMailer(true);
        $mail->CharSet = "UTF-8";

        try {
            $mail->isSMTP();
            $mail->Host = 'smtp.googlemail.com';  //gmail SMTP server
            $mail->SMTPAuth = true;
            $mail->Username = 'eaesesc.eci@gmail.com';   //username
            $mail->Password = 'equipodinamita';   //password
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;                    //smtp port

            $mail->setFrom('eaesesc.eci@gmail.com', 'ESESC');
            $mail->addAddress($correoElectronico, $nombre);

            $mail->isHTML(true);

            $mail->Subject = $tituloCorreo;
            $mail->Body    = $template;

            if (!$mail->send()) {
                return false;
            } else {
                return true;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    function getOS()
    {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        $os_platform  = "Unknown OS Platform";

        $os_array     = array(
                            '/windows nt 10/i'      =>  'Windows 10',
                            '/windows nt 6.3/i'     =>  'Windows 8.1',
                            '/windows nt 6.2/i'     =>  'Windows 8',
                            '/windows nt 6.1/i'     =>  'Windows 7',
                            '/windows nt 6.0/i'     =>  'Windows Vista',
                            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                            '/windows nt 5.1/i'     =>  'Windows XP',
                            '/windows xp/i'         =>  'Windows XP',
                            '/windows nt 5.0/i'     =>  'Windows 2000',
                            '/windows me/i'         =>  'Windows ME',
                            '/win98/i'              =>  'Windows 98',
                            '/win95/i'              =>  'Windows 95',
                            '/win16/i'              =>  'Windows 3.11',
                            '/macintosh|mac os x/i' =>  'Mac OS X',
                            '/mac_powerpc/i'        =>  'Mac OS 9',
                            '/linux/i'              =>  'Linux',
                            '/ubuntu/i'             =>  'Ubuntu',
                            '/iphone/i'             =>  'iPhone',
                            '/ipod/i'               =>  'iPod',
                            '/ipad/i'               =>  'iPad',
                            '/android/i'            =>  'Android',
                            '/blackberry/i'         =>  'BlackBerry',
                            '/webos/i'              =>  'Mobile'
                    );

        foreach ($os_array as $regex => $value) {
            if (preg_match($regex, $user_agent)) {
                $os_platform = $value;
            }
        }

        return $os_platform;
    }

    function getBrowser()
    {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        
        $browser        = "Unknown Browser";

        $browser_array = array(
                            '/msie/i'      => 'Internet Explorer',
                            '/firefox/i'   => 'Firefox',
                            '/safari/i'    => 'Safari',
                            '/chrome/i'    => 'Chrome',
                            '/edge/i'      => 'Edge',
                            '/opera/i'     => 'Opera',
                            '/netscape/i'  => 'Netscape',
                            '/maxthon/i'   => 'Maxthon',
                            '/konqueror/i' => 'Konqueror',
                            '/mobile/i'    => 'Handheld Browser'
                        );

        foreach ($browser_array as $regex => $value) {
            if (preg_match($regex, $user_agent)) {
                $browser = $value;
            }
        }

        return $browser;
    }

    function obtenerPathCredencial($nombreCentro){

        switch ($nombreCentro) {
            case 'ESCUELA DE SUBOFICIALES DEL EJÉRCITO "SARGENTO CABRAL"':
                return "assets/docs/credenciales/CREDENCIAL_ESESC.pdf";
                break;
            case 'LICEO MILITAR "GENERAL ROCA"':
                return "assets/docs/credenciales/CREDENCIAL_LMGR.pdf";
                break;
            case 'LICEO MILITAR "GENERAL ESPEJO"':
                return "assets/docs/credenciales/CREDENCIAL_LMGE.pdf";
                break;
            case 'LICEO MILITAR "GENERAL LAMADRID"':
                return "assets/docs/credenciales/CREDENCIAL_LMGL.pdf";
                break;
            case 'LICEO MILITAR "GENERAL BELGRANO"':
                return "assets/docs/credenciales/CREDENCIAL_LMGB.pdf";
                break;
            case 'LICEO MILITAR "GENERAL PAZ"':
                return "assets/docs/credenciales/CREDENCIAL_LMGP.pdf";
                break;
            case 'COMANDO DE LA Vta BRIGADA DE MONTAÑA':
                return "assets/docs/credenciales/CREDENCIAL_CVBM.pdf";
                break;
            case 'COMANDO DE LA XIIda BRIGADA DE MONTE':
                return "assets/docs/credenciales/CREDENCIAL_CXIIBM.pdf";
                break;
            case 'REGIMIENTO DE INFANTERÍA DE MONTE 29 "CNL IGNACIO JOSÉ WARNES"':
                return "assets/docs/credenciales/CREDENCIAL_RIM.pdf";
                break;
            default:
                break;
        }
    }

    function esExtension($extension, $archivo){
        $extension = strtolower($extension);
        $file_parts = pathinfo($archivo["name"]);
        $extensionArchivo = strtolower($file_parts["extension"]);
        if($extensionArchivo == $extension){
            return true;
        }
        return false;
    }

    function obtenerRutaDocumentoCandidato($nombre, $dni){
        $path = "./assets/docs/inscripciones/".$dni;
        $directorio = "." . $path;
        $ficheros1  = array_diff(scandir($directorio,1), array('..', '.'));
        foreach($ficheros1 as $fichero){
            $partes_ruta = pathinfo($path."/".$fichero);

            if($partes_ruta["filename"] == $nombre){
                return ".".$path."/".$fichero;
            }
        }
        return false;
    }

    function obtenerSpeechSaf($valor){
        switch ($valor) {
            case 1:
                return "Su comprobante/boleta  y ticket de pago, están aprobados..

Continúe el seguimiento de su estado de inscripción a través de nuestro sitio web, con su DNI y contraseña.

Ante alguna duda comuníquese con los teléfonos publicados en nuestras redes sociales o sitio web .

Muchas gracias";
                break;
            case 2:
                return "El importe abonado no es el que corresponde. El valor del derecho al examen de admisión es de $3000. Deberá volver a emitir la boleta de pago, por el valor parcial/diferencia y adjuntar un foto de ambos ticket de pago/Boleta de pago.
                
Tiene 48 hs a partir del momento que recibe su notificación vía correo electrónico y/o lee este mensaje, para volver a cargar su documentación.

Ante alguna duda comuníquese a los números publicados en nuestras redes sociales o sitio web.

Muchas gracias";
                break;
            case 3:
                return "No se cargo y/o no es legible el ticket de pago.
                
Tiene 48 hs a partir del momento que recibe su notificación vía correo electrónico y/o lee este mensaje, para volver a cargar su documentación.

Ante alguna duda comuníquese a los números publicados en nuestras redes sociales o sitio web.

Muchas gracias";
                break;
            case 4:
                return "No se cargó y/o no es legible el comprobante de pago/boleta de pago 
                
Tiene 48 hs a partir del momento que recibe su notificación vía correo electrónico y/o lee este mensaje, para volver a cargar su documentación.

Ante alguna duda comuníquese a los números publicados en nuestras redes sociales o sitio web.

Muchas gracias";
                break;
            default:
                break;
        }
    }

    function cargarNovedad(){
        
    }

    function eliminarNovedad(){

    }
    ?>
