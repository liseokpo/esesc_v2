<?php

class DBTurnosExamenes{
    private $DBConexion;

    function __construct($Conexion){
        $this -> DBConexion = $Conexion;
    }


    public function cargarTurno($nombre, $fdesde, $fhasta){        
        try {
            $stmt = $this -> DBConexion -> prepare("INSERT INTO turnos_examen(id, nombre, fecha_desde, fecha_hasta) 
                                                    VALUES (NULL,'$nombre','$fdesde','$fhasta')");
            # Otro Ejemplo de error ! DELECT en lugar de SELECT!
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar turno de examen: ".$e->getMessage();
        }

    }

    public function turnoExiste($nombre){
        $data = [
            'nombre' => $nombre,
        ];
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT nombre FROM turnos_examen WHERE turnos_examen.nombre = :nombre LIMIT 1");
            $stmt -> execute($data);
            $resultado = $stmt -> rowCount();
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar existencia de turno de examen: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar existencia de turno de examen: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function obtenerTurnoExamen($nombre){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM turnos_examen WHERE turnos_examen.nombre = '$nombre' LIMIT 1");            
            $stmt -> execute(); 
            $examen = $stmt -> fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error: obteniendo turno examen.";
        }
        return $examen;
    }

    public function obtenerTurnoExamenPorId($id){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM turnos_examen WHERE turnos_examen.id = '$id' LIMIT 1");            
            $stmt -> execute(); 
            $examen = $stmt -> fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error: obteniendo turno examen.";
        }
        return $examen;
    }

    public function modificarTurnoExamen($nombre, $fdesde, $fhasta, $id){
        try {
            $stmt = $this -> DBConexion -> prepare("UPDATE turnos_examen SET nombre = '$nombre', fecha_desde = '$fdesde', fecha_hasta = '$fhasta' WHERE turnos_examen.id = '$id'");  

            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar modificacion turnos de examen: ".$e->getMessage();
        }
    }

    public function obtenerListaTurnosExamenes(){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM turnos_examen WHERE TRUE");            
            $stmt -> execute(); 
            $lista = $stmt -> fetchAll();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo lista de turnos de examenes.";
        }
        return $lista;
    }

}


?>