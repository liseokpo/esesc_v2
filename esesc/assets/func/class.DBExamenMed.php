<?php

class DBExamenMed{
    private $DBConexion;

    function __construct($Conexion){
        $this -> DBConexion = $Conexion;
    }


    public function cargarExamen($estado, $ausente, $detalle, $id_incorporacion){        
        try {
            $stmt = $this -> DBConexion -> prepare("INSERT INTO examen_medico(id, detalle, estado, ausente, id_incorporacion) 
                                                    VALUES (NULL,'$detalle','$estado', '$ausente','$id_incorporacion')");
            # Otro Ejemplo de error ! DELECT en lugar de SELECT!
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar examen med: ".$e->getMessage();
        }

    }

    public function examenExiste($id_incorporacion){
        $data = [
            'incorporacion' => $id_incorporacion,
        ];
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT id_incorporacion FROM examen_medico WHERE examen_medico.id_incorporacion = :incorporacion LIMIT 1");
            $stmt -> execute($data);
            $resultado = $stmt -> rowCount();
        }
        catch(PDOExeption $e){
            echo "Se ha producido un error al verificar examen med: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar examen med: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function obtenerExamen($id_incorporacion){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM examen_medico WHERE id_incorporacion = '$id_incorporacion' LIMIT 1");            
            $stmt -> execute(); 
            $examen = $stmt -> fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error: obteniendo Examen med.";
        }
        return $examen;
    }

    public function modificarExamen($estado, $ausente, $detalle, $id_incorporacion){
        try {
            $fecha = $this -> fechaActual();
	    if($estado = "NULL" || $estado = "null"){
 	    $stmt = $this -> DBConexion -> prepare("UPDATE examen_medico SET estado = NULL, ausente = '$ausente', fecha_carga = '$fecha', detalle = '$detalle' WHERE examen_medico.id_incorporacion = '$id_incorporacion'");  
	    }
	    else{
	    $stmt = $this -> DBConexion -> prepare("UPDATE examen_medico SET estado = '$estado', ausente = '$ausente', fecha_carga = '$fecha', detalle = '$detalle' WHERE examen_medico.id_incorporacion = '$id_incorporacion'");  
	    }
            

            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar modificacion de examen medico: ".$e->getMessage();
        }
    }
    public function fechaActual(){
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fechaActual = date("Y-m-d H:i:s", strtotime('+0 hours'));
        return $fechaActual;
    }

    public function obtenerListaExamenes(){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM examen_medico WHERE TRUE");            
            $stmt -> execute(); 
            $lista = $stmt -> fetchAll();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo lista examenes medicos.";
        }
        return $lista;
    }

}


?>
