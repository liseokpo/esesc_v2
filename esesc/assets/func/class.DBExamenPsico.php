<?php

class DBExamenPsico{
    private $DBConexion;

    function __construct($Conexion){
        $this -> DBConexion = $Conexion;
    }


    public function cargarExamen($estado, $ausente, $id_incorporacion){        
        try {
            $stmt = $this -> DBConexion -> prepare("INSERT INTO examen_psicologico(id, estado, ausente, id_incorporacion) 
                                                    VALUES (NULL,'$estado','$ausente','$id_incorporacion')");
            # Otro Ejemplo de error ! DELECT en lugar de SELECT!
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar examen psicologico: ".$e->getMessage();
        }

    }

    public function examenExiste($id_incorporacion){
        $data = [
            'incorporacion' => $id_incorporacion,
        ];
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT id_incorporacion FROM examen_psicologico WHERE examen_psicologico.id_incorporacion = :incorporacion LIMIT 1");
            $stmt -> execute($data);
            $resultado = $stmt -> rowCount();
        }
        catch(PDOExeption $e){
            echo "Se ha producido un error al verificar examen psico: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar examen psico: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function obtenerExamen($id_incorporacion){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM examen_psicologico WHERE id_incorporacion = '$id_incorporacion' LIMIT 1");            
            $stmt -> execute(); 
            $examen = $stmt -> fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error: obteniendo Examen psico.";
        }
        return $examen;
    }

    public function modificarExamen($estado, $ausente, $id_incorporacion){
        try {
            $fecha = $this -> fechaActual();
            $stmt = $this -> DBConexion -> prepare("UPDATE examen_psicologico SET estado = '$estado', ausente = '$ausente' fecha_carga = '$fecha' WHERE examen_psicologico.id_incorporacion = '$id_incorporacion'");  

            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar modificacion de examen psicologico: ".$e->getMessage();
        }
    }
    public function fechaActual(){
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fechaActual = date("Y-m-d H:i:s", strtotime('+0 hours'));
        return $fechaActual;
    }

    public function obtenerListaExamenes(){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM examen_psicologico WHERE TRUE");            
            $stmt -> execute(); 
            $lista = $stmt -> fetchAll();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo lista examenes psicologicos.";
        }
        return $lista;
    }

}


?>