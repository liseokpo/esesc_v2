<?php

class DBCandidato{
    private $DBConexion;

    function __construct($Conexion){
        $this -> DBConexion = $Conexion;
    }
    
    //CON ESTA FUNCION CARGAMOS A LOS CANDIDATOS Y TODA LA INFORMACION CORRESPONDIENTE
    public function cargarCandidato($formulario){
        //CARGAMOS DATOS PERSONALES
        $this -> cargarDatosPersonales($formulario);
        
        $idCandidato = $this -> obtenerIdCandidato($formulario['dni']);
        
        //CARGAMOS DOMICILIO
        $this -> cargarDomicilio($idCandidato, $formulario);
        //CARGAMOS PLAN DE CARRERA
        $this -> cargarPlanCarrera($idCandidato, $formulario);
        $this -> cargarCentroPreseleccion($idCandidato, $formulario);
        $this -> cargarSecundario($idCandidato,$formulario);
        
        $this -> asignarInscripcion($idCandidato);
        //SI ES SOLD VOL, CARGAMOS DESTINO
        if($formulario['soldvol'] == '1')
        {
            $this -> cargarDestino($idCandidato, $formulario);
        }  
    }

    //CON ESTA FUNCION CARGO LOS DATOS PERSONALES DE CANDIDATOS(POSTUL/REINCORP)
    public function cargarDatosPersonales($formulario){
        try {
            $stmt = $this -> DBConexion -> prepare("INSERT INTO candidato(id, tipo_candidato, dni, nombres, apellidos, provincia, localidad, codigo_postal, telefono, celular, email, fecha_nacimiento, cuil, nacionalidad, sexo, estado_civil, grupo_sanguineo, clave_candidato, primerlogin) 
                                                    VALUES (NULL,'$formulario[tipoCandidato]','$formulario[dni]','$formulario[nombres]','$formulario[apellidos]','$formulario[provincia]','$formulario[localidad]','$formulario[codigopostal]','$formulario[telefono]','$formulario[celular]','$formulario[email]','$formulario[fnacimiento]','$formulario[cuil]','$formulario[nacionalidad]','$formulario[sexo]','$formulario[estadocivil]','$formulario[gruposanguineo]','',1)");
            # Otro Ejemplo de error ! DELECT en lugar de SELECT!
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar Datos Personales del candidato: ".$e->getMessage();
        }
    }

    public function editarDatosPersonales($idcandidato,$formulario){
        try {
            $stmt = $this -> DBConexion -> prepare("UPDATE candidato SET dni = '$formulario[dni]', nombres = '$formulario[nombres]', apellidos = '$formulario[apellidos]', provincia = '$formulario[provincia]', localidad = '$formulario[localidad]', codigo_postal = '$formulario[codigopostal]', telefono = '$formulario[telefono]', celular = '$formulario[celular]', email = '$formulario[email]', fecha_nacimiento = '$formulario[fnacimiento]', cuil = '$formulario[cuil]', nacionalidad = '$formulario[nacionalidad]', sexo = '$formulario[sexo]', estado_civil = '$formulario[estadocivil]', grupo_sanguineo = '$formulario[gruposanguineo]' WHERE candidato.id = '$idcandidato'");
            # Otro Ejemplo de error ! DELECT en lugar de SELECT!
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al editar Datos Personales del candidato: ".$e->getMessage();
        }
    }
    
    public function editarMail($idcandidato,$formulario){
        try {
            $stmt = $this -> DBConexion -> prepare("UPDATE candidato SET email = '$formulario[email]' WHERE candidato.id = '$idcandidato'");
            # Otro Ejemplo de error ! DELECT en lugar de SELECT!
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al editar Mail del candidato: ".$e->getMessage();
        }
    }
    
    //CON ESTA FUNCION CARGO EL DOMICILIO DE LOS CANDIDATOS(POSTUL/REINCORP)
    public function cargarDomicilio($id, $formulario){
        try {
            $stmt = $this -> DBConexion -> prepare("INSERT INTO domicilio(id, calle, numero, piso, depto, id_candidato) VALUES (NULL, '$formulario[domicilio]', '$formulario[nro]', '$formulario[piso]', '$formulario[depto]', '$id')");
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar el domicilio del candidato: ".$e->getMessage();
            echo"<br>";            
        }
    }

    public function editarDomicilio($idcandidato,$formulario){
        try {
            $stmt = $this -> DBConexion -> prepare("UPDATE domicilio SET calle = '$formulario[domicilio]', numero = '$formulario[nro]', piso = '$formulario[piso]', depto = '$formulario[depto]' WHERE domicilio.id_candidato = '$idcandidato'");
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al editar el domicilio del candidato: ".$e->getMessage();
            echo"<br>";            
        }
    }

    //CON ESTA FUNCION CARGO EL DESTINO DE LOS SOLD VOL.
    public function cargarDestino($id, $formulario){
        try {
            $stmt = $this -> DBConexion -> prepare("INSERT INTO soldado_voluntario(id, destino, id_candidato) VALUES (NULL, '$formulario[destinosoldvol]', '$id')");
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar el Destino del Soldado Voluntario: ".$e->getMessage();
            echo"<br>";            
        }
    }

    public function editarDestino($idcandidato,$formulario){
        try {
            $stmt = $this -> DBConexion -> prepare("UPDATE soldado_voluntario SET destino = '$formulario[destinosoldvol]' WHERE soldado_voluntario.id_candidato = '$idcandidato'");
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al editar el Destino del Soldado Voluntario: ".$e->getMessage();
            echo"<br>";            
        }
    }

    //CON ESTA FUNCION CARGO EL SECUNDARIO DE LOS POSTULANTES
    public function cargarSecundario($id,$formulario){
        try {
            $stmt = $this -> DBConexion -> prepare("INSERT INTO secundario(id, titulo, nombre, provincia, localidad, direccion, telefono, cant_materias_adeudadas, plan, anio_cursado, resolucion, id_candidato) 
            VALUES (NULL, '$formulario[tsecundario]', '$formulario[nsecundario]', '$formulario[psecundario]', '$formulario[lsecundario]', '$formulario[dircolegio]', '$formulario[telcolegio]', '$formulario[cantmateriassecundario]', '$formulario[plansecundario]', '$formulario[anio_cursado]', '$formulario[resolucion]', '$id')");
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar el Secundario del candidato: ".$e->getMessage();
            echo"<br>";            
        }
    }

    public function editarSecundario($idcandidato,$formulario){
        try {
            $stmt = $this -> DBConexion -> prepare("UPDATE secundario SET titulo = '$formulario[tsecundario]', nombre = '$formulario[nsecundario]', provincia = '$formulario[psecundario]', localidad = '$formulario[lsecundario]', direccion = '$formulario[dircolegio]', telefono = '$formulario[telcolegio]', cant_materias_adeudadas = '$formulario[cantmateriassecundario]', plan = '$formulario[plansecundario]', anio_cursado = '$formulario[anio_cursado]' WHERE secundario.id_candidato = '$idcandidato'");
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al editar el Secundario del candidato: ".$e->getMessage();
            echo"<br>";            
        }
    }

    //CON ESTA FUNCION CARGO EL PLAN DE CARRERA (POSTUL/REINCORP)
    public function cargarPlanCarrera($id, $formulario){
        try {
            $stmt = $this -> DBConexion -> prepare("INSERT INTO plan_carrera(id, tipo_plan, orientacion, id_candidato) VALUES (NULL, '$formulario[plancarrera]', '$formulario[orientacion]', '$id')");
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar el Plan de Carrera: ".$e->getMessage();
            echo"<br>";            
        }
    }

    public function editarPlanCarrera($idcandidato,$formulario){
        
        try {
            $stmt = $this -> DBConexion -> prepare("UPDATE plan_carrera SET tipo_plan = '$formulario[plancarrera]', orientacion = '$formulario[orientacion]' WHERE plan_carrera.id_candidato = '$idcandidato'");
            $stmt -> execute();
            $stmt = null;
        }
        
        catch(PDOException $e) {
            echo "Se ha producido un error al editar el Plan de Carrera: ".$e->getMessage();
            echo"<br>";            
        }
    }

    //CON ESTA FUNCION CARGO EL CENTRO DE PRESELECCION (POSTUL/REINCORP)
    public function cargarCentroPreseleccion($id, $formulario){
        try {
            $stmt = $this -> DBConexion -> prepare("INSERT INTO centro_preseleccion(id, nombre, id_candidato) VALUES (NULL, '$formulario[centropreseleccion]', '$id')");
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar el Centro de Preselección: ".$e->getMessage();
            echo"<br>";            
        }
    }

    public function editarCentroPreseleccion($idcandidato,$formulario){
        try {
            $stmt = $this -> DBConexion -> prepare("UPDATE centro_preseleccion SET nombre = '$formulario[centropreseleccion]' WHERE centro_preseleccion.id_candidato = '$idcandidato'");
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al editar el Centro de Preselección: ".$e->getMessage();
            echo"<br>";            
        }
    }

    public function cargarClaveCandidato($idcandidato,$clave){
        $data = [
            'clave' => $clave,
            'idcandidato' => $idcandidato,
        ];
        try {                                     

            $stmt = $this -> DBConexion -> prepare("UPDATE candidato SET clave_candidato = :clave WHERE candidato.id = :idcandidato") ;
                                                    
            $stmt -> execute($data);
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar Clave del candidato: ".$e->getMessage();
        }
    }

    public function verificarClaveCandidato($idcandidato,$clave){
        $data = [
            'clave' => $clave,
            'idcandidato' => $idcandidato,
        ];
        try {                                     

            $stmt = $this -> DBConexion -> prepare("SELECT TRUE FROM candidato WHERE candidato.id = :idcandidato AND candidato.clave_candidato = :clave") ;
            $stmt -> execute($data);
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar Clave del candidato: ".$e->getMessage();
        }
    }

    public function modificarInscripcion($id, $formulario){
        $data = [
            'detalle' => $formulario["detallenuevo"],
            'estado' => $formulario["estadonuevo"],
            'id_candidato' => $id,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("UPDATE inscripcion SET detalle = :detalle, estado = :estado WHERE inscripcion.id_candidato = :id_candidato");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al modificar la inscripcion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

    public function modificarInscripcionSaf($idcandidato,$formulario){
        $data = [
            'detalle_saf' => $formulario["detallenuevo"],
            'revision_saf' => $formulario["estadonuevo"],
            'id_candidato' => $idcandidato,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("UPDATE inscripcion SET detalle_saf = :detalle_saf, revision_saf = :revision_saf WHERE inscripcion.id_candidato = :id_candidato");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al modificar la inscripcion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }


    public function modificarInscripcionExt($idcandidato,$formulario){
        $data = [
            'detalle' => $formulario["detallenuevo"],
            'revision_ext' => $formulario["estadonuevo"],
            'id_candidato' => $idcandidato,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("UPDATE inscripcion SET detalle = :detalle, revision_ext = :revision_ext WHERE inscripcion.id_candidato = :id_candidato");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al modificar la inscripcion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

    public function modificarInscripcionExtConVal($idcandidato,$val){
        $data = [
            'estado' => $val,
            'id_candidato' => $idcandidato,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("UPDATE inscripcion SET revision_ext = :estado WHERE inscripcion.id_candidato = :id_candidato");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al modificar el estado de Inscripcion ext: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null; 
    }

    public function modificarInscripcionSafConVal($idcandidato,$val){
        $data = [
            'estado' => $val,
            'id_candidato' => $idcandidato,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("UPDATE inscripcion SET revision_saf = :estado WHERE inscripcion.id_candidato = :id_candidato");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al modificar el estado de Inscripcion saf: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null; 
    }
    
    public function modificarInscripcionConVal($idcandidato,$val){
        $data = [
            'estado' => $val,
            'id_candidato' => $idcandidato,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("UPDATE inscripcion SET estado = :estado WHERE inscripcion.id_candidato = :id_candidato");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al modificar la Inscripcion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null; 
    }

    public function asignarFechaAceptacionInscripcion($id)
    {
        $data = [
            'id' => $id,
            'fecha_aceptacion' => $this -> fechaActual(),
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("UPDATE inscripcion SET fecha_aceptacion = :fecha_aceptacion WHERE id_candidato = :id");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al agregar fecha aceptacion inscripcion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }
    public function desasignarFechaAceptacionInscripcion($id)
    {
        $data = [
            'id' => $id,
            'fecha_aceptacion' => NULL,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("UPDATE inscripcion SET fecha_aceptacion = :fecha_aceptacion WHERE id_candidato = :id");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al desasignar fecha aceptacion inscripcion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

    public function asignarNroInscripto($nro, $idcandidato){
        $data = [
            'id' => $idcandidato,
            'nro' => $nro,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("UPDATE inscripcion SET nro_inscripto = :nro WHERE id_candidato = :id");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al agregar fecha aceptacion inscripcion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

    public function asignarInscripcion($id){
        $data = [
            'id' => NULL,
            'detalle' => '',
            'estado' => 'EN OBSERVACION',
            'fecha_aceptacion' => NULL,
            'nro_inscripto' => '0',
            'id_candidato' => $id,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("INSERT INTO inscripcion(id, detalle, estado, fecha_aceptacion, nro_inscripto, id_candidato) VALUES (:id, :detalle, :estado, :fecha_aceptacion, :nro_inscripto, :id_candidato)");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al asignar la inscripcion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

    public function obtenerInscripcion($idCandidato){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM inscripcion WHERE id_candidato = '$idCandidato' LIMIT 1");            
            $stmt -> execute(); 
            $inscripcion = $stmt -> fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error: obteniendo inscripción.";
        }
        return $inscripcion;
    }

    public function inscripcionExiste($id_candidato){
        $data = [
            'id_candidato' => $id_candidato,
        ];
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT id_candidato FROM inscripcion WHERE inscripcion.id_candidato = :id_candidato LIMIT 1");
            $stmt -> execute($data);
            $resultado = $stmt -> rowCount();
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar inscripcion: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar inscripcion: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function eliminarInscripcion($id_candidato){
        $inscripcion = $this -> obtenerInscripcion($id_candidato);
        $id_inscripcion = $inscripcion['id'];
        $data = [
            'id_inscripcion' => $id_inscripcion,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("DELETE FROM inscripcion WHERE id = :id_inscripcion");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al eliminar inscripcion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

    public function primerLogin($primerlogin,$idcandidato){
        $data = [
            'primerlogin' => $primerlogin,
            'id_candidato' => $idcandidato,
        ];
        try{        
            $stmt = $this -> DBConexion -> prepare("UPDATE candidato SET primerlogin = :primerlogin WHERE candidato.id = :id_candidato");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al modificar campo primer login: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

    public function obtenerCandidatoporId($idcandidato){
        $data = [
            'idcandidato' => $idcandidato,
        ];
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM candidato WHERE candidato.id = :idcandidato LIMIT 1");            
            $stmt -> execute($data);
            $candidato = $stmt->fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo candidato por id.";
        } 
        return $candidato;   
    }

    public function obtenerCandidatoporEmail($correo){
        $data = [
            'correo' => $correo,
        ];
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM candidato WHERE candidato.email = :correo LIMIT 1");            
            $stmt -> execute($data);
            $candidato = $stmt->fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            return false;
        } 
        return $candidato;   
    }

    public function obtenerIdCandidato($dni){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT id FROM candidato WHERE dni ='$dni' LIMIT 1");            
            $stmt -> execute(); 
            $idCandidato = $stmt->fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo ID de candidato.";
        } 
        return $idCandidato['id'];
    }

    public function obtenerListaCandidatos(){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM candidato WHERE TRUE");            
            $stmt -> execute(); 
            $lista = $stmt -> fetchAll();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo lista candidatos.";
        }
        return $lista;
    }

    

    public function obtenerListaInscriptos(){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM inscripcion WHERE TRUE");            
            $stmt -> execute(); 
            $lista = $stmt -> fetchAll();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo lista inscriptos.";
        }
        return $lista;
    }

    public function obtenerDomicilio($idCandidato){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM domicilio WHERE id_candidato = '$idCandidato' LIMIT 1");            
            $stmt -> execute(); 
            $domicilio = $stmt -> fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo domicilio.";
        }
        return $domicilio;
    }

    public function obtenerPlanCarrera($idCandidato){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM plan_carrera WHERE id_candidato = '$idCandidato' LIMIT 1");            
            $stmt -> execute(); 
            $plancarrera = $stmt -> fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo Plan Carrera.";
        }
        return $plancarrera;
    }

    public function obtenerSoldadoVoluntario($idCandidato){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM soldado_voluntario WHERE id_candidato = '$idCandidato' LIMIT 1");            
            $stmt -> execute(); 
            $soldvol = $stmt -> fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            return false;
        }
        return $soldvol;
    }

    public function eliminarSoldadoVoluntario($id_candidato){
        $soldvol = $this -> obtenerSoldadoVoluntario($id_candidato);
        $id_soldvol = $soldvol['id'];
        $data = [
            'id_soldvol' => $id_soldvol,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("DELETE FROM soldado_voluntario WHERE id = :id_soldvol");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al eliminar el sold vol: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

    public function obtenerSecundario($idCandidato){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM secundario WHERE id_candidato = '$idCandidato' LIMIT 1");            
            $stmt -> execute(); 
            $secundario = $stmt -> fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo secundario.";
        }
        return $secundario;
    }
    
    public function obtenerCentroPreseleccion($idCandidato){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM centro_preseleccion WHERE id_candidato = '$idCandidato' LIMIT 1");            
            $stmt -> execute(); 
            $centro = $stmt -> fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error: obteniendo Preinscripción.";
        }
        return $centro;
    }

    public function recuperarContrasenia($codigo = null){
        if(isset($codigo)){
            try{
                # Creamos la consulta
                $stmt = $this -> DBConexion -> prepare("SELECT * FROM recuperar_pass WHERE codigo ='".$codigo."'");                            
                $stmt -> execute(); 
                
                $recuperar = $stmt -> fetch();
                # Liberamos los recursos utilizados por $stmt
                $stmt = null;
                return $recuperar;
            }
            catch(PDOException $err)
            {
                echo"<br>".$err."<br>";
                return false;
            }
        }
        return false;
    }

    public function recoverPassword($id, $codigoRecuperacion, $fechaRecuperacion){
        $data = [
            'id' => NULL,
            'codigo' => $codigoRecuperacion,
            'fecha_expiracion' => $fechaRecuperacion,
            'usado' => 0,
            'id_candidato' => $id,
        ];
        try{                                        
            $stmt = $this -> DBConexion -> prepare("INSERT INTO recuperar_pass(id, codigo, fecha_expiracion, usado, id_candidato) VALUES (:id, :codigo, :fecha_expiracion, :usado, :id_candidato)");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            return false;
        }
        catch(Exception $e){
            return false;
        }
        $stmt = null;
        return true;
    }

    public function obtenerIdUsuariosCodigo($crec){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT id_candidato, id_usuario FROM recuperar_pass WHERE codigo = '".$crec."'");   
            $stmt -> execute(); 
            $resultado = $stmt -> fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo id de codigos de usuarios.".$err;
        } 
        return $resultado;
    }

    public function codigoUsado($usado,$idcandidato){
        $data = [
            'usado' => $usado,
            'id_candidato' => $idcandidato,
        ];
        try{        
            $stmt = $this -> DBConexion -> prepare("UPDATE recuperar_pass SET usado = :usado WHERE recuperar_pass.id_candidato = :id_candidato");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al modificar uso de codigo re recuperacion : ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

    public function dniExiste($dni){
        $data = [
            'dni' => $dni,
        ];
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT dni FROM candidato WHERE candidato.dni = :dni LIMIT 1");
            $stmt -> execute($data);
            $resultado = $stmt -> rowCount();
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar dni: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar dni: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function emailExiste($email){
        $data = [
            'email' => $email,
        ];
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT email FROM candidato WHERE candidato.email = :email LIMIT 1");
            $stmt -> execute($data);
            $resultado = $stmt -> rowCount();

        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar email: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar email: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function planCarreraExiste($idcandidato){
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT id FROM plan_carrera WHERE plan_carrera.id_candidato = '$idcandidato' LIMIT 1");
            $stmt -> execute();
            $resultado = $stmt -> rowCount();
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar plan carrera: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar plan carrera: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function secundarioExiste($idcandidato){
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT id FROM secundario WHERE secundario.id_candidato = '$idcandidato' LIMIT 1");
            $stmt -> execute();
            $resultado = $stmt -> rowCount();
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar secundario: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar secundario: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function centroPreseleccionExiste($idcandidato){
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT id FROM centro_preseleccion WHERE centro_preseleccion.id_candidato = '$idcandidato' LIMIT 1");
            $stmt -> execute();
            $resultado = $stmt -> rowCount();
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar centro preseleccion: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar centro preseleccion: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function soldadoVoluntarioExiste($idcandidato){
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT id FROM soldado_voluntario WHERE soldado_voluntario.id_candidato = '$idcandidato' LIMIT 1");
            $stmt -> execute();
            $resultado = $stmt -> rowCount();
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar soldado voluntario: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar soldado voluntario: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function domicilioExiste($idcandidato){
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT id FROM domicilio WHERE domicilio.id_candidato = '$idcandidato' LIMIT 1");
            $stmt -> execute();
            $resultado = $stmt -> rowCount();
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar domicilio: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar domicilio: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function fechaActual(){
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fechaActual = date("Y-m-d H:i:s", strtotime('+0 hours'));
        return $fechaActual;
    }

    public function getSexo($id_candidato){
        
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT sexo FROM candidato WHERE id ='$id_candidato' LIMIT 1");            
            $stmt -> execute(); 
            $resultado = $stmt->fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo SEXO de candidato.";
        } 
        return $resultado['sexo'];
    }



    public function cargarFolderDocumentos($path, $id_inscripcion){
        $data = [
            'id' => NULL,
            'path' => $path,
            'id_inscripcion' => $id_inscripcion,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("INSERT INTO folder_documentos(id, path, id_inscripcion) VALUES (:id, :path, :id_inscripcion)");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al asignar path de documentos: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

    public function folderDocumentosExiste($id_inscripcion){
        $data = [
            'id_inscripcion' => $id_inscripcion,
        ];
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT id_inscripcion FROM folder_documentos WHERE folder_documentos.id_inscripcion = :id_inscripcion LIMIT 1");
            $stmt -> execute($data);
            $resultado = $stmt -> rowCount();
        }
        catch(PDOException $e){
            echo "Se ha producido un error al verificar folder docs: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar folder docs: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function obtenerFolderDocumentos($id_inscripcion){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM folder_documentos WHERE id_inscripcion = '$id_inscripcion' LIMIT 1");            
            $stmt -> execute(); 
            $folderInscripcion = $stmt -> fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error: obteniendo inscripción.";
        }
        return $folderInscripcion;
    }

    public function vaciarDetalleInscripcion($idcandidato){
        $data = [
            'detalle' => '',
            'id_candidato' => $idcandidato,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("UPDATE inscripcion SET detalle = :detalle WHERE inscripcion.id_candidato = :id_candidato");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al vaciar el detalle de la Inscripcion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null; 
    }

    public function vaciarDetalleSafInscripcion($idcandidato){
        $data = [
            'detalle_saf' => '',
            'id_candidato' => $idcandidato,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("UPDATE inscripcion SET detalle_saf = :detalle_saf WHERE inscripcion.id_candidato = :id_candidato");
            $stmt -> execute($data);
        }
        catch(PDOException $e){
            echo "Se ha producido un error al vaciar el detalle_saf de la Inscripcion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null; 
    }


}

?>
