<?php

class DBMantenimiento{

    private $DBConexion;

    function __construct($Conexion){
        $this -> DBConexion = $Conexion;
    }

    public function enMantenimiento(){
        try {
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM `mantenimiento` LIMIT 1");
            $stmt -> execute();
            $mantenimiento = $stmt -> fetch();
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al intentar cargar padres: ".$e->getMessage();
            echo"<br>";
        }

        if($mantenimiento == 1)
            return TRUE;
        return FALSE;
    }
}

?>