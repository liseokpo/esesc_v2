<?php

class DBExamenInt{
    private $DBConexion;

    function __construct($Conexion){
        $this -> DBConexion = $Conexion;
    }

    public function cargarExamen($lengua, $mate, $detalle, $tecnico, $ausente, $id_incorporacion){
        $promedio = $this -> calcularPromedio($lengua, $mate);
        $estado = $this -> calcularEstado($lengua, $mate);
        
        try {
            $stmt = $this -> DBConexion -> prepare("INSERT INTO examen_intelectual(id, lengua_puntaje, mate_puntaje, promedio, detalle, secundario_tecnico, ausente, estado, id_incorporacion) 
                                                    VALUES (NULL,'$lengua','$mate','$promedio','$detalle','$tecnico','$ausente','$estado','$id_incorporacion')");
            # Otro Ejemplo de error ! DELECT en lugar de SELECT!
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar examen int: ".$e->getMessage();
        }

    }

    public function calcularPromedio($lengua, $mate){
        return (($lengua + $mate) / 2) / 10;
    }

    public function calcularEstado($lengua, $mate){
        if($lengua < 40 || $mate < 40){
            return "DESAPROBADO";
        }
        return "APROBADO";
    }

    public function examenExiste($id_incorporacion){
        $data = [
            'incorporacion' => $id_incorporacion,
        ];
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT id_incorporacion FROM examen_intelectual WHERE examen_intelectual.id_incorporacion = :incorporacion LIMIT 1");
            $stmt -> execute($data);
            $resultado = $stmt -> rowCount();
        }
        catch(PDOExeption $e){
            echo "Se ha producido un error al verificar examen int: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar examen int: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function obtenerExamen($id_incorporacion){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM examen_intelectual WHERE id_incorporacion = '$id_incorporacion' LIMIT 1");            
            $stmt -> execute(); 
            $examen = $stmt -> fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error: obteniendo Examen int.";
        }
        return $examen;
    }

    public function modificarExamen($lengua, $mate, $detalle, $tecnico, $ausente, $id_incorporacion){
        $promedio = $this -> calcularPromedio($lengua, $mate);
        $estado = $this -> calcularEstado($lengua, $mate);

        try {
            $fecha = $this -> fechaActual();
            $stmt = $this -> DBConexion -> prepare("UPDATE examen_intelectual SET lengua_puntaje = '$lengua', mate_puntaje = '$mate' , promedio = '$promedio', estado = '$estado', fecha_carga = '$fecha', detalle = '$detalle', secundario_tecnico = '$tecnico', ausente = '$ausente'  WHERE examen_intelectual.id_incorporacion = '$id_incorporacion'");  

            # Otro Ejemplo de error ! DELECT en lugar de SELECT!
            $stmt -> execute();
            $stmt = null;
        }
        catch(PDOException $e) {
            echo "Se ha producido un error al cargar modificacion de examen intelectual: ".$e->getMessage();
        }
    }
    public function fechaActual(){
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fechaActual = date("Y-m-d H:i:s", strtotime('+0 hours'));
        return $fechaActual;
    }

    public function obtenerListaExamenes(){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM examen_intelectual WHERE TRUE");            
            $stmt -> execute(); 
            $lista = $stmt -> fetchAll();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo lista examenes intelectuales.";
        }
        return $lista;
    }
}
?>