<?php
class DBIncorporacion{
    private $DBConexion;

    function __construct($Conexion){
        $this -> DBConexion = $Conexion;
    }

    public function asignarIncorporacion($id_candidato){
        $data = [
            'id' => NULL,
            'detalle' => '',
            'fecha_modificacion' => NULL,
            'merito' => NULL,
            'estado' => 'EN OBSERVACION',
            'id_turno_examen' => NULL,
            'id_candidato' => $id_candidato,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("INSERT INTO incorporacion(id, detalle, fecha_modificacion, merito, estado, id_turno_examen, id_candidato) VALUES (:id, :detalle, :fecha_modificacion, :merito, :estado, :id_turno_examen, :id_candidato)");
            $stmt -> execute($data);
        }
        catch(PDOExeption $e){
            echo "Se ha producido un error al asignar la incorporacion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

    public function incorporacionExiste($id_candidato){
        $data = [
            'id_candidato' => $id_candidato,
        ];
        try{                                        
            $stmt = $this -> DBConexion -> prepare("SELECT id_candidato FROM incorporacion WHERE incorporacion.id_candidato = :id_candidato LIMIT 1");
            $stmt -> execute($data);
            $resultado = $stmt -> rowCount();
        }
        catch(PDOExeption $e){
            echo "Se ha producido un error al verificar incorporacion: ".$e->getMessage();
            echo"<br>";   
        }
        catch(Exception $e){
            echo "Se ha producido un error al verificar incorporacion: ".$e->getMessage();
            echo"<br>";   
        }
        if($resultado == 0){
            return false;
        }
        return true;
    }

    public function eliminarIncorporacion($id_candidato){
        $incorporacion = $this -> obtenerIncorporacion($id_candidato);
        $id_incorporacion = $incorporacion['id'];
        $data = [
            'id_incorporacion' => $id_incorporacion,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("DELETE FROM incorporacion WHERE id = :id_incorporacion");
            $stmt -> execute($data);
        }
        catch(PDOExeption $e){
            echo "Se ha producido un error al eliminar incorporacion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

    public function modificarIncorporacion($id, $formulario){
        $data = [
            'detalle' => $formulario["detallenuevo"],
            'estado' => $formulario["estadonuevo"],
            'turno' => $formulario["turnoexamennuevo"],
            'id_candidato' => $id,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("UPDATE incorporacion SET detalle = :detalle, estado = :estado, id_turno_examen = :turno WHERE incorporacion.id_candidato = :id_candidato");
            $stmt -> execute($data);
        }
        catch(PDOExeption $e){
            echo "Se ha producido un error al modificar la incorporacion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

    public function asignarFechaModificacionIncorporacion($id_candidato)
    {
        $data = [
            'id' => $id_candidato,
            'fecha_modificacion' => $this -> fechaActual(),
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("UPDATE incorporacion SET fecha_modificacion = :fecha_modificacion WHERE id_candidato = :id");
            $stmt -> execute($data);
        }
        catch(PDOExeption $e){
            echo "Se ha producido un error al agregar fecha modificacion incorporacion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }
    
    public function desasignarFechaModificacionIncorporacion($id_candidato)
    {
        $data = [
            'id' => $id_candidato,
            'fecha_modificacion' => NULL,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("UPDATE incorporacion SET fecha_modificacion = :fecha_modificacion WHERE id_candidato = :id");
            $stmt -> execute($data);
        }
        catch(PDOExeption $e){
            echo "Se ha producido un error al desasignar fecha modificacion incorporacion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }

    public function obtenerIncorporacion($id_candidato){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM incorporacion WHERE id_candidato = '$id_candidato' LIMIT 1");            
            $stmt -> execute(); 
            $incorporacion = $stmt -> fetch();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error: obteniendo incorporacion.";
        }
        return $incorporacion;
    }

    public function obtenerListaIncorporados(){
        try{
            # Creamos la consulta
            $stmt = $this -> DBConexion -> prepare("SELECT * FROM incorporacion WHERE TRUE");            
            $stmt -> execute(); 
            $lista = $stmt -> fetchAll();
            # Liberamos los recursos utilizados por $stmt
            $stmt = null;
        }
        catch(PDOException $err)
        {
            // Mostramos un mensaje genérico de error.
            echo "Error obteniendo lista de incorporados.";
        }
        return $lista;  
    }

    public function fechaActual(){
        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fechaActual = date("Y-m-d H:i:s", strtotime('+0 hours'));
        return $fechaActual;
    }

    public function cargarMerito($id_candidato, $notaIntelectual, $notaFisico){
        $merito = ($notaIntelectual + ($notaFisico*2)) / 3;
        $data = [
            'id' => $id_candidato,
            'merito' => $merito,
        ];
        try{
            $stmt = $this -> DBConexion -> prepare("UPDATE incorporacion SET merito = :merito WHERE id_candidato = :id");
            $stmt -> execute($data);
        }
        catch(PDOExeption $e){
            echo "Se ha producido un error al agregar fecha aceptacion incorporacion: ".$e->getMessage();
            echo"<br>";   
        }
        $stmt = null;
    }
}
?>