// LISTAMOS PROVINCIAS Y LOCALIDADES(SEGUN LA PROVINCIA)
$(document).ready(function () {

  $.ajax({
      url:'assets/provincias.json',
      dataType: 'json',
      type: 'get',
      cache: true,
      success: function(data){
          var listdata = $(data.provincias);
          listdata.sort(function(a, b){
              if(a.nombre < b.nombre) { return -1; }
              if(a.nombre > b.nombre) { return 1; }
              return 0;
          })
          for(var i = 0; i < listdata.length; i++){
              $('<option value="'+listdata[i].nombre.toUpperCase()+'">' + listdata[i].nombre.toUpperCase() + '</option>').appendTo('#selectprov');
          }
      }
  });
    $.ajax({
        url:'assets/localidades.json',
        dataType: 'json',
        type: 'get',
        cache: true,
        success: function(data){
            listdata2 = $(data.localidades);
            listdata2.sort(function(a, b){
                if(a.nombre < b.nombre) { return -1; }
                if(a.nombre > b.nombre) { return 1; }
                return 0;
            })
        }
    });

    $("#selectprov").change(function () {

        $('#selectloc').empty().append('<option value="" selected disabled>Elija localidad</option>');
            
        var val = $(this).val();
        for(var i = 0; i < listdata2.length; i++){
            if (val == listdata2[i].provincia.nombre.toUpperCase()) {
                $('<option value="'+listdata2[i].nombre.toUpperCase()+'">' + listdata2[i].nombre.toUpperCase() + '</option>').appendTo('#selectloc');
            }
        }
    });
});

// LISTAR FUERZAS ARMADAS Y GRADOS
$(document).ready(function () {

  $.ajax({
      url:'assets/grados.json',
      dataType: 'json',
      type: 'get',
      cache: true,
      success: function(data){
          var listdata = $(data.grados);
          listdata.sort(function(a, b){
              if(a.nombre < b.nombre) { return -1; }
              if(a.nombre > b.nombre) { return 1; }
              return 0;
          })
          var fuerzaActual = "";
          for(var i = 0; i < listdata.length; i++){
              if(fuerzaActual != listdata[i].fuerza){
                $('<option value="'+listdata[i].fuerza.toUpperCase()+'">' + listdata[i].fuerza.toUpperCase() + '</option>').appendTo('#selectffaa');
                fuerzaActual = listdata[i].fuerza;          
              }
          }

          $("#selectffaa").change(function () {

            $('#selectgradoffaa')
                .empty()
                .append('<option selected disabled>Seleccione un grado</option>')
            ;
            var val = $(this).val();
            for(var i = 0; i < listdata.length; i++){
                if (val == listdata[i].fuerza.toUpperCase()) {
                    $('<option value="'+listdata[i].grado.toUpperCase()+'">' + listdata[i].grado_completo.toUpperCase() + '</option>').appendTo('#selectgradoffaa');
                }
            }
        });
      }
  });  
});


// LISTAR CENTROS DE PRESELECCION
$(document).ready(function () {
    $.ajax({
        url:'assets/centros_preseleccion.json',
        dataType: 'json',
        type: 'get',
        cache: true,
        success: function(data){
            var listdata = $(data.centros_preseleccion);
            
            for(var i = 0; i < listdata.length; i++){
                  $('<option value="'+listdata[i].id+'">' + listdata[i].nombre.toUpperCase() + ' - ' + listdata[i].direccion.toUpperCase() + ' - ' + listdata[i].localidad.toUpperCase() + ' - ' + listdata[i].provincia.toUpperCase() + '</option>').appendTo('#selectcentrospresel');
            }
        }
    });  
});
    
// LISTAR CODIGO UNIDADES SSVV

$(document).ready(function () {
    $.ajax({

        url:'assets/codigo_unidades_ssvv.json',
        dataType: 'json',
        type: 'get',
        cache: true,
        success: function(data){
            var listdata = $(data.codigos_ssvv);
            listdata.sort(function(a, b){
                if(a.codigo < b.codigo) { return -1; }
                if(a.codigo > b.codigo) { return 1; }
                return 0;
            });
            for(var i = 0; i < listdata.length; i++){
                $('<option value="'+listdata[i].codigo.toUpperCase()+'">' + listdata[i].codigo.toUpperCase() + ' - ' + listdata[i].desc_abreviada.toUpperCase() + '</option>').appendTo('#destinosoldvol');
            }
        }
    });
});







// LISTAR FUERZAS ARMADAS Y GRADOS
$(document).ready(function () {

    

    $("#selectffaa").change(function () {

        $('#selectgradoffaa')
            .empty()
            .append('<option selected disabled>Seleccione un grado</option>')
        ;
        var val = $(this).val();
        for(var i = 0; i < listdata.length; i++){
            if (val == listdata[i].fuerza.toUpperCase()) {
                $('<option value="'+listdata[i].grado.toUpperCase()+'">' + listdata[i].grado_completo.toUpperCase() + '</option>').appendTo('#selectgradoffaa');
            }
        }
    });
});








// <!-- FORMULARIO CON PASOS -->
$(document).ready(function(){

  //funcion para cambiar el estado de la barra de progreso
  function setProgressBar(curStep){
          var percent = parseFloat(100 / steps) * curStep;
          percent = percent.toFixed();
          $(".progress-bar")
          .css("width",percent+"%")
          .html(percent+"%");   
  }


  
  var current = 1,current_step,next_step,steps;
  steps = $("fieldset").length;

  //AL APRETAR SIGUIENTE ESTA FUNCION REVISA LOS INPUTS DE #PASO1
  $(".next").on("click", function(){
      var elementois = $(this).parent().find('select , input');
      cant_elementos = elementois.length;
      cont = 0;
      
      for (var i = 0; i < cant_elementos; i++) {
          //CUENTO LOS INPUT CORRECTOS
          if(elementois[i].checkValidity()) {
              cont++;                
          }
          //HAY UN INPUT INCORRECTO, REPORTO EL ERROR
          else {
              cont=0;
              elementois[i].reportValidity();
          }
      }

      // SI TODOS LOS INPUT ESTAN BIEN ENTONCES PROCEDEMOS
      if(cont == cant_elementos)
      {
          current_step = $(this).parent();
          next_step = $(this).parent().next();
          next_step.show();
          current_step.hide();
          setProgressBar(++current);
          //uso esto para ir a la parte superior de la pagina
          //cuando el usuario pone siguiente
          $(window).scrollTop(400);    
      }
      
  });

  //aca no me importa si esta validado o no.
  $(".previous").click(function(){
      current_step = $(this).parent();
      next_step = $(this).parent().prev();
      next_step.show();
      current_step.hide();
      setProgressBar(--current);
      //uso esto para ir a la parte superior de la pagina
      //cuando el usuario pone previous
      $(window).scrollTop(400);
  });
  setProgressBar(current);
});






function MM_findObj(n, d) { //v4.01
    var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
      d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
    if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
    for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
    if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_swapImgRestore() { //v3.0
var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_swapImage() { //v3.0
var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
    if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


//VIDEO POPUP
window.document.onkeydown = function(e) {
  if (!e) {
    e = event;
  }
  if (e.keyCode == 27) {
    lightbox_close();
  }
}
function lightbox_open() {
  var lightBoxVideo = document.getElementById("VisaChipCardVideo");
  window.scrollTo(0, 0);
  document.getElementById('light').style.display = 'block';
  document.getElementById('fadevid').style.display = 'block';
  lightBoxVideo.play();
}
function lightbox_close() {
  var lightBoxVideo = document.getElementById("VisaChipCardVideo");
  document.getElementById('light').style.display = 'none';
  document.getElementById('fadevid').style.display = 'none';
  lightBoxVideo.pause();
}

function cambiarVisibilidad(id, visibilidad) {
  document.getElementById(id).style.display = visibilidad;
}


//funcion para agregar hermano a formulario
var contadorHermanos = 1;
function agregarHermano(divHermanos) {
    var d1 = document.getElementById(divHermanos);
    d1.insertAdjacentHTML('beforebegin', `
        <div id="hermano`+contadorHermanos+`">
            <br/><h2>HERMANO N° `+contadorHermanos+`</h2><br/>
            <div class="form-row">
                <div class="col">
                    <label for="nombreshermano`+contadorHermanos+`">Nombres</label>
                    <input type="text" class="form-control" name="nombreshermano`+contadorHermanos+`" id="nombreshermano`+contadorHermanos+`" placeholder="Ej Carlos Antonio"><br>
                </div>
                <div class="col">
                    <label for="apellidohermano`+contadorHermanos+`">Apellido</label>
                    <input type="text" class="form-control" name="apellidohermano`+contadorHermanos+`" id="apellidohermano`+contadorHermanos+`" placeholder="Ej Gutiérrez">            
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <label for="edadhermano`+contadorHermanos+`">Edad</label>
                    <input type="number" class="form-control" name="edadhermano`+contadorHermanos+`" id="edadhermano`+contadorHermanos+`" placeholder="Ej "><br>
                </div>
                <div class="col">
                    <label for="estadocivilhermano`+contadorHermanos+`">Estado Civil</label>
                    <select class="custom-select" name="estadocivilhermano`+contadorHermanos+`" id="estadocivilhermano`+contadorHermanos+`">
                        <option value="" selected disabled>Indique estado civil</option>
                        <option value="CASADO">CASADO</option>
                        <option value="DIVORCIADO">DIVORCIADO</option>
                        <option value="SOLTERO">SOLTERO</option>
                        <option value="VIUDO">VIUDO</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <label for="sexohermano`+contadorHermanos+`">Sexo</label>
                    <select class="custom-select" name="sexohermano`+contadorHermanos+`" id="sexohermano`+contadorHermanos+`">    
                        <option value="" selected disabled>Indique sexo</option>
                        <option value="F">FEMENINO</option>
                        <option value="M">MASCULINO</option>
                    </select><br>
                </div>
                <div class="col">
                    <label for="vivehermano`+contadorHermanos+`">¿Vive?</label>
                    <select class="custom-select" name="vivehermano`+contadorHermanos+`" id="vivehermano`+contadorHermanos+`">
                        <option value="" selected disabled>Indique</option>
                        <option value="1">SI</option>
                        <option value="0">NO</option>
                    </select><br>
                </div>
            </div>
            <div class="form-row">
                <div class="col"><br>
                    <label for="ocupacionhermano`+contadorHermanos+`">Ocupación</label>
                    <input type="text" class="form-control" name="ocupacionhermano`+contadorHermanos+`" id="ocupacionhermano`+contadorHermanos+`" placeholder="Ej "><br>
                </div>
                <div class="col"><br>
                    <label for="estudioshermano`+contadorHermanos+`">Estudios</label>
                    <input type="text" class="form-control" name="estudioshermano`+contadorHermanos+`" id="estudioshermano`+contadorHermanos+`" placeholder="Ej ">            
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <label for="empleadodehermano`+contadorHermanos+`">Empleado de</label>
                    <input type="text" class="form-control" name="empleadodehermano`+contadorHermanos+`" id="empleadodehermano`+contadorHermanos+`" placeholder="Ej "><br>
                </div>
                <div class="col">
                    <label for="aniocursadohermano`+contadorHermanos+`">Año cursado</label>
                    <input type="text" class="form-control" name="aniocursadohermano`+contadorHermanos+`" id="aniocursadohermano`+contadorHermanos+`" placeholder="Ej ">            
                </div>
            </div>
            <label for="sosthogarhermano`+contadorHermanos+`">¿Es principal sostén del hogar?</label>
            <select class="custom-select" name="sosthogarhermano`+contadorHermanos+`" id="sosthogarhermano`+contadorHermanos+`">
                <option value="" selected disabled>Indique</option>
                <option value="1">SI</option>
                <option value="0">NO</option>
            </select><br><br>
        </div>`);
    contadorHermanos ++;
}



//funcion para agregar hermano a formulario
var contadorHijos = 1;
function agregarHijo(divHijos) {
    var d1 = document.getElementById(divHijos);
    d1.insertAdjacentHTML('beforebegin', `
        <div id="hijo`+contadorHijos+`">
            <br/><h2>HIJO N° `+contadorHijos+`</h2><br/>
            <div class="form-row">
       
                <div class="col">
                    <label for="nombreshijo`+contadorHijos+`">Nombres</label>
                    <input type="text" class="form-control" name="nombreshijo`+contadorHijos+`" id="nombreshijo`+contadorHijos+`" placeholder="Ej Carlos Antonio"><br>
                </div>
                <div class="col">
                    <label for="apellidohijo`+contadorHijos+`">Apellido</label>
                    <input type="text" class="form-control" name="apellidohijo`+contadorHijos+`" id="apellidohijo`+contadorHijos+`" placeholder="Ej Gutiérrez">            
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <label for="fnachijo`+contadorHijos+`">Fecha de nacimiento</label>
                    <input type="date" class="form-control" name="fnachijo`+contadorHijos+`" id="fnachijo`+contadorHijos+`" placeholder="Ej "><br>
                </div>
                <div class="col">
                <label for="nacionalidadhijo`+contadorHijos+`">Nacionalidad</label>
                <input type="text" class="form-control" name="nacionalidadhijo`+contadorHijos+`" id="nacionalidadhijo`+contadorHijos+`" placeholder="Ej "><br>
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <label for="sexohijo`+contadorHijos+`">Sexo</label>
                    <select class="custom-select" name="sexohijo`+contadorHijos+`" id="sexohijo`+contadorHijos+`">    
                        <option value="" selected disabled>Indique sexo</option>
                        <option value="F">FEMENINO</option>
                        <option value="M">MASCULINO</option>
                    </select><br>
                </div>
                <div class="col">
                    <label for="vivehijo`+contadorHijos+`">¿Vive?</label>
                    <select class="custom-select" name="vivehijo`+contadorHijos+`" id="vivehijo`+contadorHijos+`">
                        <option value="" selected disabled>Indique</option>
                        <option value="1">SI</option>
                        <option value="0">NO</option>
                    </select><br>
                </div>
            </div>
            <div class="form-row">
                <div class="col"><br>
                    <label for="domiciliohijo`+contadorHijos+`">Domicilio</label>
                    <input type="text" class="form-control" name="domiciliohijo`+contadorHijos+`" id="domiciliohijo`+contadorHijos+`" placeholder="Ej "><br>
                </div>
                <div class="col"><br>
                    <label for="localidadhijo`+contadorHijos+`">Localidad</label>
                    <input type="text" class="form-control" name="localidadhijo`+contadorHijos+`" id="localidadhijo`+contadorHijos+`" placeholder="Ej "><br>
                </div>
            </div>
            <div class="form-row">
                <div class="col">
                    <label for="provinciahijo`+contadorHijos+`">Provincia</label>
                    <input type="text" class="form-control" name="provinciahijo`+contadorHijos+`" id="provinciahijo`+contadorHijos+`" placeholder="Ej "><br>
                </div>
                <div class="col">
                    <label for="cphijo`+contadorHijos+`">Código Postal</label>
                    <input type="text" class="form-control" name="cphijo`+contadorHijos+`" id="cphijo`+contadorHijos+`" placeholder="Ej ">            
                </div>
            </div>
            <div class="form-group">
                <label for="estudioshijo`+contadorHijos+`">Estudios: </label>
                <input type="text" class="form-control" name="estudioshijo`+contadorHijos+`" id="estudioshijo`+contadorHijos+`" placeholder="">
            </div>
        </div>`);
        contadorHijos ++;
}


//funcion para quitar hermano del formulario
function quitarHermano(){
    ultimoHermano = contadorHermanos - 1;
    if(quitarElemento("hermano"+ultimoHermano)){
        contadorHermanos--;
    }
}

//funcion para quitar hermano del formulario
function quitarHijo(){
    ultimoHijo = contadorHijos - 1;
    if(quitarElemento("hijo"+ultimoHijo)){
        contadorHijos--;
    }
}

//funcion para quitar elemento con id
function quitarElemento(idElemento){
    if(document.getElementById(idElemento)){
        document.getElementById(idElemento).remove();
        return true;
    }
    return false;
}





$(document).ready(function () {
    $( () => {
	
        //On Scroll Functionality
        $(window).scroll( () => {
            var windowTop = $(window).scrollTop();
            windowTop > 100 ? $('nav').addClass('navShadow') : $('nav').removeClass('navShadow');
            windowTop > 100 ? $('ul').css('top','100px') : $('ul').css('top','160px');
        });
        
        //Click Logo To Scroll To Top
        $('#logo').on('click', () => {
            $('html,body').animate({
                scrollTop: 0
            },500);
        });
        
        //Smooth Scrolling Using Navigation Menu
        $('a[href*="#"]').on('click', function(e){
            $('html,body').animate({
                scrollTop: $($(this).attr('href')).offset().top - 100
            },500);
            e.preventDefault();
        });
        
        //Toggle Menu
        $('#menu-toggle').on('click', () => {
            $('#menu-toggle').toggleClass('closeMenu');
            $('ul').toggleClass('showMenu');
            
            $('li').on('click', () => {
                $('ul').removeClass('showMenu');
                $('#menu-toggle').removeClass('closeMenu');
            });
        });

        //PARA IDENTIFICAR URL Y HACER UN SCROLL 100px
        // The function actually applying the offset
        function offsetAnchor() {
            if(location.hash.length !== 0) {
                window.scrollTo(window.scrollX, window.scrollY - 100);
            }
        }
        // This is here so that when you enter the page with a hash,
        // it can provide the offset in that case too. Having a timeout
        // seems necessary to allow the browser to jump to the anchor first.
        window.setTimeout(offsetAnchor, 1); // The delay of 1 is arbitrary and may not always work right (although it did in my testing)
        
    });
});
window.onload = function() {
    var $recaptcha = document.querySelector("#g-recaptcha-response");

    if($recaptcha) {
        $recaptcha.setAttribute("required", "required");
    }
};


function Validate(campo1, campo2) {
    var password = document.getElementById(campo1).value;
    var confirmPassword = document.getElementById(campo2).value;
    if (password != confirmPassword) {
        // alert("Los emails no coinciden.");
        return false;
    }
    return true;
}
function validateMail(p1, p2, mensaje) {
    if (p2.value != p1.value || p1.value != p2.value) {
        p2.setCustomValidity(mensaje);
    } else {
        p2.setCustomValidity('');
    }
}

function mostrarPassword(nombreElemento, botonShowPass){
    var cambio = document.getElementById(nombreElemento);
    var boton = document.getElementById(botonShowPass);
    if(cambio.type == "password"){
        cambio.type = "text";

        $(boton).removeClass('fa fa-eye-slash').addClass('fa fa-eye');
    }else{
        cambio.type = "password";
        $(boton).removeClass('fa fa-eye').addClass('fa fa-eye-slash');
    }
} 


function intercambiarDisplay(id1, idEditar){
    element = document.getElementById(id1);
    elementEditar = document.getElementById(idEditar);
    if(elementEditar.style.display == "none"){
        element.style.display = "none";
        elementEditar.removeAttribute("style");
    }
    else{
        element.removeAttribute("style");
        elementEditar.style.display = "none";
    }
}

function displayNoneById(id){
    element = document.getElementById(id);
    element.style.display = "none";
}

function removeDisplayNoneById(id){
    element = document.getElementById(id);
    element.style.display = "";
}