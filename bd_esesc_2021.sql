-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-05-2021 a las 14:27:21
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_esesc_2021`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidato`
--

CREATE TABLE `candidato` (
  `id` int(11) NOT NULL,
  `tipo_candidato` enum('POSTULANTE','REINCORPORADO') NOT NULL,
  `dni` int(11) NOT NULL,
  `nombres` varchar(255) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `provincia` varchar(150) NOT NULL,
  `localidad` varchar(150) NOT NULL,
  `codigo_postal` varchar(150) NOT NULL,
  `telefono` varchar(150) NOT NULL,
  `celular` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `cuil` varchar(150) NOT NULL,
  `nacionalidad` enum('ARGENTINO_NAT','ARGENTINO_OP') NOT NULL,
  `sexo` enum('MASCULINO','FEMENINO') NOT NULL,
  `estado_civil` enum('SOLTERO (SIN HIJOS)','SOLTERO (CON HIJOS)') NOT NULL,
  `grupo_sanguineo` enum('0+','0-','A+','A-','AB+','AB-','B+','B-') NOT NULL,
  `clave_candidato` varchar(255) DEFAULT NULL,
  `primerlogin` tinyint(1) NOT NULL,
  `fecha_registro` DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `id` int(11) NOT NULL,
  `descripcion` enum('SUPERUSUARIO','ADMINISTRADOR','PUBLICADOR','VISUALIZADOR','DIV EVAL','DIV ESTUDIO','DIV EDU FIS','SEC SANIDAD','DIV INCORP','SAF') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`id`, `descripcion`) VALUES
(1, 'SUPERUSUARIO'),
(2, 'ADMINISTRADOR'),
(3, 'PUBLICADOR'),
(4, 'VISUALIZADOR'),
(5, 'DIV EVAL'),
(6, 'DIV ESTUDIO'),
(7, 'DIV EDU FIS'),
(8, 'SEC SANIDAD'),
(9, 'DIV INCORP'),
(10, 'SAF');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro_preseleccion`
--

CREATE TABLE `centro_preseleccion` (
  `id` int(11) NOT NULL,
  `nombre` enum('ESCUELA DE SUBOFICIALES DEL EJÉRCITO "SARGENTO CABRAL"','LICEO MILITAR "GENERAL ROCA"','LICEO MILITAR "GENERAL ESPEJO"','LICEO MILITAR "GENERAL LAMADRID"','LICEO MILITAR "GENERAL BELGRANO"','LICEO MILITAR "GENERAL PAZ"','COMANDO DE LA Vta BRIGADA DE MONTAÑA','COMANDO DE LA XIIda BRIGADA DE MONTE','REGIMIENTO DE INFANTERÍA DE MONTE 29 "CNL IGNACIO JOSÉ WARNES"') NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilio`
--

CREATE TABLE `domicilio` (
  `id` int(11) NOT NULL,
  `calle` varchar(150) NOT NULL,
  `numero` int(11) NOT NULL,
  `piso` varchar(150) NOT NULL,
  `depto` varchar(150) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `editardatos`
--

CREATE TABLE `editardatos` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_candidato` int(11) NOT NULL,
  `permiso` enum('DATOS_PERSONALES','PLAN_CARRERA','SOLD_VOL','SECUNDARIO','FOTOS_DNI','FOTO_COMP_PAGO','FOTO_TICKET_PAGO') COLLATE utf8_unicode_ci NOT NULL,
  `fecha_emision` datetime NOT NULL,
  `fecha_uso` datetime DEFAULT NULL,
  `usado` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_fisico`
--

CREATE TABLE `examen_fisico` (
  `id` int(11) NOT NULL,
  `ffbb` int(11) NOT NULL,
  `abdo` int(11) NOT NULL,
  `trote` int(11) NOT NULL,
  `ffbb_puntaje` decimal(10,2) NOT NULL,
  `abdo_puntaje` decimal(10,2) NOT NULL,
  `trote_puntaje` decimal(10,2) NOT NULL,
  `promedio` decimal(10,2) NOT NULL,
  `detalle` varchar(255) NOT NULL,
  `ausente` tinyint(4) NOT NULL DEFAULT 0,
  `fecha_carga` datetime NOT NULL DEFAULT current_timestamp(),
  `estado` enum('APROBADO','DESAPROBADO') NOT NULL,
  `id_incorporacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_intelectual`
--

CREATE TABLE `examen_intelectual` (
  `id` int(11) NOT NULL,
  `lengua_puntaje` int(10) NOT NULL,
  `mate_puntaje` int(10) NOT NULL,
  `promedio` decimal(10,2) NOT NULL,
  `detalle` blob NOT NULL,
  `secundario_tecnico` tinyint(4) NOT NULL DEFAULT 0,
  `ausente` tinyint(4) NOT NULL DEFAULT 0,
  `fecha_carga` datetime DEFAULT current_timestamp(),
  `estado` enum('APROBADO','DESAPROBADO') NOT NULL,
  `id_incorporacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_medico`
--

CREATE TABLE `examen_medico` (
  `id` int(11) NOT NULL,
  `detalle` varchar(255) NOT NULL,
  `fecha_carga` datetime DEFAULT current_timestamp(),
  `estado` enum('APTO','APTO CONDICIONAL','NO APTO') DEFAULT NULL,
  `ausente` tinyint(4) NOT NULL DEFAULT 0,
  `id_incorporacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_psicologico`
--

CREATE TABLE `examen_psicologico` (
  `id` int(11) NOT NULL,
  `fecha_carga` datetime DEFAULT current_timestamp(),
  `estado` enum('APTO','NO APTO') DEFAULT NULL,
  `ausente` tinyint(4) NOT NULL DEFAULT 0,
  `id_incorporacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `folder_documentos`
--

CREATE TABLE `folder_documentos` (
  `id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `id_inscripcion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_modif`
--

CREATE TABLE `historial_modif` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `tabla` varchar(150) NOT NULL,
  `campo` varchar(150) NOT NULL,
  `valor_anterior` varchar(255) NOT NULL,
  `valor_nuevo` varchar(255) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incorporacion`
--

CREATE TABLE `incorporacion` (
  `id` int(11) NOT NULL,
  `detalle` blob NOT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `merito` decimal(10,2) DEFAULT NULL,
  `estado` enum('APROBADO','DESAPROBADO','EN OBSERVACION','RESERVA') NOT NULL,
  `id_candidato` int(11) NOT NULL,
  `id_turno_examen` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inscripcion`
--

CREATE TABLE `inscripcion` (
  `id` int(11) NOT NULL,
  `detalle` blob NOT NULL,
  `estado` enum('INSCRIPTO','INSCRIPTO CONDICIONAL','INSCRIPCION NO APROBADA','EN OBSERVACION') NOT NULL,
  `fecha_aceptacion` datetime DEFAULT NULL,
  `revision_saf` enum('SIN NOVEDAD','CON NOVEDAD','EN OBSERVACION') NOT NULL DEFAULT 'EN OBSERVACION',
  `detalle_saf` blob NOT NULL,
  `nro_inscripto` varchar(255) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista_aprobados`
--

CREATE TABLE `lista_aprobados` (
  `id` int(11) NOT NULL,
  `dni` int(11) NOT NULL,
  `nombres` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `nro_inscr` varchar(150) NOT NULL,
  `estado` enum('APTO','DESAPROBADO','CONDICIONAL') NOT NULL,
  `tipo` enum('POSTULANTE','REINCORPORADO') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mantenimiento`
--

CREATE TABLE `mantenimiento` (
  `id` int(11) NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mantenimiento`
--

INSERT INTO `mantenimiento` (`id`, `estado`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan_carrera`
--

CREATE TABLE `plan_carrera` (
  `id` int(11) NOT NULL,
  `tipo_plan` enum('ESPECIALIDADES','ARMAS') NOT NULL,
  `orientacion` varchar(150) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recuperar_pass`
--

CREATE TABLE `recuperar_pass` (
  `id` int(11) NOT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_expiracion` datetime NOT NULL,
  `usado` tinyint(1) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secundario`
--

CREATE TABLE `secundario` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `provincia` varchar(255) NOT NULL,
  `localidad` varchar(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `telefono` varchar(150) NOT NULL,
  `cant_materias_adeudadas` int(11) NOT NULL,
  `plan` enum('TECNICO','BACHILLER','FINES') NOT NULL,
  `anio_cursado` enum('COMPLETO','INCOMPLETO','EN_CURSO') DEFAULT NULL,
  `resolucion` varchar(150) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `soldado_voluntario`
--

CREATE TABLE `soldado_voluntario` (
  `id` int(11) NOT NULL,
  `destino` varchar(255) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turnos_examen`
--

CREATE TABLE `turnos_examen` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `fecha_desde` date NOT NULL,
  `fecha_hasta` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `cargo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `candidato`
--
ALTER TABLE `candidato`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `centro_preseleccion`
--
ALTER TABLE `centro_preseleccion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `editardatos`
--
ALTER TABLE `editardatos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`),
  ADD KEY `id_candidato_2` (`id_candidato`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `examen_fisico`
--
ALTER TABLE `examen_fisico`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_incorporacion` (`id_incorporacion`);

--
-- Indices de la tabla `examen_intelectual`
--
ALTER TABLE `examen_intelectual`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_incorporacion` (`id_incorporacion`);

--
-- Indices de la tabla `examen_medico`
--
ALTER TABLE `examen_medico`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_incorporacion` (`id_incorporacion`);

--
-- Indices de la tabla `examen_psicologico`
--
ALTER TABLE `examen_psicologico`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_incorporacion` (`id_incorporacion`);

--
-- Indices de la tabla `folder_documentos`
--
ALTER TABLE `folder_documentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_inscripcion` (`id_inscripcion`);

--
-- Indices de la tabla `historial_modif`
--
ALTER TABLE `historial_modif`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `incorporacion`
--
ALTER TABLE `incorporacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`),
  ADD KEY `id_turno_examen` (`id_turno_examen`);

--
-- Indices de la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `lista_aprobados`
--
ALTER TABLE `lista_aprobados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mantenimiento`
--
ALTER TABLE `mantenimiento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `plan_carrera`
--
ALTER TABLE `plan_carrera`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `recuperar_pass`
--
ALTER TABLE `recuperar_pass`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `secundario`
--
ALTER TABLE `secundario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `soldado_voluntario`
--
ALTER TABLE `soldado_voluntario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `turnos_examen`
--
ALTER TABLE `turnos_examen`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `candidato`
--
ALTER TABLE `candidato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `centro_preseleccion`
--
ALTER TABLE `centro_preseleccion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `editardatos`
--
ALTER TABLE `editardatos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `examen_fisico`
--
ALTER TABLE `examen_fisico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `examen_intelectual`
--
ALTER TABLE `examen_intelectual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `examen_medico`
--
ALTER TABLE `examen_medico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `examen_psicologico`
--
ALTER TABLE `examen_psicologico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `folder_documentos`
--
ALTER TABLE `folder_documentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `historial_modif`
--
ALTER TABLE `historial_modif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `incorporacion`
--
ALTER TABLE `incorporacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `lista_aprobados`
--
ALTER TABLE `lista_aprobados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mantenimiento`
--
ALTER TABLE `mantenimiento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `plan_carrera`
--
ALTER TABLE `plan_carrera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `recuperar_pass`
--
ALTER TABLE `recuperar_pass`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `secundario`
--
ALTER TABLE `secundario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `soldado_voluntario`
--
ALTER TABLE `soldado_voluntario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `turnos_examen`
--
ALTER TABLE `turnos_examen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `centro_preseleccion`
--
ALTER TABLE `centro_preseleccion`
  ADD CONSTRAINT `centro_preseleccion_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD CONSTRAINT `domicilio_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `editardatos`
--
ALTER TABLE `editardatos`
  ADD CONSTRAINT `editardatos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `editardatos_ibfk_2` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `examen_fisico`
--
ALTER TABLE `examen_fisico`
  ADD CONSTRAINT `examen_fisico_ibfk_1` FOREIGN KEY (`id_incorporacion`) REFERENCES `incorporacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `examen_intelectual`
--
ALTER TABLE `examen_intelectual`
  ADD CONSTRAINT `examen_intelectual_ibfk_1` FOREIGN KEY (`id_incorporacion`) REFERENCES `incorporacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `examen_medico`
--
ALTER TABLE `examen_medico`
  ADD CONSTRAINT `examen_medico_ibfk_1` FOREIGN KEY (`id_incorporacion`) REFERENCES `incorporacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `examen_psicologico`
--
ALTER TABLE `examen_psicologico`
  ADD CONSTRAINT `examen_psicologico_ibfk_1` FOREIGN KEY (`id_incorporacion`) REFERENCES `incorporacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `folder_documentos`
--
ALTER TABLE `folder_documentos`
  ADD CONSTRAINT `folder_documentos_ibfk_1` FOREIGN KEY (`id_inscripcion`) REFERENCES `inscripcion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `incorporacion`
--
ALTER TABLE `incorporacion`
  ADD CONSTRAINT `incorporacion_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `incorporacion_ibfk_2` FOREIGN KEY (`id_turno_examen`) REFERENCES `turnos_examen` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `inscripcion`
--
ALTER TABLE `inscripcion`
  ADD CONSTRAINT `inscripcion_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`);

--
-- Filtros para la tabla `plan_carrera`
--
ALTER TABLE `plan_carrera`
  ADD CONSTRAINT `plan_carrera_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `recuperar_pass`
--
ALTER TABLE `recuperar_pass`
  ADD CONSTRAINT `recuperar_pass_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `secundario`
--
ALTER TABLE `secundario`
  ADD CONSTRAINT `secundario_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `soldado_voluntario`
--
ALTER TABLE `soldado_voluntario`
  ADD CONSTRAINT `soldado_voluntario_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
