-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 22-11-2019 a las 12:58:37
-- Versión del servidor: 5.7.28-0ubuntu0.18.04.4
-- Versión de PHP: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `abuelo`
--

CREATE TABLE `abuelo` (
  `id` int(11) NOT NULL,
  `apellido` varchar(150) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `sexo` enum('F','M') NOT NULL,
  `vivo` tinyint(1) NOT NULL,
  `tipo_abuelo` enum('MATERNO','PATERNO') NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baja_empleo_ffaaffss`
--

CREATE TABLE `baja_empleo_ffaaffss` (
  `id` int(11) NOT NULL,
  `fecha_baja` date NOT NULL,
  `tipo_baja` enum('VOLUNTARIA','OBLIGATORIA') NOT NULL,
  `id_empleo_ffaaffss` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `baja_estudio_ins_ffaa`
--

CREATE TABLE `baja_estudio_ins_ffaa` (
  `id` int(11) NOT NULL,
  `tipo` enum('VOLUNTARIA','OBLIGATORIA') NOT NULL,
  `motivo` varchar(255) NOT NULL,
  `fecha_baja` date NOT NULL,
  `id_estudio_ins_ffaa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calificacion_soldado_voluntario`
--

CREATE TABLE `calificacion_soldado_voluntario` (
  `id` int(11) NOT NULL,
  `anio1` varchar(50) NOT NULL,
  `anio2` varchar(50) NOT NULL,
  `calificacion_anio1` varchar(150) NOT NULL,
  `calificacion_anio2` varchar(150) NOT NULL,
  `promedio` varchar(50) NOT NULL,
  `id_soldado_voluntario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidato`
--

CREATE TABLE `candidato` (
  `id` int(11) NOT NULL,
  `dni` int(11) NOT NULL,
  `apellido` varchar(150) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `localidad` varchar(150) NOT NULL,
  `provincia` varchar(150) NOT NULL,
  `codigo_postal` varchar(150) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `celular` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `lugar_nacimiento` varchar(150) NOT NULL,
  `provincia_nacimiento` varchar(150) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `cuil` varchar(150) NOT NULL,
  `cuit` varchar(150) NOT NULL,
  `cedula_identidad` varchar(150) NOT NULL,
  `expedida_por` varchar(255) NOT NULL,
  `nacionalidad` varchar(150) NOT NULL,
  `ced_extranjero` varchar(150) NOT NULL,
  `sexo` enum('F','M') NOT NULL,
  `estado_civil` enum('CASADO','SOLTERO','DIVORCIADO','VIUDO') NOT NULL,
  `grupo_sanguineo` enum('SIN_ESPECIFICAR','0+','0-','A+','A-','AB+','AB-','B+','B-') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `candidato`
--

INSERT INTO `candidato` (`id`, `dni`, `apellido`, `nombre`, `localidad`, `provincia`, `codigo_postal`, `telefono`, `celular`, `email`, `lugar_nacimiento`, `provincia_nacimiento`, `fecha_nacimiento`, `cuil`, `cuit`, `cedula_identidad`, `expedida_por`, `nacionalidad`, `ced_extranjero`, `sexo`, `estado_civil`, `grupo_sanguineo`) VALUES
(69, 2000000, 'TEVEZ', 'CARLITOS', 'LOS POLVORINES', 'BUENOS AIRES', '1613', '', '1150546474', 'samaniegoeliseo@gmail.com', 'SAN MIGUEL', 'BUENOS AIRES', '1997-05-30', '2040491265', '', '', '', 'ARGENTINO', '-', 'M', 'SOLTERO', '0+'),
(109, 40491265, 'SAMANIEGO', 'ELISEO', 'LOS POLVORINES', 'BUENOS AIRES', '1613', '', '1550546474', 'samaniegoeliseo@gmail.com', 'SAN MIGUEL', 'BUENOS AIRES', '1997-05-30', '20404912659', '', '', '', 'ARGENTINO', '', 'F', 'SOLTERO', '0+'),
(115, 1231231231, 'SAMANIEGO', 'NARA', 'ABASTO', 'BUENOS AIRES', '1613', '123123123', '123123123', 'narasamaniego@gmail.com', 'ASDASDA', 'ASDASA', '1999-11-16', '12312312312312', '', '', '', 'ASDASDASD', '', 'F', 'CASADO', 'A+'),
(116, 42591584, 'OVIEDO', 'TAMARA', 'CAZADORES CORRENTINOS', 'CORRIENTES', '1619', '', '1545646545', 'oviedo@gmail.com', 'CURUZU', 'CORRIENTES', '1998-05-30', '20425915849', '', '', '', 'ARGENTINA', '', 'F', 'SOLTERO', 'B+'),
(117, 65445489, 'BENITEZ', 'CARLOS', 'ABASTO', 'BUENOS AIRES', '1617', '', '1548484974', 'carlosb@gmail.com', 'NORDELTA', 'BUENOS AIRES', '1998-08-03', '20654454899', '', '', '', 'ARGENTINO', '', 'M', 'DIVORCIADO', 'A-'),
(121, 645456454, 'LOPEZ', 'JOSE', 'ANILLACO', 'LA RIOJA', '1897', '', '0116548484', 'josejose@hotmail.com', 'COSTA', 'BUENOS AIRES', '1997-08-04', '', '1231231231', '', '', 'ARGENTINO', '', 'M', 'SOLTERO', 'AB-'),
(122, 645456454, 'LOPEZ', 'JOSE', 'ANILLACO', 'LA RIOJA', '1897', '', '0116548484', 'josejose@hotmail.com', 'COSTA', 'BUENOS AIRES', '1997-08-04', '', '1231231231', '', '', 'ARGENTINO', '', 'M', 'SOLTERO', 'AB-');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `id_cargo` int(4) NOT NULL,
  `descripcion_cargo` enum('SUPERUSUARIO','ADMINISTRADOR','PUBLICADOR','VISUALIZADOR') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`id_cargo`, `descripcion_cargo`) VALUES
(1, 'SUPERUSUARIO'),
(2, 'ADMINISTRADOR'),
(3, 'PUBLICADOR'),
(4, 'VISUALIZADOR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro_preseleccion`
--

CREATE TABLE `centro_preseleccion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `direccion` varchar(150) NOT NULL,
  `localidad` varchar(150) NOT NULL,
  `provincia` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto_emergencia`
--

CREATE TABLE `contacto_emergencia` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `apellido` varchar(150) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `localidad` varchar(150) NOT NULL,
  `provincia` varchar(150) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `celular` varchar(50) NOT NULL,
  `parentesco` varchar(150) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentacion`
--

CREATE TABLE `documentacion` (
  `id` int(11) NOT NULL,
  `estado` enum('FALTANTE','COMPLETA') NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documento`
--

CREATE TABLE `documento` (
  `id` int(11) NOT NULL,
  `url_imagen` varchar(255) NOT NULL,
  `tipo_documento` enum('DNI','ACTA_NACIMIENTO','CUIT_CUIL','CONSTANCIA_ESTUDIOS','CERTIFICADO_FIN_ESTUDIOS','FOTO_CUERPO_ENTERO','FOTO_CARNET','CERTIFICADO_RNR','LIBRETA_NACIONAL_CONDUCIR','PARTIDA_NACIMIENTO_HIJO','DNI_HIJO') NOT NULL,
  `id_documentacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilio`
--

CREATE TABLE `domicilio` (
  `id` int(11) NOT NULL,
  `calle` varchar(150) NOT NULL,
  `numero` varchar(150) NOT NULL,
  `piso` varchar(150) NOT NULL,
  `depto` varchar(150) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `domicilio`
--

INSERT INTO `domicilio` (`id`, `calle`, `numero`, `piso`, `depto`, `id_candidato`) VALUES
(1, 'JOSE LEON', '1335', '', '', 117),
(2, 'JOSE LEON', '1335', '', '', 117),
(3, 'BALVIN 100', '58484', '2', '1A', 121),
(4, 'BALVIN 100', '58484', '2', '1A', 121);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilio_exterior`
--

CREATE TABLE `domicilio_exterior` (
  `id` int(11) NOT NULL,
  `motivo` varchar(255) NOT NULL,
  `lugar` varchar(150) NOT NULL,
  `f_desde` date NOT NULL,
  `f_hasta` date NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `domicilio_exterior`
--

INSERT INTO `domicilio_exterior` (`id`, `motivo`, `lugar`, `f_desde`, `f_hasta`, `id_candidato`) VALUES
(1, 'Habia poca comida en el pais y migramos xdxdxd', 'CHILE, SANTIAGO', '1999-09-06', '2000-08-06', 121);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleo`
--

CREATE TABLE `empleo` (
  `id` int(11) NOT NULL,
  `tipo_contrato` enum('EFECTIVO','TEMPORARIO') NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `direccion` varchar(150) NOT NULL,
  `codigo_postal` varchar(150) NOT NULL,
  `localidad` varchar(150) NOT NULL,
  `provincia` varchar(150) NOT NULL,
  `tarea_desenpeniada` varchar(255) NOT NULL,
  `antiguedad_anios` int(11) NOT NULL,
  `antiguedad_meses` int(11) NOT NULL,
  `causa_baja` varchar(150) NOT NULL,
  `sosten_hogar` tinyint(1) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleo_civil_padre`
--

CREATE TABLE `empleo_civil_padre` (
  `id` int(11) NOT NULL,
  `organismo` varchar(150) NOT NULL,
  `categoria` varchar(150) NOT NULL,
  `puesto` varchar(150) NOT NULL,
  `id_padre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleo_civil_padre`
--

INSERT INTO `empleo_civil_padre` (`id`, `organismo`, `categoria`, `puesto`, `id_padre`) VALUES
(1, 'Liceo Militar San José', 'Carlos123', 'Encargado', 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleo_ffaaffss`
--

CREATE TABLE `empleo_ffaaffss` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `grado_destino_actual` varchar(255) NOT NULL,
  `jerarquia_alcanzada` varchar(150) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleo_ffaa_padre`
--

CREATE TABLE `empleo_ffaa_padre` (
  `id` int(11) NOT NULL,
  `grado` varchar(150) NOT NULL,
  `arma_servicio` varchar(150) NOT NULL,
  `situacion_revista` enum('EN_ACTIVIDAD','RETIRADO') NOT NULL,
  `destino` varchar(150) NOT NULL,
  `cargo` varchar(150) NOT NULL,
  `anio_retiro` year(4) NOT NULL,
  `nombre_fuerza` varchar(150) NOT NULL,
  `id_padre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleo_ffaa_padre`
--

INSERT INTO `empleo_ffaa_padre` (`id`, `grado`, `arma_servicio`, `situacion_revista`, `destino`, `cargo`, `anio_retiro`, `nombre_fuerza`, `id_padre`) VALUES
(4, 'MI', 'ASDASDAS', 'EN_ACTIVIDAD', 'ASDASDA', 'ASDASDASD', 1990, 'ARMADA ARGENTINA', 13),
(5, 'SP', 'asdasd', 'EN_ACTIVIDAD', 'asdasd', 'asdasd', 1998, 'EJÉRCITO ARGENTINO', 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleo_padre`
--

CREATE TABLE `empleo_padre` (
  `id` int(11) NOT NULL,
  `ramo` varchar(150) NOT NULL,
  `tareas` varchar(255) NOT NULL,
  `tipo_empleado` enum('PROPIETARIO','SOCIO','EMPLEADO') NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `id_padre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estudio_ins_ffaa`
--

CREATE TABLE `estudio_ins_ffaa` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `jerarquia_alcanzada` varchar(150) NOT NULL,
  `cursando` tinyint(1) NOT NULL,
  `id_formacion_academica` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formacion_academica`
--

CREATE TABLE `formacion_academica` (
  `id` int(11) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `formacion_academica`
--

INSERT INTO `formacion_academica` (`id`, `id_candidato`) VALUES
(1, 117),
(4, 117),
(5, 121),
(6, 121);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formacion_academica_padre`
--

CREATE TABLE `formacion_academica_padre` (
  `id` int(11) NOT NULL,
  `descripcion_titulo` varchar(150) DEFAULT NULL,
  `nivel_academico` enum('SIN_NIVEL','PRIMARIO_INC','PRIMARIO_COMP','SECUNDARIO_INC','SECUNDARIO_COMP','TERCIARIO_INC','TERCIARIO_COMP','UNIVERSITARIO_INC','UNIVERSITARIO_COMP') DEFAULT NULL,
  `id_padre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `formacion_academica_padre`
--

INSERT INTO `formacion_academica_padre` (`id`, `descripcion_titulo`, `nivel_academico`, `id_padre`) VALUES
(1, 'Tecnico Mecánico', 'SECUNDARIO_COMP', 15),
(2, '', NULL, 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hermano`
--

CREATE TABLE `hermano` (
  `id` int(11) NOT NULL,
  `apellido` varchar(150) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `sexo` enum('F','M') NOT NULL,
  `vivo` tinyint(1) NOT NULL,
  `edad` int(11) NOT NULL,
  `ocupacion` varchar(255) DEFAULT NULL,
  `lugar_trabajo` varchar(255) DEFAULT NULL,
  `estudio` varchar(255) DEFAULT NULL,
  `anio_estudio` varchar(150) DEFAULT NULL,
  `estado_civil` enum('CASADO','SOLTERO','DIVORCIADO','VIUDO') NOT NULL,
  `sosten_hogar` tinyint(1) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hijo`
--

CREATE TABLE `hijo` (
  `id` int(11) NOT NULL,
  `apellido` varchar(150) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `sexo` enum('F','M') NOT NULL,
  `vivo` tinyint(1) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `nacionalidad` varchar(150) NOT NULL,
  `domicilio` varchar(150) NOT NULL,
  `localidad` varchar(150) NOT NULL,
  `provincia` varchar(150) NOT NULL,
  `codigo_postal` varchar(150) NOT NULL,
  `estudios` varchar(150) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_modif`
--

CREATE TABLE `historial_modif` (
  `id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `tabla` varchar(255) NOT NULL,
  `campo` varchar(255) NOT NULL,
  `cont_prev_modif` varchar(255) NOT NULL,
  `cont_post_modif` varchar(255) NOT NULL,
  `id_candidato` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idioma_conocido`
--

CREATE TABLE `idioma_conocido` (
  `id` int(11) NOT NULL,
  `idioma` varchar(150) NOT NULL,
  `habla` tinyint(1) NOT NULL,
  `lee` tinyint(1) NOT NULL,
  `escribe` tinyint(1) NOT NULL,
  `id_formacion_academica` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `idioma_conocido`
--

INSERT INTO `idioma_conocido` (`id`, `idioma`, `habla`, `lee`, `escribe`, `id_formacion_academica`) VALUES
(1, 'Quechuo', 1, 0, 0, 1),
(2, 'Jeringoso', 1, 1, 1, 1),
(3, 'Quechuo', 1, 0, 0, 1),
(4, 'Jeringoso', 1, 1, 1, 1),
(5, 'Quechuo', 1, 0, 0, 1),
(6, 'Jeringoso', 1, 1, 1, 1),
(7, 'Quechuo', 1, 0, 0, 1),
(8, 'Jeringoso', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `opinion_jefe_soldado_voluntario`
--

CREATE TABLE `opinion_jefe_soldado_voluntario` (
  `id` int(11) NOT NULL,
  `opinion` varchar(255) NOT NULL,
  `f_opinion` date NOT NULL,
  `lugar` varchar(255) NOT NULL,
  `id_soldado_voluntario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `otra_ocupacion_padre`
--

CREATE TABLE `otra_ocupacion_padre` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `id_padre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `otra_ocupacion_padre`
--

INSERT INTO `otra_ocupacion_padre` (`id`, `descripcion`, `id_padre`) VALUES
(1, 'Jugador de tennis', 15),
(2, '', 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `otro`
--

CREATE TABLE `otro` (
  `id` int(11) NOT NULL,
  `lugar` varchar(255) NOT NULL,
  `nivel_alcanzado` varchar(255) NOT NULL,
  `titulo_obtenido` varchar(255) NOT NULL,
  `id_formacion_academica` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `otro`
--

INSERT INTO `otro` (`id`, `lugar`, `nivel_alcanzado`, `titulo_obtenido`, `id_formacion_academica`) VALUES
(1, '', '', '', 1),
(2, '', '', '', 1),
(3, '', '', '', 1),
(4, '', '', '', 1),
(5, '', '', '', 5),
(6, '', '', '', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `padre`
--

CREATE TABLE `padre` (
  `id` int(11) NOT NULL,
  `apellido` varchar(150) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `sexo` enum('F','M') NOT NULL,
  `vivo` tinyint(1) NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `nacionalidad` varchar(150) NOT NULL,
  `naturalizado` tinyint(1) DEFAULT NULL,
  `domicilio_actual` varchar(255) NOT NULL,
  `localidad` varchar(150) NOT NULL,
  `provincia` varchar(150) NOT NULL,
  `codigo_postal` varchar(150) NOT NULL,
  `carta_ciudadania` varchar(255) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `dni` int(11) DEFAULT NULL,
  `pasaporte_nro` varchar(150) NOT NULL,
  `sosten_hogar` tinyint(1) DEFAULT NULL,
  `nivel_ocupacional` enum('OCUPADO','DESOCUPADO','JUBILADO','AMO_CASA') DEFAULT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `padre`
--

INSERT INTO `padre` (`id`, `apellido`, `nombre`, `sexo`, `vivo`, `fecha_nacimiento`, `nacionalidad`, `naturalizado`, `domicilio_actual`, `localidad`, `provincia`, `codigo_postal`, `carta_ciudadania`, `telefono`, `dni`, `pasaporte_nro`, `sosten_hogar`, `nivel_ocupacional`, `id_candidato`) VALUES
(6, 'SAMANIEGO', 'ZACARIAS', 'M', 1, '1954-09-12', 'PARAGUAY', 1, 'MURGIONDO 145', 'LOS POLVORINES', 'BUENOS AIRES', '1614', '', '1560014922', 20159159, '', 0, 'OCUPADO', 109),
(7, 'PECARRERE', 'NORMA MABEL', 'F', 1, '1960-12-17', 'ARGENTINA', 1, 'DANTE ALIGHIERI 2747', 'LOS POLVORINES', 'BUENOS AIRES', '1613', '', '1536695816', 24159159, '', 1, 'OCUPADO', 109),
(13, 'APELLIDOPADRE', 'NOMBREPADRE', 'M', 1, '2019-11-13', 'ARGENTINA', 0, '', '', '', '', '', '', 12312312, '', 1, 'DESOCUPADO', 115),
(14, 'APELLIDOMADRE', 'NOMBREMADRE', 'F', 0, '2019-11-18', 'ARGENTINA', 0, '', '', '', '', '', '', 12312312, '', NULL, NULL, 115),
(15, 'OVIEDO', 'ROBERTO', 'M', 1, '1960-05-30', 'ARGENTINA', 1, 'LOS FUENTES 5444', 'CURUZU', 'CORRIENTES', '5489', '', '1589876549', 15649584, '', 0, 'OCUPADO', 116),
(16, 'LOPEZ', 'NORMA', 'F', 0, '1965-04-05', 'BOLIVIA', 1, '', '', '', '', '', '', 14564897, '', 0, NULL, 116);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `id` int(11) NOT NULL,
  `nro_comprobante` varchar(255) NOT NULL,
  `monto` int(11) NOT NULL,
  `forma_de_pago` enum('GIRO','EFECTIVO','DEPÓSITO','') NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan_carrera`
--

CREATE TABLE `plan_carrera` (
  `id` int(11) NOT NULL,
  `tipo_plan` enum('ARMAS','ESP Y SERVICIOS') NOT NULL,
  `orientacion` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `postulante`
--

CREATE TABLE `postulante` (
  `id` int(11) NOT NULL,
  `id_preseleccion_postulante` int(11) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preseleccion_postulante`
--

CREATE TABLE `preseleccion_postulante` (
  `id` int(11) NOT NULL,
  `autoasignacion` tinyint(1) NOT NULL DEFAULT '0',
  `id_centro_preseleccion` int(11) NOT NULL,
  `id_plan_carrera` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preseleccion_reincorporado`
--

CREATE TABLE `preseleccion_reincorporado` (
  `id` int(11) NOT NULL,
  `id_centro_preseleccion` int(11) NOT NULL,
  `id_plan_carrera` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `racionamiento`
--

CREATE TABLE `racionamiento` (
  `id` int(11) NOT NULL,
  `solicita_racionamiento` tinyint(1) NOT NULL DEFAULT '1',
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reincorporado`
--

CREATE TABLE `reincorporado` (
  `id` int(11) NOT NULL,
  `id_solicitud_reincorporado` int(11) NOT NULL,
  `id_preseleccion_reincorporado` int(11) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sancion_soldado_voluntario`
--

CREATE TABLE `sancion_soldado_voluntario` (
  `id` int(11) NOT NULL,
  `tipo` varchar(150) NOT NULL,
  `causa` varchar(255) NOT NULL,
  `graduacion` varchar(150) NOT NULL,
  `total` varchar(150) NOT NULL,
  `id_soldado_voluntario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secundario`
--

CREATE TABLE `secundario` (
  `id` int(11) NOT NULL,
  `titulo` varchar(150) NOT NULL,
  `anio_alcanzado` varchar(150) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `localidad` varchar(150) NOT NULL,
  `provincia` varchar(150) NOT NULL,
  `cant_materias_adeudadas` int(11) NOT NULL,
  `estado_secundario` enum('COMPLETO','INCOMPLETO') NOT NULL,
  `id_formacion_academica` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `secundario`
--

INSERT INTO `secundario` (`id`, `titulo`, `anio_alcanzado`, `nombre`, `direccion`, `localidad`, `provincia`, `cant_materias_adeudadas`, `estado_secundario`, `id_formacion_academica`) VALUES
(1, 'TECNICO ASTRONAUTA', '6', 'TECNICA 1 DE LA GALAXIA', 'CALLE ROCOSA 12345', 'SATURNO', 'VIA LACTEA', 3, 'INCOMPLETO', 1),
(2, 'TECNICO ASTRONAUTA', '6', 'TECNICA 1 DE LA GALAXIA', 'CALLE ROCOSA 12345', 'SATURNO', 'VIA LACTEA', 3, 'INCOMPLETO', 1),
(3, 'TECNICO ASTRONAUTA', '6', 'TECNICA 1 DE LA GALAXIA', 'CALLE ROCOSA 12345', 'SATURNO', 'VIA LACTEA', 3, 'INCOMPLETO', 1),
(4, 'TECNICO ASTRONAUTA', '6', 'TECNICA 1 DE LA GALAXIA', 'CALLE ROCOSA 12345', 'SATURNO', 'VIA LACTEA', 3, 'INCOMPLETO', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `soldado_voluntario`
--

CREATE TABLE `soldado_voluntario` (
  `id` int(11) NOT NULL,
  `grado` varchar(150) NOT NULL,
  `familiares_a_cargo` varchar(255) NOT NULL,
  `destino_act` varchar(255) NOT NULL,
  `f_incorporacion` date NOT NULL,
  `ciclo_cap_aprobada` varchar(150) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud`
--

CREATE TABLE `solicitud` (
  `id` int(11) NOT NULL,
  `estado` enum('ACEPTADA','CONDICIONAL','RECHAZADA','EN_TRAMITE') NOT NULL,
  `detalle` varchar(255) NOT NULL,
  `nro_inscripto` varchar(255) NOT NULL,
  `fecha_solicitud` date NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud_esesc`
--

CREATE TABLE `solicitud_esesc` (
  `id` int(11) NOT NULL,
  `solicito_ingresar` tinyint(1) NOT NULL,
  `pudo_ingresar` tinyint(1) NOT NULL,
  `tipo_baja` enum('VOLUNTARIA','OBLIGATORIA') NOT NULL,
  `motivo_baja` varchar(150) NOT NULL,
  `anio_baja` year(4) NOT NULL,
  `id_formacion_academica` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud_reincorporado`
--

CREATE TABLE `solicitud_reincorporado` (
  `id` int(11) NOT NULL,
  `desea_reincorp` tinyint(1) NOT NULL,
  `cant_solicitudes` int(11) NOT NULL,
  `ultimo_anio_reincorporado` int(11) NOT NULL,
  `anio_carrera` enum('PRIMERO','SEGUNDO') NOT NULL,
  `causa_baja` enum('PROBLEMAS_PERSONALES','CONDUCTA','NO_CUMPLIA_EXIG_ACADEMICAS','JUSTICIA_MILITAR','OTROS') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud_smv`
--

CREATE TABLE `solicitud_smv` (
  `id` int(11) NOT NULL,
  `anio_solicitud` int(11) NOT NULL,
  `lugar` varchar(150) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tutor`
--

CREATE TABLE `tutor` (
  `id` int(11) NOT NULL,
  `apellido` varchar(150) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `edad` int(11) NOT NULL,
  `nacionalidad` varchar(150) NOT NULL,
  `domicilio` varchar(150) NOT NULL,
  `localidad` varchar(150) NOT NULL,
  `provincia` varchar(150) NOT NULL,
  `codigo_postal` varchar(150) NOT NULL,
  `dni` int(11) NOT NULL,
  `telefono` varchar(150) NOT NULL,
  `profesion` varchar(255) NOT NULL,
  `parentesco` varchar(150) NOT NULL,
  `id_candidato` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(10) NOT NULL,
  `user` varchar(150) NOT NULL,
  `pass` varchar(150) NOT NULL,
  `cargo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `user`, `pass`, `cargo`) VALUES
(1, 'suboficial', '123456', 2),
(6, 'eliseo', '123456', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `abuelo`
--
ALTER TABLE `abuelo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `baja_empleo_ffaaffss`
--
ALTER TABLE `baja_empleo_ffaaffss`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_empleo_ffaaffss` (`id_empleo_ffaaffss`);

--
-- Indices de la tabla `baja_estudio_ins_ffaa`
--
ALTER TABLE `baja_estudio_ins_ffaa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_estudio_ins_ffaa` (`id_estudio_ins_ffaa`);

--
-- Indices de la tabla `calificacion_soldado_voluntario`
--
ALTER TABLE `calificacion_soldado_voluntario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_soldado_voluntario` (`id_soldado_voluntario`);

--
-- Indices de la tabla `candidato`
--
ALTER TABLE `candidato`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id_cargo`);

--
-- Indices de la tabla `centro_preseleccion`
--
ALTER TABLE `centro_preseleccion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contacto_emergencia`
--
ALTER TABLE `contacto_emergencia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`) USING BTREE;

--
-- Indices de la tabla `documentacion`
--
ALTER TABLE `documentacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `documento`
--
ALTER TABLE `documento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_documentacion` (`id_documentacion`);

--
-- Indices de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`) USING BTREE;

--
-- Indices de la tabla `domicilio_exterior`
--
ALTER TABLE `domicilio_exterior`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`) USING BTREE;

--
-- Indices de la tabla `empleo`
--
ALTER TABLE `empleo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `empleo_civil_padre`
--
ALTER TABLE `empleo_civil_padre`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_padre` (`id_padre`);

--
-- Indices de la tabla `empleo_ffaaffss`
--
ALTER TABLE `empleo_ffaaffss`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `empleo_ffaa_padre`
--
ALTER TABLE `empleo_ffaa_padre`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_padre` (`id_padre`);

--
-- Indices de la tabla `empleo_padre`
--
ALTER TABLE `empleo_padre`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_padre` (`id_padre`);

--
-- Indices de la tabla `estudio_ins_ffaa`
--
ALTER TABLE `estudio_ins_ffaa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_formacion_academica` (`id_formacion_academica`);

--
-- Indices de la tabla `formacion_academica`
--
ALTER TABLE `formacion_academica`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `formacion_academica_padre`
--
ALTER TABLE `formacion_academica_padre`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_padre` (`id_padre`);

--
-- Indices de la tabla `hermano`
--
ALTER TABLE `hermano`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `hijo`
--
ALTER TABLE `hijo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `historial_modif`
--
ALTER TABLE `historial_modif`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `idioma_conocido`
--
ALTER TABLE `idioma_conocido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_formacion_academica` (`id_formacion_academica`);

--
-- Indices de la tabla `opinion_jefe_soldado_voluntario`
--
ALTER TABLE `opinion_jefe_soldado_voluntario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_soldado_voluntario` (`id_soldado_voluntario`);

--
-- Indices de la tabla `otra_ocupacion_padre`
--
ALTER TABLE `otra_ocupacion_padre`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_padre` (`id_padre`);

--
-- Indices de la tabla `otro`
--
ALTER TABLE `otro`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_formacion_academica` (`id_formacion_academica`);

--
-- Indices de la tabla `padre`
--
ALTER TABLE `padre`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `plan_carrera`
--
ALTER TABLE `plan_carrera`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `postulante`
--
ALTER TABLE `postulante`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`),
  ADD KEY `id_preseleccion_postulante` (`id_preseleccion_postulante`);

--
-- Indices de la tabla `preseleccion_postulante`
--
ALTER TABLE `preseleccion_postulante`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_plan_carrera` (`id_plan_carrera`),
  ADD KEY `id_centro_preseleccion` (`id_centro_preseleccion`);

--
-- Indices de la tabla `preseleccion_reincorporado`
--
ALTER TABLE `preseleccion_reincorporado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_centro_preseleccion` (`id_centro_preseleccion`),
  ADD KEY `id_plan_carrera` (`id_plan_carrera`);

--
-- Indices de la tabla `racionamiento`
--
ALTER TABLE `racionamiento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `reincorporado`
--
ALTER TABLE `reincorporado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_solicitud_reincorporado` (`id_solicitud_reincorporado`),
  ADD KEY `id_preseleccion_reincorporado` (`id_preseleccion_reincorporado`),
  ADD KEY `id_candidato` (`id_candidato`) USING BTREE;

--
-- Indices de la tabla `sancion_soldado_voluntario`
--
ALTER TABLE `sancion_soldado_voluntario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_soldado_voluntario` (`id_soldado_voluntario`);

--
-- Indices de la tabla `secundario`
--
ALTER TABLE `secundario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_formacion_academica` (`id_formacion_academica`);

--
-- Indices de la tabla `soldado_voluntario`
--
ALTER TABLE `soldado_voluntario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`) USING BTREE;

--
-- Indices de la tabla `solicitud`
--
ALTER TABLE `solicitud`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `solicitud_esesc`
--
ALTER TABLE `solicitud_esesc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_formacion_academica` (`id_formacion_academica`);

--
-- Indices de la tabla `solicitud_reincorporado`
--
ALTER TABLE `solicitud_reincorporado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `solicitud_smv`
--
ALTER TABLE `solicitud_smv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `tutor`
--
ALTER TABLE `tutor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_candidato` (`id_candidato`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cargo_2` (`cargo`),
  ADD KEY `cargo_3` (`cargo`),
  ADD KEY `cargo_4` (`cargo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `abuelo`
--
ALTER TABLE `abuelo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `baja_empleo_ffaaffss`
--
ALTER TABLE `baja_empleo_ffaaffss`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `baja_estudio_ins_ffaa`
--
ALTER TABLE `baja_estudio_ins_ffaa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `calificacion_soldado_voluntario`
--
ALTER TABLE `calificacion_soldado_voluntario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `candidato`
--
ALTER TABLE `candidato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT de la tabla `centro_preseleccion`
--
ALTER TABLE `centro_preseleccion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `contacto_emergencia`
--
ALTER TABLE `contacto_emergencia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `documentacion`
--
ALTER TABLE `documentacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `documento`
--
ALTER TABLE `documento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `domicilio_exterior`
--
ALTER TABLE `domicilio_exterior`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `empleo`
--
ALTER TABLE `empleo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `empleo_civil_padre`
--
ALTER TABLE `empleo_civil_padre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `empleo_ffaaffss`
--
ALTER TABLE `empleo_ffaaffss`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `empleo_ffaa_padre`
--
ALTER TABLE `empleo_ffaa_padre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `empleo_padre`
--
ALTER TABLE `empleo_padre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `estudio_ins_ffaa`
--
ALTER TABLE `estudio_ins_ffaa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `formacion_academica`
--
ALTER TABLE `formacion_academica`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `formacion_academica_padre`
--
ALTER TABLE `formacion_academica_padre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `hermano`
--
ALTER TABLE `hermano`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `hijo`
--
ALTER TABLE `hijo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `historial_modif`
--
ALTER TABLE `historial_modif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `idioma_conocido`
--
ALTER TABLE `idioma_conocido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `opinion_jefe_soldado_voluntario`
--
ALTER TABLE `opinion_jefe_soldado_voluntario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `otra_ocupacion_padre`
--
ALTER TABLE `otra_ocupacion_padre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `otro`
--
ALTER TABLE `otro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `padre`
--
ALTER TABLE `padre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `pago`
--
ALTER TABLE `pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `plan_carrera`
--
ALTER TABLE `plan_carrera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `postulante`
--
ALTER TABLE `postulante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `preseleccion_postulante`
--
ALTER TABLE `preseleccion_postulante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `preseleccion_reincorporado`
--
ALTER TABLE `preseleccion_reincorporado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sancion_soldado_voluntario`
--
ALTER TABLE `sancion_soldado_voluntario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `secundario`
--
ALTER TABLE `secundario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `soldado_voluntario`
--
ALTER TABLE `soldado_voluntario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `solicitud`
--
ALTER TABLE `solicitud`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `solicitud_esesc`
--
ALTER TABLE `solicitud_esesc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `solicitud_reincorporado`
--
ALTER TABLE `solicitud_reincorporado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `solicitud_smv`
--
ALTER TABLE `solicitud_smv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tutor`
--
ALTER TABLE `tutor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `abuelo`
--
ALTER TABLE `abuelo`
  ADD CONSTRAINT `abuelo_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `baja_empleo_ffaaffss`
--
ALTER TABLE `baja_empleo_ffaaffss`
  ADD CONSTRAINT `baja_empleo_ffaaffss_ibfk_1` FOREIGN KEY (`id_empleo_ffaaffss`) REFERENCES `empleo_ffaaffss` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `baja_estudio_ins_ffaa`
--
ALTER TABLE `baja_estudio_ins_ffaa`
  ADD CONSTRAINT `baja_estudio_ins_ffaa_ibfk_1` FOREIGN KEY (`id_estudio_ins_ffaa`) REFERENCES `estudio_ins_ffaa` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `calificacion_soldado_voluntario`
--
ALTER TABLE `calificacion_soldado_voluntario`
  ADD CONSTRAINT `calificacion_soldado_voluntario_ibfk_1` FOREIGN KEY (`id_soldado_voluntario`) REFERENCES `soldado_voluntario` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `contacto_emergencia`
--
ALTER TABLE `contacto_emergencia`
  ADD CONSTRAINT `contacto_emergencia_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `documentacion`
--
ALTER TABLE `documentacion`
  ADD CONSTRAINT `documentacion_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `documento`
--
ALTER TABLE `documento`
  ADD CONSTRAINT `documento_ibfk_1` FOREIGN KEY (`id_documentacion`) REFERENCES `documentacion` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD CONSTRAINT `domicilio_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `domicilio_exterior`
--
ALTER TABLE `domicilio_exterior`
  ADD CONSTRAINT `domicilio_exterior_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `empleo`
--
ALTER TABLE `empleo`
  ADD CONSTRAINT `empleo_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `empleo_civil_padre`
--
ALTER TABLE `empleo_civil_padre`
  ADD CONSTRAINT `empleo_civil_padre_ibfk_1` FOREIGN KEY (`id_padre`) REFERENCES `padre` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `empleo_ffaaffss`
--
ALTER TABLE `empleo_ffaaffss`
  ADD CONSTRAINT `empleo_ffaaffss_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `empleo_ffaa_padre`
--
ALTER TABLE `empleo_ffaa_padre`
  ADD CONSTRAINT `empleo_ffaa_padre_ibfk_1` FOREIGN KEY (`id_padre`) REFERENCES `padre` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `empleo_padre`
--
ALTER TABLE `empleo_padre`
  ADD CONSTRAINT `empleo_padre_ibfk_1` FOREIGN KEY (`id_padre`) REFERENCES `padre` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `estudio_ins_ffaa`
--
ALTER TABLE `estudio_ins_ffaa`
  ADD CONSTRAINT `estudio_ins_ffaa_ibfk_1` FOREIGN KEY (`id_formacion_academica`) REFERENCES `formacion_academica` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `formacion_academica`
--
ALTER TABLE `formacion_academica`
  ADD CONSTRAINT `formacion_academica_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `formacion_academica_padre`
--
ALTER TABLE `formacion_academica_padre`
  ADD CONSTRAINT `formacion_academica_padre_ibfk_1` FOREIGN KEY (`id_padre`) REFERENCES `padre` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `hermano`
--
ALTER TABLE `hermano`
  ADD CONSTRAINT `hermano_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `hijo`
--
ALTER TABLE `hijo`
  ADD CONSTRAINT `hijo_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `historial_modif`
--
ALTER TABLE `historial_modif`
  ADD CONSTRAINT `historial_modif_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `historial_modif_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `idioma_conocido`
--
ALTER TABLE `idioma_conocido`
  ADD CONSTRAINT `idioma_conocido_ibfk_1` FOREIGN KEY (`id_formacion_academica`) REFERENCES `formacion_academica` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `opinion_jefe_soldado_voluntario`
--
ALTER TABLE `opinion_jefe_soldado_voluntario`
  ADD CONSTRAINT `opinion_jefe_soldado_voluntario_ibfk_1` FOREIGN KEY (`id_soldado_voluntario`) REFERENCES `soldado_voluntario` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `otra_ocupacion_padre`
--
ALTER TABLE `otra_ocupacion_padre`
  ADD CONSTRAINT `otra_ocupacion_padre_ibfk_1` FOREIGN KEY (`id_padre`) REFERENCES `padre` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `otro`
--
ALTER TABLE `otro`
  ADD CONSTRAINT `otro_ibfk_1` FOREIGN KEY (`id_formacion_academica`) REFERENCES `formacion_academica` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `padre`
--
ALTER TABLE `padre`
  ADD CONSTRAINT `padre_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pago`
--
ALTER TABLE `pago`
  ADD CONSTRAINT `pago_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `postulante`
--
ALTER TABLE `postulante`
  ADD CONSTRAINT `postulante_ibfk_1` FOREIGN KEY (`id_preseleccion_postulante`) REFERENCES `preseleccion_postulante` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `postulante_ibfk_2` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `preseleccion_postulante`
--
ALTER TABLE `preseleccion_postulante`
  ADD CONSTRAINT `preseleccion_postulante_ibfk_2` FOREIGN KEY (`id_plan_carrera`) REFERENCES `plan_carrera` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `preseleccion_postulante_ibfk_3` FOREIGN KEY (`id_centro_preseleccion`) REFERENCES `centro_preseleccion` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `preseleccion_reincorporado`
--
ALTER TABLE `preseleccion_reincorporado`
  ADD CONSTRAINT `preseleccion_reincorporado_ibfk_1` FOREIGN KEY (`id_centro_preseleccion`) REFERENCES `centro_preseleccion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `preseleccion_reincorporado_ibfk_2` FOREIGN KEY (`id_plan_carrera`) REFERENCES `plan_carrera` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `racionamiento`
--
ALTER TABLE `racionamiento`
  ADD CONSTRAINT `racionamiento_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `reincorporado`
--
ALTER TABLE `reincorporado`
  ADD CONSTRAINT `reincorporado_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`),
  ADD CONSTRAINT `reincorporado_ibfk_2` FOREIGN KEY (`id_solicitud_reincorporado`) REFERENCES `solicitud_reincorporado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reincorporado_ibfk_3` FOREIGN KEY (`id_preseleccion_reincorporado`) REFERENCES `preseleccion_reincorporado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sancion_soldado_voluntario`
--
ALTER TABLE `sancion_soldado_voluntario`
  ADD CONSTRAINT `sancion_soldado_voluntario_ibfk_1` FOREIGN KEY (`id_soldado_voluntario`) REFERENCES `soldado_voluntario` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `secundario`
--
ALTER TABLE `secundario`
  ADD CONSTRAINT `secundario_ibfk_1` FOREIGN KEY (`id_formacion_academica`) REFERENCES `formacion_academica` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `soldado_voluntario`
--
ALTER TABLE `soldado_voluntario`
  ADD CONSTRAINT `soldado_voluntario_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `solicitud`
--
ALTER TABLE `solicitud`
  ADD CONSTRAINT `solicitud_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `solicitud_esesc`
--
ALTER TABLE `solicitud_esesc`
  ADD CONSTRAINT `solicitud_esesc_ibfk_1` FOREIGN KEY (`id_formacion_academica`) REFERENCES `formacion_academica` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `solicitud_smv`
--
ALTER TABLE `solicitud_smv`
  ADD CONSTRAINT `solicitud_smv_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tutor`
--
ALTER TABLE `tutor`
  ADD CONSTRAINT `tutor_ibfk_1` FOREIGN KEY (`id_candidato`) REFERENCES `candidato` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`cargo`) REFERENCES `cargo` (`id_cargo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
